import { Component, ViewChild, SimpleChanges } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';

import { Router, Routes, ActivatedRoute, Route } from '@angular/router';



import { NavService } from './core/services/nav.service';
import { MatDrawer } from '@angular/material/sidenav';
import { User, NavItem } from './core/models';
import { AuthSecureService } from './core/services/auth-secure.service';
import { UserService } from './core/services/user.service';
import { Tools } from './core/helpers/tools';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MyAlertService } from './core/services/MyAlert.service';
import { PermissionService } from './core/services/permission.service';
import { MatDialog } from '@angular/material/dialog';
import { UsersGuideDialogComponent } from './components/users-guide-dialog/users-guide-dialog.component';
import { environment } from 'src/environments/environment';
import { ChangelogDialogComponent } from './components/changelog-dialog/changelog-dialog.component';



class ItemMenu {
  title?: string;
  icon?: string;
  route: string;
  children?: Array<ItemMenu>
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @ViewChild('appDrawer') appDrawer: MatDrawer;

  public Permission = new Object();

  public currentUser: User;
  public pathArray: Array<string> = new Array<string>();

  public title = 'Back Office Tienda en Linea';
  public panelOpenState = false;
  private tools: Tools;
  public closing: boolean;
  public country: string;
  public navItems: NavItem[];
  private returnUrl: string;
  public tlboVersion = '';

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );


  constructor(
    private breakpointObserver: BreakpointObserver,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
    private authSecureService: AuthSecureService,
    private router: Router,
    private navService: NavService,
    private userService: UserService,
    private permissionService: PermissionService,
    private route: ActivatedRoute,
    private alert: MyAlertService,
    public dialog: MatDialog) {

      this.tlboVersion = environment.version;
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    // let _iso = this.route.snapshot.params.country;
    //console.log(this.authSecureService.countryValue);



    iconRegistry.addSvgIcon(
      'thumbs-claro',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/claro-red.svg'));

    this.authSecureService.closing.subscribe(x => {
      this.closing = x;
    });
    this.authSecureService.sessionCountry.subscribe(country => {
      this.country = country.value;
    });

    this.authSecureService.user.subscribe(x => {
      this.currentUser = x;

      if (x && !this.closing) {
        this.loadSideNav();
        //console.log("cargando nav");
      }
    });



  }

  accessList = [];

  loadSideNav() {


    this.userService.getPermissions().subscribe(response => {
      if (response.code == 200) {
        if (response.result.status == "ok") {
          this.accessList = response.result.data.UserPermissions ? response.result.data.UserPermissions : [];
          this.navItems = this.loadRouterTree(this.router.config, '/', null);
          this.sendPermission(this.Permission);
          this.router.navigate([this.returnUrl]);
        } else {
          this.alert.warn('Permisos no cargados');
        }
      } else {
        this.alert.warn('Permisos no cargados');
      }
    }, error => {

      this.alert.error('No se pudieron cargar los permisos');
    });

  }

  loadRouterTree(root: Routes, route: string, parent: Route): Array<NavItem> {
    let list: Array<NavItem> = [];
    if (root) {
      for (let idx = 0; idx < root.length; idx++) {
        const element = root[idx];
        if (element?.data?.show) {
          let itemMenu = new NavItem();
          itemMenu.route = '';
          if (element.children?.length > 0) {
            itemMenu.icon = element.data.icon;
            itemMenu.title = element.data.title;
            let SEPARATOR = '';
            if (parent?.data?.isRoute) {
              SEPARATOR = '/';
            } else {
              SEPARATOR = (route == '' || route == '/') ? '' : '/';
            }
            itemMenu.children = this.loadRouterTree(element.children, `${route}${SEPARATOR}${element.path}`, element);
            if (itemMenu?.children?.length > 0) {
              list.push(itemMenu);
            }
            if (element?.data?.isRoute) {
              itemMenu.route = `${route}${SEPARATOR}${element.path}`;
              delete itemMenu.children;
              if (this.validatePathAccess(itemMenu.route)) {
                element.data.access = true;
                if (!parent?.data?.isRoute) {
                  list.push(itemMenu);
                }
              }
            }
          } else {
            let SEPARATOR = element.path == '' ? '' : '/';
            itemMenu.route = `${route}${SEPARATOR}${element.path}`;
            itemMenu.icon = element.data.icon;
            itemMenu.title = element.data.title;
            if (this.validatePathAccess(itemMenu.route)) {
              element.data.access = true;
              if (!parent?.data?.isRoute) {
                list.push(itemMenu);
              }
            }
          }
        } else {
          let SEPARATOR = '';
          if (parent?.data?.isRoute) {
            SEPARATOR = element.path == '' ? '' : '/';
          } else {
            SEPARATOR = (route == '' || route == '/') ? '' : '/';
          }
          if (this.validatePathAccess(`${route}${SEPARATOR}${element.path}`)) {
            element.data.access = true;
            // console.log(`${route}${SEPARATOR}${element.path}`);
          }
        }
      }
    }
    return list;
  }

  validatePathAccess(path: string): boolean {
    let result = false;
    for (let index = 0; index < this.accessList.length; index++) {
      const element = this.accessList[index];
      /* if (path == element) { */
      if (path == element.path) {
        result = true;
        this.Permission[element.path] = element.permission;
        break;
      }
    }
    return result;
  }

  sendPermission(data) {
    this.permissionService.setPermission(data);
  }

  navItemsTemp: NavItem[] = new Array<NavItem>();


  ngOnInit(): void {

  }

  /* logout() {
    this.authSecureService.logout();
    this.router.navigate(['/login']);
  } */

  ngAfterViewInit() {
    if (this.currentUser)
      this.navService.appDrawer = this.appDrawer;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.currentUser)
      this.navService.appDrawer = this.appDrawer;
  }

  toggle() {
    if (this.currentUser)
      this.navService.toggle();
  }
  

  showUserGuide(){
    this.dialog.open(UsersGuideDialogComponent, {
      
      panelClass: 'trend-dialog',
      width: '90%', 
      height: '95vh',
    });
  }

  showChangelog(){
    /* if(this.currentUser && this.currentUser.profileType == 'E'){
      this.dialog.open(ChangelogDialogComponent, {
         width: '90%', 
       });
    } */
    
  }
}
