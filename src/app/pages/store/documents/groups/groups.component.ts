import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { File } from 'src/app/core/models/File';
import { Group } from 'src/app/core/models/Group';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { GroupService } from 'src/app/core/services/group.service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { NewFileDialogComponent } from '../files/dialogs/new-file-dialog/new-file-dialog.component';
import { FileFlowDialogComponent } from './dialogs/file-flow-dialog/file-flow-dialog.component';


@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {

  public optionsView :  ViewOptions = {
    displayedColumns: ['name', 'category', 'docs', 'lastModify', 'actions'],
    pageSize: [25, 35, 50],
    loading: false 
  };
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  
  queryValue: string = "";

  public dataTable: MatTableDataSource<Group> = new MatTableDataSource<Group>();

  public groups = [];

  
  constructor(
    public dialog: MatDialog,
    private alert: MyAlertService,
    private groupService: GroupService,
  ) {
   }

  ngOnInit(): void {
    this.loadData();
  }


 



  loadData() {
    this.optionsView.loading = true;
    this.dataTable = new MatTableDataSource<Group>();
    /* const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    }); */
    this.groupService.get ().subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
        this.groups = response.result.data.groups || [];
        this.dataTable = new MatTableDataSource<Group>(this.groups);
        this.dataTable.paginator = this.paginator;
        this.dataTable.sort = this.sort;
        this.applyFilter();
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      this.optionsView.loading = false;
      /* dialogRefLoadingPage.close(); */
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        /* dialogRefLoadingPage.close(); */
        this.optionsView.loading = false;
      });
  }

  applyFilter() {
    this.dataTable.filter = this.queryValue.trim().toLowerCase();
  }

  setFlow(element: Group){ 
    const dialogRefLoadingPage = this.dialog.open(FileFlowDialogComponent, {
      width: '80%',
      data: element,
      disableClose: false
    });
    dialogRefLoadingPage.afterClosed().subscribe(
      data=>{
        if(data){

        }
      });
  }

  createFile(){
    const dialogRefLoadingPage = this.dialog.open(NewFileDialogComponent, {
      width: '95%',
      data: "",
      disableClose: false
    });
    dialogRefLoadingPage.afterClosed().subscribe(
      data=>{
        if(data){
        }
      });
  }

  deleteRow(row: any) {
    this.groups.splice(this.groups.indexOf(row), 1);
    this.dataTable._updateChangeSubscription();
  }

  delete(row: any) {
    const dialogConfirm = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        title: 'Confirmación',
        description: 'Desea eliminar la marca?'
      }
    });
    dialogConfirm.afterClosed().subscribe(result => {
      if (result) {
        const dialogLoading = this.dialog.open(LoadingPageDialogComponent, {
          width: '50%',
          data: "",
          disableClose: true
        });
        this.groupService.delete(row.id).subscribe(response => {
          if (response.code == 200 && response.result.status == "ok") {
              this.alert.success("Se ha eliminado la marca " + row.name);
              this.deleteRow(row);
          } else {
            this.alert.warn(response?.result?.message || 'Proceso no completado');
          }
          dialogLoading.close();
        }, err => {
          if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          } 
          dialogLoading.close();
        });
      }
    });
  }

  openNewDialogFile() {
    /* this.dialog.open(BrandCreateDialogComponent, {
      width: '70%',
      data: {
        brand: null
      }
    }); */
  }
  editFile(file?: File) {
    /* if (brand)
      brand.status = 2;
    const dialogBrandDetail = this.dialog.open(BrandCreateDialogComponent, {
      width: '70%',
      data: {
        brand: brand || null
      }
    });
    dialogBrandDetail.afterClosed().subscribe(result => {
      if (result) {
        this.loadBrands();
      }
    }); */
  }

 

}
