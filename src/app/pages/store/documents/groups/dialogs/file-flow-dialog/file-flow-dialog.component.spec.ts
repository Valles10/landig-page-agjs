import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileFlowDialogComponent } from './file-flow-dialog.component';

describe('FileFlowDialogComponent', () => {
  let component: FileFlowDialogComponent;
  let fixture: ComponentFixture<FileFlowDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileFlowDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileFlowDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
