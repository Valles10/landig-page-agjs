import { SelectionModel } from '@angular/cdk/collections';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Tools } from 'src/app/core/helpers/tools';
import { File } from 'src/app/core/models/File';
import { Group } from 'src/app/core/models/Group';
import { DocumentService } from 'src/app/core/services/document-service';
import { FileVersionService } from 'src/app/core/services/FileVersion.service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
  { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
  { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
  { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
  { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
  { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
  { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
  { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
  { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
  { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
];

@Component({
  selector: 'app-file-flow-dialog',
  templateUrl: './file-flow-dialog.component.html',
  styleUrls: ['./file-flow-dialog.component.css']
})
export class FileFlowDialogComponent implements OnInit {
  typesOfShoes: string[] = ['Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers'];
  todo = [
    'Get to work',
    'Pick up groceries',
    'Go home',
    'Fall asleep'
  ];

  done = [
    'Get up',
    'Brush teeth',
    'Take a shower',
    'Check e-mail',
    'Walk dog'
  ];
  /*   todoList = [];
    doneList = [];
   */
  group: Group;

  files: Array<File> = [];
  filesFlow: Array<File> = [];
  loadingFiles = false;
  loadingFlow = false;
  tools: Tools = new Tools();

  constructor(
    public dialogRef: MatDialogRef<FileFlowDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private versionService: FileVersionService,
    private alert: MyAlertService,
    private documentService: DocumentService,
  ) {
    this.group = data;
    
  }

  ngOnInit(): void {
    if (this.group) {
      this.loadFiles();
    }


  }



  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }


  loadFiles() {
    this.files = [];
    this.loadingFiles = true;
    this.documentService.getFiles().subscribe(
      response => {
        if (response.code == 200 && response.result.status == "ok") {
          this.files = response.result.data.files; 
          this.loadFilesFlow(this.files);
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
        this.loadingFiles = false;
       },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        } 
        this.loadingFiles = false;

       }
    );
  }

  loadFilesFlow(files: Array<File>) {
    this.filesFlow = [];
    this.loadingFlow = true;
    this.documentService.getFilesFlow().subscribe(
      response => {
        if (response.code == 200 && response.result.status == "ok") {
          const tempFlow = response.result.data.files;
           /* console.log(this.tools.removeMultiItemFromArr(this.files,tempFlow)); */
            
          for (let index = 0; index < tempFlow.length; index++) {
            const element = tempFlow[index];
            /* this.files.indexOf(element); */
            console.log(this.files.map(function(e) { return e.id; }).indexOf(element.id));
            
            this.files.splice(this.files.map(function(e) { return e.id; }).indexOf(element.id), 1)
            
          }
          this.filesFlow = tempFlow; 
          
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
        this.loadingFlow = false;
       },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        } 
        this.loadingFlow = false;

       }
    );
  }



}
