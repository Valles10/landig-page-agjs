import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileLabelsComponent } from './file-labels.component';

describe('FileLabelsComponent', () => {
  let component: FileLabelsComponent;
  let fixture: ComponentFixture<FileLabelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileLabelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileLabelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
