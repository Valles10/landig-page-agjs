import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';


const TYPES = [
  {
    code: 'text',
    name: 'Texto'
  },
  {
    code: 'number',
    name: 'Numerico'
  },
  {
    code: 'date',
    name: 'Fecha'
  },
  {
    code: 'boolean',
    name: 'Condicional '
  }
];


@Component({
  selector: 'app-create-label',
  templateUrl: './create-label.component.html',
  styleUrls: ['./create-label.component.css']
})
export class CreateLabelComponent implements OnInit {


  labelForm: FormGroup;
  public types = TYPES;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<CreateLabelComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private alert: MyAlertService
  ) { }

  ngOnInit(): void {
    if (this.data?.label != null) {
      /*  this.labelForm = new FormGroup({
         brand_id: new FormControl(this.data.label?.id.toString() || "0"),
         name: new FormControl(this.data.brand.name || '',
           [
             Validators.required,
             Validators.maxLength(254)
           ]),
         status: new FormControl((this.data.brand.status || (this.data.brand.id ? 2 : 3))),
         description: new FormControl(this.data.brand.description || '',
           [
             Validators.maxLength(254)
           ])
       }); */
    } else {
      this.labelForm = new FormGroup({
        id: new FormControl('0'),
        name: new FormControl('',
          [
            Validators.required,
            Validators.maxLength(254)
          ]),
        code: new FormControl('',
          [
            Validators.required,
            Validators.maxLength(254)
          ]),
        type: new FormControl('',
          [
            Validators.required
          ]),
        length: new FormControl('',
          [
            Validators.required
          ]),
        example: new FormControl('',
          [
            Validators.required,
            Validators.maxLength(254)
          ]),
        description: new FormControl('',
          [
            Validators.maxLength(254)
          ]),
        status: new FormControl()
      });
    }
  }


  get id() { return this.labelForm.get('id') }
  get name() { return this.labelForm.get('name') }
  get code() { return this.labelForm.get('code') }
  get type() { return this.labelForm.get('type') }
  get length() { return this.labelForm.get('length') }
  get example() { return this.labelForm.get('example') }
  get description() { return this.labelForm.get('description') }


  /* 
    get status() { return this.brandForm.get('status') }
    get brand_id() { return this.brandForm.get('brand_id') }
    get name() { return this.brandForm.get('name') }
    get description() { return this.brandForm.get('description') } */

  save(): void {
    /* const dialoadRef = this.dialog.open(LoadingPageDialogComponent, {
      width: '400px',
      data: {
        description: `Se`,
        title: "Confirmación de cambios",
      },
      disableClose: true
    });
    this.brandService.managementBrand(this.brandForm.value).subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") { if (<number>this.status.value == 2) {
            this.alert.success('Se ha actualizado la marca '+ this.name.value);
          } else {
            this.alert.success('Se ha creado la marca '+ this.name.value);
          }
          this.dialogRef.close(true);
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      dialoadRef.close();
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        } 
        dialoadRef.close();
      }); */
  }
}
