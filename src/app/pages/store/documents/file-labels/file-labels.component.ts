import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PropertiesService } from 'src/app/core/services/properties.service';
import { CreateLabelComponent } from './dialogs/create-label/create-label.component';

const DATA  = [
  { 
    name: 'Nombre',
    description: '',
    type: 'text',
    length: 0,
    create_at: '',
    update_at: '',
  },
  { 
    name: 'Fecha',
    description: '',
    type: 'date',
    length: 0,
    create_at: '',
    update_at: '',
  },
  { 
    name: 'Fecha Nacimiento',
    description: '',
    type: 'date',
    length: 0,
    create_at: '',
    update_at: '',
  },
  { 
    name: 'DPI',
    description: '',
    type: 'text',
    length: 0,
    create_at: '',
    update_at: '',
  },
  { 
    name: 'NIT',
    description: '',
    type: 'text',
    length: 0,
    create_at: '',
    update_at: '',
  },
];

@Component({
  selector: 'app-file-labels',
  templateUrl: './file-labels.component.html',
  styleUrls: ['./file-labels.component.css']
})
export class FileLabelsComponent implements OnInit {

  public queryValue = '';
  public labels = DATA;

  displayedColumns: string[] = ['name', 'description', 'type', 'update_at', 'actions'];
  dataSource = new MatTableDataSource(DATA);
  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  public loading = false;
  public prop = {
    defaultSIM: ''
  }
  constructor(
    public dialog: MatDialog,
    /* private propertiesService: PropertiesService, */
    private alert: MyAlertService
  ) { }

  ngOnInit(): void {
    console.log("Hello prepago");
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  /* loadProperties() {
    this.loading = true;
    this.propertiesService.getImage().subscribe(response => {
      if (response.code == 200 && response.result.status == 'ok') {
        try {
          this.propertiesService.setProperties(response.result?.data?.value || {});
        } catch (e) {
        }
      } else {
        this.alert.warn(response?.result?.message || "Propiedades no cargadas");
      }
      this.loading = false;
    },
      err => {
        this.loading = false;
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  } */

  applyFilter() {
    this.dataSource.filter = this.queryValue.trim().toLowerCase();
  }

  /*  */


  createLabel(){
    const dialoadRef = this.dialog.open(CreateLabelComponent, {
      width: '65%',
      disableClose: false
    });
    
  }

}
