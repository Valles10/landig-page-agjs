import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FileLabelsComponent } from './file-labels.component';

const routes: Routes = [{ path: '', component: FileLabelsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FileLabelsRoutingModule { }
