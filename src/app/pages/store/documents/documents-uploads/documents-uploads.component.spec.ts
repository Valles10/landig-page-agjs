import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentsUploadsComponent } from './documents-uploads.component';

describe('DocumentsUploadsComponent', () => {
  let component: DocumentsUploadsComponent;
  let fixture: ComponentFixture<DocumentsUploadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentsUploadsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentsUploadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
