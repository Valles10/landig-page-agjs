import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DocumentsUploadsRoutingModule } from './documents-uploads-routing.module';
import { DocumentsUploadsComponent } from './documents-uploads.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatChipsModule } from '@angular/material/chips';


@NgModule({
  declarations: [DocumentsUploadsComponent],
  imports: [
    CommonModule,
    DocumentsUploadsRoutingModule,
    FormsModule,
    MatFormFieldModule,
    MatIconModule, 
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatTooltipModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatMenuModule,
    MatProgressBarModule,
    MatDialogModule,
    MatChipsModule
  ]
})
export class DocumentsUploadsModule { }
