import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { File } from 'src/app/core/models/File';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { DocumentService } from 'src/app/core/services/document-service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { VersionService } from 'src/app/core/services/version.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-documents-uploads',
  templateUrl: './documents-uploads.component.html',
  styleUrls: ['./documents-uploads.component.css']
})
export class DocumentsUploadsComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  queryValue: string = "";

  files: MatTableDataSource<File> = new MatTableDataSource<File>();
  viewOptions : ViewOptions =  {
    displayedColumns: ['name', 'description',  'lastModify', 'actions', 'download'],
    pageSize: environment.pagination,
    loading: false
  };

  

  
  constructor(
    public dialog: MatDialog,
    private documentService: DocumentService,
    private alert: MyAlertService,
    private versionService: VersionService,
  ) {
    
   }

  ngOnInit(): void {
    this.loadFiles();
  }


  

  loadFiles() {
    this.viewOptions.loading = true;
    this.files = new MatTableDataSource<File>();
    /* const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    }); */
    this.documentService.getFiles ().subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
        this.files = new MatTableDataSource<File>(response.result.data.files);
        this.files.paginator = this.paginator;
        this.files.sort = this.sort;
        this.applyFilter();
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      this.viewOptions.loading = false;
      
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        
        this.viewOptions.loading = false;
      });
  }

  applyFilter() {
    this.files.filter = this.queryValue.trim().toLowerCase();
  }

  createVersion(){
    /* const dialogRefLoadingPage = this.dialog.open(NewVersionDialogComponent, {
      width: '50%',
      data: "",
      disableClose: false
    });
    dialogRefLoadingPage.afterClosed().subscribe(
      data=>{
        if(data){
          this.reloadVersions();
        }
      }); */
  }

  createFile(){
    /* const dialogRefLoadingPage = this.dialog.open(NewFileDialogComponent, {
      width: '95%',
      data: "",
      disableClose: false
    });
    dialogRefLoadingPage.afterClosed().subscribe(
      data=>{
        if(data){
          this.reloadVersions();
        }
      }); */
  }

  deleteRow(row: any) {
    this.files.data.splice(this.files.data.indexOf(row), 1);
    this.files._updateChangeSubscription();
  }

  


}
