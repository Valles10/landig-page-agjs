import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DocumentsUploadsComponent } from './documents-uploads.component';

const routes: Routes = [{ path: '', component: DocumentsUploadsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentsUploadsRoutingModule { }
