import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FilesRoutingModule } from './files-routing.module';
import { FilesComponent } from './files.component';
import { FileLabelsRoutingModule } from '../file-labels/file-labels-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { NewVersionDialogComponent } from './dialogs/new-version-dialog/new-version-dialog.component';
import { NewFileDialogComponent } from './dialogs/new-file-dialog/new-file-dialog.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import {ScrollingModule} from '@angular/cdk/scrolling'; 
import { DownloadFileDialogComponent } from './dialogs/download-file-dialog/download-file-dialog.component';
import { MyPdfViewerComponent } from '../../../../components/pdf-viewer/my-pdf-viewer.component';


@NgModule({
  declarations: [FilesComponent, NewVersionDialogComponent, NewFileDialogComponent,DownloadFileDialogComponent, MyPdfViewerComponent],
  imports: [
    CommonModule,
    FilesRoutingModule,
    FileLabelsRoutingModule,
    FormsModule,
    MatFormFieldModule,
    MatIconModule, 
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatTooltipModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    MatProgressBarModule,
    MatDialogModule,
    MatStepperModule,
    MatCheckboxModule,
    PdfViewerModule,
    ScrollingModule
  ],
  entryComponents: [
    NewVersionDialogComponent,
    DownloadFileDialogComponent,
    NewFileDialogComponent
  ]
})
export class FilesModule { }
