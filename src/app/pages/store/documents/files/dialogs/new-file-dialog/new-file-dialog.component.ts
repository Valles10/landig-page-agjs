import { SelectionModel } from '@angular/cdk/collections';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';

import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { map, tap, catchError, last } from 'rxjs/operators';
import { of } from 'rxjs';



import { DocumentService } from 'src/app/core/services/document-service';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { Document } from 'src/app/core/models/document';

export function requiredFileType(type: string[]) {
  return function (control: FormControl) {
    const file = control?.value;
    console.log(file);

    if (file) {
      const extension = file.name.split('.')[1].toLowerCase();
      if (!type.includes(extension.toLowerCase())) {
        return {
          requiredFileType: true
        };
      }

      return null;
    }

    return null;
  };
}

@Component({
  selector: 'app-new-file-dialog',
  templateUrl: './new-file-dialog.component.html',
  styleUrls: ['./new-file-dialog.component.css']
})
export class NewFileDialogComponent implements OnInit {

  /* Etiquetas */
  public displayedColumns: string[] = ['select', 'name', 'description', 'type'];
  public dataSource = new MatTableDataSource([]);
  public queryValue = '';
  public labels = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  public selection = new SelectionModel<any>(true, []);

  /* Documento */
  public documentForm: FormGroup;
  public fileToUpload: File = null;
  public documentModel: Document;

  public generateDocuent = false;

  public loadingEdition = false;
  public loadingEditionError = false;

  public loadingLabels = false;

  constructor(
    public dialogRef: MatDialogRef<NewFileDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private documentService: DocumentService,
    private alert: MyAlertService,
    private _http: HttpClient,
  ) {
    this.documentModel = data.document;
  }

  ngOnInit(): void {
    if (this.data.document) {
      this.loadingEdition = true;
      // Edicion de documento
      this.documentForm = new FormGroup({
        name: new FormControl(this.documentModel.name, [
          Validators.required,
          Validators.maxLength(250)
        ]),
        description: new FormControl(this.documentModel.description, []),
        systemReserved: new FormControl((this.documentModel.systemReserved == "1" ? true : false), []),
        document: new FormControl(null, [
          Validators.required,
          requiredFileType(['docx'])
        ]),
      });

      //Descargar documento para edicion
      this.documentService.getBlob(this.documentModel.path).subscribe(
        blob => {
          try {
            this.documentModel.fileEdition = new File([blob], this.documentModel.fileRepositoryName);
            this.document.patchValue(this.documentModel.fileEdition);
            this.fileToUpload = this.documentModel.fileEdition;
            this.documentModel.fileEditionChange = false; // bandera para indicar que el documento que se utilizara sera el del modelo 
          } catch (err) {
            this.alert.error('Archivo no se pudo cargar, no se puede editar');
            this.loadingEditionError = true;
          }
          this.loadingEdition = false;
        },
        err => {
          this.loadingEdition = false;
          this.loadingEditionError = true;
          this.alert.error('Archivo no se pudo cargar, no se puede editar');
        });
    } else {
      this.documentForm = new FormGroup({
        name: new FormControl('', [
          Validators.required,
          Validators.maxLength(250)
        ]),
        description: new FormControl('', []),
        document: new FormControl(null, [
          Validators.required,
          requiredFileType(['docx'])
        ]),
        systemReserved: new FormControl(null, [])
      });
    }
  }

  get name() { return this.documentForm.get('name'); }
  get description() { return this.documentForm.get('description'); }
  get document() { return this.documentForm.get('document'); }
  get systemReserved() { return this.documentForm.get('systemReserved'); }

  nextStep() {
    const btnNext = document.getElementById('btnNext') as HTMLInputElement;
    btnNext.click();
  }

  changeStep(event) {
    if (event.selectedIndex == 1) {
      const btnNext = document.getElementById('btnNext') as HTMLInputElement;
      btnNext.click();
    }
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = null;
    if (files.length > 0) {
      const f = files.item(0);
      this.document.patchValue(f);
      this.fileToUpload = f;
      if (this.documentModel)
        this.documentModel.fileEditionChange = true; // bandera para indicar que se selecciono un nuevo documento durante la edicion 
    } else {
      this.fileToUpload = null;
      this.document.patchValue(null);
    }
  }


  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }


  /* Etiquetas */
  applyFilter() {
    this.dataSource.filter = this.queryValue.trim().toLowerCase();
  }


  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }
  /*  Fin Etiquetas */





  /* Previsulizacion documento */
  public doc = '';

  getBase64() {
    var reader = new FileReader();
    reader.readAsDataURL(this.fileToUpload);
    reader.onload = () => {
      this.wordToPdf(reader.result.toString().split(',')[1]);
    };
    reader.onerror = function (error) {
    };
  }

  wordToPdf(b64) {
    this.generateDocuent = true;
    this.documentService.wordToPdf({
      test: "test",
      file: b64
    }).subscribe(
      result => {
        this.doc = `data:application/pdf;base64,${result.PDF}`;
        this.generateDocuent = false;
      },
      err => {
        this.generateDocuent = false;
      }
    );
  }


  loadConfig() {
    if (this.documentForm.valid && !this.generateDocuent) {
      // Si el documento es editado y no se agrega un nuevo documento se configura el que fue descargado 
      if (this.documentModel && !this.documentModel.fileEditionChange) {
        this.fileToUpload = this.documentModel.fileEdition;
      }
      /* this.labels = DATA; */
      this.dataSource = new MatTableDataSource(this.labels);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.getBase64();
      this.loadLabels();
    }
  }



  save() {


    if (this.documentModel && !this.documentModel.fileEditionChange) {
      this.fileToUpload = this.documentModel.fileEdition;
    }

    let labels = [];
    this.selection.selected.forEach(element => {
      labels.push(element.id);
    });

    const fd = new FormData();
    fd.append('name', this.name.value);
    fd.append('description', this.description.value);
    fd.append('document', this.fileToUpload);
    /* fd.append('label', `[${labels.toString()}]` ); */
    fd.append('label', '[4,5,6]');
    fd.append('systemReserved', (this.systemReserved.value ? "1" : "0"));

    console.log(fd);



    if (this.data.document) {
      // Edicion




    } else {
      //Creacion 
      const req = new HttpRequest('POST', `http://10.218.41.26:28081/TL_BO_BackEnd/webresources/files/management/createFile`, fd);
      this._http.request(req).pipe(
        map(event => {
          switch (event.type) {
            case HttpEventType.UploadProgress:
              /*  file.progress = Math.round(event.loaded * 100 / event.total); */
              break;
            case HttpEventType.Response:
              return event;
          }
        }),
        tap(message => { }),
        last(),
        catchError((error: HttpErrorResponse) => {
          /* file.inProgress = false;
          file.canRetry = true;
          file.canCancel = false;
          this.alert.warn("Error cargando la imagen: " + file.image.name);
          this.loading = false;
          return of(`${file.image.name} upload failed.`); */
          return of(`upload failed.`);
        })
      ).subscribe(
        (event: any) => {
          if (typeof (event) === 'object') {
            /* file.completeUpload = true;
            file.inProgress = false;
            file.canRetry = false;
            file.canCancel = false;
            this.fileCounter++;
            this.loading = false;
            this.data.correlatives.push(Number(file.metadata.correlative));
            this.alert.success("Se ha cargado la imagen: " + file.image.name);
            this.uploadImages();
            this.complete.emit(event.body); */
          }
        }
      );
    }

  }


  loadLabels() {

    this.loadingLabels = true;
    if (this.documentModel && !this.documentModel.fileEditionChange) {
      this.fileToUpload = this.documentModel.fileEdition;
    }

    /* let labels = [];
    this.selection.selected.forEach(element => {
      labels.push(element.id);
    }); */

    const fd = new FormData();
    fd.append('document', this.fileToUpload);

    console.log(fd);


    const req = new HttpRequest('POST', `http://10.207.41.25:5022/TL_BO_BackEnd_test/webresources/files/management/file/scan`, fd);
    this._http.request(req).pipe(map(event => {
      switch (event.type) {
        case HttpEventType.UploadProgress:
          /*  file.progress = Math.round(event.loaded * 100 / event.total); */
          break;
        case HttpEventType.Response:
          return event;
      }
    }),
      tap(message => { }),
      last(),
      catchError((error: HttpErrorResponse) => {
        /* file.inProgress = false;
        file.canRetry = true;
        file.canCancel = false;
        this.alert.warn("Error cargando la imagen: " + file.image.name);
        this.loading = false;
        return of(`${file.image.name} upload failed.`); */
        this.loadingLabels = false;

        return of(`upload failed.`);
      })
    ).subscribe(
      (event: any) => {
        if (typeof (event) === 'object') {
          console.log(event.body);
          let response = event.body;
          if (
            response?.code == 200 &&
            response?.result?.status == 'ok' &&
            response?.result?.data?.labels) {
            /* this.labels = Array.isArray(response.result.data.labels) ? response.result.data.labels :[]; */
            console.log(Array.isArray(response.result.data.labels) ? response.result.data.labels : []);

          } else {
            this.alert.warn(response?.result?.data?.message || 'Proceso no completado');
          }


          /* file.completeUpload = true;
          file.inProgress = false;
          file.canRetry = false;
          file.canCancel = false;
          this.fileCounter++;
          this.loading = false;
          this.data.correlatives.push(Number(file.metadata.correlative));
          this.alert.success("Se ha cargado la imagen: " + file.image.name);
          this.uploadImages();
          this.complete.emit(event.body); */
        }
        this.loadingLabels = false;

      }
    );


  }

}


const DATA = [
  {
    id: 1,
    name: 'Nombre',
    description: '',
    type: 'text',
    length: 0,
    create_at: '',
    update_at: '',
  },
  {
    id: 2,
    name: 'Fecha',
    description: '',
    type: 'date',
    length: 0,
    create_at: '',
    update_at: '',
  },
  {
    id: 1213,

    name: 'Fecha Nacimiento',
    description: '',
    type: 'date',
    length: 0,
    create_at: '',
    update_at: '',
  },
  {
    id: 1123,
    name: 'DPI',
    description: '',
    type: 'text',
    length: 0,
    create_at: '',
    update_at: '',
  },
  {
    id: 1123,
    name: 'NIT',
    description: '',
    type: 'text',
    length: 0,
    create_at: '',
    update_at: '',
  },
];

