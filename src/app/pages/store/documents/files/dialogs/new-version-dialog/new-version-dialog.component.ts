import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Params, Route, Router } from '@angular/router';
import { ProductVersion } from 'src/app/core/models/ProductVersion';
import { FileVersionService } from 'src/app/core/services/FileVersion.service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-new-version-dialog',
  templateUrl: './new-version-dialog.component.html',
  styleUrls: ['./new-version-dialog.component.css']
})
export class NewVersionDialogComponent implements OnInit {
  

  public version: ProductVersion;
  public versions: ProductVersion[] = [];
  public loadVersions = false;
  public versionForm: FormGroup;
  

  constructor(
    private _formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<NewVersionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private versionService: FileVersionService,
    private alert: MyAlertService,
    
  ) { 
    /* this.versions = data.versions || []; */
    this.version = data.version ;
  }

  ngOnInit(): void {
    if(this.version){

    }else{
      this.versionForm = new FormGroup({
        name: new FormControl('',[
          Validators.required
        ]),
        frmVersion: new FormControl('', [

        ])
      });
    }
    this.getVersions();
  }

  get name() { return this.versionForm.get('name')}
  get frmVersion() { return this.versionForm.get('frmVersion')}




  getVersions(): void {
    this.versions = [];
    this.loadVersions = true;
    this.versionService.all().subscribe(
      response => {
        if (response.code == 200 && response.result.status == 'ok') {
          this.versions  = response.result.data.versions;
          /* for (let index = 0; index < v.length; index++) {
            let element = v[index];
            delete element.lastUpdate;
          }
          this.versions = v;
          if (initVersion) {
            let find = this.versions.find(x => x.versionId == initVersion.versionId);
            this.selectedVersion = find;
          } */
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado');
        }
        this.loadVersions = false;
      },
      err => {
        this.loadVersions = false;
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }

  save() {
    console.log(this.versionForm.value);
    
  }

  

}
