import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { MatTableDataSource } from '@angular/material/table';
import { File } from 'src/app/core/models/File';

export class ReportFile{
  idFile: string;
  fileName: string;
  fileURL: string;
  creationDate: string;
  fileResult: string;
  fileStatus: string;
}

@Component({
  selector: 'app-download-file-dialog',
  templateUrl: './download-file-dialog.component.html',
  styleUrls: ['./download-file-dialog.component.css']
})
export class DownloadFileDialogComponent implements OnInit {

  viewOptions: ViewOptions = {
    displayedColumns: ['name', 'description', 'lastModify','path'],
    pageSize: [25, 35, 50],
    loading: true
  };
  filesDataSource: any = new MatTableDataSource<File>();
  public file: File;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<DownloadFileDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) { 
    this.file = data.file;
  }

  ngOnInit(): void {
    this.filesDataSource = new MatTableDataSource<File>(this.file.historyFiles);
    this.viewOptions.loading = false;
  }

}
