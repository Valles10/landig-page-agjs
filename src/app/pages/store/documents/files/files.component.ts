import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { Document } from 'src/app/core/models/document';
import { File } from 'src/app/core/models/File';
import { ProductVersion } from 'src/app/core/models/ProductVersion';
import { DocumentService } from 'src/app/core/services/document-service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { VersionService } from 'src/app/core/services/version.service';
import { environment } from 'src/environments/environment';
import { DownloadFileDialogComponent } from './dialogs/download-file-dialog/download-file-dialog.component';
import { NewFileDialogComponent } from './dialogs/new-file-dialog/new-file-dialog.component';
import { NewVersionDialogComponent } from './dialogs/new-version-dialog/new-version-dialog.component';

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.css']
})
export class FilesComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  public file: File = new File();
  queryValue: string = "";

  files: MatTableDataSource<Document> = new MatTableDataSource<Document>();
  loading = false;

  public versions = [];
  public loadVersions = false;
  public selectedVersion: ProductVersion;
  public initVersion = environment.v.fileVersion.init;
  private paramVersion;

  constructor(
    public dialog: MatDialog,
    private documentService: DocumentService,
    private alert: MyAlertService,
    private route: ActivatedRoute,
    private router: Router,
  ) {

    this.paramVersion = this.route.snapshot.queryParamMap.get('v') || environment.v.fileVersion.init.versionId;
  }

  ngOnInit(): void {
    /* this.getVersions(this.initVersion); */
    this.loadFiles();
  }


  /* reloadVersions() {
    this.getVersions(this.selectedVersion);
  } */

  /* getVersions(initVersion?: ProductVersion): void {
    this.versions = [];
    this.loadVersions = true;
    this.versionService.all().subscribe(
      response => {
        if (response.code == 200 && response.result.status == 'ok') {
          let v = response.result.data.versions;
          for (let index = 0; index < v.length; index++) {
            let element = v[index];
            delete element.lastUpdate;
          }
          this.versions = v;
          if (initVersion) {
            let find = this.versions.find(x => x.versionId == initVersion.versionId);
            this.selectedVersion = find;
          }
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado');
        }
        this.loadVersions = false;
      },
      err => {
        this.loadVersions = false;
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  } */

  versionChange() {
    if (this.selectedVersion) {
      this.changingQueryParams();
      this.loadFiles();
    }
  }

  loadFiles() {
    this.loading = true;
    this.files = new MatTableDataSource<Document>();
    /* const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    }); */
    this.documentService.getFiles().subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
        this.files = new MatTableDataSource<Document>(response.result.data.documents);
        this.files.paginator = this.paginator;
        this.files.sort = this.sort;
        this.applyFilter();
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      this.loading = false;
      /* dialogRefLoadingPage.close(); */
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        /* dialogRefLoadingPage.close(); */
        this.loading = false;
      });
  }

  applyFilter() {
    this.files.filter = this.queryValue.trim().toLowerCase();
  }

  /* createVersion() {
    const dialogRefLoadingPage = this.dialog.open(NewVersionDialogComponent, {
      width: '50%',
      data: "",
      disableClose: false
    });
    dialogRefLoadingPage.afterClosed().subscribe(
      data => {
        if (data) {
          this.reloadVersions();
        }
      });
  } */

  createFile(document?: Document) {
    if (document) {
      const dialogRefLoadingPage = this.dialog.open(NewFileDialogComponent, {
        width: '95%',
        data: {
          document: document
        },
        disableClose: false
      });
      dialogRefLoadingPage.afterClosed().subscribe(
        data => {
          if (data) {

          }
        });
      
    } else {
      const dialogRefLoadingPage = this.dialog.open(NewFileDialogComponent, {
        width: '95%',
        data: "",
        disableClose: false
      });
      dialogRefLoadingPage.afterClosed().subscribe(
        data => {
          if (data) {

          }
        });
    }

  }

  deleteRow(row: any) {
    this.files.data.splice(this.files.data.indexOf(row), 1);
    this.files._updateChangeSubscription();
  }

  deleteFile(row: any) {
    const dialogConfirm = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        title: 'Confirmación',
        description: 'Desea eliminar la marca?'
      }
    });
    dialogConfirm.afterClosed().subscribe(result => {
      if (result) {
        const dialogLoading = this.dialog.open(LoadingPageDialogComponent, {
          width: '50%',
          data: "",
          disableClose: true
        });
        this.documentService.managementBrand({
          brand_id: row.id.toString(),
          status: 1
        }).subscribe(response => {
          if (response.code == 200 && response.result.status == "ok") {
            this.alert.success("Se ha eliminado la marca " + row.name);
            this.deleteRow(row);
          } else {
            this.alert.warn(response?.result?.message || 'Proceso no completado');
          }
          dialogLoading.close();
        }, err => {
          if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          }
          dialogLoading.close();
        });
      }
    });
  }

  openNewDialogFile() {
    /* this.dialog.open(BrandCreateDialogComponent, {
      width: '70%',
      data: {
        brand: null
      }
    }); */
  }
  editFile(file?: File) {
    /* if (brand)
      brand.status = 2;
    const dialogBrandDetail = this.dialog.open(BrandCreateDialogComponent, {
      width: '70%',
      data: {
        brand: brand || null
      }
    });
    dialogBrandDetail.afterClosed().subscribe(result => {
      if (result) {
        this.loadBrands();
      }
    }); */
  }


  changingQueryParams() {
    this.paramVersion = this.selectedVersion.versionId;
    const queryParams: Params = { v: this.paramVersion };
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: queryParams,
      });
  }

  showHistoryDialog(file?: File) {
    this.dialog.open(DownloadFileDialogComponent, {
      width: '70%',
      data: {
        file: file
      }
    });
  }

}
