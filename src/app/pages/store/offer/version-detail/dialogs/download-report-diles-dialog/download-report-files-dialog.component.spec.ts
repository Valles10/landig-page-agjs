import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadReportFilesDialogComponent } from './download-report-files-dialog.component';

describe('DownloadReportDilesDialogComponent', () => {
  let component: DownloadReportFilesDialogComponent;
  let fixture: ComponentFixture<DownloadReportFilesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DownloadReportFilesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadReportFilesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
