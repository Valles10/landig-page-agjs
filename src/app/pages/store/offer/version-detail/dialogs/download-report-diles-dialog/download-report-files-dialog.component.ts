import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { MatTableDataSource } from '@angular/material/table';

export class ReportFile{
  idFile: string;
  fileName: string;
  fileURL: string;
  creationDate: string;
  fileResult: string;
  fileStatus: string;
}

@Component({
  selector: 'app-download-report-files-dialog',
  templateUrl: './download-report-files-dialog.component.html',
  styleUrls: ['./download-report-files-dialog.component.css']
})
export class DownloadReportFilesDialogComponent implements OnInit {

  viewOptions: ViewOptions = {
    displayedColumns: ['name', 'result', 'status','date','url'],
    pageSize: [25, 35, 50],
    loading: true
  };
  filesDataSource: any = new MatTableDataSource<ReportFile>();
  public files = [];
  public date: string = '';

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<DownloadReportFilesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) { 
    this.files = data.files;
    this.date = data. date;
  }

  ngOnInit(): void {
    this.filesDataSource = new MatTableDataSource<ReportFile>(this.files);
    this.viewOptions.loading = false;
  }

}
