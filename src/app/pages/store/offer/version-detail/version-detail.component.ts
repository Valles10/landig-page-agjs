import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductVersion } from 'src/app/core/models/ProductVersion';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { VersionService } from 'src/app/core/services/version.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { ActivatedRoute } from '@angular/router';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { LoadReport } from 'src/app/core/models/LoadReport';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DownloadReportFilesDialogComponent } from './dialogs/download-report-diles-dialog/download-report-files-dialog.component';
import * as _moment from 'moment';

import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';
import {
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PasswordValidationDialogComponent } from 'src/app/components/shared/password-validation-dialog/password-validation-dialog.component';
import { PermissionService } from 'src/app/core/services/permission.service';

const moment = _moment;

@Component({
  selector: 'app-version-detail',
  templateUrl: './version-detail.component.html',
  styleUrls: ['./version-detail.component.css'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: MAT_DATE_LOCALE, useValue: 'es-us' },
  ],
})
export class VersionDetailComponent implements OnInit {
  public viewOptions: ViewOptions = {
    displayedColumns: ['userName', 'executionResult', 'uploadDate', 'executionDate',/*  'Enviroment',  */'accion'],
    pageSize: [25, 35, 50],
    loading: true
  };
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  reportsDataSource: MatTableDataSource<LoadReport> = new MatTableDataSource<LoadReport>();

  public arrayPermission = new Array();

  public version: ProductVersion;
  private idVersion;
  public reports: Array<LoadReport>;

  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl()
  });
  minDate: Date;
  maxDate: Date;

  now = new Date();
  startDate: FormControl;
  endDate: FormControl;


  constructor(
    private permissionService: PermissionService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private versionService: VersionService,
    private _formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private alert: MyAlertService
  ) {
    this.idVersion = this.route.snapshot.params.version;
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 20, 0, 1);
    this.maxDate = new Date(currentYear + 1, 11, 31);
  }

  ngOnInit(): void {
    this.selectPermission();
    this.now = new Date();
    this.now.setDate(this.now.getDate() - 30);
    this.startDate = new FormControl(this.now);
    this.endDate = new FormControl(new Date());
    this.reload();

  }

  reload() {
    this.load();
    this.loadReport();
  }

  load() {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '70%',
      data: "",
      disableClose: true
    });
    this.version = null;
    this.versionService.get(this.idVersion).subscribe(
      response => {
        if (response.code == 200 && response.result.status == 'ok') {
          this.version = response.result.data.version;
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado para las versiones')
        }
        dialogRefLoadingPage.close();
      },
      err => {
        dialogRefLoadingPage.close();
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      }
    );

  }


  loadReport() {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '70%',
      data: "",
      disableClose: true
    });

    this.reports = [];
    this.reportsDataSource = new MatTableDataSource<LoadReport>();
    this.versionService.report(this.idVersion, this.getStrStartDate(), this.getStrEndDate()).subscribe(
      response => {
        if (response.code == 200 && response.result.status == 'ok') {
          this.reports = response.result.data.reports;
          this.reportsDataSource = new MatTableDataSource<LoadReport>(this.reports);
          this.reportsDataSource.paginator = this.paginator;
          this.reportsDataSource.sort = this.sort;
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado para el reporte')
        }
        dialogRefLoadingPage.close();
      },
      err => {
        dialogRefLoadingPage.close();
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      }
    );

  }

  toUat(version: ProductVersion) {
    if (['A', 'U', 'V'].includes(version?.status)) {
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        width: '400px',
        data: {
          description: `Esta seguro de pasar a UAT la version ${version.name}?`,
          title: "Confirmación"
        },
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.toUATProcess(version);
        }
      });
    } else {
      this.alert.info('Acción no válida');
    }
  }

  toUATProcess(version: ProductVersion) {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '70%',
      data: "",
      disableClose: true
    });
    this.versionService.toUAT(version.versionId).subscribe(response => {
      if (response.code == 200 && response.result.status == 'ok') {
        version.status = (version.status == 'V') ? "U" : version.status;
        version.versionStatus = 'P';
        this.alert.success('Cambio de versión se encuentra en proceso');
        console.log(version);
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      dialogRefLoadingPage.close();
    },
      err => {
        dialogRefLoadingPage.close();

        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }

  toValidationProcess(version: ProductVersion) {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '70%',
      data: "",
      disableClose: true
    });
    this.versionService.validateOffer(version.versionId).subscribe(response => {
      if (response.code == 200 && response.result.status == 'ok') {
        version.status = "V";
        this.alert.success('Versión ' + version?.name + ' validada con exito');
        console.log(version);
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      dialogRefLoadingPage.close();
    },
      err => {
        dialogRefLoadingPage.close();
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }

  toProdProcess(version: ProductVersion) {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '70%',
      data: "",
      disableClose: true
    });
    this.versionService.toProd(version.versionId).subscribe(response => {
      if (response.code == 200 && response.result.status == 'ok') {
        version.status = "B";
        version.versionStatus = 'P';
        this.alert.success('Se ha programado la carga para la versión ' + version?.name);
        console.log(version);
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      dialogRefLoadingPage.close();
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialogRefLoadingPage.close();
      });
  }

  toValidation(version: ProductVersion) {
    if (['U', 'V'].includes(version?.status) && !['P'].includes(version?.versionStatus)) {
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        width: '400px',
        data: {
          description: `Esta seguro de validar la version ${version.name}?`,
          title: "Confirmación"
        },
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.toValidationProcess(version);
        }
      });
    } else {
      this.alert.info('Acción no válida');
    }
  }

  toProd(version: ProductVersion) {
    if (['V'].includes(version?.status) && !['P'].includes(version?.versionStatus)) {
      const dialogRef = this.dialog.open(PasswordValidationDialogComponent, {
        width: '400px',
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.toProdProcess(version);
        }
      });
    } else {
      this.alert.info('Acción no válida');

    }
  }


  searchLoads() {
    this.loadReport();
  }

  getStrStartDate() {
    return moment(this.startDate.value).format('DD-MM-YYYY').toString();
  }

  getStrEndDate() {
    return moment(this.endDate.value).format('DD-MM-YYYY').toString();
  }



  showDownloadFiles(element) {
    const dialogRef = this.dialog.open(DownloadReportFilesDialogComponent, {
      width: '75%',
      data: {
        files: element.loadedFiles,
        date: element.uploadDate
      }
    });
  }
  versionStatus(version: ProductVersion): string {
    switch (version.versionStatus) {
      case 'C':
        return 'Ultima carga realizada con exito...';
      case 'F':
        return 'Ultima carga de la version fallo...';
      case 'P':
        return 'En proceso de carga...';
      default:
        return '';
    }
  }

  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/store/offer/version-management');
  }

}
