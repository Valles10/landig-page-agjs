import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { InventoryService } from 'src/app/core/services/Inventory.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Product } from 'src/app/core/models/Product';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { SupplierService } from 'src/app/core/services/supplier.service';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { SupplierProductDetailDialogComponent } from './components/supplier-product-detail-dialog/supplier-product-detail-dialog.component';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-supplier-product',
  templateUrl: './supplier-product.component.html',
  styleUrls: ['./supplier-product.component.css']
})
export class SupplierProductComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  public arrayPermission = new Array();
  displayedColumns = ['SKU', 'NAME', 'BRAND', 'CATEGORY', 'SUPPLIER', 'PRICE', 'ACCION'];
  products: MatTableDataSource<Product> = new MatTableDataSource<Product>();
  loading = false;
  queryValue: string = '';

  constructor(public dialog: MatDialog,
    private permissionService: PermissionService,
    private supplierService: SupplierService,
    private inventoryService: InventoryService,
    private alert: MyAlertService) { }

  ngOnInit() {
    this.selectPermission();
    this.loadingProduct();
  }

  loadingProduct(){
    this.loading = true;
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.supplierService.getAllProduct().subscribe(response => {
      if (response.code == 200 && response.result.status == 'ok') {
          this.products = new MatTableDataSource<Product>(response.result.data.products);
          this.products.paginator = this.paginator;
          this.products.sort = this.sort;
          this.loading = false;
          this.applyFilter();
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      dialogRefLoadingPage.close();
    }, err => {
      if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
        this.alert.warn(err.error.result.message);
      } else {
        this.alert.error('Proceso no completado');
      }
      dialogRefLoadingPage.close();
    });
  }
  applyFilter() {
    this.products.filter = this.queryValue.trim().toLowerCase();
  }

  selectedRowDetailDialog(row: any) {
    const dialogRef = this.dialog.open(SupplierProductDetailDialogComponent, {
      width: '80%',
      data: row
    });
    dialogRef.afterClosed().subscribe(result => {
      /* if (result.status) {
        this.deleteRow(row);
      } */
      this.loadingProduct();
    });
  }

  deleteRow(row: any) {
    this.products.data.splice(this.products.data.indexOf(row), 1);
    this.products._updateChangeSubscription();
  }

  uploadConfirmDialog(row: any) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        description: "Desea realizar la carga a inventario local?",
        title: "Confirmación"
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
          width: '70%',
          data: "",
          disableClose: true
        });
        this.inventoryService.add(row.sku).subscribe(response => {
          if (response.code == 200 && response.result.status == 'ok') {
            this.alert.success('Se ha cargado el producto '+ row?.sku + ' al inventario local');
              this.deleteRow(row);
          } else {
            this.alert.warn(response?.result?.message || 'Proceso no completado');
          }
          dialogRefLoadingPage.close();
        }, err => {
          if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          }
          dialogRefLoadingPage.close();
        });
      }
    });
  }
  
  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/store/offer/supplier-product');
  }

}
