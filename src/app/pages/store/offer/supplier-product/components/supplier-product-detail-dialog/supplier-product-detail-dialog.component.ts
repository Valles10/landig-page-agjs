import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { Product } from 'src/app/core/models/Product';
import { InventoryService } from 'src/app/core/services/Inventory.service';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-supplier-product-detail-dialog',
  templateUrl: './supplier-product-detail-dialog.component.html',
  styleUrls: ['./supplier-product-detail-dialog.component.css']
})
export class SupplierProductDetailDialogComponent implements OnInit  {

  public arrayPermission = new Array();


  loadedProduct = false;
  constructor(
    private permissionService: PermissionService,
    private inventoryService: InventoryService,
    private alert: MyAlertService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<SupplierProductDetailDialogComponent>,

    @Inject(MAT_DIALOG_DATA) public data: Product) { }

  ngOnInit(): void {
        this.selectPermission();

  }

  onNoClick(): void {
    this.dialogRef.close({
      status: this.loadedProduct
    });
  }


  uploadConfirmDialog($event: MouseEvent, row: any) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        description: "Desea realizar la carga a inventario local?",
        title: "Confirmación"
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
          width: '70%',
          data: ""
        });
        if (!this.loadedProduct) {
          this.inventoryService.add(this.data.sku ).subscribe(response => {
            if (response.code == 200 && response.result.status == 'ok') {
                this.loadedProduct = true;
                this.alert.success('Se ha cargado el producto '+this.data?.sku + ' al inventario local');
                this.onNoClick();
            } else {
              this.alert.warn(response?.result?.message || 'Proceso no completado');
            }
            dialogRefLoadingPage.close();
          }, err => {
            if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
              this.alert.warn(err.error.result.message);
            } else {
              this.alert.error('Proceso no completado');
            }
            dialogRefLoadingPage.close();
          });
        } else {
          dialogRefLoadingPage.close();
          this.alert.info('Acción no permitida');
        }
      }
    });
  }

  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/store/offer/supplier-product');
  }

}
