import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierProductDetailDialogComponent } from './supplier-product-detail-dialog.component';

describe('SupplierProductDetailDialogComponent', () => {
  let component: SupplierProductDetailDialogComponent;
  let fixture: ComponentFixture<SupplierProductDetailDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierProductDetailDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierProductDetailDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
