import { Component, OnInit, ElementRef, ViewChild, Injectable } from '@angular/core';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { Feature } from 'src/app/core/models/Feature';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { CategoryService } from 'src/app/core/services/Category.service';
import { FeatureService } from 'src/app/core/services/feature.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SelectionModel } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs';
import { CategoryTypes } from 'src/app/core/models/Category';
import { CategoryCreateDialogComponent } from './dialogs/category-create-dialog/category-create-dialog.component';
import { AvailableFeaturesDialogComponent } from '../product-detail/dialogs/available-features-dialog/available-features-dialog.component';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PermissionService } from 'src/app/core/services/permission.service';

export class CategoryItemNode {
  children: CategoryItemNode[];
  idCategory: string;
  name: string;
  description?: string;
  checked?: boolean;
  /* checked?: boolean;
  editable?: boolean; */
  categoryTypeId?: string;
  categoryType?: string;
  display?: boolean;
  macroCategory?: boolean;
  groupOfDocuments: string;
}

export class CategoryItemFlatNode {
  idCategory: string;
  name: string;
  description?: string;
  checked?: boolean;
  /* checked?: boolean;
  editable?: boolean; */
  display?: boolean;
  macroCategory?: boolean;
  children: CategoryItemNode[];
  level: number;
  expandable: boolean;
  categoryTypeId?: string;
  categoryType?: string;
  groupOfDocuments: string;

}



@Injectable()
export class ChecklistDatabase {

  dataChange = new BehaviorSubject<CategoryItemNode[]>([]);
  get data(): CategoryItemNode[] { return this.dataChange.value; }

  constructor() { }

  initialize(treeData: any) {
    const data = this.buildFileTree(treeData, 0);
    this.dataChange.next(data);
  }

  buildFileTree(obj: { [key: string]: any }, level: number): CategoryItemNode[] {
    return Object.keys(obj).reduce<CategoryItemNode[]>((accumulator, key) => {
      const value = obj[key];
      const node = new CategoryItemNode();
      node.name = value.name;
      node.idCategory = value.idCategory;
      node.checked = false;
      node.macroCategory = value.macroCategory || false;
      node.display = value.display || false;
      node.description = value.description || '';
      node.categoryType = value.categoryType || '';
      node.categoryTypeId = value.categoryTypeId || '';
      node.groupOfDocuments = value.groupOfDocuments || '';

      if (value != null) {
        if (typeof value === 'object') {
          if (value.children instanceof Array) {
            node.children = this.buildFileTree(value.children, level + 1);
          }
        }
      }
      return accumulator.concat(node);
    }, []);
  }

  insertItem(parent: CategoryItemNode, item: CategoryItemNode) {
    if (parent.children) {
      parent.children.push(item);
      this.dataChange.next(this.data);
    }
  }

  deleteItem(parent: CategoryItemNode, node: CategoryItemNode) {
    parent.children.splice(parent.children.indexOf(node), 1);
    this.dataChange.next(this.data);
  }

  updateItem(node: CategoryItemNode, item: CategoryItemNode) {
    node.name = item.name;
    node.idCategory = item.idCategory;
    node.checked = item.checked;
    /* node.editable = item.editable; */
    node.display = item.display;
    node.macroCategory = item.macroCategory;
    node.description = item.description;
    node.categoryType = item.categoryType;
    node.categoryTypeId = item.categoryTypeId;
    node.groupOfDocuments = item.groupOfDocuments;
    this.dataChange.next(this.data);
  }
}

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  flatNodeMap = new Map<CategoryItemFlatNode, CategoryItemNode>();
  nestedNodeMap = new Map<CategoryItemNode, CategoryItemFlatNode>();
  selectedParent: CategoryItemFlatNode | null = null;
  newItemName = '';
  treeControl: FlatTreeControl<CategoryItemFlatNode>;
  treeFlattener: MatTreeFlattener<CategoryItemNode, CategoryItemFlatNode>;
  dataSource: MatTreeFlatDataSource<CategoryItemNode, CategoryItemFlatNode>;
  getLevel = (node: CategoryItemFlatNode) => node.level;
  isExpandable = (node: CategoryItemFlatNode) => node.expandable;
  getChildren = (node: CategoryItemNode): CategoryItemNode[] => node.children;
  hasChild = (_: number, _nodeData: CategoryItemFlatNode) => _nodeData.expandable;
  hasNoContent = (_: number, _nodeData: CategoryItemFlatNode) => _nodeData.name === '';
  transformer = (node: CategoryItemNode, level: number) => {
    const existingNode = this.nestedNodeMap.get(node);
    const flatNode = existingNode && existingNode.name === node.name
      ? existingNode
      : new CategoryItemFlatNode();
    flatNode.name = node.name;
    flatNode.checked = node.checked;
    flatNode.idCategory = node.idCategory;
    flatNode.display = node.display;
    flatNode.macroCategory = node.macroCategory;
    flatNode.description = node.description;
    flatNode.children = node.children;
    flatNode.categoryType = node.categoryType;
    flatNode.categoryTypeId = node.categoryTypeId;
    flatNode.groupOfDocuments = node.groupOfDocuments;
    flatNode.level = level;
    flatNode.expandable = !!node.children;
    this.flatNodeMap.set(flatNode, node);
    this.nestedNodeMap.set(node, flatNode);
    return flatNode;
  }

  getParentNode(node: CategoryItemFlatNode): CategoryItemFlatNode | null {
    const currentLevel = this.getLevel(node);
    if (currentLevel < 1) {
      return null;
    }
    const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;
    for (let i = startIndex; i >= 0; i--) {
      const currentNode = this.treeControl.dataNodes[i];
      if (this.getLevel(currentNode) < currentLevel) {
        return currentNode;
      }
    }
    return null;
  }

  addNewItem(node: CategoryItemFlatNode, child: CategoryItemNode) {
    const parentNode = this.flatNodeMap.get(node);
    this._database.insertItem(parentNode!, child);
    this.treeControl.expand(node);
  }

  disabledSelectionTree(tree: Array<CategoryItemFlatNode>, idEnabled: string) {
    if (idEnabled == '0') {
      tree.forEach((node: CategoryItemFlatNode) => {
        /* node.editable = false; */
        node.checked = false;
      });
    } /* else {
      tree.forEach((node: CategoryItemFlatNode) => {
        node.editable = false;
        node.checked = false;
      });
    } */
  }
  deleteItem(node: CategoryItemFlatNode) {
    const nestedNode = this.flatNodeMap.get(node);
    const parentNode = this.getParentNode(node);
    this._database.deleteItem(parentNode!, nestedNode);
  }

  /*  setEditNodeItem(node: CategoryItemFlatNode) {
     node.editable = !node.editable;
     if (node.editable)
       setTimeout(() => this.itemEdit.nativeElement.focus(), 0);
   } */

  removeNodeCategory(node: CategoryItemFlatNode) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        description: `Si se elimina la categoria todas las categorias que dependan de ella tambien seran eliminadas. desea completar esta acción?`,
        title: "Confirmación"
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.resetFeatures();
        const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
          width: '80%',
          data: ""
        });
        this.categoryService.deleteCategory({
          idCategory: node.idCategory.toString(),
          name: '',
          description: '',
          categoryTypeId: '',
          display: false,
          groupOfDocuments: ''
        }).subscribe(response => {
          if (response.code == 200 && response.result.status == "ok") {
            this.alert.success('Se ha eliminado la categoria ' + node.name);
            this.deleteItem(node);
          } else {
            this.alert.warn(response?.result?.message || 'Proceso no completado');
          }
          dialogRefLoadingPage.close();
        }
          , err => {
            if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
              this.alert.warn(err.error.result.message);
            } else {
              this.alert.error('Proceso no completado');
            }
            dialogRefLoadingPage.close();
          });

      }
    });

  }



  onClickCategory(event, node: CategoryItemNode) {
    event.preventDefault();
    let temp = node.checked;
    this.disabledSelectionTree(this.treeControl.dataNodes, '0');
    if (!temp) {
      this.selectedCategoryNode = node;
      this.loadingFeatures(node);
    } else {
      this.resetFeatures();
    }
    node.checked = !temp;
  }

  selectFeature(row) {
    if (this.arrayPermission.includes('edit')) {
      if (row.category == this.selectedCategoryNode.idCategory) {
        row.selected = !row.selected;
        if (row.selected) {
          row.status = 0;
        } else {
          row.status = 1;
        }
      }
    }
  }

  saveFeaturesTemplate() {
    let featuresMod: Array<Feature> = new Array<Feature>();
    for (var item of this.features.data) {
      if (item.status == 1) {
        featuresMod.push({
          id: item.id,
          status: 0
        });
      }
    }

    if (featuresMod.length > 0) {
      const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
        width: '80%',
        data: "",
        disableClose: true
      });
      this.categoryService.dissasociateFeatures({
        "idCategory": this.selectedCategoryNode.idCategory,
        "features": featuresMod
      }).subscribe(response => {
        if (response.code == 200 && response.result.status == "ok") {
          this.alert.success('Caracteristicas actualizadas con exito');
          let indexRemoval: number[] = [];
          this.features.data.forEach(row => {
            if (row.status == 1) {
              indexRemoval.push(this.features.data.indexOf(row));
            } else {
              row.editable = false;
              row.status = 0;
              row.oldValue = (row.value) ? row.value.trim() : "";
              row.value = (row.value) ? row.value.trim() : "";
            }
          });
          let indexSet = new Set(indexRemoval);
          this.features.data = this.features.data.filter((value, i) => !indexSet.has(i));
          dialogRefLoadingPage.close();
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado');
          dialogRefLoadingPage.close();
        }
      }, err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialogRefLoadingPage.close();
      });
    }
  }

  validateFeaturesChanges(): boolean {
    let result = true;
    this.features.data.forEach(element => {
      if (element.status == 1)
        result = false;
    });
    return result;
  }

  resetFeatures() {
    this.selectedCategoryNode = null;
    this.features = new MatTableDataSource<Feature>();
  }
  loadingFeatures(nodeCategory: CategoryItemNode) {
    this.flagFoadingFeatures = true;
    this.features = new MatTableDataSource<Feature>();
    this.categoryService.getFeatures({ idCategory: nodeCategory.idCategory }).subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
        if (response.result?.data?.categories instanceof Array) {
          let newArrFeatures: Array<any> = new Array<any>();
          let categoriess: Array<any> = response.result.data.categories;
          categoriess.forEach(element => {
            element.features.forEach(f => {
              newArrFeatures.push({
                category: element.idCategory,
                text: f.text,
                traduction: f.traduction,
                id: Number(f.id) || 0,
                selected: true,
                status: 0,
                oldValue: true,
                position: (this.selectedCategoryNode.idCategory != element.category ? 0 : 1)
              })
            });
          });
          newArrFeatures.sort((a, b) => {
            if (a.position < b.position)
              return 1;
            if (a.position > b.position)
              return -1;
            return 0;
          });
          this.features = new MatTableDataSource<Feature>(newArrFeatures);
          this.features.paginator = this.paginator;
          this.features.sort = this.sort;
        } else {
          this.alert.info('Datos de caracteristicas no son validos');
        }
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      this.flagFoadingFeatures = false;
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        this.flagFoadingFeatures = false;
      });
    this.featuresTemplate.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" });
  }



  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('featureChBox') refFeatureChBox;
  @ViewChild('itemValue', { static: false }) itemValue: ElementRef;
  @ViewChild('itemEdit', { static: false }) itemEdit: ElementRef;

  @ViewChild('refCategory') refCategory;
  @ViewChild("featuresTemplate") featuresTemplate: ElementRef;

  public arrayPermission = new Array();
  public selectedCategoryNode: CategoryItemNode = null;
  public flagFoadingFeatures = false;
  public loading = true;
  public panelOpenState = false;
  public features: MatTableDataSource<Feature> = new MatTableDataSource<Feature>();
  public queryValue = '';
  constructor(
    private permissionService: PermissionService,
    public dialog: MatDialog,
    private categoryService: CategoryService,
    private featureService: FeatureService,
    private _database: ChecklistDatabase,
    private alert: MyAlertService
  ) {
    this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel,
      this.isExpandable, this.getChildren);
    this.treeControl = new FlatTreeControl<CategoryItemFlatNode>(this.getLevel, this.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
    _database.dataChange.subscribe(data => {
      this.dataSource.data = data;
    });
  }

  ngOnInit(): void {
    this.selectPermission();
    this.loadingCategories();
  }

  loadingCategories() {
    this.loading = true;
    this._database.initialize([
      {
        name: 'Categorias',
        idCategory: '-1',
        children: []
      }
    ]);
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.categoryService.getCategoriesTree().subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
        if (response.result.data.categoriesTree instanceof Array) {
          if (response.result.data.categoriesTree.length > 0) {
            this._database.initialize([
              {
                name: 'Categorias',
                idCategory: '-1',
                categoryType: '',
                children: response.result.data.categoriesTree
              }
            ]);
            if (this.treeControl.dataNodes.length > 0) {
              this.treeControl.expand(this.treeControl.dataNodes[0]);
            }
          } else {
            this._database.initialize([
              {
                name: 'Categorias',
                idCategory: '-1',
                children: []
              }
            ]);
            if (this.treeControl.dataNodes.length > 0) {
              this.treeControl.expand(this.treeControl.dataNodes[0]);
            }
          }
        } else {
          this.alert.warn('Datos de categorias no son validas');
        }
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      dialogRefLoadingPage.close();
    }
      , err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialogRefLoadingPage.close();
      });
  }

  /*   loadingCategoriesType() {
      this.loading = true;
      const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
        width: '80%',
        data: "",
        disableClose: true
      });
      this.categoryService.getTypes().subscribe(response => {
        if (response.code == 200 && response.result.status == "ok") {
          if (response.result.data.category_types instanceof Array) {
            this.typeOfCategories = response.result.data.category_types;
          } else {
            this.typeOfCategories = [];
            this.alert.info('Tipos de categorias no son validos');
          }
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado');
        }
        dialogRefLoadingPage.close();
      }
        , err => {
          if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          }
          dialogRefLoadingPage.close();
        });
    } */

  addEdit(row: Feature) {
    this.setEditable(row);
  }

  updateModelCharacteristic(row: Feature) {
    this.updateModel(row);
  }

  deleteFeature(row: Feature) {
    this.deleteModel(row);
  }

  setEditable(row: Feature) {
    // Valida que no se eliminara
    if (row.status != 1) {
      // Valida que sea editable
      if (row.editable) {
        row.editable = false;
      } else {
        row.editable = true;
      }
    } else if (row.status == 1) {
      row.editable = false;
    }
    else {
      row.editable = true;
    }
  }

  updateModel(row: Feature) {
    //No se borrara
    if (row.status != 1) {
      // valida valores que no sean iguales
      if (row.oldValue.trim() == row.value.trim()) {
        // los valores no se cambiaran en actualizacion
        // No se insertara
        if (row.status != 3) {
          row.status = 0;
        }
      } else {
        // Los valores son diferentes
        // si los valores no se insertan
        if (row.status != 3) {
          row.status = 2;
        }
      }
    }
  }

  deleteModel(row: Feature) {
    // Si estaba en modo eliminacion
    if (row.status == 1) {
      /* if (row.futureStatus) {
        row.status = 3;
      } else { */
      row.status = 0;
      this.updateModel(row);
      //}
    } else {
      //Si es uno nuevo que se agregara
      if (row.status == 3) {
        this.features.data.splice(this.features.data.indexOf(row), 1);
        this.features._updateChangeSubscription();
      }
      else
        /* if (row.futureStatus)
          row.status = 3;
        else */
        row.status = 1;
      row.editable = false;
    }
  }
  showValidFeatures(idCategory: string) {
    if (idCategory) {
      const dialogRef = this.dialog.open(AvailableFeaturesDialogComponent, {
        width: '80%',
        data: {
          idCategory: idCategory,
          features: this.features.data
        }
      });
      dialogRef.afterClosed().subscribe((result: SelectionModel<Feature>) => {
        if (typeof result != 'undefined') {
          if (result.selected instanceof Array) {
            result.selected.forEach((element: Feature) => {
              let feature = new Feature();
              feature.id = element.id;
              feature.traduction = element?.traduction;
              feature.text = element.text;
              feature.status = 4;
              feature.selected = true;
              feature.category = this.selectedCategoryNode.idCategory
              this.features.data.push(
                feature);
              this.features.data.sort((a: Feature, b: Feature) => {
                if (a.status < b.status)
                  return 1;
                if (a.status > b.status)
                  return -1;
                return 0;
              });
              this.features._updateChangeSubscription();
            })
          }
        }
      });
    } else {
      this.alert.info('No hay categoria seleccionada');
    }
  }

  saveFeatures() {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.featureService.updateFeatures({
      sku: '',
      characteristics: this.features.data
    }).subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
        if (response.result.data instanceof Array) {
          let indexRemoval: number[] = [];
          this.features.data.forEach(row => {
            if (row.status == 1) {
              indexRemoval.push(this.features.data.indexOf(row));
            } else {
              row.editable = false;
              row.status = 0;
              row.oldValue = (row.value) ? row.value.trim() : "";
              row.value = (row.value) ? row.value.trim() : "";
            }
          });
          let indexSet = new Set(indexRemoval);
          this.features.data = this.features.data.filter((value, i) => !indexSet.has(i));
          this.features._updateChangeSubscription();
          this.alert.success('Caracteristicas actualizadas con exito');
        } else {
          this.alert.success('Datos de actualizacion de caracteristicas no es valido');
        }
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      dialogRefLoadingPage.close();
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialogRefLoadingPage.close();
      });

  }

  formIsValid(value, type) {
    if (type && type != '' && value != '' && (value.replace(/\s/g, '') != '')) {
      return true
    } else {
      return false;
    }
  }

  formIsValidTwo(node: CategoryItemFlatNode) {
    if (node.categoryTypeId && node.categoryTypeId != '' && node.name != '' && (node.name.replace(/\s/g, '') != '')) {
      return true
    } else {
      return false;
    }
  }

  /* getCategoryName(id: string) {
    let res = this.typeOfCategories.find(item => item.id_category_type == id);
    if (res) {
      return res.name
    } else {
      return ''
    }
  } */


  add(node: CategoryItemFlatNode) {
    const dialogRedCreateCategory = this.dialog.open(CategoryCreateDialogComponent, {
      width: '80%',
      data: {
        parentId: node.idCategory.toString(),
        idCategory: '0',
        name: node.name,
        categoryTypeId: node.categoryTypeId,
        groupOfDocuments: node.groupOfDocuments
      },
      disableClose: false
    });

    dialogRedCreateCategory.afterClosed().subscribe(data => {
      if (data) {
        this.addNewItem(node, {
          idCategory: data.idCategory,
          name: data.name,
          checked: false,
          display: data.display,
          macroCategory: data.macroCategory,
          /* editable: data.editable, */
          children: [],
          description: data.description || '',
          categoryTypeId: data.categoryTypeId,
          categoryType: data.categoryType,
          groupOfDocuments: data.groupOfDocuments
        });

      }
    });
  }

  update(node: CategoryItemFlatNode) {
    const parentNode = this.getParentNode(node);
    const dialogRedCreateCategory = this.dialog.open(CategoryCreateDialogComponent, {
      width: '80%',
      data: {
        parentId: parentNode.idCategory.toString() == '-1' ? '' : parentNode.idCategory.toString(),
        idCategory: node.idCategory.toString(),
        name: node.name,
        categoryTypeId: node.categoryTypeId,
        display: node.display,
        macroCategory: node.macroCategory,
        description: node.description,
        groupOfDocuments: node.groupOfDocuments
      },
      disableClose: false
    });

    dialogRedCreateCategory.afterClosed().subscribe(data => {
      if (data) {
        const nestedNode = this.flatNodeMap.get(node);
        this._database.updateItem(nestedNode!, {
          idCategory: node.idCategory,
          name: data.name,
          checked: false,
          description: data.description,
          /* filter: data.filter, */
          macroCategory: data.macroCategory,
          display: data.display,
          children: node.children,
          categoryTypeId: data.categoryTypeId,
          categoryType: data.categoryType,
          groupOfDocuments: data.groupOfDocuments
        });
        this.resetFeatures();
      }
    });
  }

  applyFilter() {
    this.features.filter = this.queryValue.trim().toLowerCase();
  }
  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/store/offer/categories');
  }

}
