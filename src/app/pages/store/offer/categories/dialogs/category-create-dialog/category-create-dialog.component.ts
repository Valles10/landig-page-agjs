import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CategoryService } from 'src/app/core/services/Category.service';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { CategoryTypes } from 'src/app/core/models/Category';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { DocumentService } from 'src/app/core/services/document-service';
import { Group } from 'src/app/core/models/Group';
import { GroupService } from 'src/app/core/services/group.service';

@Component({
  selector: 'app-category-create-dialog',
  templateUrl: './category-create-dialog.component.html',
})
export class CategoryCreateDialogComponent implements OnInit {

  public categoryForm: FormGroup;
  public typeOfCategories: Array<CategoryTypes> = [];
  public documentsGroups: Array<Group> = [];

  @ViewChild('_type') _type;
  @ViewChild('_doc') _doc;


  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<CategoryCreateDialogComponent>,
    private categoryService: CategoryService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private alert: MyAlertService,
    private groupService: GroupService
  ) { }

  ngOnInit(): void {
    if (this.data?.idCategory != '0') {
      this.categoryForm = new FormGroup({
        idCategory: new FormControl(this.data.idCategory),
        name: new FormControl(this.data.name || '',
          [
            Validators.required,
            Validators.maxLength(254),
            Validators.pattern(/^([a-zA-Z]*)+([a-zA-Z0-9\_\-\ ]*)?$/)
          ]),
        type: new FormControl(this.data.categoryTypeId || '', [
          Validators.required
        ]),
        description: new FormControl(this.data.description || '', [
          Validators.required,
          Validators.maxLength(254)
        ]),
        display: new FormControl(this.data.display || false, [
        ]),
        macroCategory: new FormControl(this.data.macroCategory || false, [
        ]),
        groupOfDocuments: new FormControl(this.data.groupOfDocuments || '', [
        ]),

      });
    } else {
      this.categoryForm = new FormGroup({
        idCategory: new FormControl('0'),
        name: new FormControl('',
          [
            Validators.required,
            Validators.maxLength(254),
            Validators.pattern(/^([a-zA-Z]*)+([a-zA-Z0-9\_\-\ ]*)?$/)
          ]),
        type: new FormControl('', [
          Validators.required
        ]),
        description: new FormControl('', [
          Validators.required,
          Validators.maxLength(254)
        ]),
        display: new FormControl(false, [
        ]),
        macroCategory: new FormControl(false, [
        ]),
        groupOfDocuments: new FormControl('', [
        ]),
      });
    }
    this.loadingCategoriesType();
    this.loadingGroupDocuments();

  }
  get name() { return this.categoryForm.get('name') }
  get type() { return this.categoryForm.get('type') }
  get idCategory() { return this.categoryForm.get('idCategory') }
  get description() { return this.categoryForm.get('description') }
  get display() { return this.categoryForm.get('display') }
  get macroCategory() { return this.categoryForm.get('macroCategory') }
  get groupOfDocuments() { return this.categoryForm.get('groupOfDocuments') }



  loadingCategoriesType() {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.categoryService.getTypes().subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {

        if (response.result.data.category_types instanceof Array) {
          this.typeOfCategories = response.result.data.category_types;
        } else {
          this.typeOfCategories = [];
          this.alert.info('Tipos de categorias no son validos ');
        }
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      dialogRefLoadingPage.close();
    }, err => {
      this.processError(err);
      dialogRefLoadingPage.close();
    });
  }

  loadingGroupDocuments() {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.groupService.get().subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {

        if (response.result.data.groups instanceof Array) {
          this.documentsGroups = response.result.data.groups;
        } else {
          this.documentsGroups = [];
          this.alert.info('Grupos de documentos no encontrados');
        }
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      dialogRefLoadingPage.close();
    }, err => {
      this.processError(err);
      dialogRefLoadingPage.close();
    });
  }
  save() {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    if (this.idCategory.value == '0') {
      this.categoryService.addCategory({
        idCategory: this.idCategory.value,
        name: this.name.value,
        parentId: this.data.parentId.toString(),
        description: this.description.value,
        categoryTypeId: this.type.value,
        display: this.display.value,
        macroCategory: this.macroCategory.value,
        groupOfDocuments: this.groupOfDocuments.value
      }).subscribe(response => {
        if (response.code == 200 && response.result.status == "ok") {
          this.alert.success('Categoria ' + this.name.value + ' agregada con exito');
          this.dialogRef.close({
            idCategory: response.result.data.category.idCategory,
            name: this.name.value,
            checked: false,
            editable: false,
            display: this.display.value,
            macroCategory: this.macroCategory.value,
            children: [],
            description: this.description.value,
            categoryTypeId: this.type.value,
            categoryType: this._type._elementRef.nativeElement.innerText.trim(),
            groupOfDocuments: this.groupOfDocuments.value
          });
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado');
        }
        dialogRefLoadingPage.close();
      }, err => {
        this.processError(err);
        dialogRefLoadingPage.close();
      });
    } else {
      this.categoryService.updateCategory({
        idCategory: this.idCategory.value,
        name: this.name.value,
        description: this.description.value || '',
        parentId: this.data.parentId.toString() == '-1' ? '' : this.data.parentId.toString(),
        categoryTypeId: this.type.value,
        display: this.display.value,
        macroCategory: this.macroCategory.value,
        groupOfDocuments: this.groupOfDocuments.value
      }).subscribe(response => {
        if (response.code == 200 && response.result.status == "ok") {
          this.alert.success('Categoria ' + this.name.value + ' actualizada con exito');
          this.dialogRef.close({
            idCategory: this.idCategory.value,
            name: this.name.value,
            checked: false,
            editable: false,
            display: this.display.value,
            macroCategory: this.macroCategory.value,
            description: this.description.value,
            categoryTypeId: this.type.value,
            categoryType: this._type._elementRef.nativeElement.innerText.trim()
          });
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado');
        }
        dialogRefLoadingPage.close();
      }, err => {
        this.processError(err);
        dialogRefLoadingPage.close();
      });
    }
  }

  processError(err) {
    if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
      this.alert.warn(err.error.result.message);
    } else {
      this.alert.error('Proceso no completado');
    }
  }
}
