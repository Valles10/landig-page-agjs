import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Feature } from 'src/app/core/models/Feature';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { FeatureService } from 'src/app/core/services/feature.service';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { FeatureCreateDialogComponent } from './dialogs/feature-create-dialog/feature-create-dialog.component';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-features',
  templateUrl: './features.component.html'
})
export class FeaturesComponent implements OnInit {


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('formName') formName: ElementRef;

  public arrayPermission = new Array();
  public feature: Feature = new Feature();

  queryValue: string = "";
  idEditor: string = "0";


  displayedColumns = ['traduction', 'text', 'description', 'lastModify', 'actions'];
  features: MatTableDataSource<Feature> = new MatTableDataSource<Feature>();
  loading = false;


  featureForm = new FormGroup({
    name: new FormControl('',
      Validators.required),
    id: new FormControl('')
  });

  constructor(
    private permissionService: PermissionService,
    public dialog: MatDialog,
    private featureService: FeatureService,
    private alert:MyAlertService
  ) { }

  ngOnInit(): void {
    this.selectPermission();
    this.featureForm.patchValue({
      name: '',
      id: null
    });
    this.loadFeatures();
  }

  loadFeatures() { 
    this.loading = true;
    this.features = new MatTableDataSource<Feature>();
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.featureService.getFeatures().subscribe(response => {
      if (response.code == 200 && response.result.status == 'ok') {
          this.features = new MatTableDataSource<Feature>(response.result.data.characteristics);
          this.features.data.forEach(element => {
            element.position = 0;
          });
          this.features.paginator = this.paginator;
          this.features.sort = this.sort;
          this.loading = false;
          this.applyFilter();
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      dialogRefLoadingPage.close();
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        } 
        dialogRefLoadingPage.close();
      });
  }
  applyFilter() {
    //const filterValue = (event.target as HTMLInputElement).value;
    this.features.filter = this.queryValue.trim().toLowerCase();
  }

  deleteRow(row: any) {
    this.features.data.splice(this.features.data.indexOf(row), 1);
    this.features._updateChangeSubscription();
  }

  deleteFeature(row: any) {
    if (row.lock == 0) {
      const dialogConfirm = this.dialog.open(ConfirmDialogComponent, {
        width: '400px',
        data: {
          title: 'Confirmación',
          description: 'Desea eliminar la característica?'
        }
      });
      dialogConfirm.afterClosed().subscribe(result => {
        if (result) {
          const dialogLoading = this.dialog.open(LoadingPageDialogComponent, {
            width: '50%',
            data: ""
          });
          this.featureService.deleteFeature(
            row.id.toString()).subscribe(response => {
              if (response.code == 200 && response.result.status == "ok") {
                  this.alert.success("Se ha eliminado la caracteristica " + row.text);
                  this.deleteRow(row);
              } else {
                this.alert.warn(response?.result?.message || 'Proceso no completado');
              }
              dialogLoading.close();
            }, err => {
              if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
                this.alert.warn(err.error.result.message);
              } else {
                this.alert.error('Proceso no completado');
              } 
              dialogLoading.close();
            });
        }
      });
    }
  }

  editFeature(row: Feature) {
    this.featureForm.patchValue({
      name: row.name,
      id: row.id.toString()
    });
    this.formName.nativeElement.focus();
  }

  edit(feature?: Feature) {
    const dialogBrandDetail = this.dialog.open(FeatureCreateDialogComponent, {
      width: '60%',
      data: {
        feature: feature
      }
    });
    dialogBrandDetail.afterClosed().subscribe(result => {
      if (result) {
        this.loadFeatures();
      }
    });
  }

  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/store/offer/features');
  }
  
}
