import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FeatureService } from 'src/app/core/services/feature.service';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatCheckbox } from '@angular/material/checkbox';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';

@Component({
  selector: 'app-feature-create-dialog',
  templateUrl: './feature-create-dialog.component.html'
})
export class FeatureCreateDialogComponent implements OnInit {

  public featureForm: FormGroup;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<FeatureCreateDialogComponent>,
    private featureService: FeatureService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private alert: MyAlertService
  ) {
  }

  ngOnInit(): void {
    if (this.data.feature?.id) {
      this.featureForm = new FormGroup({
        name: new FormControl({
          value: this.data.feature?.traduction,
          disabled: this.data?.feature?.lock == '1' ? true : false
        },
          [
            Validators.required,
            Validators.maxLength(254),
          ]),
        id: new FormControl(this.data.feature?.id),
        identifier: new FormControl({
          value: this.data.feature?.text || '',
          disabled: this.data?.feature?.lock == '1' ? true : false
        }
          , [
            Validators.required,
            Validators.maxLength(254),
            Validators.pattern(/^([a-zA-Z0-9])+([a-zA-Z0-9\_]*)+$/)
          ]),
        displayable: new FormControl({
          value: this.data.feature?.displayable == '1' ? true : false,
          disabled: this.data?.feature?.lock == '1' ? true : false
        }),
        filter: new FormControl({
          value: this.data.feature?.filter == '1' ? true : false,
          disabled: this.data?.feature?.lock == '1' ? true : false
        }),
        image: new FormControl({
          value: this.data.feature?.image == '1' ? true : false,
          disabled: this.data?.feature?.lock == '1' ? true : false
        }),
        general: new FormControl({
          value: this.data.feature?.general || '0',
          disabled: this.data?.feature?.lock == '1' ? true : false
        }),
        example: new FormControl({
          value: this.data.feature?.placeholder || '',
          disabled: this.data?.feature?.lock == '1' ? true : false
        }, [Validators.maxLength(50)]),
        description: new FormControl({
          value: this.data?.feature?.description || '',
          disabled: this.data?.feature?.lock == '1' ? true : false
        }, [Validators.maxLength(50)]),
        boolean_value: new FormControl({
          value: this.data.feature?.boolean_value == '1' ? true : false,
          disabled: this.data?.feature?.lock == '1' ? true : false
        })

      });
    } else {
      this.featureForm = new FormGroup({
        name: new FormControl('', [
          Validators.required,
          Validators.maxLength(254),
        ]),
        id: new FormControl(null),
        identifier: new FormControl('', [
          Validators.required,
          Validators.maxLength(254),
          Validators.pattern(/^([a-zA-Z0-9])+([a-zA-Z0-9\_]*)+$/)

        ]),
        displayable: new FormControl(false),
        filter: new FormControl(false),
        image: new FormControl(false),
        general: new FormControl('0'),
        example: new FormControl('', [Validators.maxLength(50)]),
        description: new FormControl('', [Validators.maxLength(50)]),
        boolean_value: new FormControl(false)
      });
    }

  }

  get id() { return this.featureForm.get('id') };
  get name() { return this.featureForm.get('name') };
  get identifier() { return this.featureForm.get('identifier') };
  get image() { return this.featureForm.get('image') };
  get displayable() { return this.featureForm.get('displayable') };
  get filter() { return this.featureForm.get('filter') };
  get general() { return this.featureForm.get('general') };
  get example() { return this.featureForm.get('example') };
  get description() { return this.featureForm.get('description') };
  get boolean_value() { return this.featureForm.get('boolean_value') };


  booleanChange() {
    if (!this.boolean_value.value) {
      this.example.disable();
      this.example.setValue('');
    } else {
      this.example.enable();
    }
  }


  save() {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.featureService.saveFeatures({
      id: this.id.value,
      name: this.name.value,
      identifier: this.identifier.value,
      image: this.boolean_value.value ? '0' : (this.image.value ? "1" : "0"),
      displayable: this.displayable.value ? "1" : "0",
      filter: this.filter.value ? "1" : "0",
      general: this.boolean_value.value ? '0' : !this.image.value ? '0' : this.general.value,
      description: this.description.value,
      placeholder: this.example.value,
      boolean_value: this.image.value ? '0' : this.boolean_value.value ? "1" : "0",
    }).subscribe(response => {
      if (response.code == 200 && response.result.status == 'ok') {
        if (this.id.value) {
          this.alert.success('Se ha actualizado la caracteristica ' + this.name.value);
        } else {
          this.alert.success('Se ha creado la caracteristica ' + this.name.value);
        }
        this.dialogRef.close(true);
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      dialogRefLoadingPage.close();
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialogRefLoadingPage.close();
      });
  }

}
