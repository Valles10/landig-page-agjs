import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureCreateDialogComponent } from './feature-create-dialog.component';

describe('FeatureCreateDialogComponent', () => {
  let component: FeatureCreateDialogComponent;
  let fixture: ComponentFixture<FeatureCreateDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeatureCreateDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeatureCreateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
