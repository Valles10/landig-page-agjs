import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { VersionService } from 'src/app/core/services/version.service';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { ProductVersion } from 'src/app/core/models/ProductVersion';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { CreateVersionDialogComponent } from '../inventory/dialogs/create-version-dialog/create-version-dialog.component';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { PasswordValidationDialogComponent } from 'src/app/components/shared/password-validation-dialog/password-validation-dialog.component';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { FormBuilder } from '@angular/forms';
import { PermissionService } from 'src/app/core/services/permission.service';


const STATUS = [
  {
    id: 'All',
    name: 'Todo'
  },
  {
    id: 'A',
    name: 'Activo'
  },
  {
    id: 'I',
    name: 'UAT'
  },
  {
    id: 'D',
    name: 'Produccion'
  }
];


@Component({
  selector: 'app-version-management',
  templateUrl: './version-management.component.html',
  styleUrls: ['./version-management.component.css'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: { displayDefaultIndicatorType: false }
  }]
})
export class VersionManagementComponent implements OnInit {


  isLinear = false;

  public arrayPermission = new Array();
  public queryValue: string = '';
  public statuses = STATUS;

  private versions: Array<ProductVersion> = [];
  public filteredVersions: Array<ProductVersion> = [];
  loading = false;


  constructor(public dialog: MatDialog,
    private permissionService: PermissionService,
    private versionService: VersionService,
    private _formBuilder: FormBuilder,
    private alert: MyAlertService
  ) {
  }

  ngOnInit(): void {
    this.selectPermission();
    this.load();
  }

  applyFilter() {
    this.filteredVersions = this.versions.filter((e) => e.name.toLowerCase().indexOf(this.queryValue.toLowerCase()) > -1);
  }

  load() {
    this.loading = true;
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.versions = [];
    this.filteredVersions = [];
    this.versionService.all().subscribe(
      response => {
        if (response.code == 200 && response.result.status == 'ok') {
          this.versions = response.result.data.versions;
          this.filteredVersions = this.versions.slice();
          this.applyFilter();
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado');
        }
        this.loading = false;
        dialogRefLoadingPage.close();
      }, err => {
        dialogRefLoadingPage.close();
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      }
    );
  }

  createVersion() {
    const dialogRef = this.dialog.open(CreateVersionDialogComponent, {
      width: '50%',
      data: {
        versions: this.versions
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.load();
      }
    });
  }

  deleteVersion(version: ProductVersion) {
    event.preventDefault();
    if (['A', 'U', 'V'].includes(version?.status)) {
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        width: '400px',
        data: {
          description: `Esta seguro de eliminar la version ${version.name}?`,
          title: "Confirmación"
        },
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
            width: '70%',
            data: "",
            disableClose: true
          });
          this.versionService.delete(version.versionId).subscribe(response => {
            if (response.code == 200 && response.result.status == 'ok') {
              this.load();
              this.alert.success('Se ha eliminado la versión ' + version?.name);
            } else {
              this.alert.warn(response?.result?.message || 'Proceso no completado');
            }
            dialogRefLoadingPage.close();
          },
            err => {
              dialogRefLoadingPage.close();
              if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
                this.alert.warn(err.error.result.message);
              } else {
                this.alert.error('Proceso no completado');
              }
            });
        }
      });
    } else {
      this.alert.info('Acción no válida');
    }
  }

  updateVersion(version: ProductVersion) {
    const dialogRef = this.dialog.open(CreateVersionDialogComponent, {
      width: '50%',
      data: {
        versions: this.versions,
        version: version
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result) {
        this.load();
      }
    });
  }

  toUat(version: ProductVersion) {
    if (['A', 'U', 'V'].includes(version?.status)) {
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        width: '400px',
        data: {
          description: `Esta seguro de pasar a UAT la version ${version.name}?`,
          title: "Confirmación"
        },
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.toUATProcess(version);
        }
      });
    } else {
      this.alert.info('Acción no válida');
    }
  }

  toUATProcess(version: ProductVersion) {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '70%',
      data: "",
      disableClose: true
    });
    this.versionService.toUAT(version.versionId).subscribe(response => {
      if (response.code == 200 && response.result.status == 'ok') {
        version.status = (version.status == 'V') ? "U" : version.status;
        version.versionStatus = 'P';
        this.alert.success('Cambio de versión se encuentra en proceso');
        console.log(version);
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      dialogRefLoadingPage.close();
    },
      err => {
        dialogRefLoadingPage.close();
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }

  toValidationProcess(version: ProductVersion) {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '70%',
      data: "",
      disableClose: true
    });
    this.versionService.validateOffer(version.versionId).subscribe(response => {
      if (response.code == 200 && response.result.status == 'ok') {
        version.status = "V";
        this.alert.success('Versión ' + version?.name + ' validada con exito');
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado')
      }
      dialogRefLoadingPage.close();
    },
      err => {
        dialogRefLoadingPage.close();
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }

  toProdProcess(version: ProductVersion) {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '70%',
      data: "",
      disableClose: true
    });
    this.versionService.toProd(version.versionId).subscribe(response => {
      if (response.code == 200 && response.result.status == 'ok') {
        version.status = "B";
        version.versionStatus = 'P';
        this.alert.success('Se ha programado la carga para la versión ' + version?.name);
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      dialogRefLoadingPage.close();
    },
      err => {
        dialogRefLoadingPage.close();
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }

  toValidation(version: ProductVersion) {
    if (['U', 'V'].includes(version?.status) && !['P'].includes(version?.versionStatus)) {
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        width: '400px',
        data: {
          description: `Esta seguro de validar la version ${version.name}?`,
          title: "Confirmación"
        },
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.toValidationProcess(version);
        }
      });
    } else {
      this.alert.info('Acción no válida');

    }
  }

  toProd(version: ProductVersion) {
    if (['V'].includes(version?.status) && !['P'].includes(version?.versionStatus)) {
      const dialogRef = this.dialog.open(PasswordValidationDialogComponent, {
        width: '400px',
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.toProdProcess(version);
        }
      });
    } else {
      this.alert.info('Acción no válida');

    }
  }

  versionStatus(version: ProductVersion): string {
    switch (version.versionStatus) {
      case 'C':
        return 'Ultima carga realizada con exito...';
      case 'F':
        return 'Ultima carga de la version fallo...';
      case 'P':
        return 'En proceso de carga...';
      default:
        return '';
    }
  }

  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/store/offer/version-management');
  }

}


