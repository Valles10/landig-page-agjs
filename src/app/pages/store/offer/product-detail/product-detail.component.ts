import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { Category } from 'src/app/core/models/Category';
import { MatTableDataSource } from '@angular/material/table';
import { Feature } from 'src/app/core/models/Feature';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  public sku: string;
  productInfo: FeaturesData = new FeaturesData();
  public tmcode: string =  '';
 
  constructor(
    public dialog: MatDialog,
    private route: ActivatedRoute,
  ) {
    this.sku = this.route.snapshot.params.sku;
  }

  ngOnInit(): void {
  }

  propagateDataProductDetail(mensaje) {
    this.productInfo = mensaje.productInfo;
    this.tmcode = mensaje.tmcode;
  }

}


export class FeaturesData{
  category: Category;
  error: number;
  features: MatTableDataSource<Feature>
  constructor(){
    this.category = new Category();
    this.error= -1;
    this.features = new  MatTableDataSource<Feature>();
  }
}