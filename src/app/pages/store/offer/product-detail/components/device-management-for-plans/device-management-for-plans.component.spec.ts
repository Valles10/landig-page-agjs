import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceManagementForPlansComponent } from './device-management-for-plans.component';

describe('DeviceManagementForPlansComponent', () => {
  let component: DeviceManagementForPlansComponent;
  let fixture: ComponentFixture<DeviceManagementForPlansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceManagementForPlansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceManagementForPlansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
