import { Component, Input, OnInit } from '@angular/core';


const PLAN = {
  tmcode: 'AAA',
  planPrices: [
    {
      price: '1000',
      term: '12',
      check: true
    },
    {
      price: '2000',
      term: '14',
      check: true
    },
    {
      price: '3000',
      term: '18',
      check: false
    }
  ]
};

const PRODUCTS = `{
  "result": {
    "data": {
      "localInventory": [
        {
          "category_name": "S20",
          "lastModify": "15/10/2020 16:27:40",
          "description": "Samsung S20",
          "brand_name": "Samsung",
          "sale_price": "0",
          "offer_price": "0",
          "categoryType": "PREPAGO",
          "group_id": "I02.7032.654A",
          "price": "7995",
          "supplier": "1",
          "name": "Samsung S20",
          "categoryTypeId": "1",
          "sku": "I02.7032.653",
          "category": "100",
          "supplier_name": "ODA",
          "brand": "2",
          "status": "0"
        },
        {
          "category_name": "sin_categoria",
          "lastModify": "12/10/2020 11:27:28",
          "description": "SIM TRIPLE CORTE LTE 256KB (D)",
          "brand_name": "Apple",
          "sale_price": "0",
          "offer_price": "0",
          "categoryType": "OTROS",
          "price": "0.01",
          "supplier": "1",
          "name": "SIM TRIPLE CORTE LTE 256KB (D)",
          "categoryTypeId": "5",
          "sku": "I09.7037.286",
          "category": "1",
          "supplier_name": "ODA",
          "brand": "3",
          "status": "0"
        },
        {
          "category_name": "S20",
          "lastModify": "16/10/2020 14:43:05",
          "description": "HUA Y7 2019 NG NEGRO (P)",
          "brand_name": "Apple",
          "skuSim": "I09.7037.327",
          "sale_price": "0",
          "offer_price": "0",
          "categoryType": "PREPAGO",
          "group_id": "I02.2598.305",
          "price": "999",
          "supplier": "1",
          "name": "HUA Y7 2019 NG NEGRO (P)",
          "categoryTypeId": "1",
          "sku": "I02.2598.305",
          "category": "100",
          "supplier_name": "ODA",
          "brand": "3",
          "status": "0"
        },
        {
          "category_name": "sin_categoria",
          "lastModify": "16/10/2020 14:41:21",
          "description": "HUAWEI Y6 2019 NEGRO (P)",
          "brand_name": "Apple",
          "sale_price": "0",
          "offer_price": "0",
          "categoryType": "OTROS",
          "price": "995",
          "supplier": "1",
          "name": "HUAWEI Y6 2019 NEGRO (P)",
          "categoryTypeId": "5",
          "sku": "I02.2598.306",
          "category": "1",
          "supplier_name": "ODA",
          "brand": "3",
          "status": "0"
        },
        {
          "category_name": "postpago",
          "lastModify": "15/10/2020 16:57:49",
          "description": "PLAN S/CONTRATO 20GB AP",
          "brand_name": "Apple",
          "skuSim": "I09.7037.286",
          "sale_price": "0",
          "offer_price": "0",
          "categoryType": "PLAN",
          "tmCode": "AAAA",
          "group_id": "Z04.0742.032A",
          "price": "299",
          "supplier": "1",
          "name": "PLAN S/CONTRATO 20GB AP",
          "categoryTypeId": "2",
          "sku": "Z04.0742.033",
          "category": "101",
          "supplier_name": "ODA",
          "brand": "3",
          "status": "0"
        },
        {
          "category_name": "15GB",
          "lastModify": "15/10/2020 16:57:46",
          "description": "Sin Compromisos 15 GB LTE",
          "brand_name": "Claro",
          "skuSim": "I09.7037.286",
          "sale_price": "0",
          "offer_price": "0",
          "categoryType": "PLAN",
          "tmCode": "BBBB",
          "group_id": "Z04.0742.032A",
          "price": "199",
          "supplier": "1",
          "name": "Sin Compromisos 15 GB LTE",
          "categoryTypeId": "2",
          "sku": "Z04.0742.032",
          "category": "104",
          "supplier_name": "ODA",
          "brand": "4",
          "status": "0"
        },
        {
          "category_name": "sin_categoria",
          "lastModify": "16/10/2020 14:20:28",
          "description": "SMG GXS20P128G985 NG NEGRO (P)",
          "brand_name": "Apple",
          "sale_price": "0",
          "offer_price": "0",
          "categoryType": "OTROS",
          "price": "8995",
          "supplier": "1",
          "name": "SMG GXS20P128G985 NG NEGRO (P)",
          "categoryTypeId": "5",
          "sku": "I02.7032.652",
          "category": "1",
          "supplier_name": "ODA",
          "brand": "3",
          "status": "0"
        }
      ]
    },
    "message": "Solicitud procesada correctamente",
    "status": "ok"
  },
  "code": 200
}`;

@Component({
  selector: 'device-management-for-plans',
  templateUrl: './device-management-for-plans.component.html',
  styleUrls: ['./device-management-for-plans.component.css']
})
export class DeviceManagementForPlansComponent implements OnInit {

  private _tmcode = '';
  @Input()
  sku: string;
  @Input()
  get tmcode() {
    return this._tmcode;
  };
  set tmcode(tmcode) {
    this._tmcode = tmcode;
    this.loadPlan();
  };

  products = JSON.parse(PRODUCTS).result.data.localInventory;

  public plan = PLAN;
  public queryValue= '';
  /* @Input()
  get featuresData() { return this._featuresData; }
  set featuresData(featuresData) {
    this._featuresData = featuresData;
    this.featuresData.features.paginator = this.paginator;
    this.featuresData.features.sort = this.sort;
  }

  brands: Brand[];
  flagEnabledModify: boolean = true;
  loading = true;
  queryValue: string = ""; */

  constructor() { }

  ngOnInit(): void {

  }

  loadPlan() {
    if (this._tmcode != '') {

    }
  }

  loadDevices() {

  }

  applyFilter(){
    
  }

}
