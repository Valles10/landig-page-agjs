import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Brand } from 'src/app/core/models/Brand';
import { Category } from 'src/app/core/models/Category';
import { Product } from 'src/app/core/models/Product';
import { FormControl, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { InventoryService } from 'src/app/core/services/Inventory.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CategoryService } from 'src/app/core/services/Category.service';
import { FeatureService } from 'src/app/core/services/feature.service';
import { BrandService } from 'src/app/core/services/brand.service';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { MatTableDataSource } from '@angular/material/table';
import { Feature } from 'src/app/core/models/Feature';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { GroupersDialogComponent } from '../../dialogs/groupers-dialog/groupers-dialog.component';
import { CategoryTemplateDetailDialogComponent } from '../../dialogs/category-template-detail-dialog/category-template-detail-dialog.component';
import { ProductVersion } from 'src/app/core/models/ProductVersion';
import { Tools } from 'src/app/core/helpers/tools';
import { VersionService } from 'src/app/core/services/version.service';
import { environment } from 'src/environments/environment';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { SimService } from 'src/app/core/services/sim.service';
import { CatalogDialogComponent } from '../../dialogs/catalog-dialog/catalog-dialog.component';
import { PlansDialogComponent } from '../../dialogs/plans-dialog/plans-dialog.component';
import { PermissionService } from 'src/app/core/services/permission.service';


function priceValidator(priceForm: AbstractControl): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    let price = Number(priceForm.value || 0);
    let price_public = Number(control.value || 0);
    if ((Number(price_public) < Number(price)) || price_public <= 0) {
      return { 'priceValidator': true };
    }
    return null;
  };
}

function grouperValidator(sku: string): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    /*  let price = Number(priceForm.value || 0);
     let price_public = Number(control.value || 0); */
    if (sku == control.value) {
      return { 'grouperValidator': true };
    }
    return null;
  };
}

@Component({
  selector: 'product-detail',
  templateUrl: './product-info-detail.component.html',
})
export class ProductInfoDetailComponent implements OnInit {


  @Input('sku') sku: string;
  @Output() propagar = new EventEmitter<any>();

  public selectedCategory: Category = {
    categoryType: "",
    name: "",
    categoryTypeId: "",
    id: 0
  };

  brands: Brand[];
  flagEnabledModify: boolean = false;
  queryValue: string = "";
  savedProduct: boolean = false;
  correlatives: Array<number> = new Array<number>();
  category: Category;
  public product: Product = new Product();
  public nonProduct: boolean = false;
  public categories: Category[];
  public loadSkuSims = false;

  public selectedVersion: ProductVersion;
  public loadVersions = false;
  public idVersionSelected;
  public versions: ProductVersion[] = [];
  private paramVersion;

  public idDisabledPrice = true;
  public skuSims = [];
  private priceTemp = {
    public_price: "0",
    price: "0",
    offer_price: "0"
  }

  nameFormControl = new FormControl('', [
    Validators.required
  ]);


  priceFormControl = new FormControl({
    value: '',
    disabled: true
  }, []);

  publicPriceFormControl = new FormControl({
    value: ''
  }, [
    Validators.required,
    Validators.pattern(/^(\d)+([.]+(\d{1,2}))?([%])?$/),
    priceValidator(this.priceFormControl)
  ]);

  offerPriceFormControl = new FormControl('', [
    Validators.pattern(/^(\d)+([.]+(\d{1,2}))?([%])?$/),
  ]);

  public arrayPermission = new Array();

  public slides = new Array<any>();
  public editableVersions = [];
  public tempGroupId = '';
  public formGroupId: FormControl;
  constructor(
    public dialog: MatDialog,
    private inventoryService: InventoryService,
    private route: ActivatedRoute,
    private categoryService: CategoryService,
    private brandService: BrandService,
    private versionService: VersionService,
    private router: Router,
    private alert: MyAlertService,
    private permissionService: PermissionService,
  ) {
    this.paramVersion = this.route.snapshot.queryParamMap.get('v') || environment.versions.init.versionId;
  }

  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/store/offer/inventory/:sku');
  }

  ngOnInit(): void {
    this.selectPermission();
    this.formGroupId = new FormControl('', [
      Validators.required,
      grouperValidator(this.sku)
    ]);
    this.editableVersions = environment.versions.validEdition;
    this.getVersions();

  }

  getVersions(initVersion?: ProductVersion): void {
    this.versions = [];
    this.loadVersions = true;
    this.versionService.byProduct(this.sku).subscribe(
      response => {
        if (response.code == 200 && response.result.status == 'ok') {
          let v = response.result.data.versions;
          for (let index = 0; index < v.length; index++) {
            let element = v[index];
            delete element.lastUpdate;
          }
          this.versions = v;
          if (this.paramVersion) {
            let find = this.versions.find(x => x.versionId == this.paramVersion);
            this.selectedVersion = find;
          }
          this.loadProduct();
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado');
        }
        this.loadVersions = false;
      },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        this.loadVersions = false;

      });
  }

  versionChange() {
    this.changingQueryParams();
    this.loadProduct();
  }

  changingQueryParams() {
    this.paramVersion = this.selectedVersion.versionId;
    const queryParams: Params = { v: this.paramVersion };
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: queryParams,
      });
  }
  loadProduct(): void {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.nonProduct = false;
    this.categoryService.getCategories().subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
        this.categories = response.result.data.categories;
        this.brandService.getBrans().subscribe(response => {
          if (response.code == 200 && response.result.status == "ok") {
            this.brands = response.result.data.brands;
            this.inventoryService.getProduct({ productSKU: this.sku, versionId: this.paramVersion }).subscribe(response => {
              if (response.code == 200 && response.result.status == "ok") {
                if (response.result.data.localInventory.length == 1) {
                  this.product = response.result.data.localInventory[0];
                  this.selectedCategory = {
                    categoryType: this.product.categoryType,
                    name: this.product.category_name,
                    categoryTypeId: this.product.categoryTypeId,
                    id: Number(this.product.category)
                  }
                  this.product.brand = Number(this.product.brand);
                  this.product.category = Number(this.product.category);
                  this.product.nameOld = this.product.name;
                  this.product.skuSim = this.product.skuSim || '';
                  this.product.group_id = this.product.group_id || "";
                  this.tempGroupId = this.product.group_id;

                  if (this.tempGroupId != this.sku) {
                    if (this.tempGroupId != '') {
                      this.formGroupId.disable();
                    }
                  }
                  this.product.tmCode = this.product.tmCode || '';
                  let price = Number((this.product.price || "").replace(',', '.'));
                  this.product.price = typeof price == "number" ? Number(price).toFixed(2).toString() : "0";
                  price = Number((this.product.sale_price || "").replace(',', '.'));
                  this.product.sale_price = typeof price == "number" ? Number(price).toFixed(2).toString() : "0";
                  if (this.product.sale_price) {
                    price = Number((this.product.sale_price || "").replace(',', '.'));
                    this.product.sale_price = typeof price == "number" ? Number(price).toFixed(2).toString() : "0";
                  }
                  this.flagEnabledModify = true;
                  this.product.features.forEach(el => {
                    el.editable = false;
                    el.status = 0;
                    el.oldValue = (el.value) ? el.value.trim() : "";
                    el.value = (el.value) ? el.value.trim() : "";
                  });
                  this.propagar.emit(
                    {
                      productInfo: {
                        category: new Category(this.product.category, this.product.category_name),
                        error: 0,
                        features: new MatTableDataSource<Feature>(this.product.features)
                      },
                      tmcode: this.product.tmCode || ''
                    }
                  );
                  this.nonProduct = true;
                  this.idDisabledPrice = !this.editableVersions.includes(this.selectedVersion?.status);
                  this.priceTemp = {
                    price: this.product.price,
                    public_price: this.product.sale_price,
                    offer_price: this.product.offer_price
                  };
                } else {
                  this.propagar.emit(
                    {
                      productInfo: {
                        error: 1,
                        category: new Category(),
                        features: new MatTableDataSource<Feature>()
                      }
                    }
                  );
                  this.alert.warn('Producto no encontrado');
                }
              } else {
                this.propagar.emit(

                  {
                    productInfo: {
                      error: 1, category: new Category(),
                      features: new MatTableDataSource<Feature>()
                    }
                  }
                );
                this.alert.warn(response?.result?.message || 'No se encontro el producto');
              }
              dialogRefLoadingPage.close();
            }, err => {
              if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
                this.alert.warn(err.error.result.message);
              } else {
                this.alert.error('Proceso no completado');
              }
              dialogRefLoadingPage.close();
              this.propagar.emit(
                {
                  productInfo: {
                    error: 1,
                    category: new Category(),
                    features: new MatTableDataSource<Feature>()
                  }
                }
              );
            });

          } else {
            this.alert.warn(response?.result?.message || 'No se encontro marcas');
          }
        }, err => {
          if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          }
          dialogRefLoadingPage.close();
          this.propagar.emit(
            {
              productInfo: {
                error: 1,
                category: new Category(),
                features: new MatTableDataSource<Feature>()
              }
            }
          );
        });
      } else {
        this.alert.warn(response?.result?.message || 'No se encontro categorias');
      }
    }, err => {
      if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
        this.alert.warn(err.error.result.message);
      } else {
        this.alert.error('Proceso no completado');
      }
      dialogRefLoadingPage.close();
      this.propagar.emit(
        {
          productInfo: {
            error: 1,
            category: new Category(),
            features: new MatTableDataSource<Feature>()
          }
        }
      );
    });
  }

  saveProduct() {
    this.category = this.categories.find(element => element.id == this.product.category);
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        description: `Si se realizara cambio de categoria se removera las características heredadas, desea continuar?`,
        title: "Confirmación de cambios"
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
          width: '70%',
          data: "",
          disableClose: true
        });
        if (!this.editableVersions.includes(this.selectedVersion?.status)) {
          this.product.price = this.priceTemp.price;
          this.product.sale_price = this.priceTemp.public_price;
          this.product.offer_price = this.priceTemp.offer_price;
        }

        this.product.version = this.selectedVersion.versionId;
        this.product.offer_price = this.product.offer_price ? this.product.offer_price : '0';
        this.product.description = this.product?.description ? this.product?.description : '';
        delete this.product.features;
        this.inventoryService.updateProduct({
          product: this.product
        }).subscribe(response => {
          if (response.code == 200 && response.result.status == "ok") {
            this.alert.success('Se ha actualizado el producto ' + this.product.sku);
            this.loadProduct();
          } else {
            this.alert.warn(response?.result?.message || 'Proceso no completado');
          }
          dialogRefLoadingPage.close();
        },
          err => {
            if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
              this.alert.warn(err.error.result.message);
            } else {
              this.alert.error('Proceso no completado');
            }
            dialogRefLoadingPage.close();
          });

      }
    });
  }

  validatePrice() {
    var patt = new RegExp(/^(\d)+([.]+(\d{1,2}))?$/);
    if (patt.test(this.product.sale_price))
      return true;
    else
      return false;
  }

  focusOutPublicPrice() {
    var patt = new RegExp(/^(\d)+([%])$/);
    if (patt.test(this.product.sale_price)) {
      this.product.sale_price = (parseFloat(this.product.price) + ((parseFloat(this.product.price) * parseFloat(this.product.sale_price)) / 100)).toFixed(2).toString();
    }
  }

  focusOutOfferPrice() {
    var patt = new RegExp(/^(\d)+([%])$/);
    if (patt.test(this.product.offer_price)) {
      this.product.offer_price = (parseFloat(this.product.sale_price) - ((parseFloat(this.product.sale_price) * parseFloat(this.product.offer_price)) / 100)).toFixed(2).toString();
    }
  }

  openGroupersDialog() {
    const dialogRef = this.dialog.open(GroupersDialogComponent, {
      width: '60%',
      data: {
        grouper: this.product.group_id,
        sku: this.product.sku
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result?.grouper) {
        this.product.group_id = result.grouper;
      }
    });
  }

  openCatalogDialog() {
    const dialogRef = this.dialog.open(CatalogDialogComponent, {
      width: '60%',
      data: {
        skuSim: this.product.skuSim
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result?.skuSim) {
        this.product.skuSim = result.skuSim;
      }
    });
  }

  showTemplateCategory(category: number) {

    const dialogRefLoadingPage = this.dialog.open(CategoryTemplateDetailDialogComponent, {
      width: '80%',
      data: {
        category: category
      }
    });
  }

  changeCategory($event) {
    this.product.category = this.selectedCategory.id;
    if (!['PLAN'].includes(this.selectedCategory.categoryType)) {
      this.product.skuSim = '';
    }
    if (!['ADDONS'].includes(this.selectedCategory.categoryType)) {
      this.product.tmCode = '';
    }
  }

  compareCategoriesWhithId(o1: Category, o2: Category): boolean {
    return o1.id === o2.id;
  }
  openTMCodeDialog() {
    const dialogRef = this.dialog.open(PlansDialogComponent, {
      width: '60%',
      data: {
        sku: this.sku,
        tmcode: this.product.tmCode
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result?.tmcode) {
        this.product.tmCode = result.tmcode;
      }
    });
  }

  /*
   - Validacion para planes 
    - Cuando se cambia de categoria el tmCode debe de eliminarse si se regresa a la misma categoria se colocara el tmcode que tenga como valor inicial 
    - Cuando sea plan aplicara lo anterior mensionado  
    - Cuando se actualice el tmcode se reseteara las terminales asignadas al tmcode anterior
   */

}
