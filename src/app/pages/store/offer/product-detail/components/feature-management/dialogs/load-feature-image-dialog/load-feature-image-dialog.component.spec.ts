import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadFeatureImageDialogComponent } from './load-feature-image-dialog.component';

describe('LoadFeatureImageDialogComponent', () => {
  let component: LoadFeatureImageDialogComponent;
  let fixture: ComponentFixture<LoadFeatureImageDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadFeatureImageDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadFeatureImageDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
