import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Category } from 'src/app/core/models/Category';
import { MatTableDataSource } from '@angular/material/table';
import { Feature } from 'src/app/core/models/Feature';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { Product } from 'src/app/core/models/Product';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Brand } from 'src/app/core/models/Brand';
import { FeatureService } from 'src/app/core/services/feature.service';
import { AvailableFeaturesDialogComponent } from '../../dialogs/available-features-dialog/available-features-dialog.component';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { LoadFeatureImageDialogComponent } from './dialogs/load-feature-image-dialog/load-feature-image-dialog.component';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'features-management',
  templateUrl: './feature-management.component.html',
})
export class FeatureManagementComponent implements OnInit {

  public arrayPermission = new Array();
  private _featuresData = new FeaturesData();
  @Input() sku: string;

  @Input()
  get featuresData() { return this._featuresData; }
  set featuresData(featuresData) {
    this._featuresData = featuresData;
    this.featuresData.features.paginator = this.paginator;
    this.featuresData.features.sort = this.sort;
  }

  brands: Brand[];
  flagEnabledModify: boolean = true;
  loading = true;
  queryValue: string = "";

  savedProduct: boolean = false;
  correlatives: Array<number> = new Array<number>();
  product: Product;
  category: Category = new Category();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private permissionService: PermissionService,
    private alert: MyAlertService,
    public dialog: MatDialog,
    private featureService: FeatureService

  ) { }

  ngOnInit(): void {
    this.selectPermission();

  }

  showValidFeatures() {
    const dialogRef = this.dialog.open(AvailableFeaturesDialogComponent, {
      width: '80%',
      data: {
        sku: this.sku,
        features: this.featuresData.features.data
      }
    });
    dialogRef.afterClosed().subscribe((result: SelectionModel<Feature>) => {
      if (typeof result != 'undefined') {
        if (result.selected.length > 0) {
          result.selected.forEach((element: Feature) => {
            let feature = new Feature();
            feature.id = element.id;
            feature.description = element.description;
            feature.editable = false;
            feature.oldValue = "";
            feature.status = 4;
            feature.value = "";
            feature.traduction = element.traduction;
            feature.text = element.text;
            feature.displayable = element.displayable;
            feature.filter = element.filter;
            feature.image = element.image;
            feature.general = element.general;
            feature.boolean_value = element.boolean_value;
            feature.placeholder = element.placeholder;
            //feature.futureStatus = true;
            this.featuresData.features.data.push(
              feature);
            this.featuresData.features.data.sort((a: Feature, b: Feature) => {
              if (a.status < b.status)
                return 1;
              if (a.status > b.status)
                return -1;
              // a must be equal to b
              return 0;
            });
            this.featuresData.features._updateChangeSubscription();
          })
        }
      }
    });
  }

  applyFilter() {
    this.featuresData.features.filter = this.queryValue.trim().toLowerCase();
  }


  saveFeatures() {

    let f = [];
    for (let index = 0; index < this.featuresData.features.data.length; index++) {
      const element = this.featuresData.features.data[index];
      if (element.status == 2 || element.status == 1) {
        f.push(element);
      }
    }
    console.log(f);
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.featureService.updateFeatures({
      sku: this.sku,
      characteristics: f
    }).subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
        let indexRemoval: number[] = [];
        this.featuresData.features.data.forEach(row => {
          if (row.status == 1) {
            indexRemoval.push(this.featuresData.features.data.indexOf(row));
          } else {
            row.editable = false;
            row.status = 0;
            row.oldValue = (row.value) ? row.value.trim() : "";
            row.value = (row.value) ? row.value.trim() : "";
          }
        });
        let indexSet = new Set(indexRemoval);
        this.featuresData.features.data = this.featuresData.features.data.filter((value, i) => !indexSet.has(i));
        this.featuresData.features._updateChangeSubscription();
        this.alert.success('Caracteristicas actualizadas con exito');

      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      dialogRefLoadingPage.close();
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialogRefLoadingPage.close();
      });

  }

  addEdit(row: Feature) {
    this.setEditable(row);
  }

  updateModelFeature(row: Feature) {
    console.log(row);
    this.updateModel(row);
  }

  deleteFeature(row: Feature) {
    this.deleteModel(row);
  }

  setEditable(row: Feature) {
    /*  // Valida que no se eliminara
     if (row.status != 1) {
       // Valida que sea editable
       
       if (row.editable)
         row.editable = false;
       else
         row.editable = true;
     } else if (row.status == 1) {
       row.editable = false;
     } else {
       row.editable = true;
     } */

    if (row.status != 1) {
      row.editable = !row.editable;
    } else {
      row.editable = false;
    }
  }

  updateModel(row: Feature) {
    //No se borrara
    if (row.status != 1) {
      // valida valores que no sean iguales
      if (row.oldValue.trim() == row.value.trim()) {
        // los valores no se cambiaran en actualizacion
        // No se insertara
        if (row.status != 3) {
          row.status = 0;
        }
      } else {
        // Los valores son diferentes
        // si los valores no se insertan
        if (row.status != 3) {
          row.status = 2;
        }
      }
    }
  }

  deleteModel(row: Feature) {
    // Si estaba en modo eliminacion
    if (row.status == 1) {
      /* if (row.futureStatus) {
        row.status = 3;
      } else { */
      row.status = 0;
      this.updateModel(row);
      //}
    } else {
      //Si es uno nuevo que se agregara
      if (row.status == 3) {
        this.featuresData.features.data.splice(this.featuresData.features.data.indexOf(row), 1);
        this.featuresData.features._updateChangeSubscription();
      }
      else
        /* if (row.futureStatus)
          row.status = 3;
        else */
        row.status = 1;
      row.editable = false;
    }
  }



  uploadImage(feature: Feature) {
    const dialogRefLoadImage = this.dialog.open(LoadFeatureImageDialogComponent, {
      width: '700px',
      data: {
        feature: feature,
        sku: this.sku
      },
      disableClose: false
    });

    dialogRefLoadImage.afterClosed().subscribe(
      res => {

      }
    );

  }
  
  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/store/offer/inventory/:sku');
  }


}

export class FeaturesData {
  category: Category;
  error: number;
  features: MatTableDataSource<Feature>
  constructor() {
    this.category = new Category();
    this.error = -1;
    this.features = new MatTableDataSource<Feature>();
  }

}
