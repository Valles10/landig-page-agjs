import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { Tools } from 'src/app/core/helpers/tools';
import { Feature } from 'src/app/core/models/Feature';
import { AuthSecureService } from 'src/app/core/services/auth-secure.service';
import { MediaService } from 'src/app/core/services/media.service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PropertiesService } from 'src/app/core/services/properties.service';
import { FeatureImage, ImagesProperties, ProductImage } from 'src/app/pages/tools/properties/components/product-images-props/product-images-props.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-load-feature-image-dialog',
  templateUrl: './load-feature-image-dialog.component.html',
  styleUrls: ['./load-feature-image-dialog.component.css']
})
export class LoadFeatureImageDialogComponent implements OnInit {

  public feature: Feature;
  public suc = true;
  public loading = false;
  public sku: string = '';
  public path = '';
  public updatingPath = false;
  private fileName = '';
  public readingFile = false;
  private tools: Tools;
  public imageProps: FeatureImage;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<LoadFeatureImageDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private alert: MyAlertService,
    private mediaService: MediaService,
    private propertiesService: PropertiesService,
    private secureService: AuthSecureService
  ) {
    this.feature = this.data.feature;
    this.sku = this.data.sku;
    this.tools = new Tools();
  }

  ngOnInit(): void {
    this.loadProperties();
    this.loadPath();
  }

  loadPath() {
    if (!this.updatingPath) {
      this.updatingPath = true;
      const time = new Date().getTime();
      const cdn = environment.cdn.replace('{country}', this.secureService.sessionCountryValue.value).toLowerCase();
      /* const countrySession = this.secureService.sessionCountryValue.value
      const cdn = environment.cdnProd.replace('{country}', countrySession?.toLowerCase() == 'cr' ? '' : '.' + countrySession).toLowerCase(); */
      /* console.log(cdn2); */

      if (this.feature.general == '0') {
        this.fileName = this.feature.text.replace('_img', '') + '_' + this.sku;
        this.path = cdn + 'espots/product_detail/' + this.fileName + '.jpg?time=' + time;
      } else {
        this.fileName = this.feature.text
        this.path = cdn + 'espots/socialmedia/' + this.fileName + '.jpg?time=' + time;
      }
      this.updatingPath = false;
    }
  }

  inputChange($input: HTMLInputElement) {
    this.loading = true;
    for (let index = 0; index < $input.files.length; index++) {
      const file = $input.files[index];
      this.readFileAsDataURL(file)
        .then(e => {
          if (!e) {
            //console.log("Archivo se cargara");
            let tempFile = new File([file], this.fileName + '.' + file.name.split('.').pop(), { type: file.type });
            //  console.log(tempFile);
            const dialogRefConfirm = this.dialog.open(ConfirmDialogComponent, {
              width: '400px',
              data: {
                title: 'Confirmación',
                description: 'Archivo es adecuado, proceder con la carga del archivo?'
              }
            });
            dialogRefConfirm.afterClosed().subscribe(res => {
              if (res) {
                this.uploadImage(tempFile);
              }
            });
          } else {
            this.loading = false;
            this.alert.warn('Archivo no soportado');
          }
          this.loading = false;
          $input.value = "";
        }).catch(
          err => {
            $input.value = "";
            this.loading = false;
            this.alert.warn('Error en la lectura del archivo');
          }
        );
    }
  }

  async readFileAsDataURL(file: File) {
    let error = false;
    if (file) {
      if (this.imageProps?.types.includes(file.type)) {
        let _size = file.size;
        if (_size < this.imageProps?.minimunSize && _size > this.imageProps?.maximunSize) {
          this.alert.warn('Tamaño del archivo no es soportado');
          error = true;
        } else {
          const ext = '.' + file.name.split('.').pop();
          if (!['.jpg'].includes(ext)) {
            this.alert.warn('Extensión no es valida');
            error = true;
          } else {
            await new Promise((resolve) => {
              let Img = new Image();
              Img.onload = (e: any) => {
                let height = e.path ? e.path[0].height : e.target ? e.target.height : "Error";
                let width = e.path ? e.path[0].width : e.target ? e.target.width : "Error"
                switch (this.imageProps?.dimensions?.comparision) {
                  case "any":
                    let re = this.tools.getMaxSizeFromArr(this.imageProps?.dimensions?.sizes);
                    if (re < height && re < width) {
                      this.alert.warn('Dimensiones de imagen no soportadas');
                      error = true;
                    }
                    break;
                  case 'match':
                    let idxWW = this.tools.existsSizeFromArr(this.imageProps?.dimensions?.sizes, { width: width, height: height })
                    if (!idxWW) {
                      this.alert.warn('Dimensiones de imagen no soportadas');
                      error = true;
                    }
                    break;
                  default:
                    this.alert.warn('Dimensiones de imagen no se puede comparar');
                    error = true;
                    break;
                }
                resolve("ok");
              }
              Img.src = URL.createObjectURL(file);
            });
          }
        }
      } else {
        this.alert.warn('Tipo de archivo no es soportado');
        error = true;
      }
    } else {
      this.alert.warn('No se puede leer el archivo');
      error = true;
    }
    return error;
  }

  loadImages() {
    if (!this.readingFile) {
      this.readingFile = true;
      const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
      fileUpload.click();
      this.readingFile = false;
    }
  }

  updateImage() {
    this.loadPath();
  }

  private uploadImage(file: File) {
    this.loading = true;
    const fd = new FormData();
    fd.append('sku', this.sku);
    fd.append('file', file);
    fd.append('type', this.feature.general);
    this.mediaService.uploadFeature(fd).subscribe(
      response => {
        if (response.code == 200 && response.result.status == 'ok') {
          this.alert.success('Imagen cargadas');
        } else {
          this.alert.warn(response?.result?.message || "Proceso no completado");
        }
        this.loading = false;
      },
      err => {
        this.loading = false;
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }

  getComparisionImg() {
    //return this.supportedFileConfig.productImages.dimensions.comparision;
    return this.imageProps?.dimensions?.comparision;

  }

  getDimensions() {
    /* if (this.supportedFileConfig.productImages.dimensions.comparision == "any") { */
    if (this.getComparisionImg() == "any") {
      let res = [];
      /*  for (let size of this.supportedFileConfig.productImages.dimensions.sizes) { */

      for (let size of this.imageProps?.dimensions.sizes) {
        if (!this.tools.existsItemFromArr(res, size?.height))
          res.push(size.height);
        if (!this.tools.existsItemFromArr(res, size?.width))
          res.push(size.width);
      }
      return res.toString();
    }
    /* else if (this.supportedFileConfig.productImages.dimensions.comparision == "match") { */
    else if (this.imageProps?.dimensions?.comparision == "match") {
      let res = "";
      /*  for (let size of this.supportedFileConfig.productImages.dimensions.sizes) { */
      for (let size of this.imageProps?.dimensions?.sizes) {
        res += size?.width + 'x' + size?.height + ', '
      }
      return res;
    } else {
      return "Sin configuración...";
    }
  }

  loadProperties() {
    this.loading = true;
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.propertiesService.getImage().subscribe(response => {
      if (response.code == 200 && response.result.status == 'ok') {
        let props: ImagesProperties;
        try {
          props = (JSON.parse(response.result?.data?.value || "{}"));
          /* console.log(<ProductImage>props.productImages);
          console.log(props?.productImages instanceof ProductImage);
          console.log(this.instanceOfFeatureImage(props?.productImages)); */
          if (this.instanceOfFeatureImage(props?.featureImages)) {
            this.imageProps = props.featureImages;
          } else {
            this.alert.error('Propiedades no configuradas correctamente');
          }
        } catch (e) {
          this.alert.error('Propiedades no configuradas correctamente');
        }
      } else {
        this.alert.warn(response?.result?.message || "Propiedades no cargadas");
      }
      this.loading = false;
      dialogRefLoadingPage.close();
    },
      err => {
        dialogRefLoadingPage.close();
        this.loading = false;
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }

  getMaxSize() {
    let tempSize = this.imageProps?.maximunSize * 1024;
    let fSExt = new Array('Bytes', 'KB', 'MB', 'GB'),
      i = 0; while (tempSize > 900) { tempSize /= 1024; i++; }
    return (Math.round(tempSize * 100) / 100) + ' ' + fSExt[i];
  }

  close() {
    if (this.loading) {
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        width: '400px',
        data: {
          description: `Imagen aun se esta cargado, desea continuar?`,
          title: "Confirmación de cambios"
        }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          dialogRef.close();
          this.dialogRef.close();
        }

      });
    } else {
      this.dialogRef.close();
    }
  }
  instanceOfFeatureImage(obj: any): obj is FeatureImage {
    return 'maximunSize' in obj
      && 'minimunSize' in obj
      && 'types' in obj
      && 'dimensions' in obj
      && typeof obj?.maximunSize === 'number'
      && typeof obj?.minimunSize === 'number'
      && Array.isArray(obj?.types)
      && obj?.dimensions?.comparision
      && obj?.dimensions?.sizes
      && Array.isArray(obj?.dimensions?.sizes)
  }


}
