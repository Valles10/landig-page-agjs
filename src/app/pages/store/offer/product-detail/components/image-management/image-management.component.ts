import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { SwiperOptions } from 'swiper';
import { environment } from 'src/environments/environment';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { MediaService } from 'src/app/core/services/media.service';
import { ImageUploadDialogComponent } from '../../dialogs/image-upload-dialog/image-upload-dialog.component';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { AuthSecureService } from 'src/app/core/services/auth-secure.service';
import { SwiperComponent } from 'ngx-useful-swiper';
import { PermissionService } from 'src/app/core/services/permission.service';


@Component({
  selector: 'image-management',
  templateUrl: './image-management.component.html',
  styleUrls: ['./image-management.component.css']
})
export class ImageManagementComponent implements OnInit {

  public arrayPermission = new Array();
  config: SwiperOptions = {
    pagination: { el: '.swiper-pagination', clickable: true },
    navigation: {
      nextEl: '.swiper-button-prev',
      prevEl: '.swiper-button-next'
    },
    spaceBetween: 30,
    slidesPerView: 3,
  };

  @Input('sku') sku: string;
  @Input('error') error: number;
  @ViewChild('usefulSwiper', { static: false }) usefulSwiper: SwiperComponent;

  public slides = new Array<any>();
  public status = {
    loading: false,
    error: false
  }
  private correlatives = new Array<any>();

  selectedImage: {
    idImage: string;
    mini: string;
    thumbImage: string;
    fullresImage: string;
    data: string;
  } = {
      idImage: "1",
      mini: "",
      thumbImage: "",
      fullresImage: "",
      data: ""
    };

  constructor(
    private permissionService: PermissionService,
    private alert: MyAlertService,
    public dialog: MatDialog,
    private mediaService: MediaService,
    private secureService: AuthSecureService
  ) { }

  ngOnInit(): void {
    this.selectPermission();
    this.readImages();
  }

  readImages(): void {
    this.status.loading = true;
    this.slides = [];
    this.correlatives = [];


    this.mediaService.getAllImagesByProduct({ sku: this.sku }).subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {

        let images = response.result.data.imagePaths;
        const cdn = environment.cdn.replace('{country}', this.secureService.sessionCountryValue.value).toLowerCase();
        /* const countrySession = this.secureService.sessionCountryValue.value
        const cdn = environment.cdnProd.replace('{country}', countrySession?.toLowerCase() == 'cr' ? '' : '.' + countrySession).toLowerCase(); */
        /* console.log(cdn); */
        if (images instanceof Array) {
          images.forEach((element, index) => {
            let spl = element.split("_");
            if (spl.length == 2) {
              let idxExt = spl[spl.length - 1];
              let secondpl = idxExt.split('.');
              if (secondpl.length == 2) {
                let ext = secondpl[secondpl.length - 1];
                let idx = Number(secondpl[secondpl.length - 2]);
                if (typeof idx == "number") {
                  let imgPartialName = spl[spl.length - 2];
                  let imgCompleteName = imgPartialName + '_' + idx + '_';
                  let time = new Date().getTime();
                  let imgObj = {
                    idImage: idx.toString(),
                    mini: cdn + this.sku + '/' + imgCompleteName + '90x90' + '.' + ext.toLocaleLowerCase() + "?time=" + time,
                    thumbImage: cdn + this.sku + '/' + imgCompleteName + '312x400' + '.' + ext.toLocaleLowerCase() + "?time=" + time,
                    fullresImage: cdn + this.sku + '/' + imgCompleteName + '1000x1000' + '.' + ext.toLocaleLowerCase() + "?time=" + time,
                    data: '10'
                  };
                  if (index == 0) {
                    this.selectedImage = imgObj;
                  }
                  this.correlatives.push(idx);
                  this.slides.push(imgObj);
                }
              }
            }
          });
          this.usefulSwiper.swiper.update();
        }
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      this.status.loading = false;
    },
      (err) => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        this.status.loading = false;
        this.status.error = true;
      }
    );
  }

  ngAfterViewInit() {
  }

  showSlider() {
  }


  getImage(img) {
    this.selectedImage = img;
  }

  showFormImages() {
    const dialogRef = this.dialog.open(ImageUploadDialogComponent, {
      width: '75%',
      data: {
        sku: this.sku,
        correlatives: this.correlatives,
        title: "Confirmación"
      },
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && result.uploadedFiles > 0) {
        this.readImages();
      }
    });
  }



  deleteImage() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        description: `Desea eliminar la imagen?`,
        title: "Confirmación de cambios"
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
          width: '70%',
          data: "",
          disableClose: true
        });
        dialogRefLoadingPage.close();
      } else {
      }
    });
  }

  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/store/offer/inventory/:sku');
  }

}
