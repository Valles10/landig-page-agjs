import { Component, Input, OnInit } from '@angular/core';
import { Plan } from 'src/app/core/models/Plan';

const PLANS: Array<Plan> = [
  {
    tmcode: 'AAA',
    planPrices: [
      {
        price: '1000',
        term: '12',
        check: true
      },
      {
        price: '2000',
        term: '14'
      },
      {
        price: '3000',
        term: '18'
      }
    ]
  },
  {
    tmcode: 'BBBB',
    planPrices: [
      {
        price: '1000',
        term: '12'
      },
      {
        price: '2000',
        term: '14'
      },
      {
        price: '3000',
        term: '18'
      }
    ]
  },
  {
    tmcode: 'CCC',
    planPrices: [
      {
        price: '1000',
        term: '12'
      },
      {
        price: '2000',
        term: '14'
      },
      {
        price: '3000',
        term: '18'
      }
    ]
  },
  {
    tmcode: 'DDDD',
    planPrices: [
      {
        price: '1000',
        term: '12'
      },
      {
        price: '2000',
        term: '14',
        check: true
        
      },
      {
        price: '3000',
        term: '18'
      }
    ]
  },
  {
    tmcode: 'XAAA',
    planPrices: [
      {
        price: '1000',
        term: '12'
      },
      {
        price: '2000',
        term: '14'
      },
      {
        price: '3000',
        term: '18'
      }
    ]
  },

];

@Component({
  selector: 'plans-management-for-devices',
  templateUrl: './plans-management-for-devices.component.html',
  styleUrls: ['./plans-management-for-devices.component.css']
})
export class PlansManagementForDevicesComponent implements OnInit {

 
  private _tmcode = '';
  @Input()
  sku: string;
  @Input()
  get tmcode() {
    return this._tmcode;
  };
  set tmcode(tmcode) {
    this._tmcode = tmcode;
    this.loadPlan();
  };

  public plans = PLANS;

  public plan = PLANS[0];
  public queryValue= '';
  /* @Input()
  get featuresData() { return this._featuresData; }
  set featuresData(featuresData) {
    this._featuresData = featuresData;
    this.featuresData.features.paginator = this.paginator;
    this.featuresData.features.sort = this.sort;
  }

  brands: Brand[];
  flagEnabledModify: boolean = true;
  loading = true;
  queryValue: string = ""; */

  constructor() { }

  ngOnInit(): void {

  }

  loadPlan() {
    if (this._tmcode != '') {

    }
  }

  loadDevices() {

  }

  applyFilter(){
    
  }

  getSelectedItems($event){
    return $event.selectedOptions.selected.length;
  } 

}
