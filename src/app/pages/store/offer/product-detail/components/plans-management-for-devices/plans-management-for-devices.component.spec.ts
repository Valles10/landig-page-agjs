import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlansManagementForDevicesComponent } from './plans-management-for-devices.component';

describe('PlansManagementForDevicesComponent', () => {
  let component: PlansManagementForDevicesComponent;
  let fixture: ComponentFixture<PlansManagementForDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlansManagementForDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlansManagementForDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
