import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Grouper } from 'src/app/core/models/Grouper';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GrouperService } from 'src/app/core/services/grouper.service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';

@Component({
  selector: 'app-groupers-dialog',
  templateUrl: './groupers-dialog.component.html'
})
export class GroupersDialogComponent implements OnInit {

  public result;
  viewOptions: ViewOptions = {
    displayedColumns: ['grouper', 'sku', 'brand', 'product'],
    pageSize: [25, 35, 50],

    loading: true
  };
  queryValue: string = "";
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  groupers: any = new MatTableDataSource<Grouper>();

  constructor(
    public dialog: MatDialog,
    private alert: MyAlertService,
    public dialogRef: MatDialogRef<GroupersDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private grouperService: GrouperService) { }


  ngOnInit(): void {

    this.grouperService.getGroupers().subscribe(response => {
      if (response.code == 200 && response.result.status == 'ok') {
        if (response.result.data.groupers instanceof Array) {
          this.groupers = new MatTableDataSource<Grouper>(response.result.data.groupers);
        } else {
          this.groupers = new MatTableDataSource<Grouper>(new Array<Grouper>());
        }
        this.groupers.paginator = this.paginator;
        this.groupers.sort = this.sort;

      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      this.viewOptions.loading = false;
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        this.viewOptions.loading = false;
      });
  }

  applyFilter() {
    this.groupers.filter = this.queryValue.trim().toLowerCase();
  }

  close() {
    this.dialogRef.close(this.result);
  }

  selectGrouper(element: Grouper) {
    if (this.data.sku != element.name) {
      this.dialogRef.close({
        grouper: element.name
      });
    } else {
      this.alert.warn('Agrupador no es valido');
    }
  }
}
