import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupersDialogComponent } from './groupers-dialog.component';

describe('GroupersDialogComponent', () => {
  let component: GroupersDialogComponent;
  let fixture: ComponentFixture<GroupersDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupersDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupersDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
