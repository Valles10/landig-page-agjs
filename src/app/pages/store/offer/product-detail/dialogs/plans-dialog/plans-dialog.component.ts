import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Plan } from 'src/app/core/models/Plan';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PlanService } from 'src/app/core/services/plan.service';


const PRICES: Array<Plan> = [
  {
    tmcode: 'AAA',
    planPrices: [
      {
        price: '1000',
        term: '12'
      },
      {
        price: '2000',
        term: '14'
      },
      {
        price: '3000',
        term: '18'
      }
    ]
  },
  {
    tmcode: 'BBBB',
    planPrices: [
      {
        price: '1000',
        term: '12'
      },
      {
        price: '2000',
        term: '14'
      },
      {
        price: '3000',
        term: '18'
      }
    ]
  },
  {
    tmcode: 'CCC',
    planPrices: [
      {
        price: '1000',
        term: '12'
      },
      {
        price: '2000',
        term: '14'
      },
      {
        price: '3000',
        term: '18'
      }
    ]
  },
  {
    tmcode: 'DDDD',
    planPrices: [
      {
        price: '1000',
        term: '12'
      },
      {
        price: '2000',
        term: '14'
      },
      {
        price: '3000',
        term: '18'
      }
    ]
  },
  {
    tmcode: 'XAAA',
    planPrices: [
      {
        price: '1000',
        term: '12'
      },
      {
        price: '2000',
        term: '14'
      },
      {
        price: '3000',
        term: '18'
      }
    ]
  },

];

@Component({
  selector: 'app-plans-dialog',
  templateUrl: './plans-dialog.component.html',
  styleUrls: ['./plans-dialog.component.css']
})
export class PlansDialogComponent implements OnInit {

  

  public result;
  viewOptions: ViewOptions = {
    displayedColumns: ['tmcode', 'plazos', 'precios'],
    pageSize: [25, 35, 50],

    loading: true
  };
  queryValue: string = "";
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  prices: any = new MatTableDataSource<Plan>();

  constructor(
    public dialog: MatDialog,
    private alert: MyAlertService,
    public dialogRef: MatDialogRef<PlansDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private tmcodeService: PlanService) { }


  ngOnInit(): void {

    this.prices = new MatTableDataSource<Plan>(PRICES);
    this.prices.paginator = this.paginator;
    this.prices.sort = this.sort;
    this.viewOptions.loading = false;
    let pr = '';
   console.log(PRICES[0].planPrices.slice().forEach(e=>{ pr += ( pr == '' ? '' : ',') + e.term}));
   console.log(pr);
   
    
  }

  load() {
    this.tmcodeService.getAll().subscribe(response => {
      if (response.code == 200 && response.result.status == 'ok') {
        if (response.result.data.producs instanceof Array) {
          this.prices = new MatTableDataSource<Plan>(response.result.data.plans);
        } else {
          this.prices = new MatTableDataSource<Plan>(new Array<Plan>());
        }
        this.prices.paginator = this.paginator;
        this.prices.sort = this.sort;
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      this.viewOptions.loading = false;
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        this.viewOptions.loading = false;
      });
  }

  applyFilter() {
    this.prices.filter = this.queryValue.trim().toLowerCase();
  }

  close() {
    this.dialogRef.close(this.result);
  }

  selectPlan(element: Plan) {
    this.dialogRef.close({
      tmcode: element.tmcode
    });
  }

  getprices(element: Plan){
    let pr = '';
   element.planPrices.slice().forEach(e=>{ pr += ( pr == '' ? '' : ',') + e.price});
   return pr;
  }

  getTerms(element: Plan){
    let pr = '';
   element.planPrices.slice().forEach(e=>{ pr += ( pr == '' ? '' : ',') + e.term});
   return pr;
  }


}
