import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { Tools } from 'src/app/core/helpers/tools';
import { environment } from 'src/environments/environment';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient, HttpRequest, HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { map, tap, catchError, last } from 'rxjs/operators';
import { of, Subscription } from 'rxjs';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { ImagesProperties, ProductImage } from 'src/app/pages/tools/properties/components/product-images-props/product-images-props.component';
import { PropertiesService } from 'src/app/core/services/properties.service';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';

@Component({
  selector: 'app-image-upload-dialog',
  templateUrl: './image-upload-dialog.component.html',
  styleUrls: ['./image-upload-dialog.component.css']
})
export class ImageUploadDialogComponent implements OnInit {


  loading = false;
  fileCounter = 0;
  tools: Tools;
  @Input() url = `${environment.apiBackend}/media/uploadImages`;;
  @Output() complete = new EventEmitter<string>();

  public filesData: Array<FileUploadModel> = new Array<FileUploadModel>();

  public maxCorrelative: number = 0;
  public nextCorrelative: number = 0;
  public sku = "";

  public imageProps: ProductImage;

  constructor(
    public dialog: MatDialog,
    private alert: MyAlertService,
    private _http: HttpClient,
    public dialogRef: MatDialogRef<ImageUploadDialogComponent>,
    private propertiesService: PropertiesService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data.correlatives?.length > 0) {
      this.nextCorrelative = Math.max(...data.correlatives) + 1;
    } else {
      this.nextCorrelative = 1;
    }
    /* console.log(this.nextCorrelative); */
    this.sku = data.sku;
    this.tools = new Tools();
  }

  ngOnInit(): void {
    this.loadProperties();
  }

  getMaxSize() {
    let tempSize = this.imageProps.maximunSize * 1024;
    let fSExt = new Array('Bytes', 'KB', 'MB', 'GB'),
      i = 0; while (tempSize > 900) { tempSize /= 1024; i++; }
    return (Math.round(tempSize * 100) / 100) + ' ' + fSExt[i];
  }

  loadProperties() {
    this.loading = true;
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.propertiesService.getImage().subscribe(response => {
      if (response.code == 200 && response.result.status == 'ok') {
        let props: ImagesProperties;
        try {
          props = (JSON.parse(response.result?.data?.value || "{}"));
          if (this.instanceOfProductImage(props?.productImages)) {
            this.imageProps = props.productImages;
            this.maxCorrelative = this.imageProps.byCarousel;
          } else {
            this.alert.error('Propiedades no configuradas correctamente');
          }
        } catch (e) {
          this.alert.error('Propiedades no configuradas correctamente');
        }
      } else {
        this.alert.warn(response?.result?.message || "Propiedades no cargadas");
      }
      this.loading = false;
      dialogRefLoadingPage.close();
    },
      err => {
        dialogRefLoadingPage.close();
        this.loading = false;
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }


  dialogClose(): void {
    if (this.validateMaxUpload(true) > 0) {
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        width: '400px',
        data: {
          description: `Hay imagenes pendientes de subir, deseas continuar?`,
          title: "Confirmación de cambios"
        }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result)
          this.dialogRef.close({
            status: this.loading,
            uploadedFiles: this.fileCounter
          });

      });
    } else {
      this.dialogRef.close({
        status: this.loading,
        uploadedFiles: this.fileCounter
      });
    }
  }

  getComparisionImg() {
    //return this.supportedFileConfig.productImages.dimensions.comparision;
    return this.imageProps.dimensions.comparision;

  }

  getDimensions() {
    /* if (this.supportedFileConfig.productImages.dimensions.comparision == "any") { */
    if (this.getComparisionImg() == "any") {
      let res = [];
      /*  for (let size of this.supportedFileConfig.productImages.dimensions.sizes) { */

      for (let size of this.imageProps.dimensions.sizes) {
        if (!this.tools.existsItemFromArr(res, size?.height))
          res.push(size.height);
        if (!this.tools.existsItemFromArr(res, size?.width))
          res.push(size.width);
      }
      return res.toString();
    }
    /* else if (this.supportedFileConfig.productImages.dimensions.comparision == "match") { */
    else if (this.imageProps.dimensions?.comparision == "match") {
      let res = "";
      /*  for (let size of this.supportedFileConfig.productImages.dimensions.sizes) { */
      for (let size of this.imageProps.dimensions?.sizes) {
        res += size?.width + 'x' + size?.height + ', '
      }
      return res;
    } else {
      return "Sin configuración...";
    }
  }


  inputChange($input: HTMLInputElement) {
    this.loading = true;
    for (let index = 0; index < $input.files.length; index++) {
      const file = $input.files[index];
      this.readFileAsDataURL(file)
        .then(e => {
          this.filesData.push(e);
        }).catch(
          err => {
            this.alert.warn('Error en la lectura de los archivos');
          }
        );
    }
    $input.value = "";
    this.loading = false;
  }

  loadImages() {
    if (this.validateMaxUpload() <= this.maxCorrelative) {
      const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
      fileUpload.click();
    } else {
      this.alert.warn('Se alcanzo maximo de archivos seleccionables');

    }
  }



  async readFileAsDataURL(file) {
    let imageData: FileUploadModel = new FileUploadModel();
    imageData.metadata.type = file.type;
    /* if (!this.supportedFileConfig.productImages.types.includes(imageData.metadata.type)) { */
    if (!this.imageProps.types.includes(imageData.metadata.type)) {
      imageData.errors.status = true;
      imageData.errors.messages.push("Archivo no soportado")
    } else {
      let reader = new FileReader();
      reader.onload = function (event) {
        imageData.metadata.imageResource = event.target.result;
      }.bind(this);
      reader.readAsDataURL(file);

    }
    let _size = file.size;
    /*  if (_size > this.supportedFileConfig.productImages.maximunSize) { */
    /* console.log(_size);
    console.log(this.imageProps.maximunSize); */
    if (_size > (this.imageProps.maximunSize * 1024)) {
      imageData.errors.status = true;
      imageData.errors.messages.push("Tamaño máximo archivo superado")
    } else {
      await new Promise((resolve) => {
        let Img = new Image();
        Img.onload = (e: any) => {
          imageData.metadata.height = e.path ? e.path[0].height : e.target ? e.target.height : "Error";
          imageData.metadata.width = e.path ? e.path[0].width : e.target ? e.target.width : "Error";
          /* switch (this.supportedFileConfig.productImages.dimensions.comparision) { */
          switch (this.getComparisionImg()) {
            case "any":
              /* let re = this.tools.getMaxSizeFromArr(this.supportedFileConfig.productImages.dimensions.sizes); */
              let re = this.tools.getMaxSizeFromArr(this.imageProps.dimensions.sizes);
              if (re < imageData.metadata.height && re < imageData.metadata.width) {
                imageData.errors.status = true;
                imageData.errors.messages.push("Dimensiones de imagen no soportado")
              }
              break;
            case 'match':
              /*  let idxWW = this.tools.existsSizeFromArr(this.supportedFileConfig.productImages.dimensions.sizes, { width: imageData.metadata.width, height: imageData.metadata.height }) */
              let idxWW = this.tools.existsSizeFromArr(this.imageProps.dimensions.sizes, { width: imageData.metadata.width, height: imageData.metadata.height })
              if (!idxWW) {
                imageData.errors.status = true;
                imageData.errors.messages.push("Dimensiones de imagen no soportado")
              }
              break;
            default:
              break;
          }
          resolve("ok");
        }
        Img.src = URL.createObjectURL(file);
      });
    }

    /* if(['jpg'].includes()){

    } */
    imageData.metadata.oldName = file.name;
    imageData.metadata.extension = '.' + file.name.split('.').pop();
    let fSExt = new Array('Bytes', 'KB', 'MB', 'GB'),
      i = 0; while (_size > 900) { _size /= 1024; i++; }
    imageData.metadata.size = (Math.round(_size * 100) / 100) + ' ' + fSExt[i];
    if (!['.jpg'].includes(imageData.metadata.extension)) {
      imageData.errors.status = true;
      imageData.errors.messages.push("Extensión no es valida")
    }
    if (imageData.errors.status) {
      imageData.metadata.name = imageData.metadata.oldName;
    } else {
      if (this.maxCorrelative >= this.nextCorrelative) {
        imageData.metadata.correlative = this.nextCorrelative.toString();
        this.nextCorrelative++;
      }
      imageData.metadata.name = this.data.sku + "_";
      imageData.image = new File([file], imageData.metadata.name + imageData.metadata.correlative + imageData.metadata.extension, { type: file.type });
    }

    return imageData;
  }

  validateDuplicateCorrelative(arrToCompare: Array<FileUploadModel>, val) {
    let res = false;
    for (let i = 0; i < arrToCompare.length; i++) {
      if (arrToCompare[i].metadata.correlative == val) {
        res = true;
        break;
      }
    }
    return res;
  }

  changeCorrelative(file: FileUploadModel, event) {
    let index = Number(event.target.value);
    let correlativeEqual = this.validateDuplicateCorrelative(this.filesData, event.target.value);
    if (index > 0 && !correlativeEqual && index <= this.maxCorrelative) {
      let newFile = new File([file.image], file.metadata.name + event.target.value + file.metadata.extension, { type: file.image.type });
      file.image = newFile;
      file.metadata.correlative = event.target.value;
      if (file.errors.editing) {
        file.errors.editing = false;
        file.errors.messages.splice(file.errors.messages.indexOf("Indice no valido"), 1);
        file.errors.messages.splice(file.errors.messages.indexOf("Correlativo ya asignado"), 1);
        file.errors.messages.splice(file.errors.messages.indexOf("Correlativo no soportado"), 1);
      }
      file.errors.status = file.errors.initialState;
    } else {
      file.metadata.correlative = event.target.value;
      if (index <= 0 || index > this.maxCorrelative) {
        file.errors.messages.splice(file.errors.messages.indexOf("Correlativo ya asignado"), 1);
        file.errors.messages.push("Correlativo no soportado");
      }
      if (correlativeEqual && index != 0) {
        file.errors.messages.splice(file.errors.messages.indexOf("Indice no valido"), 1);
        file.errors.messages.push("Correlativo ya asignado");
      }

      if (!file.errors.editing) {
        file.errors.initialState = file.errors.status;
        file.errors.status = true;
        file.errors.editing = true;
      }
    }
  }

  showErrors(element: FileUploadModel) {
    this.filesData.forEach(e => {
      if (e != element) {
        e.errors.display = false;
      }
    });
    element.errors.display = !element.errors.display;
  }

  deleteFileRow(row: any) {
    this.filesData = this.filesData.filter(function (item) {
      return item !== row
    })
  }

  retryImageLoad(file: FileUploadModel) {
    this.uploadImage(file);
    file.canRetry = false;
  }

  private uploadImage(file: FileUploadModel) {
    this.loading = true;
    const fd = new FormData();
    fd.append('sku', this.sku);
    fd.append('file', file.image);
    fd.append('type', '2');
    const req = new HttpRequest('POST', this.url, fd, {
      reportProgress: true
    });
    file.inProgress = true;
    file.canCancel = true;
    file.sub = this._http.request(req).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            break;
          case HttpEventType.Response:
            return event;
        }
      }),
      tap(message => { }),
      last(),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        file.canRetry = true;
        file.canCancel = false;
        this.alert.warn("Error cargando la imagen: " + file.image.name);
        this.loading = false;
        return of(`${file.image.name} upload failed.`);
      })
    ).subscribe(
      (event: any) => {
        if (typeof (event) === 'object') {
          file.completeUpload = true;
          file.inProgress = false;
          file.canRetry = false;
          file.canCancel = false;
          this.fileCounter++;
          this.loading = false;
          this.data.correlatives.push(Number(file.metadata.correlative));
          this.alert.success("Se ha cargado la imagen: " + file.image.name);
          this.uploadImages();
          this.complete.emit(event.body);
        }
      }
    );
  }

  public saveImages() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        description: `Imagenes con correlativo existente seran reemplazadas, desea continuar?`,
        title: "Confirmación de cambios"
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.uploadImages();
      }
    });
  }

  public uploadImages() {
    for (let file of this.filesData) {
      if (!file.errors.status && !file.completeUpload && Number(file.metadata.correlative) > 0) {
        this.uploadImage(file);
        break;
      }
    }
  }

  cancelImageUpload(file: FileUploadModel) {
    file.sub.unsubscribe();
    file.inProgress = false;
    file.canRetry = false;
    file.canCancel = false;
    this.loading = false;
  }
  private(file: FileUploadModel) {
    const index = this.filesData.indexOf(file);
    if (index > -1) {
      this.filesData.splice(index, 1);
    }
  }

  validateMaxUpload(flag?: boolean) {
    let count = 0;
    this.filesData.forEach(e => {
      if (flag) {
        if (!e.errors.status && !e.completeUpload)
          count++;
      } else {
        if (!e.errors.status && !e.completeUpload && Number(e.metadata.correlative) > 0)
          count++;
      }
    });
    return count;
  }


  resetImages() {
    if (this.validateMaxUpload(true) > 0) {
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        width: '400px',
        data: {
          description: `Hay imagenes pendientes de subir, deseas continuar?`,
          title: "Confirmación de cambios"
        }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result)
          this.filesData = new Array<FileUploadModel>();
      });
    } else {
      this.filesData = new Array<FileUploadModel>();
    }
  }

  instanceOfProductImage(obj: any): obj is ProductImage {
    return 'byCarousel' in obj
      && 'maximunSize' in obj
      && 'minimunSize' in obj
      && 'types' in obj
      && 'dimensions' in obj
      && typeof obj?.byCarousel === 'number'
      && typeof obj?.maximunSize === 'number'
      && typeof obj?.minimunSize === 'number'
      && Array.isArray(obj?.types)
      && obj?.dimensions?.comparision
      && obj?.dimensions?.sizes
      && Array.isArray(obj?.dimensions?.sizes)
  }

}




export class FileUploadModel {
  image: File;
  metadata: Metadata;
  state: string;
  inProgress: boolean;
  progress: number;
  canRetry: boolean;
  canCancel: boolean;
  sub?: Subscription;
  completeUpload: boolean;
  errors: {
    status: boolean;
    messages: Array<string>;
    display: boolean;
    editing: boolean;
    initialState: boolean;
  }

  constructor() {
    this.image = null;
    this.metadata = new Metadata();
    this.completeUpload = false;
    this.state = 'in';
    this.inProgress = false;
    this.progress = 0;
    this.canRetry = false;
    this.canCancel = false;
    this.errors = {
      status: false,
      messages: Array<string>(),
      display: false,
      editing: false,
      initialState: false
    }
  }

  getName() {
    return this.metadata.name + this.metadata.correlative + this.metadata.extension;
  }
}

export class Metadata {
  type: string;
  correlative: string;
  strName?: string;
  extension: string;
  name: string;
  size: string;
  imageResource: string;
  oldName: string;
  width: number;
  height: number;

  constructor() {
    this.strName = "";
    this.extension = "";
    this.name = "";
    this.size = "";
    this.imageResource = "";
    this.oldName = "";
    this.width = 0;
    this.height = 0;
    this.correlative = "0";
  }
}
