import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { FeatureService } from 'src/app/core/services/feature.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { Feature } from 'src/app/core/models/Feature';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { CategoryService } from 'src/app/core/services/Category.service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';

@Component({
  selector: 'app-template-features-available-dialog',
  templateUrl: './available-features-dialog.component.html',
})
export class AvailableFeaturesDialogComponent implements OnInit {

  viewOptions: ViewOptions = {
    displayedColumns: ['select', 'traduction', 'text', 'description'],
    pageSize: [25, 35, 50],
    loading: true
  };

  queryValue: string = "";
  constructor(
    private dialog: MatDialog,
    private alert: MyAlertService,
    private dialogRef: MatDialogRef<AvailableFeaturesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private featureService: FeatureService,
    private categoryService: CategoryService) { }
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  selection = new SelectionModel<Feature>(true, []);
  features: any = new MatTableDataSource<Feature>();

  ngOnInit(): void {
    if (this.data.sku) {
      this.featureService.getAvailableFeatures({ sku: this.data.sku }).subscribe(response => {
        if (response.code == 200 && response.result.status == 'ok') {
          response.result.data.characteristics.forEach((element, index) => {
            element.position = index + 1;
          });
          this.features = new MatTableDataSource<Feature>(response.result.data.characteristics);
          this.features.paginator = this.paginator;
          this.features.sort = this.sort;
          if (this.data) {
            this.data.features.forEach(row => {
              if (row.status == 3) {
                this.features.data.splice(this.features.data.indexOf(this.features.data.find(x => x.id == row.id)), 1);
                this.features._updateChangeSubscription();
              }
            });
          }
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado');
        }
        this.viewOptions.loading = false;

      },
        err => {
          if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          }
          this.viewOptions.loading = false;
        });
    }
    else if (this.data.idCategory) {
      this.featureService.getFeatures().subscribe(response => {
        if (response.code == 200 && response.result.status == 'ok') {
          response.result.data.characteristics.forEach((element, index) => {
            element.position = index + 1;
          });
          this.features = new MatTableDataSource<Feature>(response.result.data.characteristics);
          this.features.paginator = this.paginator;
          this.features.sort = this.sort;
          if (this.data) {
            this.data.features.forEach(row => {
              this.features.data.splice(this.features.data.indexOf(this.features.data.find(x => x.id == row.id)), 1);
              this.features._updateChangeSubscription();
            });
          }
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado');
        }
        this.viewOptions.loading = false;
      },
        err => {
          if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          }
          this.viewOptions.loading = false;
        });
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.features.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.features.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: Feature): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  applyFilter() {
    this.features.filter = this.queryValue.trim().toLowerCase();
  }

  saveFeatures(): void {
    if (typeof this.selection != 'undefined') {
      if (this.selection.selected.length > 0) {
        const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
          width: '80%',
          data: "",
          disableClose: true
        });
        if (this.data.sku) {
          this.selection.selected.forEach((element: Feature) => {
            element.status = 3;
            element.value = "";
          });
          this.featureService.updateFeatures({
            sku: this.data.sku,
            characteristics: this.selection.selected
          }).subscribe(response => {
            if (response.code == 200 && response.result.status == "ok") {
               this.alert.success('Caracteristicas agregadas con exito');
                this.dialogRef.close(this.selection);
            } else {
              this.alert.warn(response?.result?.message || 'Proceso no completado');
            }
            dialogRefLoadingPage.close();
          },
            err => {
              if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
                this.alert.warn(err.error.result.message);
              } else {
                this.alert.error('Proceso no completado');
              }
              dialogRefLoadingPage.close();
            });
        } else if (this.data.idCategory) {
          this.selection.selected.forEach((element: Feature) => {
            element.status = 1;
            element.id = Number(element.id);
            delete element.name;
            delete element.lock;
            delete element.position;
          });
          this.categoryService.associateFeatures({
            "idCategory": this.data.idCategory,
            "features": this.selection.selected
          }).subscribe(response => {
            if (response.code == 200 && response.result.status == "ok") {
              this.alert.success('Caracteristicas agregadas con exito');
                this.dialogRef.close(this.selection);
                dialogRefLoadingPage.close();
            } else {
              this.alert.warn(response?.result?.message || 'Proceso no completado');
              dialogRefLoadingPage.close();
            }
          },
            err => {
              if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
                this.alert.warn(err.error.result.message);
              } else {
                this.alert.error('Proceso no completado');
              }
              dialogRefLoadingPage.close();
            });
        } else {
          this.alert.info('No hay nada para procesar');
        }
      } else {
        this.alert.info('No hay nada para procesar');
      }
    } else {
      this.alert.info('No hay nada para procesar');

    }
  }

}
