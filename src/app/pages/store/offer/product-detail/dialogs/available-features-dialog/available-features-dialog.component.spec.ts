import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvailableFeaturesDialogComponent } from './available-features-dialog.component';

describe('AvailableFeaturesDialogComponent', () => {
  let component: AvailableFeaturesDialogComponent;
  let fixture: ComponentFixture<AvailableFeaturesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvailableFeaturesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailableFeaturesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
