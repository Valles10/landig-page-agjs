import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Product } from 'src/app/core/models/Product';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { InventoryService } from 'src/app/core/services/Inventory.service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';

@Component({
  selector: 'app-catalog-dialog',
  templateUrl: './catalog-dialog.component.html'
})
export class CatalogDialogComponent implements OnInit {

  public result;
  viewOptions: ViewOptions = {
    displayedColumns: ['sku', 'nombre', 'tipo'],
    pageSize: [25, 35, 50],

    loading: true
  };
  queryValue: string = "";
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  products: any = new MatTableDataSource<Product>();

  constructor(
    public dialog: MatDialog,
    private alert: MyAlertService,
    public dialogRef: MatDialogRef<CatalogDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private inventoryService: InventoryService) { }


  ngOnInit(): void {

    this.inventoryService.getCatalog().subscribe(response => {
      if (response.code == 200 && response.result.status == 'ok') {
        if (response.result.data.producs instanceof Array) {
          this.products = new MatTableDataSource<Product>(response.result.data.producs);
        } else {
          this.products = new MatTableDataSource<Product>(new Array<Product>());
        }
        this.products.paginator = this.paginator;
        this.products.sort = this.sort;
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      this.viewOptions.loading = false;
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        this.viewOptions.loading = false;
      });
  }

  applyFilter() {
    this.products.filter = this.queryValue.trim().toLowerCase();
  }

  close() {
    this.dialogRef.close(this.result);
  }

  selectSKU(element: Product) {
    this.dialogRef.close({
      skuSim: element.sku
    });
  }
}
