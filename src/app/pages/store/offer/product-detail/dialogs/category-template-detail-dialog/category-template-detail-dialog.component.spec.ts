import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryTemplateDetailDialogComponent } from './category-template-detail-dialog.component';

describe('CategoryTemplateDetailDialogComponent', () => {
  let component: CategoryTemplateDetailDialogComponent;
  let fixture: ComponentFixture<CategoryTemplateDetailDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryTemplateDetailDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryTemplateDetailDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
