import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ProgressSpinnerMode } from '@angular/material/progress-spinner';
import { Feature } from 'src/app/core/models/Feature';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CategoryService } from 'src/app/core/services/Category.service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-category-template-detail-dialog',
  templateUrl: './category-template-detail-dialog.component.html',
})
export class CategoryTemplateDetailDialogComponent implements OnInit {

  viewOptions: ViewOptions = {
    displayedColumns: ['traduction', 'text' ,'description'],
    pageSize: [25, 35, 50],
    loading: true
  };
  public queryValue = '';

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  features: Array<Feature>;
  category_name: string;
  mode: ProgressSpinnerMode = 'indeterminate';
  public featuresData = new MatTableDataSource<Feature>();
  constructor(
    public dialog: MatDialog,
    private alert: MyAlertService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private categoryService: CategoryService
  ) { }

  ngOnInit(): void {
    this.categoryService.getCategoryTemplate({ id_category: this.data.category }).subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
        this.features = response.result.data.characteristics;
        this.category_name = response.result.data.category_name;
        this.featuresData = new MatTableDataSource<Feature>(this.features);
        this.featuresData.paginator = this.paginator;
        this.featuresData.sort = this.sort;
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      this.viewOptions.loading = false;
    }, err => {
      this.viewOptions.loading = false;
      if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
        this.alert.warn(err.error.result.message);
      } else {
        this.alert.error('Proceso no completado');
      }
    });
  }

  applyFilter() {
    this.featuresData.filter = this.queryValue.trim().toLowerCase();
  }

}
