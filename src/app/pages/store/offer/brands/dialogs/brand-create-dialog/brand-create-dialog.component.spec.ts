import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandCreateDialogComponent } from './brand-create-dialog.component';

describe('BrandCreateDialogComponent', () => {
  let component: BrandCreateDialogComponent;
  let fixture: ComponentFixture<BrandCreateDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandCreateDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandCreateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
