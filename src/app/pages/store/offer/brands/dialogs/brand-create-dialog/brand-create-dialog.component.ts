import { Component, OnInit, Inject } from '@angular/core';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BrandService } from 'src/app/core/services/brand.service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';

@Component({
  selector: 'app-brand-create-dialog',
  templateUrl: './brand-create-dialog.component.html',
  
})
export class BrandCreateDialogComponent implements OnInit {

  brandForm: FormGroup;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<BrandCreateDialogComponent>,
    private brandService: BrandService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private alert: MyAlertService
  ) { }

  ngOnInit(): void {
    if (this.data.brand != null) {
      this.brandForm = new FormGroup({
        brand_id: new FormControl(this.data.brand?.id.toString() || "0"),
        name: new FormControl(this.data.brand.name || '',
          [
            Validators.required,
            Validators.maxLength(254)
          ]),
        status: new FormControl((this.data.brand.status || (this.data.brand.id ? 2 : 3))),
        description: new FormControl(this.data.brand.description || '',
          [
            Validators.maxLength(254)
          ])
      });
    } else {
      this.brandForm = new FormGroup({
        brand_id: new FormControl('0'),
        name: new FormControl('',
          [
            Validators.required,
            Validators.maxLength(254)
          ]),
        status: new FormControl(3),
        description: new FormControl('',
          [
            Validators.maxLength(254)
          ])
      });
    }
  }

  get status() { return this.brandForm.get('status') }
  get brand_id() { return this.brandForm.get('brand_id') }
  get name() { return this.brandForm.get('name') }
  get description() { return this.brandForm.get('description') }

  save(): void {
    const dialoadRef = this.dialog.open(LoadingPageDialogComponent, {
      width: '400px',
      data: {
        description: `Se`,
        title: "Confirmación de cambios",
      },
      disableClose: true
    });
    this.brandService.managementBrand(this.brandForm.value).subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") { if (<number>this.status.value == 2) {
            this.alert.success('Se ha actualizado la marca '+ this.name.value);
          } else {
            this.alert.success('Se ha creado la marca '+ this.name.value);
          }
          this.dialogRef.close(true);
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      dialoadRef.close();
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        } 
        dialoadRef.close();
      });
  }
}
