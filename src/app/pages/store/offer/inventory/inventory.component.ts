import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Product } from 'src/app/core/models/Product';
import { InventoryService } from 'src/app/core/services/Inventory.service';
import { ProductVersion } from 'src/app/core/models/ProductVersion';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { CreateVersionDialogComponent } from './dialogs/create-version-dialog/create-version-dialog.component';
import { VersionService } from 'src/app/core/services/version.service';
import { environment } from 'src/environments/environment';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {

  public viewOptions: ViewOptions = {
    displayedColumns: ['sku', 'name', 'category','brand', 'supplier', 'status', 'lastModify', 'accion'],
    pageSize: [25, 35, 50],
    loading: true
  };

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  public arrayPermission = new Array();
  public queryValue: string = "";
  public versions: ProductVersion[] = [];
  public selectedVersion: ProductVersion;
  public loadVersions = false;

  productsDataSource: MatTableDataSource<Product> = new MatTableDataSource<Product>();
  products: Array<Product> = [];
  public initVersion = environment.versions.init;

  public editableVersions = [];

  constructor(
    private permissionService: PermissionService,
    public dialog: MatDialog,
    private inventoryService: InventoryService,
    private cdr: ChangeDetectorRef,
    private versionService: VersionService,
    private alert: MyAlertService
  ) {

  }

  ngOnInit(): void {
    this.selectPermission();
    this.editableVersions = environment.versions.validEdition;
    this.getVersions(this.initVersion);
    this.loadInventory(this.initVersion);
  }

  ngAfterViewInit() {
    this.productsDataSource.paginator = this.paginator
  }

  setProductInVersion(event, row: Product) {
    event.preventDefault();
    if (this.editableVersions.includes(this.selectedVersion.status) && this.arrayPermission.includes('edit')) {
      const msg = row.statusVersion == 1 ? 'eliminar' : 'añadir'
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        width: '400px',
        data: {
          description: `Esta seguro que desea ${msg} el producto: ${row.sku} a la version ${this.selectedVersion.name}?`,
          title: "Confirmación"
        },
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
            width: '70%',
            data: "",
            disableClose: true
          });
          let statusVersion = (row.statusVersion == 1) ? 0 : 1;
          if (statusVersion == 0) {
            //DELETE
            this.versionService.deleteProduct(this.selectedVersion.versionId, row.sku).subscribe(response => {
              if (response.code == 200 && response.result.status == 'ok') {
                this.alert.success('Se ha eliminado el producto '+ row.sku + ' de la version ' + this.selectedVersion.name);
                row.statusVersion = statusVersion;
                row.statusChangeVersion = (row.oldStatusVersion == row.statusVersion) ? 0
                  : row.oldStatusVersion == 1 && row.statusVersion == 0 ? 2 : 1;
                if (this.queryValue.trim() == '') {
                  this.sortVersion();
                }
                
              } else {
                this.alert.warn(response?.result?.message || 'Proceso no completado');
              }
              dialogRefLoadingPage.close();
            },
              err => {
                if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
                  this.alert.warn(err.error.result.message);
                } else {
                  this.alert.error('Proceso no completado');
                }
                dialogRefLoadingPage.close();
              });
          } else {
            //PUT
            this.versionService.addProduct(this.selectedVersion.versionId, row.sku).subscribe(response => {
            //let message = '';
              if (response.code == 200 && response.result.status == 'ok') {
                row.statusVersion = statusVersion;
                row.statusChangeVersion = (row.oldStatusVersion == row.statusVersion) ? 0
                  : row.oldStatusVersion == 1 && row.statusVersion == 0 ? 2 : 1;
                if (this.queryValue.trim() == '') {
                  this.sortVersion();
                }
                this.alert.success('Se ha agregado el producto '+ row.sku + ' a la version ' + this.selectedVersion.name);

              } else {
                this.alert.warn(response?.result?.message || 'Proceso no completado');
              }
              dialogRefLoadingPage.close();
            },
              err => {
                dialogRefLoadingPage.close();
                if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
                  this.alert.warn(err.error.result.message);
                } else {
                  this.alert.error('Proceso no completado');
                }
              });
          }
        }
      });
    } else {
      this.alert.info('Versión no es configurable');
    }
  }

  reloadVersions(){
    this.getVersions(this.selectedVersion);
  }

  getVersions(initVersion?: ProductVersion): void {
    this.versions = [];
    this.loadVersions = true;
    this.versionService.all().subscribe(
      response => {
        if (response.code == 200 && response.result.status == 'ok') {
          let v = response.result.data.versions;
          for (let index = 0; index < v.length; index++) {
            let element = v[index];
            delete element.lastUpdate;
          }
          this.versions = v;
          if (initVersion) {
            let find = this.versions.find(x => x.versionId == initVersion.versionId);
            this.selectedVersion = find;
          }
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado');
        }
        this.loadVersions = false;
      },
      err => {
        this.loadVersions = false;
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }



  deleteVersion() {
    event.preventDefault();
    if (this.editableVersions.includes(this.selectedVersion?.status)) {
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        width: '400px',
        data: {
          description: `Esta seguro de eliminar la version ${this.selectedVersion.name}?`,
          title: "Confirmación"
        },
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
            width: '70%',
            data: "",
            disableClose: true
          });
          this.versionService.delete(this.selectedVersion?.versionId).subscribe(response => {
            if (response.code == 200 && response.result.status == 'ok') {
              this.alert.success('Se ha eliminado la versión ' + this.selectedVersion?.name);

              this.selectedVersion = null;
              this.getVersions();
              this.loadInventory();
            } else {
              this.alert.warn(response?.result?.message || 'Proceso no completado');
            }
            dialogRefLoadingPage.close();
          },
            err => {
              dialogRefLoadingPage.close();
              if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
                this.alert.warn(err.error.result.message);
              } else {
                this.alert.error('Proceso no completado');
              }
            });
        }
      });
    } else {
      this.alert.info('Versipón no es eliminable');
    }
  }

  loadInventory(versionId?): void {
    this.products = [];
    this.productsDataSource = new MatTableDataSource<Product>();
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    //const vId = versionId || this.selectedVersion.versionId;
    this.inventoryService.getAllProduct(this.initVersion.versionId).subscribe(response => {
      if (response.code == 200 && response.result.status == 'ok') {
        this.products = response.result.data.localInventory;
        if ((this.initVersion?.versionId != this.selectedVersion?.versionId)) {
          this.inventoryService.getAllProduct(this.selectedVersion?.versionId).subscribe(response => {
            if (response.code == 200 && response.result.status == 'ok') {
              for (let index = 0; index < this.products.length; index++) {
                let product = this.products[index];
                product.oldStatusVersion = 0;
                product.statusVersion = 0;
                product.statusChangeVersion = 0;
                for (let idx = 0; idx < response.result.data.localInventory.length; idx++) {
                  const versionedProduct = response.result.data.localInventory[idx];
                  if (product.sku == versionedProduct.sku) {
                    product.oldStatusVersion = 1;
                    product.statusVersion = 1;
                    break;
                  }
                }
              }
              this.products = this.sortVersion();
              this.setProducts();
            } else {
              this.alert.warn(response?.result?.message || 'Proceso no completado');
            }
            dialogRefLoadingPage.close();
          },
            err => {
              if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
                this.alert.warn(err.error.result.message);
              } else {
                this.alert.error('Proceso no completado');
              }
              dialogRefLoadingPage.close();
            });
        } else {
          for (let index = 0; index < this.products.length; index++) {
            let product = this.products[index];
            product.oldStatusVersion = 0;
            product.statusVersion = 0;
            product.statusChangeVersion = 0;
          }
          this.setProducts();
          dialogRefLoadingPage.close();
        }
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
        dialogRefLoadingPage.close();
      }
    },
      err => {
        dialogRefLoadingPage.close();
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }


  setProducts() {
    this.productsDataSource = new MatTableDataSource<Product>(this.products);
    this.cdr.detectChanges();
    this.productsDataSource.paginator = this.paginator;
    this.productsDataSource.sort = this.sort;
    this.applyFilter();

  }

  applyFilter() {
    if (this.queryValue.trim() == '') {
      this.sortVersion();
    }
    this.productsDataSource.filter = this.queryValue.trim().toLowerCase();
    this.productsDataSource._updateChangeSubscription();
  }

  showProduct(row: any) {
    /* const dialogRef = this.dialog.open(LocalProductDetailDialogComponent, {
      width: '80%',
      data: row,
    });

    dialogRef.afterClosed().subscribe(result => {
    }); */
  }

  deleteRow(row: any) {

    this.productsDataSource.data.splice(this.productsDataSource.data.indexOf(row), 1);
    this.productsDataSource._updateChangeSubscription();
  }

  deleteProduct(row: any) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        description: `Desea eliminar el producto de inventario local?`,
        title: "Confirmación"
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
          width: '70%',
          data: "",
          disableClose: true
        });
        this.inventoryService.delete(row.sku).subscribe(response => {
          if (response.code == 200 && response.result.status == 'ok') {
            this.alert.success('Se ha eliminado el  producto ' + row?.sku);
            this.deleteRow(row);
          } else {
            this.alert.warn(response?.result?.message || 'Proceso no completado');
          }
          dialogRefLoadingPage.close();
        },
          err => {
            dialogRefLoadingPage.close();
            if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
              this.alert.warn(err.error.result.message);
            } else {
              this.alert.error('Proceso no completado');
            }
          });
      }
    });
  }


  versionChange() {
    if (this.selectedVersion) {
      this.loadInventory();
    }
  }

  sortVersion() {
    return this.products.sort((n1, n2) => {
      if (n1.statusVersion < n2.statusVersion) {
        return 1;
      }
      if (n1.statusVersion > n2.statusVersion) {
        return -1;
      }
      return 0;
    });
  }


  createVersion() {
    const dialogRef = this.dialog.open(CreateVersionDialogComponent, {
      width: '50%',
      data: {
        versions: this.versions
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getVersions(this.selectedVersion);
      }
    });
  }

  updateVersion() {
    const dialogRef = this.dialog.open(CreateVersionDialogComponent, {
      width: '50%',
      data: {
        versions: this.versions,
        version: this.selectedVersion
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result) {
        this.getVersions(this.selectedVersion);
      }
    });
  }

  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/store/offer/inventory');
  }
  
}
