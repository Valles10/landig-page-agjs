import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ProductVersion } from 'src/app/core/models/ProductVersion';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { VersionService } from 'src/app/core/services/version.service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';

@Component({
  selector: 'app-create-version-dialog',
  templateUrl: './create-version-dialog.component.html',
  styleUrls: ['./create-version-dialog.component.css']
})
export class CreateVersionDialogComponent implements OnInit {

  public isLinear = false;
  public selectedVersion: ProductVersion;
  public version: ProductVersion;
  public versions: ProductVersion[] = [];
  public versionForm: FormGroup;
  public secondFormGroup: FormGroup;

  constructor(
    private _formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<CreateVersionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private versionService: VersionService,
    private alert: MyAlertService
  ) {
    this.versions = data.versions || [];
    this.version = data.version || new ProductVersion();
  }

  ngOnInit() {
    this.isLinear = true;
    this.versionForm = new FormGroup({
      name: new FormControl(this.version.name,
        [
          Validators.required
        ])
    });
    this.secondFormGroup = new FormGroup({
      frmVersion: new FormControl('',
        [])
    });
    if (this.version?.versionId)
      this.frmVersion.disable();
  }

  get name() { return this.versionForm.get('name'); }
  get frmVersion() { return this.secondFormGroup.get('frmVersion'); }


  save() {
    if (this.version.versionId) {
      //Actualizar
      this.versionService.update({ idVersion: this.version.versionId, name: this.name.value }).subscribe(
        response => {
          if (response.code == 200 && response.result.status == "ok") {
            this.alert.success('Se ha actualizado la version ' + this.name.value);
            this.dialogRef.close(true);
          } else {
            this.alert.warn(response?.result?.message || 'Proceso no completado');
          }
        },
        err => {
          if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          }
        });

    } else {
      //Crear
      const idParentVersion =  this.frmVersion.value?.versionId || 0 ;
      this.versionService.create({ idParentVersion: idParentVersion, name: this.name.value }).subscribe(
        response => {
          if (response.code == 200 && response.result.status == "ok") {
            this.alert.success('Se ha actualizado la version ' + this.name.value);
            this.dialogRef.close(true);
          } else {
            this.alert.warn(response?.result?.message || 'Proceso no completado');
          }
        },
        err => {
          if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          }
        });

    }
  }
}
