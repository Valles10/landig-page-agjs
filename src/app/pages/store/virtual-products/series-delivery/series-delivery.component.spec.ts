import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeriesDeliveryComponent } from './series-delivery.component';

describe('SeriesDeliveryComponent', () => {
  let component: SeriesDeliveryComponent;
  let fixture: ComponentFixture<SeriesDeliveryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeriesDeliveryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeriesDeliveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
