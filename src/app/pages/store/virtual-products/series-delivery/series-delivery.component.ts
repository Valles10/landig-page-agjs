import { Component, OnInit, ViewChild } from '@angular/core';
import { UpdateSerieDialogComponent } from '../series-management/dialogs/update-serie-dialog/update-serie-dialog.component';
import { Serie } from 'src/app/core/models/Serie';
import { MatTableDataSource } from '@angular/material/table';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { Tools } from 'src/app/core/helpers/tools';
import { SerieService } from 'src/app/core/services/serie.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { SeriesEmailsDialogComponent } from './dialogs/series-emails-dialog/series-emails-dialog.component';
import { PermissionService } from 'src/app/core/services/permission.service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';

@Component({
  selector: 'app-series-delivery',
  templateUrl: './series-delivery.component.html'
})
export class SeriesDeliveryComponent implements OnInit {

  public viewOptions: ViewOptions = {
    displayedColumns: ['rangeNum', 'delAttempt', 'loadDate', 'moveDate', 'userName', 'ACCION'],
    pageSize: [25, 35, 50],
    loading: true
  };

  public arrayPermission = new Array();
  public serie: Serie = new Serie();
  public queryValue: string = "";
  public series: MatTableDataSource<Serie> = new MatTableDataSource<Serie>();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    private serieService: SerieService,
    private permissionService: PermissionService,
   private alert: MyAlertService
  ) {}

  ngOnInit(): void {
    this.selectPermission();
    this.load();
  }

  load() {
    this.series = new MatTableDataSource<Serie>();
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.serieService.getAll({
      status: 'S'
    }).subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
          if (response.result.data?.virtuals?.length > 0) {
            this.series = new MatTableDataSource<Serie>(response.result.data.virtuals);
            this.series.paginator = this.paginator;
            this.series.sort = this.sort;
            this.applyFilter();
          }
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      dialogRefLoadingPage.close();
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        } 
        dialogRefLoadingPage.close();
      });
  }

  applyFilter() {
    this.series.filter = this.queryValue.trim().toLowerCase();
  }

  email(element: Serie) {
    const dialogRef = this.dialog.open(SeriesEmailsDialogComponent, {
      width: '60%',
      data: element
    });

    dialogRef.afterClosed().subscribe(
      res => {
        this.load();
      });
  }

  changeStatus() {
    this.load();
  }

  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/store/virtual-products/series-delivery');
  }

}
