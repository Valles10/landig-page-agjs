import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendSerialMailDialogComponent } from './send-serial-mail-dialog.component';

describe('SendSerialMailDialogComponent', () => {
  let component: SendSerialMailDialogComponent;
  let fixture: ComponentFixture<SendSerialMailDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendSerialMailDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendSerialMailDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
