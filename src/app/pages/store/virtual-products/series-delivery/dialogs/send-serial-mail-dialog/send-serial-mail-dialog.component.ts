import { Component, OnInit, Inject } from '@angular/core';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from 'src/app/core/models';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SerieService } from 'src/app/core/services/serie.service';
import { Serie } from 'src/app/core/models/Serie';
import { AuthSecureService } from 'src/app/core/services/auth-secure.service';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';

@Component({
  selector: 'app-send-serial-mail-dialog',
  templateUrl: './send-serial-mail-dialog.component.html'
})
export class SendSerialMailDialogComponent implements OnInit {
  
  public serieForm: FormGroup;
  private currentUser: User;
  private attempt:boolean= false;

  constructor(
    private _snackBar: MatSnackBar,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<SendSerialMailDialogComponent>,
    private serieService: SerieService,
    @Inject(MAT_DIALOG_DATA) public data: Serie,
    private authSecureService: AuthSecureService,
    private alert: MyAlertService
  ) {
    this.currentUser = this.authSecureService.userValue;
  }

  ngOnInit(): void {
    this.serieForm = new FormGroup({
      id_range: new FormControl(this.data.idRange.toString()),
      email: new FormControl('',
        [
          Validators.required,
          Validators.maxLength(50),
          Validators.email
        ])
    });
  }

  get id_range() { return this.serieForm.get('id_range'); }
  get email() { return this.serieForm.get('email'); }

  sendEmail() {
    const dialogConfirm = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        title: 'Confirmación',
        description: 'Esta seguro de enviar la clave al correo ' + this.email.value
      },
    });
    this.attempt = true;
    dialogConfirm.afterClosed().subscribe(
      res => {
        if (res) {
          const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
            width: '80%',
            data: "",
            disableClose: true
          });
          this.serieService.sendMail({
            username: this.currentUser.userName,
            ranges_num: [
              {
                email: this.email.value,
                id_range: this.id_range.value
              }
            ]
          }).subscribe(response => {
            if (response.code == 200 && response.result.status == "ok") {
              this.alert.success('Serie fue enviada con exito');
                this.dialogRef.close(true);
            } else {
              this.alert.warn(response?.result?.message || 'Proceso no completado');
            }
            dialogRefLoadingPage.close();
          },
            err => {
              if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
                this.alert.warn(err.error.result.message);
              } else {
                this.alert.error('Proceso no completado');
              }
              dialogRefLoadingPage.close();
            });
        }
      });
  }


  close(){
    this.dialogRef.close(this.attempt);
  }
}
