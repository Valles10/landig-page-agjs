import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { User } from 'src/app/core/models';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SerieService } from 'src/app/core/services/serie.service';
import { AuthSecureService } from 'src/app/core/services/auth-secure.service';
import { Serie } from 'src/app/core/models/Serie';
import { MatTableDataSource } from '@angular/material/table';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { SendSerialMailDialogComponent } from '../send-serial-mail-dialog/send-serial-mail-dialog.component';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';


@Component({
  selector: 'app-series-emails-dialog',
  templateUrl: './series-emails-dialog.component.html'
})
export class SeriesEmailsDialogComponent implements OnInit {
  public viewOptions: ViewOptions = {
    displayedColumns: ['email', 'message', 'sendDate', 'userName', 'ACCION'],
    pageSize: [5, 10, 20],
    loading: true
  };
  public serieForm: FormGroup;
  private currentUser: User;
  public emails: MatTableDataSource<Serie> = new MatTableDataSource<Serie>();
  public queryValue: string = "";
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private _snackBar: MatSnackBar,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<SeriesEmailsDialogComponent>,
    private serieService: SerieService,
    @Inject(MAT_DIALOG_DATA) public data: Serie,
    private authSecureService: AuthSecureService,
    private alert: MyAlertService
  ) {
    this.currentUser = this.authSecureService.userValue;
    console.log(data);
  }

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    this.emails = new MatTableDataSource<Serie>();
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.serieService.getSent(this.data.idRange).subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
        if (response.result?.data?.virtuals?.length > 0) {
          this.emails = new MatTableDataSource<Serie>(response.result.data.virtuals);
          this.emails.paginator = this.paginator;
          this.emails.sort = this.sort;
          this.applyFilter();
        }
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      dialogRefLoadingPage.close();
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialogRefLoadingPage.close();
      });
  }

  applyFilter() {
    this.emails.filter = this.queryValue.trim().toLowerCase();
  }

  sendEmail(element: Serie) {
    const dialogConfirm = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        title: 'Confirmación',
        description: 'Esta seguro de enviar la clave al correo ' + element.email
      },
    });
    dialogConfirm.afterClosed().subscribe(
      res => {
        if (res) {
          const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
            width: '80%',
            data: "",
            disableClose: true
          });
          this.serieService.sendMail({
            username: this.currentUser.userName,
            ranges_num: [
              {
                email: element.email,
                id_range: this.data?.idRange?.toString()
              }
            ]
          }).subscribe(response => {
            if (response.code == 200 && response.result.status == "ok") {
              this.loadData();
              this.alert.success('Serie fue enviado con exito');
            } else {
              this.alert.warn(response?.result?.message || 'Proceso no completado');
            }
            dialogRefLoadingPage.close();
          },
            err => {
              if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
                this.alert.warn(err.error.result.message);
              } else {
                this.alert.error('Proceso no completado');
              }
              dialogRefLoadingPage.close();
            });
        }
      });
  }

  newEmail() {
    console.log("nuevo email");
    const dialogRef = this.dialog.open(SendSerialMailDialogComponent, {
      width: '60%',
      data: this.data
    });

    dialogRef.afterClosed().subscribe(
      res => {
        if (res) {
          this.loadData();
        }
      });
  }
}
