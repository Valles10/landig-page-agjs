import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeriesEmailsDialogComponent } from './series-emails-dialog.component';

describe('SeriesEmailsDialogComponent', () => {
  let component: SeriesEmailsDialogComponent;
  let fixture: ComponentFixture<SeriesEmailsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeriesEmailsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeriesEmailsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
