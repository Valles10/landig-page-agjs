import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeriesManagementComponent } from './series-management.component';

describe('SeriesManagementComponent', () => {
  let component: SeriesManagementComponent;
  let fixture: ComponentFixture<SeriesManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeriesManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeriesManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
