import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Serie } from 'src/app/core/models/Serie';
import { SerieService } from 'src/app/core/services/serie.service';
import { User } from 'src/app/core/models';
import { AuthSecureService } from 'src/app/core/services/auth-secure.service';
import { Tools } from 'src/app/core/helpers/tools';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';

@Component({
  selector: 'app-update-serie-dialog',
  templateUrl: './update-serie-dialog.component.html'
})
export class UpdateSerieDialogComponent implements OnInit {

  public serieForm: FormGroup;
  private currentUser: User;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<UpdateSerieDialogComponent>,
    private serieService: SerieService,
    @Inject(MAT_DIALOG_DATA) public data: Serie,
    private authSecureService: AuthSecureService,
    private alert: MyAlertService
  ) {
    this.currentUser = this.authSecureService.userValue;
  }


  ngOnInit(): void {
    this.serieForm = new FormGroup({
      id_range: new FormControl(this.data.idRange.toString()),
      key_num: new FormControl('',
        [
          Validators.required,
          Validators.maxLength(50)
        ])
    });
  }

  get id_range() { return this.serieForm.get('id_range'); }
  get key_num() { return this.serieForm.get('key_num'); }

  save() {
    const dialoadRefLoad = this.dialog.open(LoadingPageDialogComponent, {
      width: '400px',
      data: {
        description: ``,
        title: "",
      },
      disableClose: true
    });
    this.serieService.addKey({
      username: this.currentUser.userName,
      ranges_num: [
        this.serieForm.value
      ]
    }).subscribe(res => {
      if (res.code == 200 && res.result.status == 'ok') {
        this.alert.success('Serie ingresada correctamente');
        this.dialogRef.close(true);
      } else {
        this.alert.warn(res?.result?.message || 'Proceso no completado');
      }
      dialoadRefLoad.close();
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialoadRefLoad.close();
      });
  }

}
