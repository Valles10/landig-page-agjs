import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Serie } from 'src/app/core/models/Serie';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SerieService } from 'src/app/core/services/serie.service';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { UpdateSerieDialogComponent } from './dialogs/update-serie-dialog/update-serie-dialog.component';
import { FormControl } from '@angular/forms';
import { PermissionService } from 'src/app/core/services/permission.service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';

declare let XLSX: any;


const seriesType = [
  {
    code: 'A',
    name: 'Disponible'
  },
  {
    code: 'S',
    name: 'Vendida'
  },
  {
    code: 'R',
    name: 'Reservada'
  }
];

@Component({
  selector: 'app-series-management',
  templateUrl: './series-management.component.html',
})
export class SeriesManagementComponent implements OnInit {

  public viewOptions: ViewOptions = {
    displayedColumns: ['sku', 'name','rangeNum', 'key', 'status',  'loadDate', 'moveDate','userName', 'ACCION'],
    pageSize: [25, 35, 50],
    loading: true
  };

  public arrayPermission = new Array();
  public serie: Serie = new Serie();
  public queryValue: string = "";
  public series: MatTableDataSource<Serie> = new MatTableDataSource<Serie>();
  public statusList = seriesType;

  public statusForm: FormControl;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private permissionService: PermissionService,
    private serieService: SerieService,
    private alert: MyAlertService
  ) {
  }

  ngOnInit(): void {
    this.selectPermission();
    this.statusForm = new FormControl('');
    this.load();
  }

  load() {
    this.series = new MatTableDataSource<Serie>();
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    let params;
    if (this.statusForm.value != '') {
      params = {
        status: this.statusForm.value
      }
    }
    this.serieService.getAll(params).subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
          if (response.result.data?.virtuals?.length > 0 ) {
            this.series = new MatTableDataSource<Serie>(response.result.data.virtuals);
            this.series.paginator = this.paginator;
            this.series.sort = this.sort;
            this.applyFilter();
          }
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      dialogRefLoadingPage.close();
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        } 
        dialogRefLoadingPage.close();
      });
  }

  applyFilter() {
    this.series.filter = this.queryValue.trim().toLowerCase();
  }

  edit(element: Serie) {
    const dialogRef = this.dialog.open(UpdateSerieDialogComponent, {
      width: '60%',
      data: element
    });

    dialogRef.afterClosed().subscribe(
      res => {
        if (res) {
          this.load();
        }
      });
  }

  changeStatus() {
    this.load();
  }

  downloadCsv() {
    if (this.series?.data?.length > 0) {
      const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
        width: '80%',
        data: "",
        disableClose: true
      });
      let newData: any[] = [];
      const data = this.series?.data;
      for (let index = 0; index < data.length; index++) {
        const element = data[index];
        newData.push({
          'SKU': element.sku,
          'NOMBRE': element.name,
          'SERIE': element.rangeNum,
          'KEY': (element.keyNum ? 'SI' : 'NO'),
          'ESTADO': (element.status =='A' ? 'Disponible' : (element.status == 'R' ? 'Reservada' : (element.status == 'S' ? 'Vendida' : 'No definido'))),
          'FECHA DE CARGA': element.loadDate,
          'FECHA MOVIMIENTO': element.moveDate,
          'USUARIO': element.userName
        });
      }
      const workSheet = XLSX.utils.json_to_sheet(newData);
      const workBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(workBook, workSheet, 'Series');
      const date = new Date();
      dialogRefLoadingPage.close();
      XLSX.writeFile(workBook, `Series_${(date.getDate())}_${(date.getMonth()+1)}_${date.getFullYear()}.csv`);
    } else {
      this.alert.warn('Nada que descargar');
    }
  }


  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/store/virtual-products/series');
  }
}
