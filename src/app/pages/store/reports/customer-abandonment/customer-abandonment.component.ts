import { Component, OnInit } from '@angular/core';
import * as _moment from 'moment';
import { Moment } from 'moment';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import { FormControl } from '@angular/forms';
import { MatDatepicker } from '@angular/material/datepicker';
import { ReportService } from 'src/app/core/services/report.service';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { Tools } from 'src/app/core/helpers/tools';
import { MatDialog } from '@angular/material/dialog';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';

const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-customer-abandonment',
  templateUrl: './customer-abandonment.component.html',
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    { provide: MAT_DATE_LOCALE, useValue: 'es-us' },
  ]
})
export class CustomerAbandonmentComponent implements OnInit {

  public date = new FormControl(moment());
  public data: Array<any>;
  public options;
  public headers: string[];
  public params = {
    startDate: '',
    endDate: ''
  };

  constructor(
    public dialog: MatDialog,
    private reportService: ReportService,
    private alert: MyAlertService
  ) {
  }

  ngOnInit(): void {
    this.load();
  }

  load() {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.data = [];
    this.dateChange();
    this.reportService.customerAbandonment(this.params).subscribe(
      res => {
        if (res.code == 200 && res.result.status == 'ok') {
          if (Array.isArray(res.result.data.tracerReport)) {
            let newData = Array<any>();
            let headerObj = Object.assign({}, res.result.data.tracerReport[0]);
            if (headerObj) {
              delete headerObj.step;
              let headerTemp = Object.keys(headerObj);
              for (let i = 0; i < res.result.data.tracerReport.length; i++) {
                const element = res.result.data.tracerReport[i];
                let obj: { [key: string]: any } = {};
                obj.step = element.step?.toUpperCase();
                obj.step = obj?.step?.replace('_', ' ');
                for (let index = 0; index < headerTemp.length; index++) {
                  const e = headerTemp[index];
                  obj[e] = element[e];
                }
                newData.push(obj);
              }
              this.options = {
                legend: { position: 'top', maxLines: 3 },
                hAxis: {
                  title: 'Total abandonos',
                  minValue: 0,
                },
                vAxis: {
                  title: 'Estados'
                },
                bar: { groupWidth: "50%" },
                bars: 'horizontal',
                chartArea: {
                  height: '90%',
                  width: '90%'
                },
                /* height: newData.length * 150 */
              };
              if (newData.length > 5) {
                this.options.height = newData.length * 50
              }
              if (newData?.length > 0) {
                this.headers = Object.keys(newData[0])
                this.data = newData;
              }
            }
          }
        } else {
          this.data = [];
          this.alert.warn(res?.result?.message || 'Proceso no completado');
        }
        dialogRefLoadingPage.close();
      },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialogRefLoadingPage.close();
        this.data = [];
      });
  }


  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
    this.load();
  }

  dateChange() {
    this.params.startDate = (<Moment>this.date.value).startOf('month').format('DD/MM/YYYY');
    this.params.endDate = (<Moment>this.date.value).endOf('month').format('DD/MM/YYYY');
  }
}
