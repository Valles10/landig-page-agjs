import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerAbandonmentComponent } from './customer-abandonment.component';

describe('CustomerAbandonmentComponent', () => {
  let component: CustomerAbandonmentComponent;
  let fixture: ComponentFixture<CustomerAbandonmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerAbandonmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerAbandonmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
