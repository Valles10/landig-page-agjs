import { Component, OnInit } from '@angular/core';
import { ItemChart } from 'src/app/components/my-google-charts/my-google-charts.component';
import { MatDialog } from '@angular/material/dialog';
import * as _moment from 'moment';

import { Moment } from 'moment';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import { FormControl } from '@angular/forms';
import { MatDatepicker } from '@angular/material/datepicker';
import { ReportService } from 'src/app/core/services/report.service';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';

const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-purchase-order-report',
  templateUrl: './purchase-order-report.component.html',
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    { provide: MAT_DATE_LOCALE, useValue: 'es-us' },
  ],
})
export class PurchaseOrderReportComponent implements OnInit {

  public date = new FormControl(moment());
  public data: Array<any>;
  public options;
  public headers: string[];
  public params = {
    month: 0,
    year: 0,
    day: 0
  };

  constructor(
    public dialog: MatDialog,
    private reportService: ReportService,
    private alert: MyAlertService

  ) {
  }

  ngOnInit(): void {
    this.load();
  }

  selectedItem(item: ItemChart) {
    if (item && this.data && this.data[item.row]?.input_date) {
      this.params.day = this.data[item.row].input_date;
    }
  }

  load() {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });

    this.data = [];
    this.dateChange();
    this.reportService.purchases(this.params).subscribe(
      res => {
        if (res.code == 200 && res.result.status == 'ok') {
          this.headers = ['Dia', 'Producto'];


          let slices = {};
          let pColor = {};
          for (let index = 0; index < res.result.data.statistics.length; index++) {
            let element = res.result.data.statistics[index];
            switch (element[Object.keys(element)[0]]) {
              case 'COMPLETADO':
                pColor[index] = { color: '#1B998B', pieSliceText: 'value' }; // VERDE 
                break;
              case 'EN PROGRESO':
              case 'PROCESO SIM':
              case 'PROCESO CRM':
                pColor[index] = { color: '#F9DE00', pieSliceText: 'value' }; //AMARILLO
                break;
              case 'ENVIADO': case 'ADDON':
                pColor[index] = { color: '#084887', pieSliceText: 'value' }; // AZUL
                break;
              case 'ERROR':
              case 'ERROR ODA':
                pColor[index] = { color: '#F71735', pieSliceText: 'value' };//ROJO
                break;
              case 'REPROCESADO MANUAL':
                pColor[index] = { color: '#4E37A9', pieSliceText: 'value' }; // MORADO
                break;
              case 'ANULACION':
                pColor[index] = { color: '#FEB95F', pieSliceText: 'value' }; // NARANJA
                break;
              case 'PLAN EN ESPERA ':
                pColor[index] = { color: '#C4CBCA', pieSliceText: 'value' }; // GRIS
                break;
              case 'PROCESADO':
                pColor[index] = { color: '#d6d4e9', pieSliceText: 'value' }; // BLANCO
                break;
            }
            if (Object.keys(element).length > 0) {
              slices = { ...slices, ...pColor };
            }
            element.input_date += ' (' + element.total_purchases + ')';

          }


          this.options = {
            height: 375,
            /* legend: {
              position: 'labeled',
              maxLines: 5
            }, */
            title: 'Ordenes en total: ' + res.result.data.total,
            pieStartAngle: 45,
            pieHole: 0.4,
            slices: slices
          };

          this.data = res.result.data.statistics;
        } else {
          this.data = [];
          this.alert.warn(res?.result?.message || 'Proceso no completado');
        }
        dialogRefLoadingPage.close();
      },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialogRefLoadingPage.close();
        this.data = [];
      });
  }

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
    this.params.day = 0;
    this.load();
  }

  dateChange() {
    this.params.year = (<Moment>this.date.value).year();
    this.params.month = (<Moment>this.date.value).month() + 1;
  }

  /* setParamsTemp() {
    this.params.year = this.params.year;
    this.params.month = this.params.month;
  } */
}
