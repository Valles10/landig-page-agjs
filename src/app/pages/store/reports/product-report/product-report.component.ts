import { Component, OnInit } from '@angular/core';
import { ItemChart } from 'src/app/components/my-google-charts/my-google-charts.component';
import { MatDialog } from '@angular/material/dialog';
import * as _moment from 'moment';
import { Moment } from 'moment';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import { FormControl } from '@angular/forms';
import { MatDatepicker } from '@angular/material/datepicker';
import { ReportService } from 'src/app/core/services/report.service';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';

const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-product-report',
  templateUrl: './product-report.component.html',
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    { provide: MAT_DATE_LOCALE, useValue: 'es-us' },
  ],
})
export class ProductReportComponent implements OnInit {

  public date = new FormControl(moment());
  public data: Array<any>;
  public options;
  public headers: string[];
  public params = {
    month: 0,
    year: 0,
    day: 0
  };

  constructor(
    public dialog: MatDialog,
    private reportService: ReportService,
    private alert: MyAlertService
  ) {
  }

  ngOnInit(): void {
    this.load();
  }

  selectedItem(item: ItemChart) {
    if (item && this.data && this.data[item.row]?.input_date) {
      this.params.day = this.data[item.row].input_date;
    }
  }


  load() {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.data = [];
    this.dateChange();
    this.reportService.orders(this.params).subscribe(
      res => {
        if (res.code == 200 && res.result.status == 'ok') {
          this.headers = ['Dia', 'Producto'];
          this.options = {
            height: 375, 
            colors:['#23b5ae'],
            legend: { position: 'top', maxLines: 3 },
            title: 'Total producto vendido: ' + res.result.data.total,
            hAxis: { title: 'Día', titleTextStyle: { color: '#333' } },
            vAxis: { minValue: 0 }
          };
          this.data = res.result.data.statistics;
        } else {
          this.data  = [];
          this.alert.warn(res?.result?.message || 'Proceso no completado');
        }
        dialogRefLoadingPage.close();
      },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        } 
        dialogRefLoadingPage.close();
        this.data  = [];
      });
  }

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
    this.params.day = 0;
    this.load();
  }

  dateChange() {
    this.params.year = (<Moment>this.date.value).year();
    this.params.month = (<Moment>this.date.value).month() + 1;
  }
}
