import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { FormControl, Form } from '@angular/forms';
import { MatDatepicker } from '@angular/material/datepicker';
import * as _moment from 'moment';
import { Moment } from 'moment';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { Sim } from 'src/app/core/models/sim';
import { Contract } from 'src/app/core/models/contract';
import { ContractService } from 'src/app/core/services/contract-service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { MatSelect } from '@angular/material/select';
import { SimComponent } from './dialog/sim/sim.component';
import { PermissionService } from 'src/app/core/services/permission.service';

const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'app-provisioning',
  templateUrl: './provisioning.component.html',
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    { provide: MAT_DATE_LOCALE, useValue: 'es-us' },
  ]
})
export class ProvisioningComponent implements OnInit {

  selectedValue = "";

  public viewOptions: ViewOptions = {
    displayedColumns: ['no', 'orderWcsid', 'username',  'sku','status', 'actions'], //campos que se mostraran
    pageSize: [25, 35, 50], // Configuracion de paginacion
    loading: true
  };

  sims: Sim[] = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  public arrayPermission = new Array();
  public queryValue= '';
  dataSource: MatTableDataSource<Contract> = new MatTableDataSource<Contract>();
  datos: Contract[] = [];
  date = new FormControl(moment());
  renderedData: any;
  private param = {
    year: 0,
    month: 0,
    wcsid: '',
    contract: ''
  };
  @ViewChild('dataTable') table: ElementRef;

  constructor(
    private permissionService: PermissionService,
    public dialog: MatDialog,
    public contractService: ContractService,
    private alert: MyAlertService
  ) {
    this.dataSource.connect().subscribe(d => this.renderedData = d);
  }

  ngOnInit(): void {
    this.selectPermission();
    this.load();

  }

  load(): void {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.dateChange();
    this.contractService.getNewContractList(this.param).subscribe(
      res => {
        if (res.code == 200 && res.result.status == 'ok') {
          this.datos = res.result.data.Contracts;
          this.dataSource = new MatTableDataSource<Contract>(this.datos);
          this.dataSource.paginator = this.paginator;
          this.filter();
        } else {
          this.alert.warn(res?.result?.message || 'Proceso no completado');
        }
        dialogRefLoadingPage.close();
      },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialogRefLoadingPage.close();
      });
  }

  filter() {
    /* const filterValue = (event.target as HTMLInputElement).value; */
    this.dataSource.filter = this.queryValue.trim().toLowerCase();
  }


  documentList(contract: Contract) {
    if (contract?.sku) {
      const dialogRefSimPage = this.dialog.open(SimComponent, {
        width: '450px',
        data: contract,
      });
      dialogRefSimPage.afterClosed().subscribe(
        res => {
          if (res?.contract) {
            this.load();
            /* this.datos.splice(this.datos.indexOf(this.datos.find(e => e.orderWcsid == res.contract.orderWcsid), 1));
            this.dataSource._updateChangeSubscription(); */
          }
        });
    } else {
      this.alert.info('Sin SKU');
    }

  }

  asignarSim(contract: Contract, event: MatSelect) {
    contract.simTem = event.value;
  }

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
    this.load();
  }

  dateChange() {
    this.param.year = (<Moment>this.date.value).year();
    this.param.month = (<Moment>this.date.value).month() + 1;
  }

  changeStatus() {
    this.load();
  }

  detalle(contract: Contract) {
    this.dialog.open(SimComponent, {
      width: '80%',
      data: contract,
    });
  }

  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/store/after-sales/provisioning');
  }
  
}
