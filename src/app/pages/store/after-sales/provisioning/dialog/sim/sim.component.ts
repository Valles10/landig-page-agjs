import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { User } from 'src/app/core/models';
import { Contract } from 'src/app/core/models/contract';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { AuthSecureService } from 'src/app/core/services/auth-secure.service';
import { ContractService } from 'src/app/core/services/contract-service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';

@Component({
  selector: 'app-sim',
  templateUrl: './sim.component.html',
})
export class SimComponent implements OnInit {

  public result;
  viewOptions: ViewOptions = {
    displayedColumns: ['no', 'sim'],
    pageSize: [25, 35, 50],

    loading: true
  };
  queryValue: string = "";
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  simsDataSource: any = new MatTableDataSource<any>();

  public sims = [];
  /* public selectedValue: string = ''; */
  public contract: Contract;
  public user: User;
  constructor(

    public dialogRef: MatDialogRef<SimComponent>,
    private contractService: ContractService,
    @Inject(MAT_DIALOG_DATA) public _contract: Contract,
    public dialog: MatDialog,
    public authService: AuthSecureService,
    public alert: MyAlertService
  ) {
    this.contract = _contract;

    this.user = authService.userValue;
  }

  ngOnInit(): void {
    this.load();
  }
  applyFilter() {
    this.simsDataSource.filter = this.queryValue.trim().toLowerCase();
  }

  load(): void {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '40%',
      data: "",
      disableClose: true
    });
    this.contractService.getSimList(this.contract.sku).subscribe(

      res => {
        if (res.code == 200 && res.result.status == 'ok') {
          this.sims = res.result.data.sim;
          if (Array.isArray(this.sims) && this.sims?.length <= 0) {
            this.alert.warn('No se encontraron SIM');
          } else {
            this.simsDataSource = new MatTableDataSource<any>(this.sims);
            this.simsDataSource.paginator = this.paginator;
            this.simsDataSource.sort = this.sort;
          }
        } else {
          this.alert.warn(res?.result?.message || 'Proceso no completado');
        }
        dialogRefLoadingPage.close();
        this.viewOptions.loading = false;
      },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        this.viewOptions.loading = false;

        dialogRefLoadingPage.close();
      });
  }

  updateSim(element) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        description: `Seguro quieres asignar la SIM ${element.sim} , desea continuar?`,
        title: "Confirmación de cambios"
      }
    });

    dialogRef.afterClosed().subscribe(
      res => {
        if (res) {
          const json = {
            Provisioning: {
              userName: this.user.userName,
              orderInfo: []
            }
          }
          const sim = {
            sim: element.sim,
            idOrderDetail: this.contract.orderDetailId
          }
          json.Provisioning.orderInfo.push(sim);
          if (element.sim != '') {
            const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
              width: '80%',
              data: "",
              disableClose: true
            });
            this.contractService.postAprovicionar(json).subscribe(
              res => {
                if (res.code == 200 &&( res.result.status == 'ok' || res.result.status == 'Ok')) {
                  this.dialogRef.close({
                    contract: this.contract
                  });
                  this.alert.success('Aprovisionamiento realizado correctamente');
                } else {
                  this.alert.warn(res?.result?.message || 'Proceso no completado');
                }
                dialogRefLoadingPage.close();
              },
              err => {
                if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
                  this.alert.warn(err.error.result.message);
                } else {
                  this.alert.error('Proceso no completado');
                }
                dialogRefLoadingPage.close();
              });

          } else {
            this.alert.warn('Nada que guardar');
          }
        }
      }
    )
  }

}
