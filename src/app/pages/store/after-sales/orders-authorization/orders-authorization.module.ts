import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersAuthorizationRoutingModule } from './orders-authorization-routing.module';
import { OrdersAuthorizationComponent } from './orders-authorization.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { AuthorizeOrderDialogComponent } from './dialogs/authorize-order-dialog/authorize-order-dialog.component';


@NgModule({
  declarations: [OrdersAuthorizationComponent, AuthorizeOrderDialogComponent],
  imports: [
    CommonModule,
    OrdersAuthorizationRoutingModule,
    FormsModule,
    MatFormFieldModule,
    MatIconModule, 
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatTooltipModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    MatProgressBarModule,
    MatDialogModule,
    MatDatepickerModule
  ]
})
export class OrdersAuthorizationModule { }
