import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Order } from 'src/app/core/models/Order';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';

@Component({
  selector: 'app-authorize-order-dialog',
  templateUrl: './authorize-order-dialog.component.html',
  styleUrls: ['./authorize-order-dialog.component.css']
})
export class AuthorizeOrderDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AuthorizeOrderDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public order: Order,
    public dialog: MatDialog,
    private alert: MyAlertService,
  ) { 
  }

  ngOnInit(): void {
  }

}
