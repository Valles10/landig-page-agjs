import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorizeOrderDialogComponent } from './authorize-order-dialog.component';

describe('AuthorizeOrderDialogComponent', () => {
  let component: AuthorizeOrderDialogComponent;
  let fixture: ComponentFixture<AuthorizeOrderDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorizeOrderDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorizeOrderDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
