import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrdersAuthorizationComponent } from './orders-authorization.component';

const routes: Routes = [{ path: '', component: OrdersAuthorizationComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersAuthorizationRoutingModule { }
