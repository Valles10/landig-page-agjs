import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersAuthorizationComponent } from './orders-authorization.component';

describe('OrdersAuthorizationComponent', () => {
  let component: OrdersAuthorizationComponent;
  let fixture: ComponentFixture<OrdersAuthorizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersAuthorizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersAuthorizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
