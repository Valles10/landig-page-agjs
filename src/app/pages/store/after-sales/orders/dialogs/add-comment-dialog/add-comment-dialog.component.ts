import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { User } from 'src/app/core/models';
import { AuthSecureService } from 'src/app/core/services/auth-secure.service';
import { DetailOrderService } from 'src/app/core/services/detailOrder-service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';

@Component({
  selector: 'app-add-comment-dialog',
  templateUrl: './add-comment-dialog.component.html',
})
export class AddCommentDialogComponent implements OnInit {

  public commentForm: FormGroup;
  public title: string = ''
  private oldSku: string;
  private newSKU: string = '';
  private user: User;
  private orderId: string = '';
  private orderDetailId: string = '';

  constructor(
    public dialogRef: MatDialogRef<AddCommentDialogComponent>,
    private alert: MyAlertService,
    private detailOrderService: DetailOrderService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private authService: AuthSecureService,
    public dialog: MatDialog,
  ) {
    this.oldSku = data?.oldSku || null;
    this.newSKU = data?.newSKU || null;
    this.orderId = data?.orderId || null;
    this.orderDetailId = data?.orderDetailId || null;

    this.user = this.authService.userValue;
    this.title = 'Cambio de SKU ' + this.oldSku + ' al ' + this.newSKU;

  }

  ngOnInit(): void {
    this.commentForm = new FormGroup({
      commentOne: new FormControl({
        value: '',
        disabled: !this.oldSku && !this.newSKU
      }, [
        Validators.required,
        Validators.maxLength(100)
      ])
    });

  }

  get commentOne() { return this.commentForm.get('commentOne'); }

  close(): void {
    if (this.oldSku && this.newSKU && this.user?.userName && this.orderDetailId && this.orderId) {
      if (this.commentOne.value.trim() == '') {
        this.alert.warn('Comentario no es válido');
      } else {
        this.changeSKU();
      }
    } else {
      this.alert.warn('No se puede realizar cambios');
    }
  }

  changeSKU() {
    const dialogRefLoadPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '70%',
      data: "",
      disableClose: false
    });
    this.detailOrderService.changeSKU({
      userName: this.user.userName,
      comment: this.commentOne.value,
      orderInfo: [
        {
          OldSKU: this.oldSku,
          NewSKU: this.newSKU,
          OrderIdDetail: this.orderDetailId
        }
      ]
    }, this.orderId).subscribe(
      res => {
        if (res.code == 200 && res.result.status == 'ok') {
          this.alert.success(res?.result?.message || 'Cambio realizado con exito');
          this.dialogRef.close(true);
        } else {
          this.alert.warn(res?.result?.message || 'Proceso no completado');
        }
        dialogRefLoadPage.close();
      },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialogRefLoadPage.close();
      });
  }
}
