import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { Order } from 'src/app/core/models/Order';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { DepartmentMunicipio } from 'src/app/core/services/departmentMunicipio.service';
import { Department } from 'src/app/core/models/Department';
import { Towns, Village } from 'src/app/core/models/Municipio';
import { OrderService } from 'src/app/core/services/order.service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';


@Component({
  selector: 'app-modifi-order-dialog',
  templateUrl: './modifi-order-dialog.component.html',
  styleUrls: ['./modifi-order-dialog.component.css']
})

export class ModifiOrderDialogComponent implements OnInit {

  orderForm: FormGroup;

  // public order: Order;
  public order: Order;
  public departamentoSelected: string;
  public townsSelected: string;

  public townsList: Array<Towns> = [];
  public departmentList: Array<Department> = [];
  public villageList: Array<Village> = [];

  departamentSelected = '';
  private codDepartmentSendTemp;
  private codVillageSendTemp;
  private codTownSendTemp;

  // DepartmentMunicipio: any;
  constructor(
    public dialogRef: MatDialogRef<ModifiOrderDialogComponent>,
    public departamentoMunicipio: DepartmentMunicipio,
    public orderService: OrderService,
    @Inject(MAT_DIALOG_DATA) public _order: Order,
    public dialog: MatDialog,
    private alert: MyAlertService
  ) {
    this.order = _order;
    this.codDepartmentSendTemp = this.order.codDepartmentSend || '';
    this.codVillageSendTemp = this.order.codVillageSend || '';
    this.codTownSendTemp = this.order.codTownSend || '';
  }

  ngOnInit(): void {
    this.loadDepartment();
    this.controles();
  }

  loadDepartment(): void {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.departamentoMunicipio.getDepartment().subscribe(
      res => {
        if (res.code == 200 && res.result.status == 'Ok') {
          this.departmentList = res.result.data.Department;
          if(this.codVillageSendTemp){
            this.loadTown();
          }
        } else {
          this.alert.warn(res?.result?.message || 'Proceso no completado');
        }
        dialogRefLoadingPage.close();
      },
      err => {
        dialogRefLoadingPage.close();
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }

  loadTown(): void {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.townsList = [];
    this.villageList = [];
    let dep = this.departament.value;
    this.departamentoMunicipio.getMunicipio(dep).subscribe(
      res => {
        if (res.code == 200 && res.result.status == 'Ok') {
          this.townsList = res.result.data.Towns;
          if (this.codDepartmentSendTemp == dep) {
            this.town.patchValue(this.codTownSendTemp);
            this.loadVillage();
          }
        } else {
          this.alert.warn(res?.result?.message || 'Proceso no completado');
        }
        dialogRefLoadingPage.close();
      },
      err => {

        dialogRefLoadingPage.close();
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }

  loadVillage(): void {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.villageList = [];
    let town = this.town.value;
    this.departamentoMunicipio.getVillage(town).subscribe(
      res => {
        if (res.code == 200 && res.result.status == 'Ok') {
          this.villageList = res.result.data.Village;
          if (this.codTownSendTemp == town) {
            this.village.patchValue(this.codVillageSendTemp);
          }
        } else {
          this.alert.warn(res?.result?.message || 'Proceso no completado');
        }
        dialogRefLoadingPage.close();
      },
      err => {

        dialogRefLoadingPage.close();
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }

  controles(): void {
    this.orderForm = new FormGroup({
      name: new FormControl(this.order.userName || '', [
        Validators.pattern(/^[a-zA-Z\s]*$/),
        Validators.required
      ]),
      phone_number: new FormControl(this.order.contactTelephone || '', [
        Validators.pattern(/^[0-9]*$/),
        Validators.required
      ]),
      email: new FormControl(this.order.userEmail || '', [
        Validators.required,
        Validators.email
      ]),
      address: new FormControl(this.order.clienteAddres || '', [
        Validators.required,
      ]),
      department: new FormControl(this.order.codDepartmentSend || '', [
        Validators.required,
      ]),
      town: new FormControl(this.order.codTownSend || '', [
      ]),
      village: new FormControl(this.order.codVillageSend || '', [
      ]),
      ref_client_1: new FormControl(this.order.refClient1 || '', [
        Validators.required,
      ]),
      ref_client_2: new FormControl(this.order.refClient2|| '', [
        Validators.required,
      ]),
      tax_document: new FormControl(this.order.taxDocument || '', [
        Validators.required,
      ]),
      comment: new FormControl(this.order.comment || '', [
        Validators.required,
      ]),
    });

  }

  public get departament() { return this.orderForm.get('department') };
  public get town() { return this.orderForm.get('town') };
  public get village() { return this.orderForm.get('village') };


  submmit(): void {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.orderService.post(this.orderForm.value, this.order.orderId).subscribe(
      res => {
        if (res.code == 200) {
          this.alert.success('Registro actualizado')
        } else {
          this.alert.warn('Proceso no completado');
        }
        dialogRefLoadingPage.close();
      },
      err => {
        dialogRefLoadingPage.close();
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }

  onSave() {
    console.log(this.orderForm.value);
  }
}
