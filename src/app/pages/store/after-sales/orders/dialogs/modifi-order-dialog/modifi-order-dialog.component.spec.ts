import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifiOrderDialogComponent } from './modifi-order-dialog.component';

describe('ModifiOrderDialogComponent', () => {
  let component: ModifiOrderDialogComponent;
  let fixture: ComponentFixture<ModifiOrderDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifiOrderDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifiOrderDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
