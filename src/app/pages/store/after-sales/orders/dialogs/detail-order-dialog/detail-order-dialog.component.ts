import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { DetailOrderService } from 'src/app/core/services/detailOrder-service';
import { Order } from 'src/app/core/models/Order';
import { Tools } from 'src/app/core/helpers/tools';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { DetailOrder } from 'src/app/core/models/DetailOrder';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { CatalogDialogComponent } from 'src/app/pages/store/offer/product-detail/dialogs/catalog-dialog/catalog-dialog.component';
import { User } from 'src/app/core/models';
import { AuthSecureService } from 'src/app/core/services/auth-secure.service';
import { AddCommentDialogComponent } from '../add-comment-dialog/add-comment-dialog.component';

@Component({
  selector: 'app-detail-order-dialog',
  templateUrl: './detail-order-dialog.component.html',
  styleUrls: ['./detail-order-dialog.component.css']
})
export class DetailOrderDialogComponent implements OnInit {

  public viewOptions: ViewOptions = {
    displayedColumns: ["name", "paymentProduct", "discountPrice", "simDumi", /* "inputDate", "activationDate",  */"contract", "phoneNumber", "courtDay", "cycle", "clientId", 'actions'],  //campos que se mostraran

    pageSize: [25, 35, 50], // Configuracion de paginacion
    loading: true
  };

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  dataSource: MatTableDataSource<DetailOrder> = new MatTableDataSource<DetailOrder>();
  public orderId: string;
  public data: Array<DetailOrder>;
  tools: Tools;
  private user: User;
  constructor(
    private _snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<DetailOrderDialogComponent>,
    private detailOrderService: DetailOrderService,
    @Inject(MAT_DIALOG_DATA) public order: Order,
    public dialog: MatDialog,
    private alert: MyAlertService,
    private authService: AuthSecureService
  ) {
    /* this.orderId = order.orderId; */
    this.orderId = order.odaOrderId;
    this.tools = new Tools(_snackBar);
    this.user = this.authService.userValue;
  }

  ngOnInit(): void {
    this.load();
  }

  filter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  load(): void {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });

    this.detailOrderService.get(this.orderId).subscribe(
      res => {
        if (res.code == 200 && res.result.status == 'ok') {
          this.data = res.result.data.order;
          this.dataSource = new MatTableDataSource<DetailOrder>(this.data);
          this.dataSource.paginator = this.paginator;
        } else {
          this.alert.warn(res?.result?.message || 'Proceso no completado');
        }
        dialogRefLoadingPage.close();
      },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialogRefLoadingPage.close();
      });
  }

  changeSKU(element: DetailOrder) {
    const dialogRefCatalogPage = this.dialog.open(CatalogDialogComponent, {
      width: '70%',
      data: "",
      disableClose: false
    });

    dialogRefCatalogPage.afterClosed().subscribe(res => {
      
      if (res?.skuSim) {
        const dialogRefCommentPage = this.dialog.open(AddCommentDialogComponent, {
          width: '500px',
          data: {
            oldSku: element.sku,
            newSKU: res.skuSim,
            orderId: this.orderId,
            orderDetailId: element.orderDetailId
          },
          disableClose: false
        });

        dialogRefCommentPage.afterClosed().subscribe(res => {
          if (!res) {
            this.alert.warn('No se aplico ningun cambio.');
          } else {
            this.load();

          }
        });

      }
    });


  }

}
