import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { Tracking, Evento } from 'src/app/core/models/Tracking';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { TrackingService } from 'src/app/core/services/tracking.service';
import { Order } from 'src/app/core/models/Order';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';

@Component({
  selector: 'app-trackin-dialog',
  templateUrl: './trackin-dialog.component.html',
  styleUrls: ['./trackin-dialog.component.css']
})
export class TrackinDialogComponent implements OnInit {
  
  public viewOptions: ViewOptions = {
    displayedColumns:["codigoRuta","movimiento","nombreRuta","tipoMovimiento","fechaHoraScan"], //campos que se mostraran
    pageSize: [25, 35, 50], // Configuracion de paginacion
    loading: true
  };

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  dataSource: MatTableDataSource<Evento>= new MatTableDataSource<Evento>();
  public dataTracking: Tracking;
  public trackingId: string  ;
  constructor(
    public dialogRef: MatDialogRef<TrackinDialogComponent>,
    private trackingService: TrackingService,
    @Inject(MAT_DIALOG_DATA) public order: Order,
    public dialog: MatDialog,
    private alert: MyAlertService
    ) 
    
    {
    this.trackingId = order.trackingOrder;
    
  } 

  ngOnInit(): void {
    this.load();
  }

  filter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  load():void {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });   
    
    this.trackingService.get(this.trackingId).subscribe(
      res=>{
        if (res.code == 200 && res.result.status == 'ok') {
          this.dataTracking = res.result.data.tracking;
          this.dataSource = new MatTableDataSource<Evento>(this.dataTracking.eventos);
          this.dataSource.paginator = this.paginator;
        }else {
          this.alert.warn(res?.result?.message || 'Proceso no completado');
        }
        dialogRefLoadingPage.close();
      },
      err => {
        dialogRefLoadingPage.close();
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }

}
