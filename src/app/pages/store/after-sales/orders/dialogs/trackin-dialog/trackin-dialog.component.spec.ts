import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackinDialogComponent } from './trackin-dialog.component';

describe('TrackinDialogComponent', () => {
  let component: TrackinDialogComponent;
  let fixture: ComponentFixture<TrackinDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackinDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackinDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
