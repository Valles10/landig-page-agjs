import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { Contract } from 'src/app/core/models/contract';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { DocumentService } from 'src/app/core/services/document-service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { Document } from 'src/app/core/models/document';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
})
export class DocumentsComponent implements OnInit {
  public viewOptions: ViewOptions = {
    displayedColumns: ["dateStore", "contractNo", "documentTypeName", "url"], //campos que se mostraran  
    pageSize: [25, 35, 50], // Configuracion de paginacion
    loading: true
  };

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  dataSource: MatTableDataSource<Document> = new MatTableDataSource<Document>();
  public contractId: string;
  public data: Array<Document>;
  constructor(
    public dialogRef: MatDialogRef<DocumentsComponent>,
    private documentService: DocumentService,
    @Inject(MAT_DIALOG_DATA) public contract: Contract,
    public dialog: MatDialog,
    private alert: MyAlertService
  ) {
    this.contractId = contract.contract;
  }

  ngOnInit(): void {
    this.load();
  }

  filter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  load(): void {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });

    this.documentService.get({
      contract: this.contractId
    }).subscribe(
      res => {
        if (res.code == 200 && res.result.status == 'ok') {
          this.data = res.result.data.documents;
          this.dataSource = new MatTableDataSource<Document>(this.data);
          this.dataSource.paginator = this.paginator;
        } else {
          this.alert.warn(res?.result?.message || 'Proceso no completado');
        }
        dialogRefLoadingPage.close();
      },
      err => {
        dialogRefLoadingPage.close();
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }
}
