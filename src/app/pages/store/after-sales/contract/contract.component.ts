import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { FormControl, Form } from '@angular/forms';
import { MatDatepicker } from '@angular/material/datepicker';
import * as XLSX from 'xlsx';
import * as _moment from 'moment';
import { Moment } from 'moment';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import { ContractService } from 'src/app/core/services/contract-service';
import { Contract } from 'src/app/core/models/contract';
import { DocumentsComponent } from './dialogs/documents/documents.component';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PermissionService } from 'src/app/core/services/permission.service';

const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};


@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    { provide: MAT_DATE_LOCALE, useValue: 'es-us' },
  ],
})
export class ContractComponent implements OnInit {
  public viewOptions: ViewOptions = {
    displayedColumns: ['orderWcsid', 'username', 'sku', 'clientId', 'phoneNumber', 'contract', 'courtDay', 'cycle', 'status', 'botones'], //campos que se mostraran
    pageSize: [25, 35, 50], // Configuracion de paginacion
    loading: true
  };

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  dataSource: MatTableDataSource<Contract> = new MatTableDataSource<Contract>();
  datos = [];
  date = new FormControl(moment());
  renderedData: any;
  public arrayPermission = new Array();
  private param = {
    year: 0,
    month: 0,
    wcsid: '',
    contract: ''
  };
  @ViewChild('dataTable') table: ElementRef;

  constructor(
    private permissionService: PermissionService,
    public dialog: MatDialog,
    public contractService: ContractService,
    private alert: MyAlertService
  ) {
    this.dataSource.connect().subscribe(d => this.renderedData = d);
  }

  ngOnInit(): void {
    this.selectPermission();
    this.load();

  }

  load(): void {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.dateChange();
    this.contractService.get(this.param).subscribe(
      res => {
        if (res.code == 200 && res.result.status == 'ok') {
          this.datos = res.result.data.Contracts;
          this.dataSource = new MatTableDataSource<Contract>(this.datos);
          this.dataSource.paginator = this.paginator;
        } else {
          this.alert.warn(res?.result?.message || 'Proceso no completado');
        }
        dialogRefLoadingPage.close();
      },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialogRefLoadingPage.close();
      });
  }


  filter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  documentList(orden: Contract) {
    if (orden.contract) {
      this.dialog.open(DocumentsComponent, {
        width: '55%',
        data: orden,
      });
    } else {
      this.alert.warn('Sin no. de contrato');
    }

  }

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
    this.load();
  }

  dateChange() {
    this.param.year = (<Moment>this.date.value).year();
    this.param.month = (<Moment>this.date.value).month() + 1;
  }

  changeStatus() {
    this.load();
  }

  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/store/after-sales/contract');
  }

}
