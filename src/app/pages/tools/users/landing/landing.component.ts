import { Component, OnInit, ViewChild, Input} from '@angular/core';
import { Brand } from 'src/app/core/models/Brand';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BrandService } from 'src/app/core/services/brand.service';
import { MatDialog } from '@angular/material/dialog';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  public arrayPermission = new Array();
  public brand: Brand = new Brand();
  queryValue: string = "";

  brands: MatTableDataSource<Brand> = new MatTableDataSource<Brand>();
  loading = false;

  constructor(
    public dialog: MatDialog,
    private brandService: BrandService,
    private permissionService: PermissionService,
    private alert: MyAlertService
  ) { }

  ngOnInit(): void {
    this.selectPermission();
    this.loadBrands();
  }

  loadBrands() {
    this.loading = true;
    this.brands = new MatTableDataSource<Brand>();
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.brandService.getBrans().subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
        this.brands = new MatTableDataSource<Brand>(response.result.data.brands);
        this.brands.paginator = this.paginator;
        this.brands.sort = this.sort;
        this.applyFilter();
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      this.loading = false;
      dialogRefLoadingPage.close();
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialogRefLoadingPage.close();
        this.loading = false;
      });
  }

  applyFilter() {
    this.brands.filter = this.queryValue.trim().toLowerCase();
  }

  deleteRow(row: any) {
    this.brands.data.splice(this.brands.data.indexOf(row), 1);
    this.brands._updateChangeSubscription();
  }

  deleteBrand(row: any) {
    const dialogConfirm = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        title: 'Confirmación',
        description: 'Desea eliminar la marca?'
      }
    });
    dialogConfirm.afterClosed().subscribe(result => {
      if (result) {
        const dialogLoading = this.dialog.open(LoadingPageDialogComponent, {
          width: '50%',
          data: "",
          disableClose: true
        });
        this.brandService.managementBrand({
          brand_id: row.id.toString(),
          status: 1
        }).subscribe(response => {
          if (response.code == 200 && response.result.status == "ok") {
              this.alert.success("Se ha eliminado la marca " + row.name);
              this.deleteRow(row);
          } else {
            this.alert.warn(response?.result?.message || 'Proceso no completado');
          }
          dialogLoading.close();
        }, err => {
          if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          } 
          dialogLoading.close();
        });
      }
    });
  }

  

  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/store/offer/brands');
  }
}
