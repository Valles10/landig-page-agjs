import { Component, OnInit, Input } from '@angular/core';
import { Tools } from 'src/app/core/helpers/tools';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserService } from 'src/app/core/services/user.service';
import { MatDialog } from '@angular/material/dialog';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { Profile } from 'src/app/core/models/Profile';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { User } from 'src/app/core/models';
import { ProfileService } from 'src/app/core/services/profile.service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'tlbo-profiles-by-user',
  templateUrl: './profiles-by-user.component.html'
})
export class ProfilesByUserComponent implements OnInit {

  public arrayPermission = new Array();

  public queryValue = "";
  private _user: User;
  @Input() set user(user: User) {
    if (user) {
      this.loadProfiles();
      this._user = user;
    }
  };
  get user() {
    return this._user;
  }
  public profiles: Array<Profile> = []
  public filteredProfiles: Array<Profile> = []

  constructor(
    private permissionService: PermissionService,
    private userService: UserService,
    public dialog: MatDialog,
    private profileService: ProfileService,
    private alert: MyAlertService
  ) {

  }

  ngOnInit(): void {
    this.selectPermission();
  }

  applyFilter() {
    this.filteredProfiles = this.profiles.filter((e) => e.name.toLowerCase().indexOf(this.queryValue.toLowerCase()) > -1);
  }

  allowPermissionByUser(event, element: Profile) {
    let status = element.status;
    let checked = element.check;
    if (this.arrayPermission.includes('edit') && status == "A" && this.user.status != 'I') {
      const dialogConfirm = this.dialog.open(ConfirmDialogComponent, {
        width: '400px',
        data: {
          title: 'Confirmación',
          description: 'Seguro desea ' + (checked ? 'inabilitar' : 'habilitar') + ' el perfil: ' + element.name + '?'
        },
      });
      dialogConfirm.afterClosed().subscribe(result => {
        if (result) {
          const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
            width: '80%',
            data: "",
            disableClose: true
          });
          if (checked) {

            this.userService.removeProfile(this.user.idUser.toString(), {
              idProfile: element.id_profile
            }).subscribe(response => {
              if (response.code == 200 && response.result.status == "ok") {
                this.alert.success('Perfil inhabilitado');
                element.check = !checked;
              } else {
                this.alert.warn(response?.result?.message || 'Proceso no completado')
              }
              dialogRefLoadingPage.close();
              event.source.checked = checked;
            },
              err => {
                event.source.checked = checked;
                if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
                  this.alert.warn(err.error.result.message);
                } else {
                  this.alert.error('Proceso no completado');
                }
                dialogRefLoadingPage.close();
              });

          } else {
            this.userService.activeProfile(this.user.idUser.toString(), {
              idProfile: element.id_profile
            }).subscribe(response => {
              if (response.code == 200 && response.result.status == "ok") {
                this.alert.success('Perfil habilitado');
                element.check = !checked;
              } else {
                this.alert.warn(response?.result?.message || 'Proceso no completado')
              }
              dialogRefLoadingPage.close();
              event.source.checked = checked;
            },
              err => {
                event.source.checked = checked;
                if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
                  this.alert.warn(err.error.result.message);
                } else {
                  this.alert.error('Proceso no completado');
                }
                dialogRefLoadingPage.close();
              });
          }

        } else {
          event.source.checked = checked;
        }
      });
    } else {
      event.source.checked = checked;
    }

  }

  loadProfiles() {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.profiles = [];
    this.queryValue = '';
    this.filteredProfiles = []
    this.profileService.get().subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
        if (Array.isArray(response.result.data.userProfiles)) {
          let p = response.result.data.userProfiles;
          this.profileService.getByUser(this.user.idUser.toString()).subscribe(response => {
            if (response.code == 200 && response.result.status == "ok") {
              if (Array.isArray(response.result.data.profiles)) {
                for (let index = 0; index < p.length; index++) {
                  const element = p[index];
                  element.check = false;
                  for (let i = 0; i < response.result.data.profiles.length; i++) {
                    const e = response.result.data.profiles[i];
                    if (element.id_profile == e.id_profile) {
                      element.check = true;
                    }
                  }
                }
                this.profiles = p;
                this.applyFilter();
              } else {
                this.alert.info('No se encontraron perfiles para el usuario');
              }
            } else {
              this.alert.warn(response?.result?.message || 'Proceso no completado');
            }
            dialogRefLoadingPage.close();
          },
            error => {
              if (error.error?.result?.status != 'ok' && error.error?.result?.message) {
                this.alert.warn(error.error.result.message);
              } else {
                this.alert.error('Proceso no completado');
              }
              dialogRefLoadingPage.close();
            });
        } else {
          this.alert.info('No se encontraron perfiles a cargar');
          dialogRefLoadingPage.close();
        }
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
        dialogRefLoadingPage.close();
      }
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialogRefLoadingPage.close();
      });
  }

  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/tools/user-permissions');
  }

}
