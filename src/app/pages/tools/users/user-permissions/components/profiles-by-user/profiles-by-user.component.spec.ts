import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilesByUserComponent } from './profiles-by-user.component';

describe('ProfilesByUserComponent', () => {
  let component: ProfilesByUserComponent;
  let fixture: ComponentFixture<ProfilesByUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilesByUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilesByUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
