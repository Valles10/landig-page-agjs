import { Component, OnInit } from '@angular/core';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { User } from 'src/app/core/models';
import { UserService } from 'src/app/core/services/user.service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PermissionService } from 'src/app/core/services/permission.service';
import { AuthSecureService } from 'src/app/core/services/auth-secure.service';

declare let XLSX: any;

@Component({
  selector: 'app-user-permissions',
  templateUrl: './user-permissions.component.html',
})
export class UserPermissionsComponent implements OnInit {

 /*  public arrayPermission = new Array(); */
  public queryValue = "";
  public loading: boolean = false;
  public users: Array<User> = new Array<User>();
  public filteredUsers: Array<User> = new Array<User>();
  public selectedUsers: Array<User> = new Array<User>();

  constructor(
    public dialog: MatDialog,
    private userService: UserService,
    private alert: MyAlertService,/* 
    private permissionService: PermissionService, */
    private authService: AuthSecureService
  ) {

  }

  ngOnInit(): void {/* 
    this.selectPermission(); */
    this.loadUsers();
  }

  applyFilter() {
    this.filteredUsers = this.users.filter((e) => e.name.toLowerCase().indexOf(this.queryValue.toLowerCase()) > -1);
  }

  loadUsers() {
    this.loading = true;
    this.users = [];
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.userService.getAll().subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
        this.users = response.result.data.users;
        this.users = this.sortUsers();
        this.filteredUsers = this.users.slice();
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      this.loading = false;
      dialogRefLoadingPage.close();
    },
      err => {
        this.loading = false;
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialogRefLoadingPage.close();
      });
  }

  sortUsers() {
    return this.users.sort((n1, n2) => {
      if (n1.idUser < n2.idUser) {
        return 1;
      }
      if (n1.idUser > n2.idUser) {
        return -1;
      }
      return 0;
    });
  }

  downloadCsv() {
    if (this.users?.length > 0) {
      const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
        width: '80%',
        data: "",
        disableClose: true
      });
      this.userService.getReportPermissions().subscribe(response => {
        if (response.code == 200 && response.result.status == "ok") {
          if (response?.result?.data?.userProfile && Array.isArray(response.result.data.userProfile)) {
            let newData: any[] = [];
            for (let index = 0; index < response?.result?.data?.userProfile.length; index++) {
              const element = response?.result?.data?.userProfile[index];
              newData.push({
                'USUARIO': element.userName,
                'DOMINIO': element.userDomain,
                'PERFIL': element.profileName
              });
            }
            const workSheet = XLSX.utils.json_to_sheet(newData);
            const workBook = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(workBook, workSheet, 'Usuarios');
            const date: Date = new Date();
            
            dialogRefLoadingPage.close();
            XLSX.writeFile(workBook, `Perfiles_${this.authService.sessionCountryValue.value}_${(date.getDate())}_${(date.getMonth()+1)}_${date.getFullYear()}.csv`);
          } else {
            this.alert.error('No se pudo obtener el reporte');
          }
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado');
        }
        dialogRefLoadingPage.close();
      },
        err => {
          if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('No se pudo obtener el reporte');
          }
          dialogRefLoadingPage.close();
        });
    } else {
      this.alert.warn('Nada que descargar');
    }
  }

  /* selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/tools/profiles');
  } */
}
