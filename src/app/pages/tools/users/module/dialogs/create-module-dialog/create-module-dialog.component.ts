import { Component, OnInit, Inject } from '@angular/core';
import { Module, User } from 'src/app/core/models';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ModuleService } from 'src/app/core/services/module.service';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { environment } from 'src/environments/environment';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { AuthSecureService } from 'src/app/core/services/auth-secure.service';

@Component({
  selector: 'app-create-module-dialog',
  templateUrl: './create-module-dialog.component.html'
})
export class CreateModuleDialogComponent implements OnInit {

  public module: Module = {
    idModule: 0,
    name: "",
    route: '',
    lock: '1',
    status: 0,
    idPlatform: 0,
    moduleType: 'N'
  };
  private ModuleSaved: Array<Module> = new Array<Module>();
  public moduleForm: FormGroup;
  public currentUser: User;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<CreateModuleDialogComponent>,
    private moduleService: ModuleService,
    @Inject(MAT_DIALOG_DATA) public data,
    private alert: MyAlertService,
    private authService: AuthSecureService) {
      this.currentUser = this.authService.userValue;
  }
  ngOnInit(): void {
    this.moduleForm = new FormGroup({
      name: new FormControl('',
        [
          Validators.required,
          Validators.pattern(/(.|\s)*\S(.|\s)*/)
        ]),
      route: new FormControl('',
        [
          Validators.required,
          Validators.pattern(/(.|\s)*\S(.|\s)*/)
        ]),
        type: new FormControl(['E'].includes(this.data?.module?.moduleType))
    });
    if (this.data.module != null) {
      this.module.idModule = Number(this.data.module.idModule || 0);
      this.module.name = this.data.module.name || '';
      this.module.lock = this.data.module.lock;
      this.module.status = this.data.module.lock;
      this.module.route = this.data.module.route || '';
      this.module.moduleType = this.data.module.moduleType || 'N'; 
    }
    this.module.idPlatform = environment.idPlatform;
  }

  get name() { return this.moduleForm.get('name'); }
  get route() { return this.moduleForm.get('route'); }
  get type() { return this.moduleForm.get('type'); }


  onNoClick(): void {
    this.dialogRef.close();
  }

  save(): void {

    const dialoadRef = this.dialog.open(LoadingPageDialogComponent, {
      width: '400px',
      data: {
        description: ``,
        title: "",
      },
      disableClose: true
    });

    this.module.moduleType = this.type.value ? 'E': 'N';
    if (this.module.idModule == 0) {
      this.moduleService.save({ modules: this.module }).subscribe(response => {
        if (response.code == 200 && response?.result?.status == 'ok') {
          this.ModuleSaved.push(response.result.data.module);
          this.module = {
            idModule: 0,
            name: "",
            route: '',
            lock: '1',
            status: 0,
            idPlatform: environment.idPlatform,
            moduleType: 'N'
          };
          this.alert.success('Se ha creado el modulo ' + this.module.name);
          this.dialogClose();
        } else {
          this.alert.warn(response.result.message || 'Proceso no completado');
        }
        dialoadRef.close();
      },
        err => {
          if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          }
          dialoadRef.close();
        });
    } else {
      this.moduleService.update({ modules: this.module }).subscribe(response => {
        if (response.code == 200 && response.result.status == "ok") {
          this.ModuleSaved.push(this.module);
          this.module = {
            idModule: 0,
            name: "",
            route: '',
            status: 0,
            lock: '1',
            idPlatform: environment.idPlatform,
            moduleType: 'N'
          };
          this.alert.success('Se ha actualizado el modulo ' + this.module.name);
          this.dialogClose();
        } else {
          this.alert.warn(response.result.message || 'Proceso no completado');
        }
        dialoadRef.close();
      },
        err => {
          if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          }
          dialoadRef.close();
        });
    }
  }

  formIsValid() {
    return ((!this.name.valid || this.module.name.replace(/\s/g, '') == '') || (!this.route.valid || this.module.route.replace(/\s/g, '') == ''))
  }

  dialogClose(): void {
    this.dialogRef.close(true);
  }
}
