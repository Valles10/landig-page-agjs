import {Component, OnInit, Inject,Input} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActionsModulesService } from 'src/app/core/services/actions-modules.service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { Actionsprofiles } from 'src/app/core/models/Actions-profies';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { Module } from 'src/app/core/models';

@Component({
    selector:'app-action-module-dialog',
    templateUrl: './action-module-dialog.html'
})
export class ActionModuleDialogComponent implements OnInit{
    public queryValue = "";
    loading = false;

    action: Array<Actionsprofiles> = [];
    filteredActions: Array<Actionsprofiles> = [];
    modules: Module;

    title = this.data.module.name;

    constructor(
        public dialog: MatDialog,
        public dialogRef: MatDialogRef<ActionModuleDialogComponent>,
        private actionsModulesService: ActionsModulesService,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private alert: MyAlertService
    ) { }

    applyFilter() {
        this.filteredActions = this.action.filter((e) => e.name.toLowerCase().indexOf(this.queryValue.toLowerCase()) > -1);
    }

    ngOnInit(): void{
        this.loadActions();
    }

    loadActions(){
        this.loading = true;
        const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
        width: '80%',
        data: "",
        disableClose: true
        });
        this.queryValue = '';
        this.action = [];
        this.filteredActions = [];
        this.actionsModulesService.getActions(this.data.module.idModule).subscribe(response => {
            if (response.code == 200 && response.result.status == "ok") {
                if (Array.isArray(response.result.data.actions)) {
                    let actionsTemp = response.result.data.actions;
                    this.action = actionsTemp.slice();
                    this.applyFilter();
                    for(let i = 0 ; i < actionsTemp.length; i++) {
                        let act = this.action[i];
                        for(let index =0; index < response.result.data.actions.length; index ++) {
                            if (Number(act.idModule) != 0 ) {
                                act.estado = 'alta';
                            } else {
                                if (!act.estado) {
                                act.estado = 'baja';
                                }
                            }
                        }
                            act.oldStatus = act.estado;
                        }
                } else {
                    this.alert.info('No se encontraron acciones para este modulo');
                }
            } else {
                this.alert.warn(response?.result?.message || 'Proceso no completado');
            }
            this.loading = false;
            dialogRefLoadingPage.close();
            },
                err => {
                if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
                    this.alert.warn(err.error.result.message);
                } else {
                    this.alert.error('Proceso no completado');
                }
                dialogRefLoadingPage.close();
                this.loading = false;
            });
    }

    save(): void {
        let upActions: Array<any> = [];
        for (let index = 0; index < this.action.length; index++) {
            const element = this.action[index];
            const mod = this.data.module.idModule;
            if (element.oldStatus != element.estado) {
                upActions.push({
                    status: element.estado,
                    idAction: element.id_action,
                    idModule: mod,
                    idActionModule: element.idActionModule,
                    idViewActionModule: element.id_view_action_module
                });
            }
        }
        if (upActions.length > 0) {
            const dialoadRef = this.dialog.open(LoadingPageDialogComponent, {
                width: '400px',
                data: {
                    name: ``,
                    title: "",
                },
                disableClose: true
            });
            this.actionsModulesService.saveActions({
                Actions: upActions
            }).subscribe(response => {
                if (response.code == 200 && response.result.status == "ok") {
                    this.alert.success('Cambios guardados exitosamente');
                    this.dialogRef.close(true);
                } else {
                    this.alert.warn(response?.result?.message || 'Proceso no completado');
                }
                dialoadRef.close();
            },
                err => {
                    if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
                        this.alert.warn(err.error.result.message);
                    } else {
                        this.alert.error('Proceso no completado');
                    }
                    dialoadRef.close();
                });
        } else {
            this.alert.info('Sin cambios para guardar');
        }
    }

    allowAction(event, action: Actionsprofiles) {
        let status = action.idModule;
        action.idModule = status == 0 ? 1 : 0;
        event.source.checked = action.idModule != 0 ? true : false;
        if (status == 0) {
            action.estado = 'alta';
        } else {
            action.estado = 'baja';
        }
    }
}
