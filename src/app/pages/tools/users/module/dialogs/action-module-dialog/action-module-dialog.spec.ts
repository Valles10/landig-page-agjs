import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionModuleDialogComponent } from './action-module-dialog';

describe('ActionModuleDialogComponent', () => {
  let component: ActionModuleDialogComponent;
  let fixture: ComponentFixture<ActionModuleDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionModuleDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionModuleDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});