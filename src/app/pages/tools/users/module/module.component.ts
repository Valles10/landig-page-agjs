import { Component, OnInit, ViewChild, SimpleChanges } from '@angular/core';
import { Module } from 'src/app/core/models';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { ModuleService } from 'src/app/core/services/module.service';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { CreateModuleDialogComponent } from './dialogs/create-module-dialog/create-module-dialog.component';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { environment } from 'src/environments/environment';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PermissionService } from 'src/app/core/services/permission.service';
import { ActionModuleDialogComponent } from './dialogs/action-module-dialog/action-module-dialog';

@Component({
  selector: 'app-module',
  templateUrl: './module.component.html',
})
export class ModuleComponent implements OnInit {


  public viewOptions: ViewOptions = {
    displayedColumns: ['name', 'url', 'status',  /* 'ACCION', */ 'actions'],
    pageSize: environment.pagination,
    loading: true
  };

  public arrayPermission = new Array();
  public queryValue = "";
  public loading: boolean = false;
  private modules: Array<Module> = new Array<Module>();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  dataSource: MatTableDataSource<Module> = new MatTableDataSource<Module>();

  constructor(
    public dialog: MatDialog,
    private moduleService: ModuleService,
    private permissionService: PermissionService,
    private alert: MyAlertService
  ) {
  }

  ngOnInit(): void {
    this.selectPermission();
    this.loadModules();
  }

  openDialog(module) {
    const dialogActionDetail = this.dialog.open(ActionModuleDialogComponent, {
      width: '60%',
      data: {
        module: module,
      }
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getStatusTooltip(element: Module) {
    return (element.lock == "1" ? 'Inabilitar' : element.lock == "0" ? 'Habilitar' : 'Datos inconsistenes de') + ' modulo';
  }

  onEdit(module: Module) {
    const dialogRefLoadingPage = this.dialog.open(CreateModuleDialogComponent, {
      width: '60%',
      data: {
        module: module
      },
    });

    dialogRefLoadingPage.afterClosed().subscribe(result => {
      if (result ) {
        this.loadModules();
      }
    });
  }

  loadModules() {
    this.loading = true;
    this.modules = [];
    this.dataSource = new MatTableDataSource<Module>(this.modules);
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.moduleService.get().subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
        if (Array.isArray(response.result.data.modules)) {
          this.modules = response.result.data.modules;
          this.modules = this.sortModules();
          this.dataSource = new MatTableDataSource<Module>(this.modules);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.applyFilter(this.queryValue);
        } else {
          this.alert.warn("No se encontraron datos");
        }
      } else {
        this.alert.warn(response?.result?.message || "Proceso no completado");
      }
      this.loading = false;
      dialogRefLoadingPage.close();
    },
      err => {
        dialogRefLoadingPage.close();
        this.loading = false;
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  createForm() {
    const dialogRefLoadingPage = this.dialog.open(CreateModuleDialogComponent, {
      width: '50%',
      data: {
      },
    });

    dialogRefLoadingPage.afterClosed().subscribe(result => {
      if (result) {
        this.loadModules();
      }
    });
  }

  allowModule(element: Module) {
    let lock = element.lock;
    if (lock == "0" || lock == "1") {
      const dialogRefLoadingPage = this.dialog.open(ConfirmDialogComponent, {
        width: '400px',
        data: {
          title: 'Confirmación',
          description: 'Seguro desea ' + (lock == "1" ? 'inabilitar' : 'habilitar') + ' el modulo: ' + element.name + '?'
        }
      });
      dialogRefLoadingPage.afterClosed().subscribe(result => {
        if (result) {
          const dialoadRef = this.dialog.open(LoadingPageDialogComponent, {
            width: '400px',
            data: {
              description: ``,
              title: "",
            },
            disableClose: true
          });
          this.moduleService.update({
            modules: {
              idModule: element.idModule,
              status: lock == "1" ? 0 : 1,
              name: element.name,
              route: element.route || '',
              moduleType: element.moduleType
            }
          }).subscribe(response => {
            if (response.code == 200 && response.result.status == "ok") {
                element.lock = lock == "1" ? "0" : "1";
                this.alert.success('Modulo actualizado con exito');
            } else {
              this.alert.warn(response?.result?.message || "Proceso no completado");
            }
            dialoadRef.close();
          },
            err => {
              if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
                this.alert.warn(err.error.result.message);
              } else {
                this.alert.error('Proceso no completado');
              }
              dialoadRef.close();
            });
        }
      });
    }
  }

  sortModules() {
    return this.modules.sort((n1, n2) => {
      if (n1.idModule < n2.idModule) {
        return 1;
      }
      if (n1.idModule > n2.idModule) {
        return -1;
      }
      return 0;
    });
  }

  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/tools/modules');
  }

}
