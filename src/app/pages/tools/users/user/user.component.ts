import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { User } from 'src/app/core/models';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { UserService } from 'src/app/core/services/user.service';
import { Tools } from 'src/app/core/helpers/tools';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { CreateUserDialogComponent } from './dialogs/create-user-dialog/create-user-dialog.component';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PermissionService } from 'src/app/core/services/permission.service';
import { AuthSecureService } from 'src/app/core/services/auth-secure.service';

declare let XLSX: any;

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
})
export class UserComponent implements OnInit {

  public viewOptions: ViewOptions = {
    displayedColumns: ['name', 'username', 'email', 'domain', 'update_at', 'status', 'ACCION'],
    pageSize: [25, 35, 50],
    loading: true
  };
  public arrayPermission = new Array();
  public queryValue = "";
  public loading: boolean = false;
  public users: Array<User> = new Array<User>();
  private tools: any;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: MatTableDataSource<User> = new MatTableDataSource<User>();

  constructor(
    _snackbar: MatSnackBar,
    public dialog: MatDialog,
    private userService: UserService,
    private alert: MyAlertService,
    private authService: AuthSecureService,
    private permissionService: PermissionService,
  ) {
    this.tools = new Tools(_snackbar);
  }

  ngOnInit(): void {
    this.selectPermission();
    this.loadUsers();
  }

  applyFilter() {
    this.dataSource.filter = this.queryValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  getStatusTooltip(element: User) {
    return (element.status == "A" ? 'Inabilitar' : element.status == "I" ? 'Habilitar' : 'Datos inconsistenes de') + ' usuario';
  }

  allowUser(element: User) {
    let status = element.status;
    if (status == "A" || status == "I") {
      const dialogRefLoadingPage = this.dialog.open(ConfirmDialogComponent, {
        width: '400px',
        data: {
          title: 'Confirmación',
          description: 'Seguro desea ' + (status == "A" ? 'inabilitar' : 'habilitar') + ' el usuario: ' + element.userName + '?'
        },
      });
      dialogRefLoadingPage.afterClosed().subscribe(result => {
        if (result) {
          const dialoadRef = this.dialog.open(LoadingPageDialogComponent, {
            width: '400px',
            data: {
              description: ``,
              title: "",
            },
            disableClose: true
          });
          this.userService.update({
            idUser: element.idUser,
            name: element.name,
            userName: element.userName,
            email: element.email,
            status: status == 'A' ? "I" : "A",
            userType: element.userType || '',
            country: element.country,
            oldUserName: element.userName
          }).subscribe(response => {
            if (response.code == 200 && response.result == "Exitoso") {
              element.status = status == 'A' ? "I" : "A";
              element.updatedAt = this.tools.getTimestamp('yyyy-MM-dd hh:mm:ss.s');
              this.alert.success('Usuario actualizado con exito');
            } else {
              this.alert.warn(response?.result?.message || 'Proceso no completado');
            }
            dialoadRef.close();
          },
            err => {
              dialoadRef.close();

              if (err.error?.result?.message) {
                this.alert.warn(err.error.result.message);
              } else {
                if (err.error?.result) {
                  this.alert.warn(err.error?.result);
                } else {
                  this.alert.error('Proceso no completado');
                }
              }
            });
        }
      });
    }
  }

  onEdit(user: User) {
    const dialogRefLoadingPage = this.dialog.open(CreateUserDialogComponent, {
      width: '60%',
      data: {
        user: user
      }
    });
    dialogRefLoadingPage.afterClosed().subscribe(result => {
      if (result) {
        this.loadUsers();
      }
    });
  }

  loadUsers() {
    this.loading = true;
    this.users = [];
    this.dataSource = new MatTableDataSource(this.users);
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.userService.getAll().subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
        if (Array.isArray(response.result.data.users)) {
          this.users = response.result.data.users;
          this.dataSource = new MatTableDataSource(this.users);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.applyFilter();
        } else {
          this.alert.info('No se encontraron datos');
        }
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado')
      }
      this.loading = false;
      dialogRefLoadingPage.close();
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialogRefLoadingPage.close();
        this.loading = false;

      });
  }


  createForm() {
    const dialogRefLoadingPage = this.dialog.open(CreateUserDialogComponent, {
      width: '50%',
      data: {
      }
    });
    dialogRefLoadingPage.afterClosed().subscribe(result => {
      if (result) {
        this.loadUsers();
      }
    });
  }

  downloadCsv() {
    if (this.users?.length > 0) {
      const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
        width: '80%',
        data: "",
        disableClose: true
      });
      let newData: any[] = [];
      for (let index = 0; index < this.users.length; index++) {
        const element = this.users[index];
        newData.push({
          'USUARIO': element.userName,
          'NOMBRE': element.name,
          'CORREO': element.email,
          'TIPO USUARIO': (element.userType == 'R' ? 'Regional' : element.userType == 'L' ? 'Local' : 'No definido'),
          'ESTADO': (element.status == 'A' ? 'Activo' : element.status == 'I' ? 'Inactivo' : 'No definido'),
          'DOMINIO': element.isoCode,
          'FECHA CREACION': element.createdAt,
          'FECHA ACTUALIZACION': element.updatedAt
        });
      }
      const workSheet = XLSX.utils.json_to_sheet(newData);
      const workBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(workBook, workSheet, 'Usuarios');
      const date = new Date();
      dialogRefLoadingPage.close();
      XLSX.writeFile(workBook, `Usuarios_${this.authService.sessionCountryValue.value}_${(date.getDate())}_${(date.getMonth()+1)}_${date.getFullYear()}.csv`);
    } else {
      this.alert.warn('Nada que descargar');
    }
  }

  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/tools/users');
  }
}

