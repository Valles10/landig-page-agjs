import { Component, OnInit, Inject } from '@angular/core';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Tools } from 'src/app/core/helpers/tools';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserService } from 'src/app/core/services/user.service';
import { User } from 'src/app/core/models';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { CountryMock } from 'src/app/core/mocks/country.mock';
import { AuthSecureService } from 'src/app/core/services/auth-secure.service';
import { Profile } from 'src/app/core/models/Profile';
import { ProfileService } from 'src/app/core/services/profile.service';

@Component({
  selector: 'app-create-user-dialog',
  templateUrl: './create-user-dialog.component.html',
})
export class CreateUserDialogComponent implements OnInit {

  public currentUser: User;
  public userForm: FormGroup;
  public user: User;
  private tools: Tools;
  public profiles: Profile[] = [];
  public sessionCountry;
  nameFormControl = new FormControl('', [
    Validators.required,

  ]);
  public countries: any[] = [];
  private tempCountry;
  constructor(
    private _snackBar: MatSnackBar,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<CreateUserDialogComponent>,
    private userService: UserService,
    @Inject(MAT_DIALOG_DATA) public data,
    private alert: MyAlertService,
    private authService: AuthSecureService,
    private profileService: ProfileService) {
    this.tools = new Tools(_snackBar);
    this.currentUser = this.authService.userValue;
      this.sessionCountry = this.authService?.sessionCountryValue?.code?.toString();
      console.log();
  }

  ngOnInit(): void {
    this.countries = CountryMock.getCountries();
    this.reset();

    this.userForm = new FormGroup({
      'name': new FormControl('', [
        Validators.required,
        Validators.pattern(/(.|\s)*\S(.|\s)*/)
      ]),
      'userName': new FormControl('', [
        Validators.required,
        Validators.pattern(/(.|\s)*\S(.|\s)*/)
      ]),
      'email': new FormControl(null, [
        Validators.required,
        Validators.email,
        Validators.pattern(/(.|\s)*\S(.|\s)*/)
      ]),
      type: new FormControl(this.data?.user?.userType == 'R' ? true : false),
      country: new FormControl({
        /* value: this.data?.user?.country?.toString() || this.user?.country?.toString() */
        value: this.data?.user?.country?.toString() || this.sessionCountry
        , disabled: this.data.user
      }),
      profile: new FormControl('')
    });

    if (this.data.user != null) {
      this.user.idUser = this.data.user.idUser;
      this.user.name = this.data.user.name;
      this.user.userName = this.data.user.userName;
      this.user.country = this.data.user.country;
      this.user.email = this.data.user.email;
      this.user.status = this.data.user.status;
      this.user.userType = this.data?.user?.userType || 'N';
      this.user.oldUserName = this.user.userName;
    } else {
      this.getProfiles();
    }
    this.tempCountry = this.data?.user?.country?.toString() || this.sessionCountry;

  }

  get name() { return this.userForm.get('name'); }
  get userName() { return this.userForm.get('userName'); }
  get email() { return this.userForm.get('email'); }
  get type() { return this.userForm.get('type'); }
  get country() { return this.userForm.get('country'); }
  get profile() { return this.userForm.get('profile'); }

  onNoClick(): void {
    this.dialogRef.close();
  }

  reset() {
    this.user = {
      idUser: 0,
      name: '',
      userName: '',
      status: "A",
      country: this.tools.getCountryCode(),
      updatedAt: '',
      email: '',
      userType: 'N',
      oldUserName: ''
    };
  }


  getProfiles() {
    const dialoadRef = this.dialog.open(LoadingPageDialogComponent, {
      width: '400px',
      data: {
        description: ``,
        title: "",
      },
      disableClose: true
    });

    this.profileService.get().subscribe(
      res => {
        if (res.code == 200 && res.result.status == "ok") {
          this.profiles = res.result.data.userProfiles;
        } else {
          this.alert.warn(res?.result?.message || 'Proceso no completado');
        }
        dialoadRef.close();
      },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialoadRef.close();
      }
    );
  }

  saveUser(): void {
    if (this.type.value) {
      this.user.userType = 'R';
      this.user.country = Number(this.country?.value);
    } else {
      this.user.userType = 'L';
      if (this.country.value == this.tempCountry) {
        this.user.country = Number(this.tempCountry);
      } else {
        this.user.country = Number(this.authService.sessionCountryValue.code);
      }
    }
    if (this.data.user != null) {
      this.user.country = Number(this.tempCountry);
    } else {
      this.user.country = this.type?.value ? Number(this.country?.value) : Number(this.tempCountry);
    }
    this.user.profile = this.profile?.value?.toString() || '0';
    this.save();
  }


  save() {
    const dialoadRef = this.dialog.open(LoadingPageDialogComponent, {
      width: '400px',
      data: {
        description: ``,
        title: "",
      },
      disableClose: true
    });
    if (this.user.idUser == 0) {
      this.userService.save(this.user).subscribe(response => {
        if (response.code == 200) {
          this.alert.success('Se ha creado el usuario ' + this.user.userName);

          if (this.user.profile?.toString() != '0' && response?.result?.user?.idUser) {
            this.userService.activeProfile(response.result.user.idUser.toString(), {
              idProfile: this.profile?.value
            }).subscribe(response => {
              if (response.code == 200 && response.result.status == "ok") {
                this.alert.success('Perfil habilitado');
              } else {
                this.alert.warn(response?.result?.message || 'Proceso no completado')
              }
              dialoadRef.close();
              this.dialogClose();
            },
              err => {
                if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
                  this.alert.warn(err.error.result.message);
                } else {
                  this.alert.error('Perfil no fue asignado');
                }
                dialoadRef.close();
                this.dialogClose();
              });

          } else {
            this.reset();
            dialoadRef.close();
            this.dialogClose();
          }
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado');
          dialoadRef.close();
          this.dialogClose();
        }
      },
        err => {
          if (err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          }
          dialoadRef.close();
        });
    } else {
      this.userService.update(this.user).subscribe(response => {
        if (response.code == 200) {
          this.reset();
          this.alert.success('Se ha actualizado el usuario ' + this.user.userName);
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado');
        }
        this.dialogClose();
        dialoadRef.close();
      },
        err => {
          if (err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            if (err.error?.result) {
              this.alert.warn(err.error?.result);
            } else {
              this.alert.error('Proceso no completado');
            }
          }
          dialoadRef.close();
        });
    }

  }

  formIsValid() {
    return (!this.userForm.valid || this.name.value.replace(/\s/g, '') == '' || this.userName.value.replace(/\s/g, '') == '')
  }

  dialogClose(): void {

    this.dialogRef.close(true);
  }

}
