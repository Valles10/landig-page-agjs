import { Component, OnInit, Inject } from '@angular/core';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { ProfileService } from 'src/app/core/services/profile.service';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Profile } from 'src/app/core/models/Profile';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { User } from 'src/app/core/models';
import { AuthSecureService } from 'src/app/core/services/auth-secure.service';

@Component({
  selector: 'app-create-profile-dialog',
  templateUrl: './create-profile-dialog.component.html',
})
export class CreateProfileDialogComponent implements OnInit {

  public profile: Profile = {
    id_profile: 0,
    name: "",
    description: '',
    updated_at: '',
    status: 'A',
    base_profile: '0',
    profileType: 'N'
  };
  public profiles: Array<Profile> = new Array<Profile>();
  private userProfileSaved: Array<Profile> = new Array<Profile>();
  public profileForm: FormGroup;
  public currentUser: User;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<CreateProfileDialogComponent>,
    private profileService: ProfileService,
    @Inject(MAT_DIALOG_DATA) public data,
    private alert: MyAlertService,
    private authService: AuthSecureService) {
    this.currentUser = this.authService.userValue;

  }
  ngOnInit(): void {
    this.profileForm = new FormGroup({
      name: new FormControl('',
        [
          Validators.required,
          Validators.pattern(/(.|\s)*\S(.|\s)*/)
        ]),
      description: new FormControl('',
        [
          Validators.pattern(/(.|\s)*\S(.|\s)*/)
        ]),
      type: new FormControl(['E'].includes(this.data?.profile?.profileTye))
    });

    if (this.data.profile != null) {
      this.profile.status = this.data.profile?.status;
      this.profile.id_profile = Number(this.data.profile?.id_profile || 0);
      this.profile.name = this.data.profile?.name || '';
      this.profile.description = this.data.profile?.description || '';
      this.profile.profileType = this.data.profile?.profileType || 'N';
    }

    if (this.data.profiles) {
      this.profiles = this.data.profiles;
    }
  }

  get name() { return this.profileForm.get('name'); }
  get description() { return this.profileForm.get('description'); }
  get type() { return this.profileForm.get('type'); }

  reset() {
    this.profile = {
      id_profile: 0,
      name: "",
      description: '',
      updated_at: '',
      status: 'A',
      base_profile: '0',
      profileType: 'N'
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  saveProfile(): void {
    const dialoadRef = this.dialog.open(LoadingPageDialogComponent, {
      width: '400px',
      data: {
        description: ``,
        title: "",
      },
      disableClose: true
    });
    this.profile.profileType = this.type.value ? 'E' : 'N';
    if (this.profile.id_profile) {
      this.profileService.update(this.profile).subscribe(response => {
        if (response.code == 200 && response.result.status == "ok") {
          this.userProfileSaved.push(this.profile);
          this.reset();
          this.dialogClose();
          this.alert.success('Se ha actualizado el perfil ' + this.profile.name);

        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado');
        }
        dialoadRef.close();
      },
        err => {
          if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          }
          dialoadRef.close();
        });
    } else {
      this.profileService.save(this.profile).subscribe(response => {
        if (response.code == 200 && response.result.status == "ok") {
         
          this.userProfileSaved.push(response.result.data.userProfile);
          this.reset();
          this.alert.success('Se ha creado el perfil ' + this.profile.name);
          this.dialogClose();
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado');
        }
        dialoadRef.close();
      },
        err => {
          if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          }
          dialoadRef.close();
        });
    }
  }

  dialogClose(): void {
    this.dialogRef.close(true);
  }

}
