import { Component, OnInit } from '@angular/core';
import { Profile } from 'src/app/core/models/Profile';
import { MatDialog } from '@angular/material/dialog';
import { ProfileService } from 'src/app/core/services/profile.service';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { CreateProfileDialogComponent } from './dialogs/create-profile-dialog/create-profile-dialog.component';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
})
export class ProfileComponent implements OnInit {

  public arrayPermission = new Array();
  public queryValue = "";
  public loading: boolean = false;
  private profiles: Array<Profile> = new Array<Profile>();
  public filteredProfiles: Array<Profile> = new Array<Profile>();
  public selectedProfiles: Array<Profile> = new Array<Profile>();

  constructor(
    public dialog: MatDialog,
    private profileService: ProfileService,
    private permissionService: PermissionService,
    private alert: MyAlertService
  ) {
  }

  ngOnInit(): void {
    this.selectPermission();
    this.loadUserProfiles();
  }

  applyFilter() {
    this.filteredProfiles = this.profiles.filter((e) => e.name.toLowerCase().indexOf(this.queryValue.toLowerCase()) > -1);
  }

  onEdit(event, profile) {
    event.stopPropagation();
    const dialogRefLoadingPage = this.dialog.open(CreateProfileDialogComponent, {
      width: '60%',
      data: {
        profile: profile,
        profiles: this.profiles
      }
    });

    dialogRefLoadingPage.afterClosed().subscribe(result => {
      if (result) {
        this.loadUserProfiles();
      }
    });
  }

  onDelete(event, profile) {
    event.stopPropagation();
  }

  loadUserProfiles() {
    this.loading = true;
    this.profiles = [];
  this.applyFilter();
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.profileService.get().subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
        if (Array.isArray(response.result.data.userProfiles)) {
          this.profiles = response.result.data.userProfiles;
          this.applyFilter();
        } else {
          this.alert.info('No se encontraron perfiles');
        }
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      this.loading = false;
      dialogRefLoadingPage.close();
    },
      err => {
        dialogRefLoadingPage.close();
        this.loading = false;
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }

  createForm() {
    const dialogRefLoadingPage = this.dialog.open(CreateProfileDialogComponent, {
      width: '60%',
      data: {
        profiles: this.profiles
      },
    });
    dialogRefLoadingPage.afterClosed().subscribe(result => {
      if (result ) {
            this.loadUserProfiles();
      }
    });
  }

  getStatusTooltip(element: Profile) {
    return (element.status == "A" ? 'Inabilitar' : element.status == "I" ? 'Habilitar' : 'Datos inconsistenes de') + ' perfil';
  }

  allowProfile(event, element: Profile) {
    event.stopPropagation();
    let lock = element.status;
    if (lock == "A" || lock == "I") {
      const dialogRefLoadingPage = this.dialog.open(ConfirmDialogComponent, {
        width: '400px',
        data: {
          title: 'Confirmación',
          description: 'Seguro desea ' + (status == "A" ? 'inabilitar' : 'habilitar') + ' el perfil: ' + element.name + '?'
        }
      });
      dialogRefLoadingPage.afterClosed().subscribe(result => {
        if (result) {
          const dialoadRef = this.dialog.open(LoadingPageDialogComponent, {
            width: '400px',
            data: {
              description: ``,
              title: "",
            },
            disableClose: true
          });
          this.profileService.update({
            id_profile: element.id_profile,
            status: lock == "A" ? "I" : "A",
            name: element.name,
            description: element.description || '',
            profileType: element.profileType || 'N'
          }).subscribe(response => {
            if (response.code == 200 && response.result.status == "ok") {
              element.status = lock == "A" ? "I" : "A";
              this.alert.success('Perfil ' + element?.name + (element.status == "A" ? ' habilitado' : ' inhabilitado') + ' con exito');
            } else {
              this.alert.warn(response?.result?.message || 'Proceso no completado')
            }
            dialoadRef.close();
          },
            err => {
              if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
                this.alert.warn(err.error.result.message);
              } else {
                this.alert.error('Proceso no completado');
              }
              dialoadRef.close();
            });
        }
      });
    } else {
      this.alert.warn('Estado de perfil no valido');
    }
  }

  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/tools/profiles');
  }
}
