import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActionsProfilesService } from 'src/app/core/services/actions-profiles.service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { Actionsprofiles } from 'src/app/core/models/Actions-profies';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';

@Component({
  selector: 'app-select-actions',
  templateUrl: './select-actions.component.html',
})
export class SelectActionsComponent implements OnInit {

  public queryValue = "";
  loading = false;

  action: Array<Actionsprofiles> = [];
  filteredActions: Array<Actionsprofiles> = [];
  titleProfile = this.data.profile.name;
  titleModule = this.data.module.name;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<SelectActionsComponent>,
    private actionsProfilesService: ActionsProfilesService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private alert: MyAlertService
  ) { }

  applyFilter() {
    this.filteredActions = this.action.filter((e) => e.name.toLowerCase().indexOf(this.queryValue.toLowerCase()) > -1);
  }

  ngOnInit(): void {
    this.loadActions();
  }

  loadActions() {
    this.loading = true;
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.queryValue = '';
    this.action = [];
    this.filteredActions = [];
    this.actionsProfilesService.getActions(this.data.profile.id_profile, this.data.module.idModule).subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
        if (Array.isArray(response.result.data.actions)) {
          let actionsTemp = response.result.data.actions;
          this.action = actionsTemp.slice();
          this.applyFilter();
          for(let i = 0 ; i < actionsTemp.length; i++) {
            let act = this.action[i];
            for(let index =0; index < response.result.data.actions.length; index ++) {
              if (Number(act.idProfile) != 0 ) {
                act.estado = 'alta';
              } else {
                if (!act.estado) {
                  act.estado = 'baja';
                }
              }
            }
            act.oldStatus = act.estado;
          }
        } else {
          this.alert.info('No se encontraron acciones para este modulo');
        }
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      this.loading = false;
      dialogRefLoadingPage.close();
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialogRefLoadingPage.close();
        this.loading = false;
      });
  }

  save():void {
    let upActions: Array<any> = [];
    for (let index = 0; index < this.action.length; index++) {
      const element = this.action[index];
      const prof = this.data.profile.id_profile;
      if (element.oldStatus != element.estado) {
        upActions.push({
          status: element.estado,
          idAction: element.id_action,
          id_view_action_module: element.id_view_action_module,
          idProfile: prof,
          idActionModule: element.idActionModule
        });
      }
    }
    if (upActions.length > 0 ) {
      const dialoadRef = this.dialog.open(LoadingPageDialogComponent, {
        width: '400px',
        data: {
          name: ``,
          title: "",
        },
        disableClose: true
      });
      this.actionsProfilesService.saveActions({
        Actions: upActions
      }).subscribe(response => {
        if (response.code == 200 && response.result.status == "ok") {
          this.alert.success('Cambios guardados exitosamente');
          this.dialogRef.close(true);
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado');
        }
        dialoadRef.close();
      },
        err => {
          if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          }
          dialoadRef.close();
        });
    } else {
      this.alert.info('Sin cambios para guardar');
    }
  }

  allowAction(event, action: Actionsprofiles) {
    let status = action.idProfile;
    action.idProfile = status == 0 ? 1 : 0;
    event.source.checked = action.idProfile != 0 ? true : false;
    if (status == 0) {
      action.estado = 'alta';
    } else {
      action.estado = 'baja';
    }
  }
}
