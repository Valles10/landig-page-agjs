import { Component, OnInit, Input } from '@angular/core';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { Tools } from 'src/app/core/helpers/tools';
import { ModuleService } from 'src/app/core/services/module.service';
import { Module } from 'src/app/core/models';
import { Profile } from 'src/app/core/models/Profile';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PermissionService } from 'src/app/core/services/permission.service';
import { SelectActionsComponent } from '../select-actions/select-actions.component';

@Component({
  selector: 'ecom-permissions',
  templateUrl: './permissions.component.html',
})
export class PermissionsComponent implements OnInit {

  public arrayPermission = new Array();

  modules: Array<Module> = [];
  filteredModules: Array<Module> = [];

  public queryValue = "";
  _profile: Profile;

  @Input() set profile(profile: any) {
    this._profile = profile;
    if (profile) {
      this.loadModules();
    }
  };

  get profile() {
    return this._profile;
  }


  constructor(
    public dialog: MatDialog,
    private moduleService: ModuleService,
    private permissionService: PermissionService,
    private alert: MyAlertService

  ) {
  }


  applyFilter() {
    this.filteredModules = this.modules.filter((e) => e.name.toLowerCase().indexOf(this.queryValue.toLowerCase()) > -1);
  }
  
  ngOnInit(): void {
    this.selectPermission();
  }

  /* saveModules(): void {
    if(this._profile.status != 'I'){
      let upModules: Array<any> = [];
      for (let index = 0; index < this.modules.length; index++) {
        const element = this.modules[index];
        if (element.oldStatus != element.status) {
          upModules.push({
            idPermission: element.idModule,
            status: element.status
          });
        }
      }
      if (upModules.length > 0 && this._profile.id_profile && this._profile.status == 'A') {
        const dialoadRef = this.dialog.open(LoadingPageDialogComponent, {
          width: '400px',
          data: {
            name: ``,
            title: "",
          },
          disableClose: true
        });
        this.moduleService.saveByProfile(this._profile.id_profile, {
          userProfiles: upModules
        }).subscribe(response => {
          if (response.code == 200 && response.result.status == "ok") {
            this.alert.success('Cambios guardados exitosamente');
          } else {
            this.alert.warn(response?.result?.message || 'Proceso no completado');
          }
          dialoadRef.close();
        },
          err => {
            if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
              this.alert.warn(err.error.result.message);
            } else {
              this.alert.error('Proceso no completado');
            }
            dialoadRef.close();
          });
      } else {
        this.alert.info('Sin cambios para guardar');
      }
    }else{
      this.alert.warn('Pefil no se puede realizar cambios');
    }
  } */

  loadModules() {

    if (this._profile.id_profile) {
      const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
        width: '80%',
        data: "",
        disableClose: true
      });
      this.queryValue = '';
      this.modules = [];
      this.filteredModules = [];
      this.moduleService.get().subscribe(response => {
        if (response.code == 200 && response.result.status == "ok") {
            if (Array.isArray(response.result.data.modules)) {
              let modulesTemp = response.result.data.modules;
                this.modules = modulesTemp.slice();
                this.applyFilter();
              /* this.moduleService.getByProfile(this._profile.id_profile).subscribe(response => {
                if (response.code == 200 && response.result.status == "ok") {
                    if (Array.isArray(response.result.data.modules)) {
                      for (let i = 0; i < modulesTemp.length; i++) {
                        let mod = modulesTemp[i];
                        for (let index = 0; index < response.result.data.modules.length; index++) {
                          let element = response.result.data.modules[index];
                          if (Number(element.idPermission) == Number(mod.idModule)) {
                            mod.status = 1;
                          } else {
                            if (!mod.status) {
                              mod.status = 0;
                            }
                          }
                        }
                        mod.oldStatus = mod.status;
                      }
                      this.modules = modulesTemp.slice();
                      this.applyFilter();
                    } else {
                      this.alert.info('No se encontraron modulos del perfil');
                    }
                } else {
                  this.alert.warn(response?.result?.message || 'Proceso no completado')
                }
                dialogRefLoadingPage.close();
              },
                error => {
                  if (error.error?.result?.status != 'ok' && error.error?.result?.message) {
                    this.alert.warn(error.error.result.message);
                  } else {
                    this.alert.error('Proceso no completado');
                  }
                  dialogRefLoadingPage.close();
                }); */
            } else {
              this.alert.info('No se encontraron modulos a cargar');
            }
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado');
        }
        dialogRefLoadingPage.close();
      },
        err => {
          if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          }
          dialogRefLoadingPage.close();
        });
    } else {
      this.alert.info('No se puede cargar ningun modulo');

    }
  }

  /* allowModule(event, module: Module) {
    if (module.lock == '1' && this._profile.status == 'A') {
      let status = module.status;
      module.status = status == 1 ? 0 : 1;
      event.source.checked = module.status == 1 ? true : false;
    } else {
      this.alert.info('Cambios no permitidos');
    }
  } */

  getActions(module, profile) {
    if(this.arrayPermission.includes('edit')){
      const dialogActionDetail = this.dialog.open(SelectActionsComponent, {
        width: '60%',
        data: {
          module: module,
          profile: profile
        }
      });
    }
  }

  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/tools/profiles');
  }
}
