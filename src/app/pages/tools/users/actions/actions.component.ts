import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { Action } from 'src/app/core/models/Actions';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActionService } from 'src/app/core/services/action.service';
import { MatDialog } from '@angular/material/dialog';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { ActionsCreateDialogComponent } from './dialogs/actions-create-dialog/actions-create-dialog.component';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PermissionService } from 'src/app/core/services/permission.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html'
})
export class ActionsComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('formName') formName: ElementRef;

  public arrayPermission = new Array();
  public action: Action = new Action();
  queryValue: string = "";

  actions: MatTableDataSource<Action> = new MatTableDataSource<Action>();
  loading = false;

  featureForm = new FormGroup({
    name: new FormControl('',
      Validators.required),
    code: new FormControl('',
      Validators.required),
    id_action: new FormControl('')
  });

  constructor(
    public dialog: MatDialog,
    private actionsService: ActionService,
    private permissionService: PermissionService,
    private alert: MyAlertService
  ) { }

  ngOnInit(): void {
    this.selectPermission();
    this.loadActions();
  }

  loadActions() {
    this.loading = true;
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.actionsService.getActions().subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
        this.actions = new MatTableDataSource<Action>(response.result.data.actions);
        this.actions.paginator = this.paginator;
        this.actions.sort = this.sort;
        this.applyFilter();
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      this.loading = false;
      dialogRefLoadingPage.close();
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialogRefLoadingPage.close();
        this.loading = false;
      });
  }

  applyFilter() {
    this.actions.filter = this.queryValue.trim().toLowerCase();
  }

  deleteAction(idAction: number) {
    const dialogConfirm = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        title: 'Confirmación',
        description: 'Desea eliminar la Acción?'
      }
    });
    dialogConfirm.afterClosed().subscribe(result => {
      if (result) {
        const dialogLoading = this.dialog.open(LoadingPageDialogComponent, {
          width: '50%',
          data: "",
          disableClose: true
        });
        this.actionsService.deleteAction(idAction).subscribe(response => {
          if (response.code == 200 && response.result.status === 'ok') {
              this.alert.success('Se ha eliminado la accion');
              this.loadActions();
          } else {
            this.alert.warn(response?.result?.message || 'Proceso no completado');
          }
          dialogLoading.close();
        }, err => {
          if (err.error?.result?.status !== 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          } 
          dialogLoading.close();
        });
      }
    });
  }

  edit(row: Action) {
    this.featureForm.patchValue({
      name: row.name,
      id_action: row.id_action.toString(),
      code: row.code.toString()
    });
    this.formName.nativeElement.focus();
  }

  editAction(action?: Action) {
    const dialogActionDetail = this.dialog.open(ActionsCreateDialogComponent, {
      width: '60%',
      data: {
        action: action
      }
    });
    dialogActionDetail.afterClosed().subscribe(result => {
      if (result) {
        this.loadActions();
      }
    });
  }

  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/tools/actions');
  }
}
