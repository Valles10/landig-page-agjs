import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionsCreateDialogComponent } from './actions-create-dialog.component';

describe('ActionsCreateDialogComponent', () => {
  let component: ActionsCreateDialogComponent;
  let fixture: ComponentFixture<ActionsCreateDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionsCreateDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionsCreateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
