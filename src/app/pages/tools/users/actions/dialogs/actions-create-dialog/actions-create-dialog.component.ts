import { Component, OnInit, Inject } from '@angular/core';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActionService } from 'src/app/core/services/action.service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';


@Component({
  selector: 'app-actions-create-dialog',
  templateUrl: './actions-create-dialog.component.html',
})
export class ActionsCreateDialogComponent implements OnInit {

  public actionForm: FormGroup;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ActionsCreateDialogComponent>,
    private actionService: ActionService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private alert: MyAlertService
  ) { }

  ngOnInit(): void {
    if (this.data.action?.id_action) {
      this.actionForm = new FormGroup({
        id_view_action: new FormControl(this.data.action?.id_action),
        name: new FormControl(this.data.action?.name,
          [
            Validators.required,
            Validators.maxLength(254)
          ]),
        code: new FormControl(this.data.action?.code,
          [
            Validators.required,
            Validators.maxLength(254)
          ])
      });
    } else {
      this.actionForm = new FormGroup({
        id_view_action: new FormControl('0'),
        name: new FormControl('',
          [
            Validators.required,
            Validators.maxLength(254)
          ]),
        code: new FormControl('',
          [
            Validators.required,
            Validators.maxLength(254)
          ])
      });
    }
  }

  get id_view_action() { return this.actionForm.get('id_view_action'); }
  get name() { return this.actionForm.get('name'); }
  get code() { return this.actionForm.get('code'); }

  save(): void {
    const dialoadRef = this.dialog.open(LoadingPageDialogComponent, {
      width: '400px',
      data: {
        description: `Se`,
        title: 'Confirmación de cambios',
      },
      disableClose: true
    });
    if (this.id_view_action.value == 0) {
      this.actionService.saveActions(this.actionForm.value).subscribe(response => {
        if (response.code == 200 && response.result.status == 'ok') {
              this.alert.success('Se ha creado la accion ' + this.name.value);
            this.dialogRef.close(true);
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado');
        }
        dialoadRef.close();
      },
        err => {
          if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          }
          dialoadRef.close();
        });
    } else {
      this.actionService.managementActions(this.actionForm.value).subscribe(response => {
        if (response.code == 200 && response.result.status == 'ok') {
              this.alert.success('Se ha actualizado la accion ' + this.name.value);
            this.dialogRef.close(true);
        } else {
          this.alert.warn(response?.result?.message || 'Proceso no completado');
        }
        dialoadRef.close();
      },
        err => {
          if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          }
          dialoadRef.close();
        });
      }
  }

}
