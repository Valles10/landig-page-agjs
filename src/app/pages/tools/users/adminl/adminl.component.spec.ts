import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminlComponent } from './adminl.component';

describe('AdminlComponent', () => {
  let component: AdminlComponent;
  let fixture: ComponentFixture<AdminlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
