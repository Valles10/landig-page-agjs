import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrateLandingComponent } from './crate-landing.component';

describe('CrateLandingComponent', () => {
  let component: CrateLandingComponent;
  let fixture: ComponentFixture<CrateLandingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrateLandingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrateLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
