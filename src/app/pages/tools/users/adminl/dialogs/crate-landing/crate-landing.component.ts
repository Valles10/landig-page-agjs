import { Component, OnInit } from '@angular/core';

import * as grapesjs from 'grapesjs';
import 'node_modules/grapesjs-component-code-editor';
import 'node_modules/grapesjs-preset-webpage';
import 'node_modules/grapesjs-plugin-forms';
import 'node_modules/grapesjs-custom-code';

@Component({
  selector: 'app-crate-landing',
  templateUrl: './crate-landing.component.html',
  styleUrls: ['./crate-landing.component.css']
})
export class CrateLandingComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const editor = grapesjs.init({
      domComponents: {
        
      },
      selectorManager: {  },
      container : '#gjs',
      fromElement: true,
      storageManager: {
        type : 'local',
      },
      scripts: ['https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js'],
      components: '<link rel="stylesheet"src/app/components/plugin/plugin.component.css">',
      plugins: ['grapesjs-component-code-editor','grapesjs-plugin-forms','grapesjs-preset-webpage','grapesjs-custom-code'],
      pluginsOpts: {
        'gjs-preset-newsletter': {
          modalTitleImport: 'Import template',
          modalLabelImport: 'default',
          cellStyle: {
            padding: '10px',
          },
          // ... other option
        }
      }
  });


  const pn = editor.Panels;
const panelViews = pn.addPanel({
  id: 'views'
});
panelViews.get('buttons').add([{
  attributes: {
     title: 'Open Code'
  },
  className: 'fa fa-file-code-o',
  command: 'open-code',
  togglable: false, //do not close when button is clicked again
  id: 'open-code'
}]);



const selector = editor.selectorManager;
selector.add(['.class1', '.class2','input']);
  }


}
