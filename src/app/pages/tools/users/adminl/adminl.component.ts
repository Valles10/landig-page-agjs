import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Order } from 'src/app/core/models/Order';
import { MatDialog } from '@angular/material/dialog';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { OrderService } from 'src/app/core/services/order.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Tools } from 'src/app/core/helpers/tools';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { FormControl } from '@angular/forms';
import { MatDatepicker } from '@angular/material/datepicker';
import { CrateLandingComponent } from './dialogs/crate-landing/crate-landing.component';
import { CampComponent } from './dialogs/camp/camp.component';
/* import * as XLSX from 'xlsx'; */
declare let XLSX: any;


import * as _moment from 'moment';

import { Moment } from 'moment';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PermissionService } from 'src/app/core/services/permission.service';

const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

interface ItemStatus {
  name: string;
  value: string;
}


@Component({
  selector: 'app-adminl',
  templateUrl: './adminl.component.html',
  styleUrls: ['./adminl.component.css'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    { provide: MAT_DATE_LOCALE, useValue: 'es-us' },
  ],
})
export class AdminlComponent implements OnInit {

  /* Configuracion de la tabla */
  public viewOptions: ViewOptions = {
    displayedColumns: ['odaOrderId', 'nombre', 'deliveryAddres', 'guia', 'idOrden', 'card_auth_code','botones'], //campos que se mostraran
    pageSize: [25, 35, 50], // Configuracion de paginacion
    loading: true
  };

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  dataSource: MatTableDataSource<Order> = new MatTableDataSource<Order>();
  datos: Order[] = [];
  date = new FormControl(moment());
  renderedData: any;
  public arrayPermission = new Array();
  private tools: Tools;
  private param = {
    year: 0,
    month: 0,
    status: '1,2,3,4,5,6'
  };
  @ViewChild('dataTable') table: ElementRef;

  initialStatus = '1,2,3,4,5,6';

  StatusList: Array<ItemStatus> = [
    { name: 'Todo', value: '1,2,3,4,5,6' },
    { name: 'Procesada', value: '1' },
    { name: 'Error', value: '2' },
    { name: 'Despachada', value: '3' },
    { name: 'Entregada', value: '4' },
    { name: 'Procesado manual', value: '6' }
  ];


  constructor(
    private permissionService: PermissionService,
    public dialog: MatDialog,
    public orderService: OrderService,
    _snackbar: MatSnackBar,
    private alert: MyAlertService
  ) {
    this.tools = new Tools(_snackbar);
    this.dataSource.connect().subscribe(d => this.renderedData = d);
  }

  ngOnInit(): void {
    this.selectPermission();
    this.load();

  }

  load(): void {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.dateChange();
    this.orderService.get(this.param).subscribe(
      res => {
        if (res.code == 200 && res.result.status == 'ok') {
          this.datos = res.result.data.orders;
          this.dataSource = new MatTableDataSource<Order>(this.datos);
          this.dataSource.paginator = this.paginator;
        } else {
          this.alert.warn(res?.result?.message || 'Proceso no completado');
        }
        dialogRefLoadingPage.close();
      },
      err => {
        dialogRefLoadingPage.close(); if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }


  filter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
    this.load();
  }

  dateChange() {
    this.param.year = (<Moment>this.date.value).year();
    this.param.month = (<Moment>this.date.value).month() + 1;
    this.param.status = this.initialStatus
  }

  changeStatus() {
    this.load();
  }

  ExportTOExcel() {
    if (this.dataSource.data.length > 0) {

      const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
        width: '80%',
        data: "",
        disableClose: true
      });
      let newData: any[] = [];
      for (let index = 0; index < this.datos.length; index++) {
        const element = this.datos[index];
        newData.push({
          'No. de Orden': element.orderWcsid,
          'No. de transaccion': element.odaOrderId,
          'Fecha de proceso': element.inputDate,
          'Nombre  ': element.userName,
          'Doc. de identificacion': element.personalIdentificDoc,
          'Numero tributario': element.taxDocument,
          'Telefono': element.contactTelephone,
          'Correo': element.userEmail,
          'Direccion': element.clienteAddres,
          'Direccion de entrega': element.deliveryAddres,
          'Autorizacion': element.cardAuthCode,
          'No. Guia': element.trackingOrder,
          'Estado': element.statusTypeIdD
        });
      }
      const workSheet = XLSX.utils.json_to_sheet(newData);
      const workBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(workBook, workSheet, 'Series');
      XLSX.writeFile(workBook, `OrdenesDeCompra_year_${this.param.year}_month_${this.param.month}_status_${this.param.status}.xlsx`);
      dialogRefLoadingPage.close();
    } else {
      this.alert.info('No hay datos');
    }

  }

    crearlandin( ) {
      const dialogActionDetail = this.dialog.open(CrateLandingComponent, {
        width: '60%',
        
      });
      dialogActionDetail.afterClosed().subscribe(result => {
        if (result) {
          
        }
      });
    }

    crearutm( ) {
      const dialogActionDetail = this.dialog.open(CampComponent, {
        width: '60%',
        
      });
      dialogActionDetail.afterClosed().subscribe(result => {
        if (result) {
          
        }
      });
    }

  


  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/store/after-sales/orders');
  }
  
}
