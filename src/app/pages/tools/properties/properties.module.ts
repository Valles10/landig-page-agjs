import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatStepperModule } from '@angular/material/stepper';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import {ScrollingModule} from '@angular/cdk/scrolling'; 
import { PropertiesComponent } from './properties.component';
import { RouterModule, Routes } from '@angular/router';
import { PrepaidConfigComponent } from './components/prepaid-config/prepaid-config.component';
import { ProductImagesPropsComponent } from './components/product-images-props/product-images-props.component';

import {MatChipsModule} from '@angular/material/chips';
import {MatTabsModule} from '@angular/material/tabs';
import {MatCardModule} from '@angular/material/card';
import { SkuExcludedComponent } from './components/sku-excluded/sku-excluded.component';
import { DefaultSkuComponent } from './components/default-sku/default-sku.component';

const routes: Routes = [{ path: '', component: PropertiesComponent }];

@NgModule({
  declarations: [PropertiesComponent, PrepaidConfigComponent, ProductImagesPropsComponent, SkuExcludedComponent, DefaultSkuComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatIconModule, 
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatTooltipModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    MatProgressBarModule,
    MatDialogModule,
    MatStepperModule,
    MatCheckboxModule,
    PdfViewerModule,
    ScrollingModule,
    MatChipsModule,
    MatTabsModule,
    MatCardModule
  ],
  entryComponents: [
  ]
})
export class PropertiesModule { }
