import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkuExcludedComponent } from './sku-excluded.component';

describe('SkuExcludedComponent', () => {
  let component: SkuExcludedComponent;
  let fixture: ComponentFixture<SkuExcludedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkuExcludedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkuExcludedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
