import { query } from '@angular/animations';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { CatalogDialogComponent } from 'src/app/pages/store/offer/product-detail/dialogs/catalog-dialog/catalog-dialog.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'tlbo-sku-excluded',
  templateUrl: './sku-excluded.component.html',
  styleUrls: ['./sku-excluded.component.css']
})
export class SkuExcludedComponent implements OnInit {

  public viewOptions: ViewOptions = {
    displayedColumns: ['sku', 'actions'],
    pageSize: environment.pagination,
    loading: false
  };

  public query: string = "";

  data: string[] = [];
  dataSource: MatTableDataSource<string> = new MatTableDataSource();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  public loadinSaving = false;

  constructor(
    public dialog: MatDialog,
    private settingsService: SettingsService,
    private alert: MyAlertService

  ) { }

  ngOnInit(): void {
    this.loadData();
  }


  loadData() {
    this.viewOptions.loading = true;
    this.data = [];
    this.dataSource = new MatTableDataSource([]);
    this.settingsService.getSkuExcludes().subscribe(
      response => {
        if (response.code == 200 && response?.result?.status?.toLowerCase() == 'ok' && response?.result?.data && Array.isArray(response?.result?.data) && response?.result?.data.length == 1) {
          let res: string = response.result.data[0].value;
          res = res.replace('(', '');
          res = res.replace(')', '');
          res = res.replace(/( )/g, '');

          this.data = res.split(',') || [];
          this.data = this.data.length == 1 && this.data[0] == '' ? [] : this.data;
          this.dataSource = new MatTableDataSource(this.data);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.applyFilter();
        } else {
          this.alert.warn(response?.result?.message || "Propiedades no cargadas");
        }
        this.viewOptions.loading = false;
      },
      err => {
        this.viewOptions.loading = false;
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }

  applyFilter() {
    this.dataSource.filter = this.query.trim().toLowerCase();
  }

  openCatalogDialog() {
    const dialogRef = this.dialog.open(CatalogDialogComponent, {
      width: '60%',
      /* data: {
        skuSim: this.product.skuSim
      } */
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result?.skuSim) {
        this.save(result?.skuSim);
      }
    });
  }


  save(value: string) {
    this.viewOptions.loading = true;
    let flag = false;
    for (let index = 0; index < this.data.length; index++) {
      const element = this.data[index];
      if (element == `'${value}'`)
        flag = true;
    }

    if (!flag) {
      let val = [`'${value}'`, ...this.data];
      this.settingsService.saveSkuExludes({
        key: environment.idSkuExcluded,
        value: `(${val})`
      }).subscribe(
        response => {
          if (response.code == 200) {
            this.alert.success('SKU se han actualizado...');
            this.data = [...val];
            this.dataSource = new MatTableDataSource(this.data);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            this.applyFilter();
          } else {
            this.alert.warn(response?.result?.message || "Proceso no completado");
          }
          this.viewOptions.loading = false;
        },
        err => {
          this.viewOptions.loading = false;
          if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          }
        });
    } else {
      this.viewOptions.loading = false;
      this.alert.warn('SKU ya existe');
    }

  }

  delete(item) {
    const dialogConfirm = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        title: 'Confirmación',
        description: 'Desea eliminar el SKU?'
      }
    });
    dialogConfirm.afterClosed().subscribe(result => {
      if (result) {
        let temp: string[] = [];
        for (let index = 0; index < this.data.length; index++) {
          const element = this.data[index];
          if (item != element) {
            temp.push(element);
          }
        }
        this.viewOptions.loading = true;
        this.settingsService.saveSkuExludes({
          key: environment.idSkuExcluded,
          value: `(${temp})`
        }).subscribe(
          response => {
            if (response.code == 200 && response?.result?.status.toLowerCase() == 'ok') {
              this.alert.success('SKU se ha eliminado...');
              this.viewOptions.loading = false;
              this.loadData();
            } else {
              this.viewOptions.loading = false;
              this.alert.warn(response?.result?.message || "Proceso no completado");
            }
          },
          err => {
            this.viewOptions.loading = false;
            if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
              this.alert.warn(err.error.result.message);
            } else {
              this.alert.error('Proceso no completado');
            }
          });
      }
    });
  }
}
