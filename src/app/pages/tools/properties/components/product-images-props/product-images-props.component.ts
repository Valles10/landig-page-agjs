import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { Tools } from 'src/app/core/helpers/tools';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PermissionService } from 'src/app/core/services/permission.service';
import { PropertiesService } from 'src/app/core/services/properties.service';

@Component({
  selector: 'product-images-props',
  templateUrl: './product-images-props.component.html',
})
export class ProductImagesPropsComponent implements OnInit {

  private tools: Tools;
  public selectedType: string = "";
  public selectedTypeFeature: string = "";
  public loadedProperties = false;
  public removable = true;

  public allTypesImageFiles = [];
  public allTypesImageFilesFeatures = [];

  public comparisions = [];

  public props = new ImagesProperties();
  public loading = false;
  public sizes: Size = {
    width: 0,
    height: 0
  };

  public sizeFeature: Size = {
    width: 0,
    height: 0
  };
  public arrayPermission = new Array();

  constructor(
    private permissionService: PermissionService,
    _snackbar: MatSnackBar,
    public dialog: MatDialog,
    private propertiesService: PropertiesService,
    private alert: MyAlertService
  ) {
    this.tools = new Tools(_snackbar);
  }

  ngOnInit(): void {
    this.selectPermission();
    this.loadProperties();
  }


  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/tools/properties');
  }


  reset() {
    this.comparisions = this.props.comparisions;
    this.allTypesImageFiles = this.props?.types?.slice() || [];
    this.allTypesImageFilesFeatures = this.props?.types?.slice() || [];
    this.tools.removeMultiItemFromArr(this.allTypesImageFiles, this.props?.productImages?.types || []);
    this.tools.removeMultiItemFromArr(this.allTypesImageFilesFeatures, this.props?.featureImages?.types || []);
  }

  removeType(val, type: string) {
    switch (type) {
      case 'carousel':
        if (val != '') {
          this.tools.removeItemFromArr(this.props.productImages.types, val);
          this.allTypesImageFiles.push(val);
        }
        break;
      case 'features':
        if (val != '') {
          this.tools.removeItemFromArr(this.props.featureImages.types, val);
          this.allTypesImageFilesFeatures.push(val);
        }
        break;
    }

  }

  addType(type: string) {
    switch (type) {
      case 'carousel':
        if (this.selectedType != '') {
          if (!this.tools.existsItemFromArr(this.props.productImages.types, this.selectedType))
            this.props.productImages.types.push(this.selectedType);
          this.tools.removeItemFromArr(this.allTypesImageFiles, this.selectedType);
          this.selectedType = "";
        } else {
          this.alert.info('No hay nada seleccionado');
        }
        break;
      case 'features':
        if (this.selectedTypeFeature != '') {
          if (!this.tools.existsItemFromArr(this.props.featureImages.types, this.selectedTypeFeature))
            this.props.featureImages.types.push(this.selectedTypeFeature);
          this.tools.removeItemFromArr(this.allTypesImageFilesFeatures, this.selectedTypeFeature);
          this.selectedTypeFeature = "";
        } else {
          this.alert.info('No hay nada seleccionado');
        }
        break;
    }

  }

  changeType(value: any, type: string) {
    switch (type) {
      case 'carousel':
        if (value != '') {
          this.selectedType = value;
        }
        break;
      case 'features':
        if (value != '') {
          this.selectedTypeFeature = value;
        }
        break;
    }

  }

  /* Manejo de tamaños de imagen */
  removeSize(size, type: string) {
    switch (type) {
      case 'carousel':
        if (size != '') {
          this.tools.removeItemFromArr(this.props.productImages.dimensions.sizes, size);
        }
        break;
      case 'features':
        if (size != '') {
          this.tools.removeItemFromArr(this.props.featureImages.dimensions.sizes, size);
        }
        break;
    }
  }

  addSize(type: string) {
    switch (type) {
      case 'carousel':
        if (Number(this.sizes.width) > 0 && Number(this.sizes.height) > 0) {
          if (!this.existsSizeFromArr(this.props.productImages.dimensions.sizes, this.sizes)) {
            this.props.productImages.dimensions.sizes.push(this.sizes);
            this.sizes = {
              width: 0,
              height: 0
            }
          } else {
            this.alert.warn('Dimensiones ya agregadas');
          }
        } else {
          this.alert.warn('Dimensiones no validas');
        }
        break;
      case 'features':
        if (Number(this.sizeFeature.width) > 0 && Number(this.sizeFeature.height) > 0) {
          if (!this.existsSizeFromArr(this.props.featureImages.dimensions.sizes, this.sizeFeature)) {
            this.props.featureImages.dimensions.sizes.push(this.sizeFeature);
            this.sizeFeature = {
              width: 0,
              height: 0
            }
          } else {
            this.alert.warn('Dimensiones ya agregadas');
          }
        } else {
          this.alert.warn('Dimensiones no validas');
        }
        break;
    }
  }

  existsSizeFromArr(arr: Array<any>, item: any): boolean {
    let res = false;
    for (let i of arr) {
      if (item.width == i.width && item.height == i.height) {
        res = true;
        break;
      }
    }
    return res;
  }


  saveProperties() {
    if (this.props.productImages.types.length > 0) {
      if (this.props.productImages.dimensions.sizes.length > 0) {
        if (this.props.productImages.dimensions.comparision != "") {
          if (this.props.productImages.minimunSize > 0) {
            if (this.props.productImages.maximunSize > 0 && this.props.productImages.maximunSize > this.props.productImages.minimunSize) {
              if (this.props.productImages.byCarousel > 0) {
                if (this.props.featureImages.types.length > 0) {
                  if (this.props.featureImages.dimensions.sizes.length > 0) {
                    if (this.props.featureImages.dimensions.comparision != "") {
                      if (this.props.featureImages.minimunSize > 0) {
                        if (this.props.featureImages.maximunSize > 0 && this.props.featureImages.maximunSize > this.props.featureImages.minimunSize) {
                          if (this.loadedProperties) {
                            this.save();
                          } else {
                            const dialogConfirm = this.dialog.open(ConfirmDialogComponent, {
                              width: '400px',
                              data: {
                                title: 'Confirmación',
                                description: 'Propiedades no fueron cargadas correctamente, confirma reemplazar todas las propiedades existes?'
                              },
                            });
                            dialogConfirm.afterClosed().subscribe(res => {
                              if (res) {
                                this.save();
                              }
                            });
                          }
                        } else {
                          this.alert.warn('Tamaño maximo de imagenes general/especifica no permitido');
                        }
                      } else {
                        this.alert.warn('Tamaño minimo de imagenes general/especifica no permitido');
                      }
                    } else {
                      this.alert.warn('Comparación de dimensiones general/especifica no establecido');
                    }
                  } else {
                    this.alert.warn('Tamaños de imagen general/especifica no establecido');
                  }
                } else {
                  this.alert.warn('Tipos de imagen general/especifica no establecido');
                }
              } else {
                this.alert.warn('Especificar imagenes por carrusel');
              }
            } else {
              this.alert.warn('Tamaño maximo de imagenes en carousel no permitido');
            }
          } else {
            this.alert.warn('Tamaño minimo de imagenes de carousel no permitido');
          }
        } else {
          this.alert.warn('Comparación de dimensiones para carrusel no establecido');
        }
      } else {
        this.alert.warn('Tamaños de imagen para carrusel no establecido');
      }
    } else {
      this.alert.warn('Tipos de imagen para carrusel no establecido');
    }
  }

  loadProperties() {
    this.loading = true;
    /* const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    }); */
    this.propertiesService.getImage().subscribe(response => {
      if (response.code == 200 && response.result.status == 'ok') {
        let props: ImagesProperties;
        try {
          props = JSON.parse(response.result?.data?.value || "{}");
          this.props.configure(props);
          this.loadedProperties = true;
        } catch (e) {
        }
      } else {
        this.alert.warn(response?.result?.message || "Propiedades no cargadas");
      }
      this.reset();
      this.loading = false;/* 
      dialogRefLoadingPage.close(); */
    },
      err => {/* 
        dialogRefLoadingPage.close(); */
        this.loading = false;
        this.reset();
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }

  save() {
    this.loading = true;
    /* const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    }); */
    this.propertiesService.saveImage({
      config: {
        accion: "setSiteProp",
        content: JSON.stringify(this.props)
      }
    }).subscribe(response => {
      if (response.code == 200 && response.result.status == 'ok') {
        this.alert.success('Propiedades guardadas');
      } else {
        this.alert.warn(response?.result?.message || "Proceso no completado");
      }
      this.loading = false;
      /* dialogRefLoadingPage.close(); */
    },
      err => {
       /*  dialogRefLoadingPage.close(); */
        this.loading = false;
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
      });
  }
}

export class ImagesProperties {
  types?: string[];
  comparisions?: Comparision[];
  productImages?: ProductImage;
  featureImages?: FeatureImage;

  constructor() {
    this.init();
  }

  init() {
    this.types = [
      "image/jpeg",
      "image/jpg",
      "image/pjpeg",
      "image/x-citrix-jpeg",
      "image/png",
      "image/x-citrix-png",
      "image/x-png"];
    this.comparisions = [{
      name: "Todas",
      value: "any"
    }, {
      name: "Absolutas",
      value: "match"
    }];
    this.productImages = new ProductImage();
    this.featureImages = new FeatureImage();
  }

  configure(obj: ImagesProperties) {
    this.types = obj?.types && Array.isArray(obj?.types) ? obj?.types : this.types;
    this.comparisions = obj?.comparisions && obj.comparisions instanceof Array ? obj.comparisions : this.comparisions;
    this.productImages.byCarousel = obj?.productImages?.byCarousel || this.productImages.byCarousel;
    this.productImages.dimensions.comparision = obj?.productImages?.dimensions?.comparision || this.productImages.dimensions.comparision;
    this.productImages.dimensions.sizes = obj?.productImages?.dimensions?.sizes && obj?.productImages?.dimensions?.sizes instanceof Array ? obj?.productImages?.dimensions?.sizes : this.productImages.dimensions.sizes;
    this.productImages.maximunSize = obj?.productImages?.maximunSize || this.productImages.maximunSize;
    this.productImages.minimunSize = obj?.productImages?.minimunSize || this.productImages.minimunSize;
    this.productImages.types = obj?.productImages?.types && Array.isArray(obj?.productImages?.types) ? obj?.productImages?.types : this.productImages.types;

    // Caracteristicas 
    this.featureImages.dimensions.comparision = obj?.featureImages?.dimensions?.comparision || this.featureImages.dimensions.comparision;
    this.featureImages.dimensions.sizes = obj?.featureImages?.dimensions?.sizes && obj?.featureImages?.dimensions?.sizes instanceof Array ? obj?.featureImages.dimensions.sizes : this.featureImages.dimensions.sizes;
    this.featureImages.maximunSize = obj?.featureImages?.maximunSize || this.featureImages.maximunSize;
    this.featureImages.minimunSize = obj?.featureImages?.minimunSize || this.featureImages.minimunSize;
    this.featureImages.types = obj?.featureImages?.types && Array.isArray(obj?.featureImages?.types) ? obj.featureImages.types : this.featureImages.types;

  };
}

class Comparision {
  name: string;
  value: string;
}

export class ProductImage {
  byCarousel: number;
  maximunSize: number;
  minimunSize: number;
  types: string[];
  dimensions: Dimensions;

  constructor() {
    this.byCarousel = 0;
    this.maximunSize = 0;
    this.minimunSize = 0;
    this.types = [];
    this.dimensions = new Dimensions();
  }
}
export class FeatureImage {
  maximunSize: number;
  minimunSize: number;
  types: string[];
  dimensions: Dimensions;
  constructor() {
    this.maximunSize = 0;
    this.minimunSize = 0;
    this.types = [];
    this.dimensions = new Dimensions()
  }
}

class Dimensions {
  comparision: string;
  sizes: Size[];
  constructor() {
    this.comparision = "";
    this.sizes = []
  }
}

class Size {
  width: number;
  height: number;
}