import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductImagesPropsComponent } from './product-images-props.component';

describe('ProductImagesPropsComponent', () => {
  let component: ProductImagesPropsComponent;
  let fixture: ComponentFixture<ProductImagesPropsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductImagesPropsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductImagesPropsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
