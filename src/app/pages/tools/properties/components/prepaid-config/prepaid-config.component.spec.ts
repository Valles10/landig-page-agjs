import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrepaidConfigComponent } from './prepaid-config.component';

describe('PrepaidConfigComponent', () => {
  let component: PrepaidConfigComponent;
  let fixture: ComponentFixture<PrepaidConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrepaidConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrepaidConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
