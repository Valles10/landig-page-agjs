import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PropertiesService } from 'src/app/core/services/properties.service';
import { CatalogDialogComponent } from 'src/app/pages/store/offer/product-detail/dialogs/catalog-dialog/catalog-dialog.component';

@Component({
  selector: 'tlbo-default-sku',
  templateUrl: './default-sku.component.html',
  styleUrls: ['./default-sku.component.css']
})
export class DefaultSkuComponent implements OnInit {


  public defaultSku; 
  public skuLoaded = true;
  public loading = false; 


  constructor(
    public dialog: MatDialog,
    private propertiesService: PropertiesService,
    private alert: MyAlertService
  ) { }

  ngOnInit(): void {
  }


  saveSKU(){

  }

  openCatalogDialog() {
    const dialogRef = this.dialog.open(CatalogDialogComponent, {
      width: '60%',
      /* data: {
        skuSim: this.product.skuSim
      } */
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result?.skuSim) {
        
      }
    });
  }
}
