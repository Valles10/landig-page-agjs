import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultSkuComponent } from './default-sku.component';

describe('DefaultSkuComponent', () => {
  let component: DefaultSkuComponent;
  let fixture: ComponentFixture<DefaultSkuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefaultSkuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultSkuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
