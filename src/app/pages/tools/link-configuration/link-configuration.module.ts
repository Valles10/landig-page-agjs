import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LinkConfigurationRoutingModule } from './link-configuration-routing.module';
import { LinkConfigurationComponent } from './link-configuration.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatSortModule } from '@angular/material/sort';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { CreateLinkDialogComponent } from './dialogs/create-link-dialog/create-link-dialog.component';


@NgModule({
  declarations: [LinkConfigurationComponent, CreateLinkDialogComponent],
  imports: [
    CommonModule,
    LinkConfigurationRoutingModule,
    FormsModule,
    MatFormFieldModule,
    MatIconModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatTooltipModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    MatProgressBarModule,
    MatDialogModule
  ]
})
export class LinkConfigurationModule { }
