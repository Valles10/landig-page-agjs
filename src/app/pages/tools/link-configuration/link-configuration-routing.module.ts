import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LinkConfigurationComponent } from './link-configuration.component';

const routes: Routes = [{ path: '', component: LinkConfigurationComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LinkConfigurationRoutingModule { }
