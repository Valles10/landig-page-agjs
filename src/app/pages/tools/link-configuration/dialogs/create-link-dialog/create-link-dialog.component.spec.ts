import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateLinkDialogComponent } from './create-link-dialog.component';

describe('CreateLinkDialogComponent', () => {
  let component: CreateLinkDialogComponent;
  let fixture: ComponentFixture<CreateLinkDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateLinkDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateLinkDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
