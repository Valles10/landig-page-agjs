import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { LinkService } from 'src/app/core/services/link-service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';

@Component({
  selector: 'app-create-link-dialog',
  templateUrl: './create-link-dialog.component.html',
  styleUrls: ['./create-link-dialog.component.css']
})
export class CreateLinkDialogComponent implements OnInit {



  form: FormGroup;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<CreateLinkDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private alert: MyAlertService,
    private linkService: LinkService
  ) { }

  ngOnInit(): void {
    if (this.data?.link != null) {
      this.form = new FormGroup({
        id: new FormControl(this.data?.link?.id || '0'),
        code: new FormControl(this.data?.link?.code || '',
        [
          Validators.required,
          Validators.maxLength(75)
        ]),
        name: new FormControl(this.data?.link?.name|| '',
          [
            Validators.required,
            Validators.maxLength(75)
          ]),
        value: new FormControl(this.data?.link?.value|| '',
          [
            Validators.required,
            Validators.maxLength(75)
          ]),
        description: new FormControl(this.data?.link?.description|| '',
          [
            Validators.maxLength(125)
          ]),
      });
    } else {
      this.form = new FormGroup({
        id: new FormControl('0'),
        code: new FormControl('',
        [
          Validators.required,
          Validators.maxLength(25)
        ]),
        name: new FormControl('',
          [
            Validators.required,
            Validators.maxLength(75)
          ]),
        value: new FormControl('',
          [
            Validators.required,
            Validators.maxLength(75)
          ]),
        description: new FormControl('',
          [
            Validators.maxLength(125)
          ]),
      });
    }
  }


  get id() { return this.form.get('id') }
  get name() { return this.form.get('name') }

  get code() { return this.form.get('code') }

  get value() { return this.form.get('value') }
  get description() { return this.form.get('description') }


  /* 
    get status() { return this.brandForm.get('status') }
    get brand_id() { return this.brandForm.get('brand_id') }
    get name() { return this.brandForm.get('name') }
    get description() { return this.brandForm.get('description') } */

  save(): void {
    const dialoadRef = this.dialog.open(LoadingPageDialogComponent, {
      width: '400px',
      data: {
        description: `Se`,
        title: "Confirmación de cambios",
      },
      disableClose: true
    });
    this.linkService.save(this.form.value).subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {

        this.alert.success('Se ha actualizadoel enlace ' + this.name.value);

        this.alert.success('Se ha creado el enlace ' + this.name.value);

        this.dialogRef.close(true);
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      dialoadRef.close();
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialoadRef.close();
      });
  }
}
