import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmDialogComponent } from 'src/app/components/shared/confirm-dialog/confirm-dialog.component';
import { LoadingPageDialogComponent } from 'src/app/components/shared/loading-page-dialog/loading-page-dialog.component';
import { UrlTL } from 'src/app/core/models/Url-TL';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { ActionService } from 'src/app/core/services/action.service';
import { LinkService } from 'src/app/core/services/link-service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { PermissionService } from 'src/app/core/services/permission.service';
import { environment } from 'src/environments/environment';
import { CreateLinkDialogComponent } from './dialogs/create-link-dialog/create-link-dialog.component';

@Component({
  selector: 'app-link-configuration',
  templateUrl: './link-configuration.component.html',
  styleUrls: ['./link-configuration.component.css']
})
export class LinkConfigurationComponent implements OnInit {

   public viewOptions: ViewOptions = {
    displayedColumns: ['name', 'code', 'value','description', 'options'],
    pageSize: environment.pagination,
    loading: true
  };
  
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
 /*  @ViewChild('formName') formName: ElementRef; */

  public arrayPermission = new Array();
  public action: UrlTL = new UrlTL();
  queryValue: string = "";

  links: MatTableDataSource<UrlTL> = new MatTableDataSource<UrlTL>();
  loading = false;

  /* featureForm = new FormGroup({
    name: new FormControl('',
      Validators.required),
    code: new FormControl('',
      Validators.required),
    id_action: new FormControl('')
  }); */

  constructor(
    public dialog: MatDialog,
    private linkService: LinkService,
    private permissionService: PermissionService,
    private alert: MyAlertService
  ) { }

  ngOnInit(): void {
    /* this.selectPermission(); */
    this.loadData();
  }

  loadData() {
    this.loading = true;
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });
    this.linkService.get().subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
        this.links = new MatTableDataSource<UrlTL>(response.result.data.configuredLinks );
        this.links.paginator = this.paginator;
        this.links.sort = this.sort;
        this.applyFilter();
      } else {
        this.alert.warn(response?.result?.message || 'Proceso no completado');
      }
      this.loading = false;
      dialogRefLoadingPage.close();
    },
      err => {
        if (err.error?.result?.status != 'ok' && err.error?.result?.message) {
          this.alert.warn(err.error.result.message);
        } else {
          this.alert.error('Proceso no completado');
        }
        dialogRefLoadingPage.close();
        this.loading = false;
      });
  }

  applyFilter() {
    this.links.filter = this.queryValue.trim().toLowerCase();
  }

  delete(item: UrlTL) {
    const dialogConfirm = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        title: 'Confirmación',
        description: 'Desea eliminar el registro?'
      }
    });
    dialogConfirm.afterClosed().subscribe(result => {
      if (result) {
        const dialogLoading = this.dialog.open(LoadingPageDialogComponent, {
          width: '50%',
          data: "",
          disableClose: true
        });
        this.linkService.delete(item.id.toString()).subscribe(response => {
          if (response.code == 200 && response.result.status === 'ok') {
              this.alert.success('Se ha eliminado el elemento');
              this.loadData();
          } else {
            this.alert.warn(response?.result?.message || 'Proceso no completado');
          }
          dialogLoading.close();
        }, err => {
          if (err.error?.result?.status !== 'ok' && err.error?.result?.message) {
            this.alert.warn(err.error.result.message);
          } else {
            this.alert.error('Proceso no completado');
          } 
          dialogLoading.close();
        });
      }
    });
  }

/*   edit(row: Action) {
    this.featureForm.patchValue({
      name: row.name,
      id_action: row.id_action.toString(),
      code: row.code.toString()
    });
    this.formName.nativeElement.focus();
  } */

  create(item?: UrlTL) {
    const dialogActionDetail = this.dialog.open(CreateLinkDialogComponent, {
      width: '60%',
      data: {
        link: item
      }
    });
    dialogActionDetail.afterClosed().subscribe(result => {
      if (result) {
        this.loadData();
      }
    });
  }

 /*  selectPermission() {
    this.arrayPermission = this.permissionService.getPermission('/tools/links');
  } */

}
