import { Component, OnInit } from '@angular/core';
import { AuthSecureService } from 'src/app/core/services/auth-secure.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  public user;

  constructor(
    private authSecureService: AuthSecureService,
  ) {
    this.authSecureService.closingSession(true);
    this.user = this.authSecureService.userValue.name;
  }

  ngOnInit(): void {
    this.authSecureService.logout()
      .pipe(first())
      .subscribe(
        data => {
          this.authSecureService.closeApp();
         },
        error => {
          this.authSecureService.closeApp();
          
        });
  }

}
