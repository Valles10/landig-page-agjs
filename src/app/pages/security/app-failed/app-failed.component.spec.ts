import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppFailedComponent } from './app-failed.component';

describe('AppFailedComponent', () => {
  let component: AppFailedComponent;
  let fixture: ComponentFixture<AppFailedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppFailedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppFailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
