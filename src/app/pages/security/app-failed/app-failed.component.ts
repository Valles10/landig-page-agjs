import { Component, OnInit } from '@angular/core';
import { AuthSecureService } from 'src/app/core/services/auth-secure.service';

@Component({
  selector: 'app-app-failed',
  templateUrl: './app-failed.component.html'
})
export class AppFailedComponent implements OnInit {

  constructor(
    private authSecureService: AuthSecureService
  ) { }

  ngOnInit(): void {
  }

  logout() {
    this.authSecureService.closeApp();
  }

}
