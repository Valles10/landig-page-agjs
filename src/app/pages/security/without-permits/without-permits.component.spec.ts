import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WithoutPermitsComponent } from './without-permits.component';

describe('WithoutPermitsComponent', () => {
  let component: WithoutPermitsComponent;
  let fixture: ComponentFixture<WithoutPermitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WithoutPermitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WithoutPermitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
