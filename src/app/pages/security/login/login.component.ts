import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { CountryMock } from 'src/app/core/mocks/country.mock';
import { AuthSecureService } from 'src/app/core/services/auth-secure.service';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { Country } from 'src/app/core/models/Country';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

   public loginForm: FormGroup;
  public loading = false;
  public submitted = false;
  private returnUrl: string;
  public error = '';
  public passwordType = 'password';
  public country: Country = {
    code: '0',
    value: 'Dominio',
    name: 'Dominio'
  };

  public countries: any[] = [];
  /* private countrySessionStorage;
  private countryStorage; */
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authSecureService: AuthSecureService,
    private alert: MyAlertService
  ) {
    if (this.authSecureService.userValue) {
      this.router.navigate([authSecureService.country, '/']);
    }

   /*  this.authSecureService.countrySubject.subscribe(
      country =>{
        this.countryStorage = country;
      }
    );
    this.authSecureService.sessionCountrySubject.subscribe(
      country =>{
        this.countrySessionStorage = country;
      }
    ); */
  }


  ngOnInit(): void {
    this.countries = CountryMock.getCountries();
    this.loginForm = new FormGroup({
      sessionCountry: new FormControl(this.authSecureService.sessionCountryValue,
        [
          Validators.required
        ]),
      username: new FormControl('',
        [
          Validators.required,
          Validators.pattern(/(.|\s)*\S(.|\s)*/)
        ]),
      password: new FormControl('',
        [
          Validators.required
        ]),
    });

    this.country = this.authSecureService.countryValue || {
      code: '0',
      value: 'Dominio',
      name: 'Dominio'
    };
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get f() { return this.loginForm.controls; }
  get username() { return this.loginForm.get('username'); }
  get password() { return this.loginForm.get('password'); }
  get sessionCountry() { return this.loginForm.get('sessionCountry'); }

  onSubmit() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    if (this.country.code == '0') {
      this.country = {
        code: this.sessionCountry.value.code,
        value: this.sessionCountry.value.value,
        name: this.sessionCountry.value.name,
      }
    }


    this.loading = true;
    this.authSecureService.login({
      country: Number(this.country.code),
      userName: this.username.value,
      password: this.password.value,
      sessionCountry: Number(this.sessionCountry.value.code)
    }, this.country,
      this.sessionCountry.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.loading = false;
          if (error.error?.result?.message) {
            this.alert.warn(error.error.result.message);
          } else {
            if (error.error?.result) {
              this.alert.warn(error.error?.result);
            } else {
              this.alert.error('Usuario/Contraseña incorrecto');
            }
          }
        });
  }

  changePasswordType() {
    this.passwordType = this.passwordType == 'text' ? 'password' : 'text';
  }

  setCountry(country) {
    this.country = {
      code: country.code,
      value: country.value,
      name: country.name,
    };
  }

  compareWhithId(o1: Country, o2: Country): boolean {
    return o1.code === o2.code;
  }

}
