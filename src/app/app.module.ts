import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { CommonModule, APP_BASE_HREF, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuListItemComponent } from './components/shared/menu-list-item/menu-list-item.component';
import { AngularMaterialModule } from './angular-material.module';
import { HomeComponent } from './pages/home/home.component';
import { BreadcrumbComponent } from './components/shared/breadcrumb/breadcrumb.component';
import { NavService } from './core/services/nav.service';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { ModuleComponent } from './pages/tools/users/module/module.component';
import { ConfirmDialogComponent } from './components/shared/confirm-dialog/confirm-dialog.component';
import { LoadingPageDialogComponent } from './components/shared/loading-page-dialog/loading-page-dialog.component';
import { CreateModuleDialogComponent } from './pages/tools/users/module/dialogs/create-module-dialog/create-module-dialog.component';
import { ActionModuleDialogComponent } from './pages/tools/users/module/dialogs/action-module-dialog/action-module-dialog';
import { ProfileComponent } from './pages/tools/users/profile/profile.component';
import { CreateProfileDialogComponent } from './pages/tools/users/profile/dialogs/create-profile-dialog/create-profile-dialog.component';
import { PermissionsComponent } from './pages/tools/users/profile/components/permissions/permissions.component';
import { UserComponent } from './pages/tools/users/user/user.component';
import { CreateUserDialogComponent } from './pages/tools/users/user/dialogs/create-user-dialog/create-user-dialog.component';
import { UserPermissionsComponent } from './pages/tools/users/user-permissions/user-permissions.component';
import { ProfilesByUserComponent } from './pages/tools/users/user-permissions/components/profiles-by-user/profiles-by-user.component';
import { LoginComponent } from './pages/security/login/login.component';
import { JwtInterceptor } from './core/interceptors/jwt.interceptor';
import { ErrorInterceptor } from './core/interceptors/error.interceptor';
import { appInitializer } from './core/helpers/app.initializer';
import { AuthSecureService } from './core/services/auth-secure.service';
import { LogoutComponent } from './pages/security/logout/logout.component';
import { WithoutPermitsComponent } from './pages/security/without-permits/without-permits.component';
import { AppFailedComponent } from './pages/security/app-failed/app-failed.component';
import { InventoryComponent } from './pages/store/offer/inventory/inventory.component';
import { CreateVersionDialogComponent } from './pages/store/offer/inventory/dialogs/create-version-dialog/create-version-dialog.component';
import { ProductDetailComponent } from './pages/store/offer/product-detail/product-detail.component';
import { ImageManagementComponent } from './pages/store/offer/product-detail/components/image-management/image-management.component';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';
import { ImageUploadDialogComponent } from './pages/store/offer/product-detail/dialogs/image-upload-dialog/image-upload-dialog.component';
import { FeatureManagementComponent } from './pages/store/offer/product-detail/components/feature-management/feature-management.component';
import { GroupersDialogComponent } from './pages/store/offer/product-detail/dialogs/groupers-dialog/groupers-dialog.component';
import { CategoryTemplateDetailDialogComponent } from './pages/store/offer/product-detail/dialogs/category-template-detail-dialog/category-template-detail-dialog.component';
import { ProductInfoDetailComponent } from './pages/store/offer/product-detail/components/product-info-detail/product-info-detail.component';
import { CategoriesComponent, ChecklistDatabase } from './pages/store/offer/categories/categories.component';
import { DisableControlDirective } from './core/directives/disable-contro.directive';
import { SupplierProductComponent } from './pages/store/offer/supplier-product/supplier-product.component';
import { SupplierProductDetailDialogComponent } from './pages/store/offer/supplier-product/components/supplier-product-detail-dialog/supplier-product-detail-dialog.component';
import { VersionManagementComponent } from './pages/store/offer/version-management/version-management.component';
import { StepperCustomComponent } from './components/shared/stepper-custom/stepper-custom.component';
import { VersionDetailComponent } from './pages/store/offer/version-detail/version-detail.component';
import { DownloadReportFilesDialogComponent } from './pages/store/offer/version-detail/dialogs/download-report-diles-dialog/download-report-files-dialog.component';
import { BrandCreateDialogComponent } from './pages/store/offer/brands/dialogs/brand-create-dialog/brand-create-dialog.component';
import { BrandsComponent } from './pages/store/offer/brands/brands.component';
import { FeaturesComponent } from './pages/store/offer/features/features.component';
import { FeatureCreateDialogComponent } from './pages/store/offer/features/dialogs/feature-create-dialog/feature-create-dialog.component';
import { CategoryCreateDialogComponent } from './pages/store/offer/categories/dialogs/category-create-dialog/category-create-dialog.component';
import { SeriesManagementComponent } from './pages/store/virtual-products/series-management/series-management.component';
import { UpdateSerieDialogComponent } from './pages/store/virtual-products/series-management/dialogs/update-serie-dialog/update-serie-dialog.component';
import { SeriesDeliveryComponent } from './pages/store/virtual-products/series-delivery/series-delivery.component';
import { SeriesEmailsDialogComponent } from './pages/store/virtual-products/series-delivery/dialogs/series-emails-dialog/series-emails-dialog.component';
import { SendSerialMailDialogComponent } from './pages/store/virtual-products/series-delivery/dialogs/send-serial-mail-dialog/send-serial-mail-dialog.component';
import { MyGoogleChartsComponent } from './components/my-google-charts/my-google-charts.component';
import { SaleProductReportComponent } from './components/sale-product-report/sale-product-report.component';
import { ProductReportComponent } from './pages/store/reports/product-report/product-report.component';
import { IncomeReportComponent } from './pages/store/reports/income-report/income-report.component';
import { PurchaseOrderReportComponent } from './pages/store/reports/purchase-order-report/purchase-order-report.component';
import { AvailableFeaturesDialogComponent } from './pages/store/offer/product-detail/dialogs/available-features-dialog/available-features-dialog.component';
import { CustomerAbandonmentComponent } from './pages/store/reports/customer-abandonment/customer-abandonment.component';
import { PasswordValidationDialogComponent } from './components/shared/password-validation-dialog/password-validation-dialog.component';
import { LoadFeatureImageDialogComponent } from './pages/store/offer/product-detail/components/feature-management/dialogs/load-feature-image-dialog/load-feature-image-dialog.component';
import { CatalogDialogComponent } from './pages/store/offer/product-detail/dialogs/catalog-dialog/catalog-dialog.component';
import { OrdersComponent } from './pages/store/after-sales/orders/orders.component';
import { TrackinDialogComponent } from './pages/store/after-sales/orders/dialogs/trackin-dialog/trackin-dialog.component';
import { DetailOrderDialogComponent } from './pages/store/after-sales/orders/dialogs/detail-order-dialog/detail-order-dialog.component';
import { ContractComponent } from './pages/store/after-sales/contract/contract.component';
import { DocumentsComponent } from './pages/store/after-sales/contract/dialogs/documents/documents.component';
import { ModifiOrderDialogComponent } from './pages/store/after-sales/orders/dialogs/modifi-order-dialog/modifi-order-dialog.component';
import { AddCommentDialogComponent } from './pages/store/after-sales/orders/dialogs/add-comment-dialog/add-comment-dialog.component';
import { ProvisioningComponent } from './pages/store/after-sales/provisioning/provisioning.component';
import { SimComponent } from './pages/store/after-sales/provisioning/dialog/sim/sim.component';
import { ActionsComponent } from './pages/tools/users/actions/actions.component';
import { ActionsCreateDialogComponent } from './pages/tools/users/actions/dialogs/actions-create-dialog/actions-create-dialog.component';
import { SelectActionsComponent } from './pages/tools/users/profile/components/select-actions/select-actions.component';
import { UsersGuideDialogComponent } from './components/users-guide-dialog/users-guide-dialog.component';
import { ChangelogDialogComponent } from './components/changelog-dialog/changelog-dialog.component';
import { DeviceManagementForPlansComponent } from './pages/store/offer/product-detail/components/device-management-for-plans/device-management-for-plans.component';
import { PlansManagementForDevicesComponent } from './pages/store/offer/product-detail/components/plans-management-for-devices/plans-management-for-devices.component';
import { PlansDialogComponent } from './pages/store/offer/product-detail/dialogs/plans-dialog/plans-dialog.component';
import { LandingComponent } from './pages/tools/users/landing/landing.component';
import { AdminlComponent } from './pages/tools/users/adminl/adminl.component';
import { CrateLandingComponent } from './pages/tools/users/adminl/dialogs/crate-landing/crate-landing.component';
import { CampComponent } from './pages/tools/users/adminl/dialogs/camp/camp.component';


 



@NgModule({
  declarations: [

    ActionModuleDialogComponent,
    ActionsCreateDialogComponent,
    AppComponent,
    MenuListItemComponent,
    BreadcrumbComponent,
    HomeComponent,
    LogoutComponent,
    ModuleComponent,
    ConfirmDialogComponent,
    LoadingPageDialogComponent,
    CreateModuleDialogComponent,
    ProfileComponent,
    CreateProfileDialogComponent,
    PermissionsComponent,
    UserComponent,
    CreateUserDialogComponent,
    UserPermissionsComponent,
    ProfilesByUserComponent,
    LoginComponent,
    WithoutPermitsComponent,
    AppFailedComponent,
    InventoryComponent,
    CreateVersionDialogComponent,
    ProductDetailComponent,
    ImageManagementComponent,
    ImageUploadDialogComponent,
    FeatureManagementComponent,
    AvailableFeaturesDialogComponent,
    GroupersDialogComponent,
    CategoryTemplateDetailDialogComponent,
    ProductInfoDetailComponent,
    CategoriesComponent,
    DisableControlDirective,
    OrdersComponent,
    TrackinDialogComponent,
    DetailOrderDialogComponent,
    SupplierProductComponent,
    SupplierProductDetailDialogComponent,
    VersionManagementComponent,
    StepperCustomComponent,
    VersionDetailComponent,
    DownloadReportFilesDialogComponent,
    BrandsComponent,
    BrandCreateDialogComponent,
    FeaturesComponent,
    FeatureCreateDialogComponent,
    CategoryCreateDialogComponent,
    SeriesManagementComponent,
    UpdateSerieDialogComponent,
    SeriesDeliveryComponent,
    SeriesEmailsDialogComponent,
    SendSerialMailDialogComponent,
    IncomeReportComponent,
    MyGoogleChartsComponent,
    SaleProductReportComponent,
    ProductReportComponent,
    PurchaseOrderReportComponent,
    ContractComponent,
    DocumentsComponent,
    ModifiOrderDialogComponent,
    CustomerAbandonmentComponent,
    PasswordValidationDialogComponent,
    LoadFeatureImageDialogComponent,
    CatalogDialogComponent,
    ProvisioningComponent,
    SimComponent,
    AddCommentDialogComponent,
    ActionsComponent,
    SelectActionsComponent,
    UsersGuideDialogComponent,
    ChangelogDialogComponent,
    DeviceManagementForPlansComponent,
    PlansManagementForDevicesComponent,
    PlansDialogComponent,
    LandingComponent,
    AdminlComponent,
    CrateLandingComponent,
    CampComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularMaterialModule,
    CommonModule,
    HttpClientModule,
    NgxImageZoomModule,
    NgxUsefulSwiperModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  entryComponents: [
    ConfirmDialogComponent,
    LoadingPageDialogComponent,
    CreateModuleDialogComponent,

    ActionModuleDialogComponent,

    CreateProfileDialogComponent,
    CreateUserDialogComponent,
    CreateVersionDialogComponent,
    ImageUploadDialogComponent,
    AvailableFeaturesDialogComponent,
    GroupersDialogComponent,
    CategoryTemplateDetailDialogComponent,
    TrackinDialogComponent,
    DetailOrderDialogComponent,
    SupplierProductDetailDialogComponent,
    DownloadReportFilesDialogComponent,
    BrandCreateDialogComponent,
    FeatureCreateDialogComponent,
    CategoryCreateDialogComponent,
    UpdateSerieDialogComponent,
    SeriesEmailsDialogComponent,
    SendSerialMailDialogComponent,
    ModifiOrderDialogComponent,
    PasswordValidationDialogComponent,
    LoadFeatureImageDialogComponent,
    CatalogDialogComponent,
    AddCommentDialogComponent,
    UsersGuideDialogComponent
  ],
  providers: [
    NavService,
    ChecklistDatabase,
    { provide: APP_INITIALIZER, useFactory: appInitializer, multi: true, deps: [AuthSecureService] },
    //AccessService,
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: APP_BASE_HREF, useValue: '/' },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
