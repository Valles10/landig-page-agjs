import { AuthSecureService } from '../services/auth-secure.service';


export function appInitializer(authenticationService: AuthSecureService) {
    return () => new Promise(resolve => {
        // attempt to refresh token on app start up to auto authenticate
        authenticationService.refreshToken()
            .subscribe()
            .add(resolve);
    });
}