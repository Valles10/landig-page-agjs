import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from 'src/environments/environment';
import { formatDate, registerLocaleData } from '@angular/common';
import localeES from "@angular/common/locales/es";
registerLocaleData(localeES, "es");

export class Tools {

    constructor(private _snackBar?: MatSnackBar) { }

    showMessage(message: string, options?: { duration?, verticalPosition?, horizontalPosition?}) {
        if (options) {
            this._snackBar.open(message, 'x', options);

        } else {
            this._snackBar.open(message, 'x', {
                duration: 4000,
                verticalPosition: 'bottom',
                horizontalPosition: 'right',
            });
        }
    }

    removeItemFromArr(arr: Array<any>, item: any): boolean {
        let idx = arr.indexOf(item);
        let res: boolean = false;
        if (idx != -1) {
            arr.splice(idx, 1);
            res = true;
        }
        return res;
    }

    existsItemFromArr(arr: Array<any>, item: any): boolean {
        let res: boolean = false;
        if (Array.isArray(arr)) {
            let idx = arr.indexOf(item);
            if (idx != -1) {
                res = true;
            }
        }
        return res;
    }

    existsSizeFromArr(arr: Array<any>, item: any): boolean {
        let res = false;
        if (Array.isArray(arr)) {
            for (let i of arr) {
                if (item.width == i.width && item.height == i.height) {
                    res = true;
                    break;
                }
            }
        }
        return res;
    }

    getMaxSizeFromArr(arr: Array<any>): number {
        let res = 0;
        if (Array.isArray(arr)) {
            for (let item of arr) {
                let temp = 0;
                if (item.width > item.height)
                    temp = item.width;
                else
                    temp = item.height;
                if (temp > res)
                    res = temp;
            }
        }
        return res;
    }

    removeMultiItemFromArr(baseArr: Array<any>, arr: Array<any>): Array<any> {
        let arrRemoved: Array<any> = new Array<any>();
        if (Array.isArray(baseArr) && Array.isArray(arr)) {
            for (let item of arr) {
                if (this.removeItemFromArr(baseArr, item)) {
                    arrRemoved.push(item);
                }
            }
        }
        return arrRemoved;
    }

    getCountryCode() {
        let port = window.location.port;
        let code;
        switch (port) {
            case "4854":
                code = 502;
                break;
            case "788":

                break;
            default:
                code = 502;
                break;
        }
        return code;
    }

    getTimestamp(dateFormat?: string) {
        if (dateFormat) {
            return formatDate(new Date(), dateFormat, environment.dateFormat.locale);
        } else {
            return formatDate(new Date(), environment.dateFormat.format, environment.dateFormat.locale);
        }
    }



    public static getHost(url: string, iso: string) {
        if (url.includes('{portTL}')) {
            return url.replace('{portTL}', Tools.getApiPortTL(iso).port);
        } else if (url.includes('{portBO}')) {
            return url.replace('{portBO}', Tools.getApiPortBO(iso).port);
        } else {
            return url;
        }

    }

    public static getApiPortBO(iso: string): any {
        iso = iso?.toUpperCase() || '';
        let res = { port: '0000', status: false };

        for (let index = 0; index < environment.portsBackOffice.length; index++) {
            const element = environment.portsBackOffice[index];
            if (element.country == iso) {
                res.port = element.port;
                res.status = true;
                break;
            }
        }

        /* switch (iso) {
            case "GT":
                res.port = "28081";
                res.status = true;
                break;
            case "SV":
                res.port = "28082";
                res.status = true;
                break;
            case "HN":
                res.port = "28083";
                res.status = true;
                break;
            case "NI":
                res.port = "28084";
                res.status = true;
                break;
            case "CR":
                res.port = "28085";
                res.status = true;
                break;
            case "PA":
                res.port = "28086";
                res.status = true;
                break;
            default:
                res.port = "28081";
        } */
        return res;
    }

    public static getApiPortTL(iso: string): any {
        iso = iso?.toUpperCase() || '';
        let res = { port: '0000', status: false };

        for (let index = 0; index < environment.portsTiendaEnLinea.length; index++) {
            const element = environment.portsTiendaEnLinea[index];
            if (element.country == iso) {
                res.port = element.port;
                res.status = true;
                break;
            }
        }

        /* switch (iso) {
            case "GT":
                res.port = "28081";
                res.status = true;
                break;
            case "SV":
                res.port = "28082";
                res.status = true;
                break;
            case "HN":
                res.port = "28083";
                res.status = true;
                break;
            case "NI":
                res.port = "28084";
                res.status = true;
                break;
            case "CR":
                res.port = "28085";
                res.status = true;
                break;
            case "PA":
                res.port = "28086";
                res.status = true;
                break;
            default:
                res.port = "28081";
        } */
        return res;
    }
    public static validateCountry(iso) {

    }


}