export class File {
    id: number;
    name: string;
    path: string;
    url: string;
    historyFiles?: File[];
}