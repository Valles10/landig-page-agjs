/* import { Product } from "./Product";
import { Category } from "./Category";
import { Brand } from "./Brand";
import { Characteristic } from "./Characteristic";
import { Grouper } from './Grouper';
import { UserProfile } from './UserProfile'; */
import { Module } from './Module';
import { User } from './User';
//import { UserProfile } from './UserProfile';
import { Profile } from './Profile';
import { Product } from './Product';
import { ProductVersion } from './ProductVersion';
import { Feature } from './Feature';
import { Brand } from './Brand';
import { Category, CategoryTypes } from './Category';
import { Grouper } from './Grouper';
import { Order } from './Order';
import { Tracking } from './Tracking';
import { DetailOrder } from './DetailOrder';
import { LoadReport } from './LoadReport';
import { Serie } from './Serie';
import { Statistics } from './Statistics';
import { Department } from './Department';
import { Towns, Village } from './Municipio';
import { Contract } from './contract';
import { Sim } from './sim';
import { Action } from './Actions';
import { Actionsprofiles } from './Actions-profies';
import { Plan } from './Plan';
import { File } from './File';
import { UrlTL } from './Url-TL';
import { Group } from './Group';
import { Document } from './document';

/* export interface InventoryResponse {
  code: number;
  result: {
    products: Array<Product>;
  };
} */


export interface MyResponse {
  code: number;
  result: {
    status: string;
    message: string;
    data: {
      actions: Array<Action>;
      actionsProfiles: Array<Actionsprofiles>;
      plans: Array<Plan>;
      modules: Array<Module>;
      module: Module;
      profiles: Array<Profile>;
      UserPermissions: Array<string>;
      orders: Array<Order>;
      tracking: Tracking;
      order: Array<DetailOrder>;
      Department: Array<Department>;
      Towns: Array<Towns>;
      Contract: Array<Contract>;
      Contracts: Array<Contract>;
      documents: Array<Document>;
      Village: Array<Village>;
      sim: Array<Sim>;
      files: Array<File>;

      configuredLinks: Array<UrlTL>;
      groups: Array<Group>
      /*       products: Array<Product>;
brands: Array<Brand>;
categories: Array<Category>;
characteristics : Array<Characteristic>; */
      category_name: string;
      characteristic: Feature;
      /*       ;
            
             */
      /*       features: {
              categoryId: Array<Characteristic>
              id: any
            };
            groupers: Array<Grouper>; */
      imagePaths: Array<string>;
      users: Array<User>;
      /*       userProfiles: Array<UserProfile>;
            userProfile: UserProfile;
            permission: Permission; */
      permissions: Array<Module>;

      userProfiles: Array<Profile>;
      userProfile: Profile;
      user: User;
      categoriesTree: Array<Category>;
      category: Category;
      products: Array<Product>;
      producs: Array<Product>;
      localInventory: Array<Product>;
      versions: Array<ProductVersion>;
      characteristics: Array<Feature>;
      brands: Array<Brand>;
      categories: Array<Category>;
      groupers: Array<Grouper>;
      version: ProductVersion;
      reports: Array<LoadReport>;
      category_types: Array<CategoryTypes>;
      virtuals: Array<Serie>;
      statistics: Array<Statistics>;
      total_ingresos: number;
      total: number;
      tracerReport: Array<any>;
    };
  };
}


