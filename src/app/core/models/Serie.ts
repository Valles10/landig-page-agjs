export class Serie {
    moveDate: string;
    serviceCode: number;
    idRange: number
    idWarehouse: number;
    delAttempt: string
    rangeNum: string;
    loadDate: string;
    status: string;
    sendDate: string;
    email: string
    userName: string;
    keyNum: string;
    message: string;
    name: string;
    sku: string;
}


