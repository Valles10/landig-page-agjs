export class UserProfileTwo{
    updated_at: string;
    name: string;
    description: string;
    id_profile: number;
    status: string;
    idPermission: string;
}