export class Country {
    code: string;
    value: string;
    name: string;
}