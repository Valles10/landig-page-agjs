export class Plan{
    tmcode: string;
    /* plazos: Array<string>; */
    planPrices: Array<Price>;
}


export class Price{
    price: string;
    term: string;
    check?: boolean;
}