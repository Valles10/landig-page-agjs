export class Action {
    id_action?: number;
    name: string;
    code: string;
    status: number;
    updatedAt: string;
}