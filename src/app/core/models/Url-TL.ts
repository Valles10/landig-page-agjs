export class UrlTL{
    id: number; 
    code: string;
    name: string;
    value: string; 
    description: string; 
    updateAta: string; 
}