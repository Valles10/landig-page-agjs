export class Profile {
    updated_at: string;
    name: string;
    description?: string;
    id_profile: number;
    status?: string;
    base_profile? : string;
    idPermission?: string;
    check?: boolean;
    profileType?: string;
    
}