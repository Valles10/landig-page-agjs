import { Feature } from "./Feature";
import { Category } from './Category';

export class Product {
  sku: string;
  name: string;
  category: number;
  category_name: string;
  price: string;
  supplier: string;
  supplier_name: string;
  description: string;
  status: number;
  brand: number;
  brand_name: string;
  features: Array<Feature>;
 // public_price: string;
  nameOld?: string;
  group_id?: string;
  lastModify?: string;
  offer_price?: string;
  sale_price?: string;
  version?: number;
  statusVersion?: number; // Estado si el producto se encuentra en una version
  oldStatusVersion?: number; // estado antiguo de la version
  statusChangeVersion?: number; // 0 sin cambios de version, 1 añadira a una version, 2 eliminara de una version
  //orderVersion?: number; 
  skuSim?: string;
  tmCode?: string;
  categoryType?: string;
  categoryTypeId: string;

  existencias: string;
  precio: string;
  tipo: string;
  nombre: string;
}
