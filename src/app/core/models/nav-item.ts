export class NavItem {
    title: string;
    disabled?: boolean;
    icon: string;
    route?: string;
    children?: NavItem[];
  }
  