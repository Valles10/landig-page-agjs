export class Actionsprofiles {
    id_action?: number;
    name: string;
    code?: string;
    idProfile?: number;
    idActionModule?: number;
    idModule?: number;
    id_view_action_module?: number;
    estado?: string;
    lock?: string;
    oldStatus?: string;
}