export class Grouper {
    id: number;
    brandName: string;
    name: string;
    productName: string;
    productDescription: string;
    sku: string;
    productSKU: string;
  }
  