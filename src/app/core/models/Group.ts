export class Group {
    id: number;
    name: string;
    category: string;
    docs: string;
    lastModify: string;
  }
  