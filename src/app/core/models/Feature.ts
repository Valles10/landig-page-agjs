export class Feature {
    id: number;
    name?: string;
    text?: string;
    traduction?: string;
    description?: string;

    value?: string;
    position?: number;
    identifier?: string;
    // 0 = No cambio
    // 1 = Nuevo
    // 2 = Actualizacion
    // 3 = Eliminacion
    status?: number;
    editable?: boolean;
    oldValue?: string;
    lock?: number;
    selected?: boolean;
    category?: string;
    lastModify?: string;

    general?: string;
      displayable?: string;
      image?: string;
      filter?: string;
      placeholder?:string;
      boolean_value?: string;
  }
  