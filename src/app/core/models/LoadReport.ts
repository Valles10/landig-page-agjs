export class LoadReport {
    idUser: string;
    country: 502;
    idEnviroment: string;
    Enviroment: string;
    uploadDate: string;
    executionDate: string;
    executionResult: string;
    uploadStatus: string;
    userEmail: string;
    userName: string;
    loadedFiles: Array<any>;
}