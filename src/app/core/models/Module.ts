export class Module {
    idModule: number;
    id_module?: number;
    name: string;
    //description?: string;
    route: string;
    lastModify?: string;
    idPermissions?: string;
    status?: number;
    active?: boolean;
    lock?: string;
    idPlatform: number;
    oldStatus?: number;
    idPermission?: string;
    moduleType?: string;
} 
  