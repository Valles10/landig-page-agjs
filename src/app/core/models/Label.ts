export class Label {
    idPlaceHolder: number;
    isCreated: boolean;
    placeHolderCode: string;
    placeHolderName: string;
    subPlaceHolderList: Label[];
}