export class User {
    idUser?: number;
    name: string;
    userName: string;
    email?: string;
    country?: number;
    status?: string; //A - Activo, I - Inactivo
    createdAt?: string;
    updatedAt?: string;
    jwt?: string;
    nameProfile?: string;
    password?: string;
    role?: string;
    lastModify?: string;
    authdata?: string;
    token?: string;
    userType?: string; 
    profileType?: string;
    profile?: string;
    oldUserName?: string;
    isoCode?: string;
}


