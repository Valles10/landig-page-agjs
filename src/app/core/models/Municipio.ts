export class Towns {
   TOWN_NAME: string;
   COD_DEPARTMENT: string;
   TOWN_CODE: string;
}

export class Village {
   VILLAGE_CODE: string;
   TOWN_CODE: string;
   VILLAGE_NAME: string;
}