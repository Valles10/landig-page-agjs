export class Document {
    dateStore: string;
    contractNo: string;
    documentTypeName: string;
    url: string;

    /* Documentos de contratos */
    createdAt: string;
    path: string;
    name: string;
    updateAt: string;
    description: string;
    id: string;
    status: string;
    systemReserved: string;
    fileRepositoryName: string;
    fileEditionChange: boolean; 
    fileEdition: File;
}