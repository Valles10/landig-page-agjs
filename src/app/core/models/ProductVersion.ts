export class ProductVersion {
    versionId?: number;
    idVersion?: number;
    name: string;
    editable?: number; // estado de la version para edicion 0: no editable; 1: es editable 
    baseVersion?: number;
    lastUpdate?: string;
    status?: string; // "A": Activo, I: Inactivo
    idParentVersion?: number;
    display?:boolean;
    versionStatus?: string;
  }