export interface Tracking {

    numeroGuia: string;
    remitente: string;
    destinatario: string;
    destinatarioDireccion: string;
    destinatarioTelefono: string;
    fecha: string;
    hora: string;
    status: string;
    eventos: Array<Evento>;
}
export interface Evento {
    codigoRuta: string;
    movimiento: string;
    nombreRuta: string;
    tipoMovimiento: string;
    fechaHoraScan: string;
}