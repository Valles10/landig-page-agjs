export class ViewOptions {
    displayedColumns: Array<string>;
    pageSize: Array<number>;
    loading: boolean;
}