export class Category {
    id?: number;
    idCategory?: string;
    name?: string;
    position?: number;
    status?: number;
    children?: Array<Category>
    parentId?: string;
    description?: string;
    categoryTypeId?:string;
    macroCategory?: boolean;
    display?: boolean;
    categoryType? : string;
    groupOfDocuments?: string;
    constructor( id? :number, name? :string, idCategory?: string, children?: Array<Category>, parentId?: string){
      this.idCategory = idCategory || '';
      this.id= id || 0;
      this.name = name || '';
      this.children = children || [];
      this.parentId = parentId || '';
    }
  }

  export class CategoryTypes{
    name: string;
    id_category_type: string;
  }
  