export interface DetailOrder {
    orderDetailId: string;
    orderId: string;
    sku: string;
    productHierarcy: string;
    paymentProduct: string;
    discountPrice: string;
    serviceTypeId: string
    activationId: string;
    categoryCode: string
    simDumi: string;
    inputUser: string;
    inputDate: string;
    activationDate: string;
    activationMessage: string;
    contract: string;
    phoneNumber: string;
    courtDay: string;
    cycle: string;
    clientId: string;
    name: string;
}

