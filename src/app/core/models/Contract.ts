export class Contract{
    sku:string;
    no:number;
    orderDetail:string;
    orderOda:string;
    status:string;
    phoneNumber:string;
    clientId:string;
    contract: string;
    orderWcsid: string;
    courtDay:string;
    cycle:string;
    username:string;
    sim:string;
    simTem?:string;
    orderDetailId: string;
}