export class Brand {
    id: number;
    name: string;
    status: number;
    description: string;
    lastModify: string;
}