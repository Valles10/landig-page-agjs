import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MyResponse } from '../models/MyResponse';


@Injectable({
  providedIn: 'root'
})
export class ActionService {

  constructor(private http: HttpClient) { }

  public getActions(): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/users/action`);
  }

  public saveActions(param: any): Observable<MyResponse> {
    return this.http.post<MyResponse>(`${environment.apiBackend}/users/action`, param);
  }

  public managementActions(params: any): Observable<MyResponse> {
    return this.http.put<MyResponse>(`${environment.apiBackend}/users/action`, params);
  }

  public deleteAction(idAction: number): Observable<any> {
    return this.http.delete(`${environment.apiBackend}/users/action/${idAction}`);
  }
}
