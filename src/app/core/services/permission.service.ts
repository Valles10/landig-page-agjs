import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  constructor() { }

  public objectpermission: {};

  setPermission(obj: any) {
    this.objectpermission = obj;
  }

  getPermission(path) {
    if (typeof this.objectpermission[path] == 'undefined') {
      return this.objectpermission[path] = [];
    } else {
      return this.objectpermission[path];
    }
  }

  public resetPermissions(){
    this.objectpermission = {};
  }
}
