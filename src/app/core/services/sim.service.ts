import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MyResponse } from '../models/MyResponse';



@Injectable({
    providedIn: 'root'
  })
export class SimService{
    constructor(private http: HttpClient) { }


  public getAll(): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/getGeneralInventory`);
  }
}