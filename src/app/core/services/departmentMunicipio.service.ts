import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MyResponse } from '../models/MyResponse';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
//import { UserProfile } from '../models/UserProfile';
import { Profile } from '../models/Profile';

@Injectable({
  providedIn: 'root'
})
export class DepartmentMunicipio {

  constructor(private http: HttpClient) { }
  // /TL_BO_BackEnd/webresources

  public getDepartment(): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiTiendaEnLinea}/place/Deparment`);
  }

  public getMunicipio(idDepartamento: string): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiTiendaEnLinea}/place/Town`, {
      params:
      {
        COD_DEPARTMENT: idDepartamento
      }
    });
  }
  public getVillage(idVillage: string): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiTiendaEnLinea}/place/Village`, {
      params:
      {
        TOWN_CODE: idVillage
      }
    });
  }
}
