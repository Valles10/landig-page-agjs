import { Injectable, Version } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MyResponse } from '../models/MyResponse';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ProductVersion } from '../models/ProductVersion';

@Injectable({
  providedIn: 'root'
})
export class SerieService {

  constructor(private http: HttpClient) { }

  public getAll(_params): Observable<MyResponse> {    
    return this.http.get<MyResponse>(`${environment.apiBackend}/virtual/range`,{
      params: _params
    });
  }

  public getSent(id): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/virtual/range/${id}`);
  }

  public addKey(body){
    return this.http.put<MyResponse>(`${environment.apiBackend}/virtual/range`,body);
  }
  
  public sendMail(body){
    return this.http.post<MyResponse>(`${environment.apiBackend}/virtual/range`,body);
  }
}
