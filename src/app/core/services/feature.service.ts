import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MyResponse } from '../models/MyResponse';
import { Feature } from '../models/Feature';

@Injectable({
  providedIn: 'root'
})
export class FeatureService {
  constructor(private http: HttpClient) { }


  public getAvailableFeatures(params: any): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/GetAvailableCharacteristics`, { params: params });
  }

  public updateFeatures(params: any):
    Observable<MyResponse> {
    return this.http.post<MyResponse>(`${environment.apiBackend}/UpdateLocalCharacteristics`, params, /* {
      observe: "response" as 'body',
    } */);
  }

  public getFeatures(): Observable<MyResponse> {
    return this.http.post<MyResponse>(`${environment.apiBackend}/ManagementCharacteristics`, {
      status: 4,
    });
  }

  public saveFeatures(feature: Feature): Observable<MyResponse> {
    if (feature.id)
      feature.status = 2;
    else
      feature.status = 3;
    return this.http.post<MyResponse>(`${environment.apiBackend}/ManagementCharacteristics`, feature);
  }

  public deleteFeature(id: string) {
    return this.http.post<MyResponse>(`${environment.apiBackend}/ManagementCharacteristics`, {
      id: id,
      status: 1
    });
  }


  /* public getCategoryFeatures(params: any): Observable<Response> {
    return this.http.get<Response>(`${environment.apiBackend}/GetAvailableCharacteristics`, { params: params });
  } */
/* 
  public getAll(): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/users/user`);
  }

  public save(user: any): Observable<any> {
    return this.http.post<any>(`${environment.apiBackend}/users/user`, user);
  }

  public update(user: any): Observable<any> {
    return this.http.put<MyResponse>(`${environment.apiBackend}/users/user`, user);
  }

  activeProfile(idUser: string, data: any): Observable<MyResponse> {
    return this.http.post<MyResponse>(`${environment.apiBackend}/users/user/${idUser}/profile`, data);
  }

  removeProfile(idUser: string, data: any): Observable<MyResponse> {
    return this.http.delete<MyResponse>(`${environment.apiBackend}/users/user/${idUser}/profile`, {
      params: { 
        idProfile: data.idProfile
      }
    });
  }

  public getPermissions(): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/users/verifyAccess`);
  } */
}
