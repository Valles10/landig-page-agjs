import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MyResponse } from '../models/MyResponse';

@Injectable({
  providedIn: 'root'
})
export class PropertiesService {
  constructor(private http: HttpClient) { }

  public getImage(): Observable<any> {
    return this.http.get<any>(`${environment.apiBackend}/properties/site/images`);
  }
  public saveImage(body: any): Observable<any> {
    return this.http.put<any>(`${environment.apiBackend}/properties/site/images`,body);
  }


  /* public getPrepaid(){
    return this.http.get<any>(`${environment.apiBackend}/properties/site/images`);
  }
 */
  public getDefaultSKU(){
    return this.http.get<any>(`${environment.apiBackend}/properties/site/images`);
  }

  
  getSkuExcludes(): Observable<any>{
    return this.http.get<any>(`http://10.218.41.90:4854/tienda_en_linea_RG2/resources/legacy?key=2`);
  }

  public saveSkuExludes(body: any): Observable<any> {
    return this.http.put<any>(`${environment.apiBackend}/properties/site/images`,body);
  }


}
