import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MyResponse } from '../models/MyResponse';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
//import { UserProfile } from '../models/UserProfile';
import { Profile } from '../models/Profile';

@Injectable({
  providedIn: 'root'
})
export class TrackingService {

  constructor(private http: HttpClient) { }
  // /TL_BO_BackEnd/webresources

  public get(parametro: string): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/reports/orders/trakin`, {
      params:
      {
        tracking_order: parametro
      }
    }
    );
  }

}
