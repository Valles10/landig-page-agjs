import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MyResponse } from '../models/MyResponse';

@Injectable({
  providedIn: 'root'
})
export class ActionsProfilesService {

  constructor(private http: HttpClient) { }

  public getActions(idProfile: number, idModule: number): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/users/action/${idProfile}/${idModule}/profile`);
  }

  public saveActions(param: any): Observable<MyResponse>{
    return this.http.post<MyResponse>(`${environment.apiBackend}/users/action/profile`, param);
  }
}
