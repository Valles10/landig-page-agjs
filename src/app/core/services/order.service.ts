import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MyResponse } from '../models/MyResponse';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
//import { UserProfile } from '../models/UserProfile';
import { Profile } from '../models/Profile';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) { }
  // /TL_BO_BackEnd/webresources

  public get(parametro?:any): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/reports/orders`,{params:parametro});
  }

  public post(parametros,id:string):Observable<MyResponse>{
    return this.http.post<MyResponse>(`${environment.apiTiendaEnLinea}/orders/order/${id}`,parametros);
  }

  

}
