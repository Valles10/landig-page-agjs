import { Injectable, Version } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MyResponse } from '../models/MyResponse';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ProductVersion } from '../models/ProductVersion';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  constructor(private http: HttpClient) { }


  public get(): Observable<MyResponse> {
    return this.http.get<MyResponse>(`http://10.218.41.26:28081/ServiceSimulator-v2/webresources/service/tlbo/groups-flow`);
  }

  public delete(id): Observable<MyResponse> {
    return this.http.get<MyResponse>(`http://10.218.41.26:28081/ServiceSimulator-v2/webresources/service/tlbo/groups-flow`);
  }
  public update(body): Observable<MyResponse> {
    return this.http.get<MyResponse>(`http://10.218.41.26:28081/ServiceSimulator-v2/webresources/service/tlbo/groups-flow`);
  }
  

}
