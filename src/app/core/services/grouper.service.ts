import { Injectable, Version } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MyResponse } from '../models/MyResponse';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ProductVersion } from '../models/ProductVersion';

@Injectable({
  providedIn: 'root'
})
export class GrouperService {

  constructor(private http: HttpClient) { }


  public getGroupers(): Observable<MyResponse> {
//    return this.http.get<MyResponse>(`${environment.apiBackend}/ManagementLocalinventory/groupers`);
    return this.http.get<MyResponse>(`${environment.apiBackend}/inventory/local/groupers`);
  }
  /* public get(): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/users/profile`);
  }

  public getByUser(idUser: string): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/users/user/${idUser}/profile`);
  }
*/
  public get(): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/inventory/version`);
  } 
  public update(version: ProductVersion): Observable<MyResponse> {
    return this.http.put<MyResponse>(`${environment.apiBackend}/inventory/version`, version);
  }

  public create(version: ProductVersion): Observable<MyResponse> {
    return this.http.post<MyResponse>(`${environment.apiBackend}/inventory/version`, version);
  }

  public delete(idVersion: number): Observable<MyResponse> {
    return this.http.delete<MyResponse>(`${environment.apiBackend}/inventory/version/${idVersion}`);
  }

  public addProduct(idVersion: number, idProduct){
    return this.http.put<MyResponse>(`${environment.apiBackend}/inventory/version/${idVersion}/product/${idProduct}`,{});
  }

  public deleteProduct(idVersion: number, idProduct){
    return this.http.delete<MyResponse>(`${environment.apiBackend}/inventory/version/${idVersion}/product/${idProduct}`);
  }





}
