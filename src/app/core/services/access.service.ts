import { Injectable } from '@angular/core';
import { Session } from 'src/app/core/models/Session';
import { Router } from '@angular/router';
import { User } from 'src/app/core/models/User';
import { Module } from 'src/app/core/models/Module';

@Injectable()
export class AccessService {
  private localStorageService;
  private currentAccess: Array<Module> = null;

  constructor(private router: Router) {
    this.localStorageService = localStorage;
    this.currentAccess = this.loadAccessData();
  }
  setCurrentSession(access: Array<Module>): void {
    this.currentAccess = access;
    this.localStorageService.setItem('access', JSON.stringify(access));
  }

  loadAccessData(): Array<Module> {
    var sessionStr = this.localStorageService.getItem('access');
    return (sessionStr) ? <Array<Module>>JSON.parse(sessionStr) : null;
  }
  
  getCurrentAccess(): Array<Module> {
    return this.currentAccess;
  }

  removeCurrentAccess(): void {
    this.localStorageService.removeItem('access');
    this.currentAccess = null;
  }

  /* getCurrentUser(): User {
    var access: Array<Module> = this.getCurrentAccess();
    return (access && access.user) ? session.user : null;
  }; */

  /* isAuthenticated(): boolean {
    return (this.getCurrentToken() != null) ? true : false;
  }; */

  /* getCurrentToken(): string {
    var session = this.getCurrentSession();
    return (session && session.token) ? session.token : null;
  }; */

  /* ogout(): void {
    this.removeCurrentSession();
    this.router.navigate(['/login']);
  } */

}