import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Category } from '../models/Category';
import { MyResponse } from '../models/MyResponse';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  constructor(private http: HttpClient) { }



  public getCategories(): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/GetCategory`);
  }

  public getCategoryTemplate(params: any): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/GetCategoryTemplate`, { params: params });
  }

  public getCategoriesTree(): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/category`);
  }

  public getTypes(): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/inventory/category/type`);
  }

  public addCategory(category: Category): Observable<MyResponse> {
    if (category.idCategory)
      category.idCategory = '';
    if (category.parentId)
      category.parentId = category.parentId == '-1' ? '' : category.parentId;
    category.status = 3;
    return this.http.post<MyResponse>(`${environment.apiBackend}/category/management`, category);
  }


  public updateCategory(category: Category): Observable<MyResponse> {
    category.status = 2;
    return this.http.post<MyResponse>(`${environment.apiBackend}/category/management`, category);
  }

  public deleteCategory(category: Category): Observable<MyResponse> {
    category.status = 1;
    return this.http.post<MyResponse>(`${environment.apiBackend}/category/management`, category);
  }

  public associateFeatures(params: any): Observable<MyResponse> {
    return this.http.post<MyResponse>(`${environment.apiBackend}/category/association`, params);
  }
  public dissasociateFeatures(params: any): Observable<MyResponse> {
    return this.http.post<MyResponse>(`${environment.apiBackend}/category/association`, params);
  }

  public getFeatures(params: any): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/category/features`, { params: params });
  }



/* 
  public getAll(): Observable<MyMyResponse> {
    return this.http.get<MyMyResponse>(`${environment.apiBackend}/users/user`);
  }

  public save(user: any): Observable<any> {
    return this.http.post<any>(`${environment.apiBackend}/users/user`, user);
  }

  public update(user: any): Observable<any> {
    return this.http.put<MyMyResponse>(`${environment.apiBackend}/users/user`, user);
  }

  activeProfile(idUser: string, data: any): Observable<MyMyResponse> {
    return this.http.post<MyMyResponse>(`${environment.apiBackend}/users/user/${idUser}/profile`, data);
  }

  removeProfile(idUser: string, data: any): Observable<MyMyResponse> {
    return this.http.delete<MyMyResponse>(`${environment.apiBackend}/users/user/${idUser}/profile`, {
      params: { 
        idProfile: data.idProfile
      }
    });
  }

  public getPermissions(): Observable<MyMyResponse> {
    return this.http.get<MyMyResponse>(`${environment.apiBackend}/users/verifyAccess`);
  } */
}
