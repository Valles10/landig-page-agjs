import { TestBed } from '@angular/core/testing';

import { ActionsProfilesService } from './actions-profiles.service';

describe('ActionsProfilesService', () => {
  let service: ActionsProfilesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ActionsProfilesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
