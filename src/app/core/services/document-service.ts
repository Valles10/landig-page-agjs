import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { MyResponse } from '../models/MyResponse';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  constructor(private http: HttpClient) { }

  public get(param: any): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiTiendaEnLinea}/documents`, { params: param });
  }

  public getFiles(): Observable<MyResponse> {
    /* return this.http.get<MyResponse>(`${environment.apiBackend}/GetBrand`); */
    /* http://10.218.41.26:28081/ServiceSimulator-v2/webresources/rest/tlbo/files */
    return this.http.get<MyResponse>(`http://10.218.41.26:28081/TL_BO_BackEnd/webresources/files/management/getAllFiles`/* `http://10.218.41.26:28081/ServiceSimulator-v2/webresources/service/tlbo/files` */);
  }
  public getFilesFlow(): Observable<MyResponse> {
    /* return this.http.get<MyResponse>(`${environment.apiBackend}/GetBrand`); */
    /* http://10.218.41.26:28081/ServiceSimulator-v2/webresources/rest/tlbo/files */
    return this.http.get<MyResponse>(/* `${environment.apiBackend}/GetBrand` */`http://10.218.41.26:28081/ServiceSimulator-v2/webresources/service/tlbo/files-flow`);
  }

  public managementBrand(params: any): Observable<MyResponse> {
    return this.http.post<MyResponse>(`${environment.apiBackend}/ManagementBrand`, params);
  }

  wordToPdf(body): Observable<any> {

    return this.http.post<any>(`http://10.218.41.130/api/PDFconverter`, body);
  }


  /*  getBlob(path): Observable<Blob> {
       return this.http.get<Blob>(path, { responseType: 'blob' as 'json', observe: 'response' });
   } */

  /* getBlob(path: string): Observable<any> {
    const httpOptions = {
      observe: 'response' as 'body',
      responseType: 'blob' as 'blob'
    };
    return this.http.get(path,  { responseType: 'blob', observe: 'response' });
  } */

  getBlob(path): Observable<any> {
    return this.http.get(path,{ responseType: "blob" });
  }
}