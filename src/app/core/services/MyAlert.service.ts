import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

declare let iziToast: any;
@Injectable({
  providedIn: 'root'
})
export class MyAlertService {


  private iziToastOptios = {
    theme: 'dark',
    // title: 'Info!',
    title: '',
    displayMode: 'once',
    pauseOnHover: true,
    position: 'bottomRight',
    icon: 'material-icons',
    iconText: 'info',
    backgroundColor: '#212121',
    iconColor: '#fff',
    titleSize: '16'
  };

  private snackBarOptions = {
    duration: 4000,
    verticalPosition: 'bottom',
    horizontalPosition: 'right',
  };

  constructor(private _snackBar: MatSnackBar) {
    if (iziToast) {
      iziToast.settings({
        //icon: 'material-icons', 
        timeout: 10000,
        // resetOnHover: false,
      });
    }
  }

  info(message) {
    if (iziToast) {
      this.iziToastOptios.backgroundColor = '#0288D1';
      this.iziToastOptios.title = message;
      this.iziToastOptios.iconText = 'info';
      iziToast.show(this.iziToastOptios);
    } else {
      this._snackBar.open(message, 'x', <any>this.snackBarOptions);
    }

  }

  warn(message) {
    if (iziToast) {
      this.iziToastOptios.backgroundColor = '#FFC400';
      this.iziToastOptios.title = message;
      this.iziToastOptios.iconText = 'warning';
      iziToast.show(this.iziToastOptios);
    } else {
      this._snackBar.open(message, 'x', <any>this.snackBarOptions);
    }

  }

  success(message) {
    if (iziToast) {
      this.iziToastOptios.backgroundColor = '#00BFA5';
      this.iziToastOptios.title = message;
      this.iziToastOptios.iconText = 'check_circle';
      iziToast.show(this.iziToastOptios);
    } else {
      this._snackBar.open(message, 'x', <any>this.snackBarOptions);
    }

  }

  error(message) {
    if (iziToast) {
      this.iziToastOptios.backgroundColor = '#D50000';
      this.iziToastOptios.title = message;
      this.iziToastOptios.iconText = 'error';
      iziToast.show(this.iziToastOptios);
    } else {
      this._snackBar.open(message, 'x', <any>this.snackBarOptions);
    }

  }
}