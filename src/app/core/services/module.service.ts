import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MyResponse } from '../models/MyResponse';
import { environment } from 'src/environments/environment';
import { Module } from '../models';

@Injectable({
  providedIn: 'root'
})
export class ModuleService {
  constructor(private http: HttpClient) { }


  public get(): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/users/module`);
  }

  public update(module: any): Observable<MyResponse> {
    return this.http.put<MyResponse>(`${environment.apiBackend}/users/module`, module);
  }

  public save(module: any): Observable<MyResponse> {
    return this.http.post<MyResponse>(`${environment.apiBackend}/users/module`, module);
  }

  public saveByProfile(idProfile: any, modules): Observable<MyResponse> {
    return this.http.put<MyResponse>(`${environment.apiBackend}/users/profile/${idProfile}/permission`,modules);
  } 

  public getByProfile(idProfile: any): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/users/profile/${idProfile}`);
  }
}
