import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MyResponse } from '../models/MyResponse';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
//import { UserProfile } from '../models/UserProfile';
import { Profile } from '../models/Profile';

@Injectable({
  providedIn: 'root'
})
export class ContractService {

  constructor(private http: HttpClient) { }
  // /TL_BO_BackEnd/webresources

  public get(parametro?:any): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/reports/contract`,{params:parametro});
  }

  public getNewContractList(parametro?:any): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/reports/contract/newContractList`,{params:parametro});
  }

  public getSimList(idsku:any): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/reports/contract/simList`,{params:{sku:idsku}});
  }

  public postAprovicionar(parametro:any): Observable<MyResponse> {
    return this.http.post<MyResponse>(`${environment.apiTiendaEnLinea}/postpago/provisioning`,parametro);
  }
}
