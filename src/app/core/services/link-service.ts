import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MyResponse } from '../models/MyResponse';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
//import { UserProfile } from '../models/UserProfile';
import { Profile } from '../models/Profile';

@Injectable({
  providedIn: 'root'
})
export class LinkService {

  constructor(private http: HttpClient) { }
  // /TL_BO_BackEnd/webresources

  public get(): Observable<MyResponse> {
    return this.http.get<MyResponse>(`http://10.218.41.26:28081/ServiceSimulator-v2/webresources/service/tlbo/configuration-links`);
  }

  public delete(id:string):Observable<MyResponse>{
    return this.http.delete<MyResponse>(`${environment.apiTiendaEnLinea}/url/${id}`);
  }

  public save(item):Observable<MyResponse>{
    return this.http.post<MyResponse>(`${environment.apiTiendaEnLinea}/url/`,item);
  }


  public update(item):Observable<MyResponse>{
    return this.http.post<MyResponse>(`${environment.apiTiendaEnLinea}/url/`,item);
  }
 
  

}
