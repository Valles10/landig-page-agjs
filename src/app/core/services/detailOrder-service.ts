import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MyResponse } from '../models/MyResponse';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DetailOrderService {

  constructor(private http: HttpClient) { }

  public get(idOrder:string): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/reports/orders/${idOrder}/detail`);
  }


  public changeSKU(body, idOrder){
    return this.http.post<any>(`${environment.apiTiendaEnLinea}/orders/order/disagreement/${idOrder}`,body);
  }

}
