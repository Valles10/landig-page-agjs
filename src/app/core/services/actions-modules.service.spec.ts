import { TestBed } from '@angular/core/testing';

import { ActionsModulesService } from './actions-modules.service';

describe('ActionsModulesService', () => {
  let service: ActionsModulesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ActionsModulesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
