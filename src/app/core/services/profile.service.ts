import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MyResponse } from '../models/MyResponse';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
//import { UserProfile } from '../models/UserProfile';
import { Profile } from '../models/Profile';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http: HttpClient) { }


  public get(): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/users/profile`);
  }

  public getByUser(idUser: string): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/users/user/${idUser}/profile`);
  }

  public update(profile: any): Observable<MyResponse> {
    return this.http.put<MyResponse>(`${environment.apiBackend}/users/profile`, profile);
  }

  public save(profile: any): Observable<MyResponse> {
    return this.http.post<MyResponse>(`${environment.apiBackend}/users/profile`, profile);
  }


  


}
