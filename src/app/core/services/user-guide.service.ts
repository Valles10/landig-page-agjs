import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserGuideService {
  constructor(private http: HttpClient) { }

  public getHtml(): Observable<any> {
    const requestOptions: Object = {
      /* other options here */
      responseType: 'text'
    }
    return this.http.get<any>(`assets/user-guide/Manual de usuario c273274e2d494d51817323542a324e13.html`, requestOptions);
  }

  public getChangelog(): Observable<any> {
    const requestOptions: Object = {
      /* other options here */
      responseType: 'text'
    }
    return this.http.get<any>(`assets/changelog.md`, requestOptions);
  }
}
