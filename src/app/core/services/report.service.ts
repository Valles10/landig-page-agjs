import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthSecureService } from './auth-secure.service';
import { MyResponse } from '../models/MyResponse';

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  constructor(private http: HttpClient,
    private authSecureService: AuthSecureService) { }

  public income(params): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/reports/statistics/sales/income`,{
      params: params
    });
  }

  public orders(params): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/reports/statistics/sales/orders`,{
      params: params
    });
  }

  public purchases(params): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/reports/statistics/sales/orders/status`,{
      params: params
    });
  }

  public getTopProducts(params): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/reports/statistics/sales/product`, {
      params: params
    });
  }

  public customerAbandonment(params): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiTiendaEnLinea}/orders/reports/tracer`, {
      params: params
    });
  }
}
/* export class Purchase{
  input_date: string;
  total_purchses: number
}


export class ResponseReport{

} */