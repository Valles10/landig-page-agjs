import { Injectable } from '@angular/core';
import * as SecureLS from 'secure-ls';
import { environment } from 'src/environments/environment';


@Injectable({ providedIn: 'root' })
export class SecureLSService {

    private ls;

    constructor() {
        this.ls = new SecureLS({
            encodingType: 'aes',
            isCompression: false
        });
    }

    set(key: string, value: any) {
        try {
            this.ls.set(`${environment.sufixStorage}${key}`, value);
        } catch (_e) {
            return null; 
        }
    }

    get(key: string) {
        try {
            return this.ls.get(`${environment.sufixStorage}${key}`);
        } catch (_e) {
            return null;
        }
    }

    remove(key: string) {
        try {
            return this.ls.remove(`${environment.sufixStorage}${key}`);
        } catch (_e) {
            return null;
        }
    }

}