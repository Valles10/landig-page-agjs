import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MyResponse } from '../models/MyResponse';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) { }

  public getAll(): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/users/user`);
  }

  public save(user: any): Observable<any> {
    return this.http.post<any>(`${environment.apiBackend}/users/user`, user);
  }

  public update(user: any): Observable<any> {
    return this.http.put<MyResponse>(`${environment.apiBackend}/users/user`, user);
  }

  public activeProfile(idUser: string, data: any): Observable<MyResponse> {
    return this.http.post<MyResponse>(`${environment.apiBackend}/users/user/${idUser}/profile`, data);
  }

  public removeProfile(idUser: string, data: any): Observable<MyResponse> {
    return this.http.delete<MyResponse>(`${environment.apiBackend}/users/user/${idUser}/profile`, {
      params: { 
        idProfile: data.idProfile
      }
    });
  }

  public getPermissions(): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/users/session/verifyAccess`);
  }

  public getReportPermissions(): Observable<any> {
    return this.http.get<any>(`${environment.apiBackend}/users/user/profile`);
  }
}
