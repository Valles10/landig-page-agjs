import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from 'src/app/core/models';
import { SecureLSService } from './secure-ls.service';
import { Router, Routes } from '@angular/router';
import { Tools } from '../helpers/tools';
import { MyResponse } from '../models/MyResponse';
import { Country } from '../models/Country';
import { PermissionService } from './permission.service';


enum statesUser {
    CURRENT_USER = 'user',
    JWT = 'jwt',
    COUNTRY = 'country',
    SESSION_COUNTRY = 'session'
}

@Injectable({ providedIn: 'root' })
export class AuthSecureService {
    private userSubject: BehaviorSubject<User>;
    public user: Observable<User>;
    //public userPermissions: Array<string>;

    private tokenSubject: BehaviorSubject<string>;
    public token: Observable<string>;

    public closingSubject: BehaviorSubject<boolean>;
    public closing: Observable<boolean>;

    public countrySubject: BehaviorSubject<Country>;
    public country: Observable<Country>;

    public sessionCountrySubject: BehaviorSubject<Country>;
    public sessionCountry: Observable<Country>;

    // public userPermissionsSubject: Observable<string>;

    constructor(
        private http: HttpClient,
        private secureService: SecureLSService,
        private router: Router,
        private permissionService: PermissionService) {
        if (environment.secureSession) {
            this.tokenSubject = new BehaviorSubject<string>(secureService.get(statesUser.JWT));
            this.userSubject = new BehaviorSubject<User>(secureService.get(statesUser.CURRENT_USER));
            this.countrySubject = new BehaviorSubject<any>(secureService.get(statesUser.COUNTRY));
            this.sessionCountrySubject = new BehaviorSubject<any>(secureService.get(statesUser.SESSION_COUNTRY));

        } else {
            this.tokenSubject = new BehaviorSubject<string>(localStorage.getItem(`${environment.sufixStorage}${statesUser.JWT}`));
            this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem(`${environment.sufixStorage}${statesUser.CURRENT_USER}`)));
            this.countrySubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem(`${environment.sufixStorage}${statesUser.COUNTRY}`)));
            this.sessionCountrySubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem(`${environment.sufixStorage}${statesUser.SESSION_COUNTRY}`)));

        }
        this.closingSubject = new BehaviorSubject(false);
        this.closing = this.closingSubject.asObservable();
        this.country = this.countrySubject.asObservable();
        this.sessionCountry = this.sessionCountrySubject.asObservable();
        this.token = this.tokenSubject.asObservable();
        this.user = this.userSubject.asObservable();
    }

    public get userValue(): User {
        return this.userSubject.value;
    }

    public get tokenValue(): string {
        return this.tokenSubject.value;
    }

    public get countryValue(): Country {
        return this.countrySubject.value;
    }

    public get sessionCountryValue(): Country {
        return this.sessionCountrySubject.value;
    }

    public get outValue(): boolean {
        return this.closingSubject.value;
    }

    public closingSession(status: boolean) {
        this.closingSubject.next(status);
    }



    login(userData: any, country :Country, sessionCountry:Country) {
        this.closingSubject.next(false);
        let api = environment.apiBackend.replace('{portBO}', Tools.getApiPortBO(userData.sessionCountry.toString()).port);
        return this.http.post<any>(`${api}/users/session/login`, userData).pipe(map(res => {

            let user: User = {
                jwt: res.result.data.jwt,
                name: res.result.data.name,
                userName: res.result.data.username,
                profileType: res.result.data.profileType || '',
                userType: res.result.data.userType || ''
            };
            if (environment.secureSession) {
                this.secureService.set(statesUser.JWT, user.jwt);
                this.secureService.set(statesUser.CURRENT_USER, user);
                this.secureService.set(statesUser.SESSION_COUNTRY, sessionCountry);
                this.secureService.set(statesUser.COUNTRY, country);
            } else {
                localStorage.setItem(`${environment.sufixStorage}${statesUser.JWT}`, user.jwt);
                localStorage.setItem(`${environment.sufixStorage}${statesUser.CURRENT_USER}`, JSON.stringify(user));
                localStorage.setItem(`${environment.sufixStorage}${statesUser.SESSION_COUNTRY}`, JSON.stringify(sessionCountry));
                localStorage.setItem(`${environment.sufixStorage}${statesUser.COUNTRY}`, JSON.stringify(country));
            }
            this.sessionCountrySubject.next(sessionCountry);
            this.countrySubject.next(country);
            this.tokenSubject.next(user.jwt);
            this.userSubject.next(user);
            this.startRefreshTokenTimer();
            return user;
        }));
    }


    validaPassword(userData: any) {
        /* let api = environment.apiBackend.replace('{port}', Tools.getApiPort(userData.country).port); */
        return this.http.post<MyResponse>(`${environment.apiBackend}/users/session/validateUser`, userData);


        /* return this.http.post<any>(`${api}/users/session/login`, userData).pipe(map(res => {
            let user: User = {
                jwt: res.result.data.jwt,
                name: res.result.data.name,
                userName: res.result.data.username
            };
            if (environment.secureSession) {
                this.secureService.set(statesUser.JWT, user.jwt);
                this.secureService.set(statesUser.COUNTRY, userData.country);
                this.secureService.set(statesUser.CURRENT_USER, user);

            } else {
                localStorage.setItem(`${environment.sufixStorage}${statesUser.JWT}`, user.jwt);
                localStorage.setItem(`${environment.sufixStorage}${statesUser.COUNTRY}`, userData.country);
                localStorage.setItem(`${environment.sufixStorage}${statesUser.CURRENT_USER}`, JSON.stringify(user));
                
            }
            this.tokenSubject.next(user.jwt);
            this.userSubject.next(user);
            this.countrySubject.next(userData.country);
            this.startRefreshTokenTimer();
            return user;
        })); */
    }
    logout() {
        return this.http.post<any>(`${environment.apiBackend}/users/session/logout`, {}).pipe(map(res => {
            return res;
        }));
    }

    closeApp() {
        if (environment.secureSession) {
            this.secureService.remove(statesUser.CURRENT_USER);
            this.secureService.remove(statesUser.JWT);
        } else {
            localStorage.removeItem(`${environment.sufixStorage}${statesUser.CURRENT_USER}`);
            localStorage.removeItem(`${environment.sufixStorage}${statesUser.JWT}`);
        }
        this.disablePermissions(this.router.config);
        this.stopRefreshTokenTimer();
        this.userSubject.next(null);
        this.tokenSubject.next(null);
        this.router.navigate(['/login']);
        this.permissionService.resetPermissions

    }

    disablePermissions(root: Routes) {
        if (root) {
            for (let idx = 0; idx < root.length; idx++) {
                const element = root[idx];
                if (element.children?.length > 0) {
                    this.disablePermissions(element.children);
                } else {
                    if (!element.data?.lock && element.data?.access) {
                        element.data.access = false;
                    }
                }
            }
        }
    }

    refreshToken() {
        return this.http.post<any>(`${environment.apiBackend}/users/session/refreshtoken`, {})
            .pipe(map((res) => {
                let jwt = res.result;
                if (environment.secureSession) {
                    this.secureService.set(statesUser.JWT, jwt);
                } else {
                    localStorage.setItem(`${environment.sufixStorage}${statesUser.JWT}`, JSON.stringify(jwt));
                }
                this.tokenSubject.next(jwt);
                this.startRefreshTokenTimer();
                return res;
            }));
    }

    private refreshTokenTimeout;

    private startRefreshTokenTimer() {
        const jwtToken = JSON.parse(atob(this.tokenValue.split('.')[1]));
        const expires = new Date(jwtToken.exp * 1000);
        const timeout = (expires.getTime() - Date.now()) - (60 * 1000);
        this.refreshTokenTimeout = setTimeout(() => this.refreshToken().subscribe(), timeout);
    }

    private stopRefreshTokenTimer() {
        clearTimeout(this.refreshTokenTimeout);
    }
}