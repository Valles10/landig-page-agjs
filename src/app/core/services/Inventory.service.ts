import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MyResponse } from '../models/MyResponse';
import { version } from 'process';

@Injectable({
  providedIn: 'root'
})
export class InventoryService {
  constructor(private http: HttpClient) { }

  public getAllProduct(versionId?): Observable<MyResponse> {
    /* return this.http.get<MyResponse>(`${environment.apiBackend}/GetLocalInventory`.{params: {
      versionId: versionId
    }}); */
    return this.http.get<MyResponse>(`${environment.apiBackend}/inventory/local`,{params: {
      versionId: versionId || ''
    }}); 
  }

  public getProduct(params: any): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/inventory/local`, {params: params});
  }

  public getCatalog(): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/reports/inventory`);
  }

  public setProductStatusLocalInventory(params: any): Observable<MyResponse> {
    return this.http.post<MyResponse>(`${environment.apiBackend}/GetLocalInventory`,{params: params});
  }

  /* public deleteProduct(params: any): Observable<MyResponse> {
    return this.http.delete<MyResponse>(`${environment.apiBackend}/ManagementLocalInventory`,{params: params});
  } */

  public updateProduct(params: any): Observable<MyResponse> {
    return this.http.post<MyResponse>(`${environment.apiBackend}/UpdateLocalProduct`,params);
  }


  public add(sku): Observable<MyResponse> {
    return this.http.put<MyResponse>(`${environment.apiBackend}/inventory/local/product/${sku}`,{});
  }
  public delete(sku): Observable<MyResponse> {
    return this.http.delete<MyResponse>(`${environment.apiBackend}/inventory/local/product/${sku}`,{});
  }


 /*  public save(user: any): Observable<any> {
    return this.http.post<any>(`${environment.apiBackend}/users/user`, user);
  }

  public update(user: any): Observable<any> {
    return this.http.put<MyResponse>(`${environment.apiBackend}/users/user`, user);
  }

  activeProfile(idUser: string, data: any): Observable<MyResponse> {
    return this.http.post<MyResponse>(`${environment.apiBackend}/users/user/${idUser}/profile`, data);
  }

  removeProfile(idUser: string, data: any): Observable<MyResponse> {
    return this.http.delete<MyResponse>(`${environment.apiBackend}/users/user/${idUser}/profile`, {
      params: { 
        idProfile: data.idProfile
      }
    });
  }

  public getPermissions(): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/users/verifyAccess`);
  } */
}
