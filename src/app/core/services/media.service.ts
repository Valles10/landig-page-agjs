import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MyResponse } from '../models/MyResponse';

@Injectable({
  providedIn: 'root'
})
export class MediaService {
  constructor(private http: HttpClient) { }

  public getAllImagesByProduct(params: any): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/media`,{params: params});
  }

  public uploadFeature(data: FormData): Observable<MyResponse> {
    return this.http.post<MyResponse>(`${environment.apiBackend}/media/uploadImages`, data);
  }


}
