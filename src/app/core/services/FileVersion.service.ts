import { Injectable, Version } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MyResponse } from '../models/MyResponse';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FileVersionService {

  constructor(private http: HttpClient) { }


  public all(): Observable<MyResponse> {
    return this.http.get<MyResponse>(`http://10.218.41.26:28081/ServiceSimulator-v2/webresources/rest/tlbo/fileVersion`);
  }

  public get(idVersion): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/inventory/version/${idVersion}`);
  }

  public byProduct(productSKU): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/inventory/version`, {
      params: {
        productSKU: productSKU
      }
    });
  }
/* 
  
  public report(idVersion, startDate, endDate): Observable<MyResponse> {
    return this.http.get<MyResponse>(`${environment.apiBackend}/offer/binnacle`,{
      params: {
        idOffer: idVersion,
        startDate: startDate,
        endDate: endDate
      }
    });
  }

  public update(version: ProductVersion): Observable<MyResponse> {
    return this.http.put<MyResponse>(`${environment.apiBackend}/inventory/version`, version);
  }

  public create(version: ProductVersion): Observable<MyResponse> {
    return this.http.post<MyResponse>(`${environment.apiBackend}/inventory/version`, version);
  }

  public delete(idVersion: number): Observable<MyResponse> {
    return this.http.delete<MyResponse>(`${environment.apiBackend}/inventory/version/${idVersion}`);
  }

  public addProduct(idVersion: number, idProduct) {
    return this.http.put<MyResponse>(`${environment.apiBackend}/inventory/version/${idVersion}/product/${idProduct}`, {});
  }

  public deleteProduct(idVersion: number, idProduct) {
    return this.http.delete<MyResponse>(`${environment.apiBackend}/inventory/version/${idVersion}/product/${idProduct}`);
  }

  public toUAT(versionId): Observable<MyResponse> {
    return this.http.put<MyResponse>(`${environment.apiBackend}/offer/load/${versionId}/2`, {});
  }

  public validateOffer(versionId): Observable<MyResponse> {
    return this.http.put<MyResponse>(`${environment.apiBackend}/offer/validate/${versionId}`, {});
  }

  public toProd(versionId): Observable<MyResponse> {
    return this.http.put<MyResponse>(`${environment.apiBackend}/offer/load/${versionId}/1`, {});
  }

 */



}
