import { Module } from 'src/app/core/models/Module';

export class MockAccess {
    private static access: Array<Module> = [
        {
            "idModule": 1,
            "name": 'Tienda/Marcas',
            "route": '/#/brands',
            "active": true,
            lock: '',
            idPlatform: 2
        },
        {
            "idModule": 2,
            "name": 'Tienda/Características',
            "route": '/#/features',
            "active": false,
            lock: '',
            idPlatform: 2
        },
        {
            "idModule": 3,
            "name": 'Tienda/Categorias',
            "route": '/#/manage-categories',
            "active": false,
            lock: '',
            idPlatform: 2
        },
        {
            "idModule": 4,
            "name": 'Tienda/Proveedores',
            "route": '/#/suppliers-inventory',
            "active": true,
            lock: '',
            idPlatform: 2
        },
        {
            "idModule": 5,
            "name": 'Tienda/Inventario',
            "route": '/#/inventory',
            "active": true,
            lock: '',
            idPlatform: 2
        },
        {
            "idModule": 6,
            "name": 'Configuraciones/Propiedades',
            "route": '/#/tools/properties',
            "active": true,
            lock: '',
            idPlatform: 2
        },
        {
            "idModule": 6,
            "name": 'Configuraciones/Roles',
            "route": '/#/tools/user-profile',
            "active": true,
            lock: '',
            idPlatform: 2
        },
        {
            "idModule": 7,
            "name": 'Configuraciones/Usuarios',
            "route": '/#/tools/users',
            "active": false,
            lock: '',
            idPlatform: 2
        }
    ];

    public static getAccess(){
        return this.access;
    }

}