export class CountryMock{
    private static country: Array<any> = [
        {
            value: 'GT',
            name: 'Guatemala',
            code: '502'
        },
        {
            value: 'SV',
            name: 'El Salvador',
            code: '503'
        }
        ,
        {
            value: 'HN',
            name: 'Honduras',
            code: '504'
        }
        ,
        {
            value: 'NI',
            name: 'Nicaragua',
            code: '505'
        }
        , {
            value: 'CR',
            name: 'Costa Rica',
            code: '506'
        },
        {
            value: 'PA',
            name: 'Panama',
            code: '507'
        }

    ];

    public static getCountries(){
        return this.country;
    }

}