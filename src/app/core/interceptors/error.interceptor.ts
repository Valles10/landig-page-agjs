import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthSecureService } from '../services/auth-secure.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authSecureService: AuthSecureService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(request).pipe(catchError((error: HttpErrorResponse) => {
            /* const data = {
                reason: error && error.error && error.error.reason ? error.error.reason : '',
                status: error.status
            }; */
            if ([401].indexOf(error.status) !== -1 && this.authSecureService.userValue ) {
                // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
              //  console.log(data.reason);
                this.authSecureService.closeApp();
            }
            return throwError(error);
        }))
    }
}
