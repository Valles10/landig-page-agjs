import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthSecureService } from '../services/auth-secure.service';
import { Tools } from '../helpers/tools';



@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authSecureService: AuthSecureService) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const user = this.authSecureService.userValue;
        const token = this.authSecureService.tokenValue;
        
        const isLoggedIn = user && user.jwt;
        if (isLoggedIn) {
            let url = Tools.getHost(request.url, this.authSecureService.sessionCountryValue.code);
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${token}`
                },
                url: url
            });
        } else {
            let url = Tools.getHost(request.url, this.authSecureService.sessionCountryValue.code);
            request = request.clone({
                url: url
            });
        }
        return next.handle(request);

    }
}