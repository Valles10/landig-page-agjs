import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthSecureService } from '../services/auth-secure.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthSecureService,
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authenticationService.userValue;
        if (currentUser) {
            if (!route.data.access) {
                if (!['/'].includes(state.url)) {
                    this.router.navigate(['/']);
                    return true;

                } else {
                    this.router.navigate(['without-permits']);
                }
                return false;
            }
            return true;
        }
        if (![('logout')].includes(state.url)) {
            this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        }
        return false;
    }
}


