import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthSecureService } from '../services/auth-secure.service';
import { User } from '../models';

@Injectable({ providedIn: 'root' })
export class StorageGuard implements CanActivate {
    constructor(
        private router: Router,
        private authSecureService: AuthSecureService,
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        /* const country = this.authSecureService.sessionCountryValue.code;
        const token = this.authSecureService.tokenValue;
        const user = this.authSecureService.userValue; */

    /*     console.log(country);
        console.log(country == '');
        console.log(token);
        console.log( token == '');
        console.log(user);
        console.log(this.instanceOfUser(user)); */
        

        /* if ((!country || country == '') || (!token || token == '') || !user && !this.instanceOfUser(user)) {
            this.router.navigate(['app-failed']);
            return false;
        } */
        //  console.log(country);
        // console.log(Tools.getApiPort(country));
        /* if (Tools.getApiPort(country).status) {
           // console.log("Data no corrupta");

            if (['app-failed'].includes(state.url)) {
                // this.router.navigate([country, '/']);
                this.router.navigate(['/']);
            }
            return true;
        } */
        //  console.log("Data corrupta");

        //this.router.navigate(['app-failed']);
        return true;
    }

    instanceOfUser(obj: any): obj is User {
        return 'jwt' in obj
          && 'name' in obj
          && 'profileType' in obj
          && 'userName' in obj
          && 'userType' in obj
          && typeof obj?.jwt  === 'string'
          && typeof obj?.name === 'string'
          && typeof obj?.profileType === 'string'
          && typeof obj?.userType === 'string'
          /* && typeof obj?.byCarousel === 'number'
          && typeof obj?.maximunSize === 'number'
          && typeof obj?.minimunSize === 'number'
          && Array.isArray(obj?.types)
          && obj?.dimensions?.comparision
          && obj?.dimensions?.sizes
          && Array.isArray(obj?.dimensions?.sizes) */
      }
}


