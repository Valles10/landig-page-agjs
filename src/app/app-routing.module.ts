import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';

import { ActionsComponent } from './pages/tools/users/actions/actions.component';

import { ModuleComponent } from './pages/tools/users/module/module.component';
import { ProfileComponent } from './pages/tools/users/profile/profile.component';
import { UserComponent } from './pages/tools/users/user/user.component';
import { UserPermissionsComponent } from './pages/tools/users/user-permissions/user-permissions.component';
import { LoginComponent } from './pages/security/login/login.component';
import { AuthGuard } from './core/guards/auth.guard';
import { LogoutComponent } from './pages/security/logout/logout.component';
import { WithoutPermitsComponent } from './pages/security/without-permits/without-permits.component';
import { AppFailedComponent } from './pages/security/app-failed/app-failed.component';
import { StorageGuard } from './core/guards/storage.guard';
import { InventoryComponent } from './pages/store/offer/inventory/inventory.component';
import { ProductDetailComponent } from './pages/store/offer/product-detail/product-detail.component';
import { CategoriesComponent } from './pages/store/offer/categories/categories.component';
import { SupplierProductComponent } from './pages/store/offer/supplier-product/supplier-product.component';
import { VersionManagementComponent } from './pages/store/offer/version-management/version-management.component';
import { VersionDetailComponent } from './pages/store/offer/version-detail/version-detail.component';
import { BrandsComponent } from './pages/store/offer/brands/brands.component';
import { FeaturesComponent } from './pages/store/offer/features/features.component';
import { SeriesManagementComponent } from './pages/store/virtual-products/series-management/series-management.component';
import { SeriesDeliveryComponent } from './pages/store/virtual-products/series-delivery/series-delivery.component';
import { IncomeReportComponent } from './pages/store/reports/income-report/income-report.component';
import { ProductReportComponent } from './pages/store/reports/product-report/product-report.component';
import { PurchaseOrderReportComponent } from './pages/store/reports/purchase-order-report/purchase-order-report.component';
import { CustomerAbandonmentComponent } from './pages/store/reports/customer-abandonment/customer-abandonment.component';
import { ProvisioningComponent } from './pages/store/after-sales/provisioning/provisioning.component';
import { OrdersComponent } from './pages/store/after-sales/orders/orders.component';
import { ContractComponent } from './pages/store/after-sales/contract/contract.component';
import { PreloadAllModules } from '@angular/router';
import { LandingComponent } from './pages/tools/users/landing/landing.component';
import { AdminlComponent } from './pages/tools/users/adminl/adminl.component';

const routes: Routes = [
  // {
  //   path: '',
  //   data: {
  //     breadcrumb: 'Inicio',
  //     show: false,
  //     access: true,
  //     title: 'Inicio',
  //     icon: 'home',
  //     lock: true
  //   },
  //   canActivate: [AuthGuard, StorageGuard],
  //   component: HomeComponent,

  // },
  // {
  //   path: 'store',
  //   data: {
  //     breadcrumb: 'Tienda',
  //     show: true,
  //     access: false,
  //     title: 'Tienda',
  //     icon: 'store'
  //   },
  //   children: [
  //     { path: '', redirectTo: '/', pathMatch: 'full' },
  //     {
  //       path: 'offer',
  //       data: {
  //         breadcrumb: 'Oferta',
  //         show: true,
  //         access: false,
  //         title: 'Oferta',
  //         icon: 'local_offer'
  //       },
  //       children: [
  //         { path: '', redirectTo: '/', pathMatch: 'full' },
  //         {
  //           path: 'features',
  //           component: FeaturesComponent,
  //           canActivate: [AuthGuard, StorageGuard],
  //           data: {
  //             breadcrumb: 'Caracteristicas',
  //             show: true,
  //             access: false,
  //             title: 'Caracteristicas',
  //             icon: 'category'
  //           }
  //         },
  //         {
  //           path: 'categories',
  //           component: CategoriesComponent,
  //           canActivate: [AuthGuard, StorageGuard],
  //           data: {
  //             breadcrumb: 'Categorias',
  //             show: true,
  //             access: false,
  //             title: 'Categorias',
  //             icon: 'local_offer'
  //           }
  //         },
  //         {
  //           path: 'brands',
  //           component: BrandsComponent,
  //           canActivate: [AuthGuard, StorageGuard],
  //           data: {
  //             breadcrumb: 'Marcas',
  //             show: true,
  //             access: false,
  //             title: 'Marcas',
  //             icon: 'featured_play_list'
  //           }
  //         },
  //         {
  //           path: 'supplier-product',
  //           component: SupplierProductComponent,
  //           canActivate: [AuthGuard, StorageGuard],
  //           data: {
  //             breadcrumb: 'Producto proveedores',
  //             show: true,
  //             access: false,
  //             title: 'Producto proveedores',
  //             icon: 'folder'
  //           }
  //         },
  //         {
  //           path: 'inventory',
  //           data: {
  //             breadcrumb: 'Inventario',
  //             show: true,
  //             access: false,
  //             title: 'Inventario',
  //             icon: 'local_mall',
  //             isRoute: true
  //           },
  //           children: [
  //             {
  //               path: '',
  //               component: InventoryComponent,
  //               canActivate: [AuthGuard, StorageGuard],
  //               data: {
  //                 breadcrumb: '',
  //                 show: false,
  //                 access: false,
  //                 title: 'Inventario',
  //                 icon: 'local_mall',
  //               }
  //             },
  //             {
  //               path: ':sku',
  //               component: ProductDetailComponent,
  //               canActivate: [AuthGuard, StorageGuard],
  //               data: {
  //                 breadcrumb: 'Inventario',
  //                 show: false,
  //                 access: false,
  //                 title: 'Inventario',
  //                 icon: 'local_offer'
  //               }
  //             },
  //           ]
  //         },

  //         {
  //           path: 'version-management',
  //           data: {
  //             breadcrumb: 'Cargas',
  //             show: true,
  //             access: false,
  //             title: 'Cargas',
  //             icon: 'folder_special',
  //             isRoute: true
  //           },
  //           children: [
  //             {
  //               path: '',
  //               component: VersionManagementComponent,
  //               canActivate: [AuthGuard, StorageGuard],
  //               data: {
  //                 breadcrumb: '',
  //                 show: false,
  //                 access: false,
  //                 title: 'Cargas',
  //                 icon: 'folder_special'
  //               }

  //             }, {
  //               path: ':version',
  //               component: VersionDetailComponent,
  //               canActivate: [AuthGuard, StorageGuard],
  //               data: {
  //                 breadcrumb: 'Cargas ',
  //                 show: false,
  //                 access: false,
  //                 title: 'Cargas',
  //                 icon: 'folder_special'
  //               }
  //             },
  //           ]
  //         },

  //       ]
  //     },
  //     {
  //       path: 'after-sales',
  //       data: {
  //         breadcrumb: 'PostVenta',
  //         show: true,
  //         access: false,
  //         title: 'PostVenta',
  //         icon: 'support_agent'
  //       },
  //       children: [
  //         { path: '', redirectTo: '/', pathMatch: 'full' },
  //         {
  //           path: 'provisioning',
  //           component: ProvisioningComponent,
  //           canActivate: [AuthGuard, StorageGuard],
  //           data: {
  //             breadcrumb: 'Aprovisionamiento',
  //             show: true,
  //             access: false,
  //             title: 'Aprovisionamiento',
  //             icon: 'grading'
  //           }
  //         },
  //         {
  //           path: 'orders',
  //           component: OrdersComponent,
  //           canActivate: [AuthGuard, StorageGuard],
  //           data: {
  //             breadcrumb: 'Ordenes',
  //             show: true,
  //             access: false,
  //             title: 'Ordenes',
  //             icon: 'fact_check'
  //           }
  //         },
  //         {
  //           path: 'contract',
  //           component: ContractComponent,
  //           canActivate: [AuthGuard, StorageGuard],
  //           data: {
  //             breadcrumb: 'Contratos',
  //             show: true,
  //             access: false,
  //             title: 'Contratos',
  //             icon: 'article'
  //           }
  //         },
  //         {
  //           path: 'orders-authorization',
  //           /* component: ContractComponent, */
  //           loadChildren: () => import('./pages/store/after-sales/orders-authorization/orders-authorization.module').then(m => m.OrdersAuthorizationModule),
  //           canActivate: [AuthGuard, StorageGuard],
  //           data: {
  //             breadcrumb: 'Autorizaciones',
  //             show: true,
  //             access: false,
  //             title: 'Autorizaciones',
  //             icon: 'favorite_border'
  //           }
  //         }
  //       ]
  //     },
  //     {
  //       path: 'virtual-products',
  //       data: {
  //         breadcrumb: 'Productos virtuales',
  //         show: true,
  //         access: false,
  //         title: 'Productos virtuales',
  //         icon: 'devices'
  //       },
  //       children: [
  //         { path: '', redirectTo: '/', pathMatch: 'full' },
  //         {
  //           path: 'series-delivery',
  //           component: SeriesDeliveryComponent,
  //           canActivate: [AuthGuard, StorageGuard],
  //           data: {
  //             breadcrumb: 'Entrega de series',
  //             show: true,
  //             access: false,
  //             title: 'Entrega de series',
  //             icon: 'mark_email_read'
  //           }
  //         },
  //         {
  //           path: 'series',
  //           component: SeriesManagementComponent,
  //           canActivate: [AuthGuard, StorageGuard],
  //           data: {
  //             breadcrumb: 'Series',
  //             show: true,
  //             access: false,
  //             title: 'Series',
  //             icon: 'qr_code'
  //           }
  //         }
  //       ]
  //     },
  //     {
  //       path: 'reports',
  //       data: {
  //         breadcrumb: 'Reportes',
  //         show: true,
  //         access: false,
  //         title: 'Reportes',
  //         icon: 'dashboard'
  //       },
  //       children: [
  //         { path: '', redirectTo: '/', pathMatch: 'full' },
  //         {
  //           path: 'income',
  //           component: IncomeReportComponent,
  //           canActivate: [AuthGuard, StorageGuard],
  //           data: {
  //             breadcrumb: 'Ingresos',
  //             show: true,
  //             access: false,
  //             title: 'Ingresos',
  //             icon: 'show_chart'
  //           }
  //         },
  //         {
  //           path: 'sales',
  //           component: ProductReportComponent,
  //           canActivate: [AuthGuard, StorageGuard],
  //           data: {
  //             breadcrumb: 'Ventas',
  //             show: true,
  //             access: false,
  //             title: 'Ventas',
  //             icon: 'insert_chart'
  //           }
  //         },
  //         {
  //           path: 'purchases',
  //           component: PurchaseOrderReportComponent,
  //           canActivate: [AuthGuard, StorageGuard],
  //           data: {
  //             breadcrumb: 'Ordenes',
  //             show: true,
  //             access: false,
  //             title: 'Ordenes',
  //             icon: 'pie_chart',
  //           }
  //         },
  //         {
  //           path: 'customer-abandonment',
  //           component: CustomerAbandonmentComponent,
  //           canActivate: [AuthGuard, StorageGuard],
  //           data: {
  //             breadcrumb: 'Abandono de clientes',
  //             show: true,
  //             access: false,
  //             title: 'Abandono de clientes',
  //             icon: 'bar_chart',
  //           }
  //         },
  //       ]
  //     },
  //     {
  //       path: 'documents',
  //       data: {
  //         breadcrumb: 'Documentos',
  //         show: true,
  //         access: false,
  //         title: 'Documentos',
  //         icon: 'folder_special'
  //       },
  //       children: [
  //         { path: '', redirectTo: '/', pathMatch: 'full' },
  //         {
  //           path: 'file-labels',
  //           /* component: IncomeReportComponent, */
  //           loadChildren: () => import('./pages/store/documents/file-labels/file-labels.module').then(m => m.FileLabelsModule),
  //           canActivate: [AuthGuard, StorageGuard],
  //           data: {
  //             breadcrumb: 'Etiquetas', 
  //             show: true,
  //             access: true,
  //             title: 'Etiquetas',
  //             icon: 'label'
  //           }
  //         },
  //         {
  //           path: 'files',
  //           loadChildren: () => import('./pages/store/documents/files/files.module').then(m => m.FilesModule),
  //           /* component: ProductReportComponent, */
  //           canActivate: [AuthGuard, StorageGuard],
  //           data: {
  //             breadcrumb: 'Archivos',
  //             show: true,
  //             access: true,
  //             title: 'Archivos',
  //             icon: 'insert_drive_file'
  //           }
  //         },
          
  //        /*  { path: 'groups', loadChildren: () => import('./pages/store/documents/groups/groups.module').then(m => m.GroupsModule) }, */
  //         /* { path: 'documentos-uploads', loadChildren: () => import('./pages/store/documents/documents-uploads/documents-uploads.module').then(m => m.DocumentsUploadsModule) }, */
  //         {
  //           path: 'groups',
  //           loadChildren: () => import('./pages/store/documents/groups/groups.module').then(m => m.GroupsModule) ,
  //           canActivate: [AuthGuard, StorageGuard],
  //           data: {
  //             breadcrumb: 'Grupos',
  //             show: true,
  //             access: false,
  //             title: 'Grupos',
  //             icon: 'group_work',
  //           }
  //         },
  //         {
  //           path: 'documents-uploads',
  //           loadChildren: () => import('./pages/store/documents/documents-uploads/documents-uploads.module').then(m => m.DocumentsUploadsModule),
  //           /* component: PurchaseOrderReportComponent, */
  //           canActivate: [AuthGuard, StorageGuard],
  //           data: {
  //             breadcrumb: 'Cargas',
  //             show: true,
  //             access: true,
  //             title: 'Cargas',
  //             icon: 'publish',
  //           }
  //         },
  //       ]
  //     }
  //   ]
  // },
  {
    data: {
      breadcrumb: 'Landing Page',
      show: true,
      access: false,
      title: 'Landing Page',
      icon: 'addchart'
    },
    path: 'tools',
    children: [
      { path: '', redirectTo: '/', pathMatch: 'full' },

      {
        path: 'adminl',
        component: AdminlComponent,
        canActivate: [AuthGuard, StorageGuard],
        data: {
          breadcrumb: 'Amin Landing',
          show: true,
          access: false,
          title: 'Admin Landing Page',
          icon: 'line_style'
        }
      }
    ]  
  },
  {
    data: {
      breadcrumb: 'Configuraciones',
      show: true,
      access: false,
      title: 'Configuraciones',
      icon: 'settings'
    },
    path: 'tools',
    children: [
      { path: '', redirectTo: '/', pathMatch: 'full' },
      /* { path: 'link-configuration', loadChildren: () => import('./pages/tools/link-configuration/link-configuration.module').then(m => m.LinkConfigurationModule) }, */
      {
        path: 'link-configuration',
        loadChildren: () => import('./pages/tools/link-configuration/link-configuration.module').then(m => m.LinkConfigurationModule) ,
       /*  component: PropertiesComponent, */
        /* canActivate: [AuthGuard, StorageGuard], */
        data: {
          breadcrumb: 'Parametrización URL',
          show: true,
          access: true,
          title: 'Parametrización URL',
          icon: 'link'
        }
      },
      {
        path: 'properties',
        /* component: PropertiesComponent, */
        loadChildren: () => import('./pages/tools/properties/properties.module').then(m => m.PropertiesModule),
        /* canActivate: [AuthGuard, StorageGuard], */
        data: {
          breadcrumb: 'Properties',
          show: true,
          access: true,
          title: 'Propiedades',
          icon: 'low_priority'
        }
      },
      {
        path: 'actions',
        component: ActionsComponent,
        canActivate: [AuthGuard, StorageGuard],
        data: {
          breadcrumb: 'Acciones',
          show: true,
          access: false,
          title: 'Acciones',
          icon: 'preview'
        }
      },
      // {
      //   path: 'landing',
      //   component: LandingComponent,
      //   canActivate: [AuthGuard, StorageGuard],
      //   data: {
      //     breadcrumb: 'Source',
      //     show: true,
      //     access: false,
      //     title: 'Source',
      //     icon: 'addchart'
      //   }
      // },
      // {
      //   path: 'adminl',
      //   component: AdminlComponent,
      //   canActivate: [AuthGuard, StorageGuard],
      //   data: {
      //     breadcrumb: 'Amin Landing',
      //     show: true,
      //     access: false,
      //     title: 'Admin Landing Page',
      //     icon: 'line_style'
      //   }
      // },
      {
        path: 'modules',
        component: ModuleComponent,
        canActivate: [AuthGuard, StorageGuard],
        data: {
          breadcrumb: 'Modulos',
          show: true,
          access: false,
          title: 'Modulos',
          icon: 'policy'
        }
      },
      {
        path: 'profiles',
        component: ProfileComponent,
        canActivate: [AuthGuard, StorageGuard],
        data: {
          breadcrumb: 'Perfiles',
          show: true,
          access: false,
          title: 'Perfiles',
          icon: 'admin_panel_settings'
        }
      },
      {
        path: 'users',
        component: UserComponent,
        canActivate: [AuthGuard, StorageGuard],
        data: {
          breadcrumb: 'Usuarios',
          show: true,
          access: false,
          title: 'Usuarios',
          icon: 'supervised_user_circle'
        }
      },
      {
        path: 'user-permissions',
        component: UserPermissionsComponent,
        canActivate: [AuthGuard, StorageGuard],
        data: {
          breadcrumb: 'Permisos de usuario',
          show: true,
          access: false,
          title: 'Asignación de perfiles',
          icon: 'verified_user'
        }
      }
    ]
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      show: false,
      access: true,
      title: 'login',
      lock: true,
    }
  },
  {
    path: 'logout',
    component: LogoutComponent,
    canActivate: [AuthGuard],
    data: {
      breadcrumb: '',
      show: false,
      access: true,
      title: 'logout',
      lock: true,
    }
  },
  {
    path: 'without-permits',
    component: WithoutPermitsComponent,
    canActivate: [AuthGuard, StorageGuard],
    data: {
      breadcrumb: '',
      show: false,
      access: true,
      title: 'without-permits',
      lock: true,
    }
  },
  {
    path: 'app-failed',
    component: AppFailedComponent,
    canActivate: [AuthGuard],
    data: {
      breadcrumb: '',
      show: false,
      access: true,
      title: 'app-failed',
      lock: true
    }
  },
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      preloadingStrategy: PreloadAllModules
    })],
  exports: [RouterModule]
})
export class AppRoutingModule { }