import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';


declare let google: any;
export class ItemChart {
  row: number;
  column: number;
}

@Component({
  selector: 'my-google-charts',
  templateUrl: './my-google-charts.component.html',
  /* styleUrls: ['./my-google-charts.component.css'] */
})
export class MyGoogleChartsComponent implements OnInit {

  private _data: Array<any>;
  private _options: any;
  private _type: string;
  private _headers: string[];
  public loading: boolean = false;

  public chartName = '';

  @Input()
  get data(): Array<any> { return this._data; }
  set data(data: Array<any>) {
    this._data = data;
  }

  @Input()
  get type() { return this._type; }
  set type(type) {
    this._type = type;
  }

  @Input()
  get options() { return this._options; }
  set options(options) {
    this._options = options;
  }

  @Input()
  get headers() { return this._headers; }
  set headers(headers) {
    this._headers = headers;
  }

  @Output() select: EventEmitter<any> = new EventEmitter<any>();


  constructor() {
    this.chartName = 'chart_' + new Date().getTime();
  }

  ngOnInit() {
  }


  /* loadChart() {
    // if (this._data.length > 0)
    this.loading = true;
    google.charts.load('current', { 'packages': ['corechart'] });
    google.charts.setOnLoadCallback(() => {
      if (this._data.length > 0) {
        let newdata: any[] = [];
        newdata.push(this.headers);
        for (let index = 0; index < this.data.length; index++) {
          const element = this.data[index];
          newdata.push(Object.values(element));
        }
        try {
          let dataChart = google.visualization.arrayToDataTable(newdata);
          let chartChart;
          switch (this._type) {
            case 'AreaChart':
              chartChart = new google.visualization.AreaChart(document.getElementById(this.chartName));
              google.visualization.events.addListener(chartChart, 'select', () => {
                this.select.emit(chartChart.getSelection()[0]);
              });
              chartChart.draw(dataChart, this._options);
              break;
            case 'PieChart':
              chartChart = new google.visualization.PieChart(document.getElementById(this.chartName));
              google.visualization.events.addListener(chartChart, 'select', () => {
                this.select.emit(chartChart.getSelection()[0]);
              });
              chartChart.draw(dataChart, this._options);
              break;
          }
          this.loading = false;
        } catch (_e) {
          this._data = [];
          this.loading = false;
        }
      } else {
        this.loading = false;
      }
    });
  } */

  loadChart() {
    switch (this._type) {
      case 'AreaChart':
        this.GraphAreaChart();
        break;
      case 'PieChart':
        this.GraphPieChart();
        break;
      case 'BarChart':
        this.GraphBarChart();
        break;
    }
  }
  ngOnChanges(changes: SimpleChanges) {
    this.loadChart();
  }

  GraphBarChart() {
    this.loading = true;
    google.charts.load('current', { 'packages': ['bar'] });
    google.charts.setOnLoadCallback(() => {
      if (this._data.length > 0) {
        try {
          let dataArr: any[] = [];
          dataArr.push(this.headers);
          for (let index = 0; index < this.data.length; index++) {
            const element = this.data[index];
            dataArr.push(Object.values(element));
          }
          let dataChart = google.visualization.arrayToDataTable(dataArr);
          let chart = new google.charts.Bar(document.getElementById(this.chartName));
          google.visualization.events.addListener(chart, 'select', () => {
            this.select.emit(chart.getSelection()[0]);
          });
          /* this._options.height = this._data.length * 75; */
          chart.draw(dataChart, google.charts.Bar.convertOptions(this._options));
          this.loading = false;
        } catch (_e) {
          this._data = [];
          this.loading = false;
          //console.log(_e);
        }
      } else {
        this.loading = false;
      }
    });
  }

  GraphAreaChart() {
    this.loading = true;
    google.charts.load('current', { 'packages': ['corechart'] });
    google.charts.setOnLoadCallback(() => {
      if (this._data.length > 0) {
        try {
          let dataArr: any[] = [];
          dataArr.push(this.headers);
          for (let index = 0; index < this.data.length; index++) {
            const element = this.data[index];
            dataArr.push(Object.values(element));
          }
          let dataChart = google.visualization.arrayToDataTable(dataArr);
          let chart = new google.visualization.AreaChart(document.getElementById(this.chartName));
          google.visualization.events.addListener(chart, 'select', () => {
            this.select.emit(chart.getSelection()[0]);
          });
          chart.draw(dataChart, this._options);
          this.loading = false;
        } catch (_e) {
          this._data = [];
          this.loading = false;
        }
      } else {
        this.loading = false;
      }
    });
  }
  GraphPieChart() {
    this.loading = true;
    google.charts.load('current', { 'packages': ['corechart'] });
    google.charts.setOnLoadCallback(() => {
      if (this._data.length > 0) {
        try {
          let dataArr: any[] = [];
          dataArr.push(this.headers);
          for (let index = 0; index < this.data.length; index++) {
            const element = this.data[index];
            dataArr.push(Object.values(element));
          }
          let dataChart = google.visualization.arrayToDataTable(dataArr);
          let chart = new google.visualization.PieChart(document.getElementById(this.chartName));
          google.visualization.events.addListener(chart, 'select', () => {
            this.select.emit(chart.getSelection()[0]);
          });
          chart.draw(dataChart, this._options);
          this.loading = false;
        } catch (_e) {
          this._data = [];
          this.loading = false;
        }
      } else {
        this.loading = false;
      }
    });
  }
}
