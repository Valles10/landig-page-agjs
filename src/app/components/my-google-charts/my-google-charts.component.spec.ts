import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyGoogleChartsComponent } from './my-google-charts.component';

describe('MyGoogleChartsComponent', () => {
  let component: MyGoogleChartsComponent;
  let fixture: ComponentFixture<MyGoogleChartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyGoogleChartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyGoogleChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
