import { Component, Input, OnInit, ViewChild } from '@angular/core';
import {
  PDFProgressData,
  PdfViewerComponent,
  PDFDocumentProxy,
  PDFSource,
} from 'ng2-pdf-viewer';

@Component({
  selector: 'my-pdf-viewer',
  templateUrl: './my-pdf-viewer.component.html',
  styleUrls: ['./my-pdf-viewer.component.css']
})
export class MyPdfViewerComponent implements OnInit {

  private _doc ; 
  private _height: string= '';
  private _loading = false;


  @Input()
  get doc() { return this._doc; }
  set doc(_doc) {
    this._doc = _doc;
  }

  @Input()
  get heightvh() { return this._height; }
  set heightvh(height) {    
    this._height = height;
  }

  @Input()
  get loading() { return this._loading; }
  set loading(loading) {
    this._loading = loading;
  }



  
  constructor() { }

  ngOnInit(): void {
  }

  
  error: any;
  page = 1;
  rotation = 0;
  zoom = 1.0;
  originalSize = true;
  pdf: any;
  renderText = true;
  progressData: PDFProgressData;
  isLoaded = false;
  stickToPage = false;
  showAll = true;
  autoresize = true;
  fitToPage = false;
  outline: any[];
  isOutlineShown = false;
  pdfQuery = '';

  @ViewChild(PdfViewerComponent) private pdfComponent: PdfViewerComponent;


  toggleOutline() {
    if (this.doc)
      this.isOutlineShown = !this.isOutlineShown;
  }

  incrementPage(amount: number) {
    if (this.doc)
      if ((this.page + amount) > 0) {
        this.page += amount;
      }
  }

  showAllClick() {
    if (this.doc) {
      this.showAll = !this.showAll; this.page = 1;
    }
  }

  incrementZoom(amount: number) {
    if (this.doc)
      this.zoom += amount;
  }

  rotate(angle: number) {
    if (this.doc)
      this.rotation += angle;
  }



  /**
   * Get pdf information after it's loaded
   * @param pdf
   */
  afterLoadComplete(pdf: PDFDocumentProxy) {
    this.pdf = pdf;
    this.isLoaded = true;
    this.loadOutline();
  }

  /**
   * Get outline
   */
  loadOutline() {
    this.pdf.getOutline().then((outline: any[]) => {
      this.outline = outline;
    });
  }

  /**
   * Handle error callback
   *
   * @param error
   */
  onError(error: any) {
    this.error = error; // set error

    if (error.name === 'PasswordException') {
      const password = prompt(
        'This document is password protected. Enter the password:'
      );

      if (password) {
        this.error = null;
        /* this.setPassword(password); */
      }
    }
  }



  /**
   * Pdf loading progress callback
   * @param {PDFProgressData} progressData
   */
  onProgress(progressData: PDFProgressData) {
    /* console.log(progressData); */
    this.progressData = progressData;
    this.isLoaded = false;
    this.error = null; // clear error
  }


  /**
   * Page rendered callback, which is called when a page is rendered (called multiple times)
   *
   * @param {CustomEvent} e
   */
  /*  pageRendered(e: CustomEvent) {
     console.log('(page-rendered)', e);
   } */

  searchQueryChanged(newQuery: string) {
    if (newQuery !== this.pdfQuery) {
      this.pdfQuery = newQuery;
      this.pdfComponent.pdfFindController.executeCommand('find', {
        query: this.pdfQuery,
        highlightAll: true,
      });
    } else {
      this.pdfComponent.pdfFindController.executeCommand('findagain', {
        query: this.pdfQuery,
        highlightAll: true,
      });
    }
  }

}
