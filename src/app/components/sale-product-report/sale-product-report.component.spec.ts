import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleProductReportComponent } from './sale-product-report.component';

describe('SaleProductReportComponent', () => {
  let component: SaleProductReportComponent;
  let fixture: ComponentFixture<SaleProductReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaleProductReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleProductReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
