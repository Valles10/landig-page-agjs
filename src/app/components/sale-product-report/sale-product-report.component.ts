import { Component, OnInit, Input, SimpleChanges, ViewChild } from '@angular/core';
import { ReportService } from 'src/app/core/services/report.service';
import { Statistics } from 'src/app/core/models/Statistics';
import { ViewOptions } from 'src/app/core/models/ViewOptions';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

declare let XLSX: any;

@Component({
  selector: 'sale-product-report',
  templateUrl: './sale-product-report.component.html',
  /* styleUrls: ['./sale-product-report.component.css'] */
})
export class SaleProductReportComponent implements OnInit {

  public viewOptions: ViewOptions = {
    displayedColumns: ['position', 'sku', 'name', 'soldOut', 'price', 'total'],
    pageSize: [25, 35, 50],
    loading: true
  };

  private _month: string;
  private _year: string;
  private _day: string;

  @Input()
  get month(): string { return this._month; }
  set month(data: string) {
    this._month = data;
  }

  @Input()
  get year() { return this._year; }
  set year(year) {
    this._year = year;
  }

  @Input()
  get day() { return this._day; }
  set day(day) {
    this._day = day;
  }
  public loading: boolean = false;
  public products: Statistics[] = [];
  public productsDataSource: MatTableDataSource<Statistics> = new MatTableDataSource<Statistics>();
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
 /*  @ViewChild(MatSort, { static: true }) sort: MatSort; */

  private params = {
    day: '',
    month: '',
    year: ''
  };



  constructor(
    private reportService: ReportService
  ) {
  }

  ngOnInit() {
    /* this.productsDataSource.paginator = this.paginator;
    this.productsDataSource.sort = this.sort; */
  }

  load() {
    this.loading = true;
    if (this._day == '0') 
      delete this.params.day;
    else
      this.params.day = this._day;
    if (this._month == '0')
      delete this.params.month;
    else
      this.params.month = this._month;
    if (this._year == '0')
      delete this.params.year;
    else
      this.params.year = this._year;
    this.products = [];
    this.productsDataSource = new MatTableDataSource<Statistics>();
    this.reportService.getTopProducts(this.params).subscribe(response => {
      if (response.code == 200 && response.result.status == 'ok') {
        if (response.result.data.statistics) {
          this.products = response.result.data.statistics;
          this.productsDataSource = new MatTableDataSource<Statistics>(this.products);
          this.productsDataSource.paginator = this.paginator;
          /* this.productsDataSource.sort = this.sort; */
        }
      }
      this.loading = false;
    }, err => {
      this.loading = false;
    });

  }

  ngOnChanges(changes: SimpleChanges) {
    if (this._month != '0' && this.year != '0') {
      this.load();
    }
  }



  downloadCsv() {
    if (this.products?.length > 0) {
      let newData: any[] = [];
      for (let index = 0; index < this.products.length; index++) {
        const element = this.products[index];
        newData.push({
          'POSICION': element.puesto,
          'SKU': element.sku,
          'NOMBRE': element.name,
          'VENDIDOS': element.conteo,
          'PRECIO': element.precio,
          'TOTAL': element.total_recaudado
        });
      }
      const workSheet = XLSX.utils.json_to_sheet(newData);
      const workBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(workBook, workSheet, 'Productos');
      XLSX.writeFile(workBook, `ProductosVendidos_${(this._day== '0'? '': this._day + '/')}${this._month}/${this._year}.csv`);
    }
  }

}
