import { Component, OnInit, ViewChild, Input, SimpleChanges } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';

@Component({
  selector: 'stepper-custom',
  templateUrl: './stepper-custom.component.html'
})
export class StepperCustomComponent {

  @Input() status: string;
  @ViewChild('stepper') private myStepper: MatStepper;
  totalStepsCount: number;

  constructor() { }

  goBack() {
    this.myStepper?.previous();
  }

  goForward() {
    this.myStepper?.next();
  }

  setSteps() {
    this.goBack();
    this.goBack();
    this.goBack();
    if (this.status == 'B') {
      this.goForward();
      this.goForward();
      this.goForward();
    }
    if (this.status == 'U') {
      this.goForward();
    }
    if (this.status == 'V') {
      this.goForward();
      this.goForward();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    this.setSteps();
  }

  ngAfterViewInit() {
    this.totalStepsCount = this.myStepper._steps.length;
    this.setSteps();
  }

}


