import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingPageDialogComponent } from './loading-page-dialog.component';

describe('LoadingPageDialogComponent', () => {
  let component: LoadingPageDialogComponent;
  let fixture: ComponentFixture<LoadingPageDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingPageDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingPageDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
