import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { Tools } from 'src/app/core/helpers/tools';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from 'src/app/core/models';
import { AuthSecureService } from 'src/app/core/services/auth-secure.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LoadingPageDialogComponent } from '../loading-page-dialog/loading-page-dialog.component';

@Component({
  selector: 'app-password-validation-dialog',
  templateUrl: './password-validation-dialog.component.html'
})
export class PasswordValidationDialogComponent implements OnInit {

  public passwordForm: FormGroup;
  private currentUser: User;
  private tools: Tools
  private country: string;
  public passwordType = 'password';
  constructor(
    private _snackBar: MatSnackBar,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<PasswordValidationDialogComponent>,
    private authSecureService: AuthSecureService,
    @Inject(MAT_DIALOG_DATA) public data) {
    this.currentUser = this.authSecureService.userValue;
    this.country = this.authSecureService.sessionCountryValue.code;
    this.tools = new Tools(this._snackBar);
  }

  ngOnInit(): void {
    this.passwordForm = new FormGroup({
      password: new FormControl('',
        [
          Validators.required,
          Validators.maxLength(75)
        ])
    });
  }
  get password() { return this.passwordForm.get('password'); }

  close(): void {
    this.dialogRef.close();
  }

  changePasswordType() {
    this.passwordType = this.passwordType == 'text' ? 'password' : 'text';
  }

  passwordValidation() {
    const dialogRefLoadingPage = this.dialog.open(LoadingPageDialogComponent, {
      width: '80%',
      data: "",
      disableClose: true
    });

    this.authSecureService.validaPassword({
      validateUser: {
        countryCode: Number(this.country),
        userPassword: this.password.value
      }
    }).subscribe(response => {
      if (response.code == 200 && response.result.status == "ok") {
        dialogRefLoadingPage.close();
        this.dialogRef.close(true);
      } else {
        this.tools.showMessage("Confirmación no exitosa");
      }
      dialogRefLoadingPage.close();
    },
      err => {
        dialogRefLoadingPage.close();
        this.tools.showMessage("Confirmación no exitosa");
      });
    
  }

}
