import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PasswordValidationDialogComponent } from './password-validation-dialog.component';

describe('PasswordValidationDialogComponent', () => {
  let component: PasswordValidationDialogComponent;
  let fixture: ComponentFixture<PasswordValidationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PasswordValidationDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordValidationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
