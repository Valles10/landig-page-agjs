import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { UserGuideService } from 'src/app/core/services/user-guide.service';

@Component({
  selector: 'app-changelog-dialog',
  templateUrl: './changelog-dialog.component.html',
  styleUrls: ['./changelog-dialog.component.css']
})
export class ChangelogDialogComponent implements OnInit {

  public loading = false;
  public mdChangelog = '';
  constructor(
    public dialogRef: MatDialogRef<ChangelogDialogComponent>,
    public alert: MyAlertService,
    private userGuideService: UserGuideService,
  ) { }


  ngOnInit(): void {
    this.loading = true;
    this.userGuideService.getChangelog().subscribe(data => {
      this.mdChangelog = data;
      this.loading = true;
    }, err => {
      this.alert.info('No se pudo cargar historial de cambios...');
      this.dialogRef.close();
      this.loading = true;
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  /* onError($err) {
    this.alert.info('No se pudo cargar historial de cambios...');
    this.dialogRef.close();
    this.loading = true;
  } */

  onReady() {
    this.loading = true;
  }
}
