import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MyAlertService } from 'src/app/core/services/MyAlert.service';
import { UserGuideService } from 'src/app/core/services/user-guide.service';

@Component({
  selector: 'app-users-guide-dialog',
  templateUrl: './users-guide-dialog.component.html'
})
export class UsersGuideDialogComponent implements OnInit {
  public htmlData = '';
  public loading: boolean = false;
  constructor(public dialogRef: MatDialogRef<UsersGuideDialogComponent>,
    private userGuideService: UserGuideService,
    private alert: MyAlertService) { }

  ngOnInit(): void {
    /* this.load(); */
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  load() {
    this.loading = true;
    this.userGuideService.getHtml().subscribe(data => {
      this.htmlData = data;
      this.loading = false;
    }, err => {
      this.alert.warn('No se pudo cargar la guia');
      this.loading = false;
    });

  }


}
