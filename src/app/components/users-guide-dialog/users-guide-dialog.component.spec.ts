import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersGuideDialogComponent } from './users-guide-dialog.component';

describe('UsersGuideDialogComponent', () => {
  let component: UsersGuideDialogComponent;
  let fixture: ComponentFixture<UsersGuideDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersGuideDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersGuideDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
