# Change Log

## [v1.0.0.1b] - 27-11-2020

### Added
        - Campo Descripción para caracteristicas
        - Boton actualizar Categorias
        - Boton actualizar Marcas
        - Boton actualizar caracteristicas
        - Grid tabla producto proveedores
        - Grid tabla Inventario
        - Grid tabla entrega Series
        - Grid tabla Series
        - Grid tabla Ordenes
        - Grid tabla contratos
        - Versión del proyecto 
        - Boton de documentación 
        - Modal Changelog

### Removed

### Changed
        - Centro encabezados de tablas
### Fixed
        - Dimensiones grafica abandono clientes 


 

