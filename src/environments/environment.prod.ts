
export const environment = {
  production: true,
  version: 'TLBO v1.0.0.3',
  apiTiendaEnLinea: "http://192.168.8.155:{portTL}/tienda_en_linea_RG1/resources",
  apiBackend: 'http://192.168.8.155:{portBO}/TL_BO_BackEnd/webresources', 
  portsBackOffice: [
    { country: '502', port: '5022' },
    { country: '503', port: '5023' },
    { country: '504', port: '5024' },
    { country: '505', port: '5025' },
    { country: '506', port: '5026' },
    { country: '507', port: '5027' },
  ],
  portsTiendaEnLinea: [
    { country: '502', port: '4854' },
    { country: '503', port: '4856' },
    { country: '504', port: '4858' },
    { country: '505', port: '4860' },
    { country: '506', port: '4862' },
    { country: '507', port: '4864' },
  ],
  versions: {
    validEdition: ['A', 'U', 'V'],
    init: { versionId: 3, name: 'INIT', status: 'I' },
  },
  v: {
    fileVersion: {
      validEdition: ['A', 'U', 'V'],
      init: { versionId: 1, name: 'PROD', status: 'I' },
    }
  },
  cdn: "https://test-dev.tienda-{country}.tmx-internacional.com/cdn/",
  cdnProd: 'https://tiendaenlinea.claro.com.{country}/cdn/',
  pagination: [25, 35, 50],
  idPlatform: 2,
  sufixStorage: '_tlbo_',
  dateFormat: {
    locale: 'es-Es',
    format: 'dd/MM/yyyy hh:mm:ss'
  },
  secureSession: true
};


