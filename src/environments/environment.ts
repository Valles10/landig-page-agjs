// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  version: 'TLBO v1.0.0.3b-permisos',
  production: true,
  apiTiendaEnLinea: 'http://10.218.41.90:{portTL}/tienda_en_linea_RG1/resources',
  /* apiBackend: 'http://10.207.41.25:{portBO}/TL_BO_BackEnd/webresources', */
  /* apiBackend: 'http://10.218.41.90:4854/TL_BO_BackEnd/webresources', */
 /*  apiBackend: 'http://10.218.41.26:28081/TL_BO_BackEnd/webresources', */
 /*apiBackend: 'http://192.168.8.155:{portBO}/TL_BO_BackEnd/webresources', */
 apiBackend: 'http://10.207.41.25:{portBO}/TL_BO_BackEnd_NI/webresources',
  portsBackOffice: [
    { country: '502', port: '5022' },
    { country: '503', port: '5023' },
    { country: '504', port: '5024' },
    { country: '505', port: '5025' },
    { country: '506', port: '5026' },
    { country: '507', port: '5027' },
  ],
  portsTiendaEnLinea: [
    { country: '502', port: '4854' },
    { country: '503', port: '4856' },
    { country: '504', port: '4858' },
    { country: '505', port: '4860' },
    { country: '506', port: '4862' },
    { country: '507', port: '4864' },
  ],
  versions: {
    validEdition: ['A', 'U', 'V'],
    init: { versionId: 3, name: 'INIT', status: 'I' },
  },
  v: {
    fileVersion: {
      validEdition: ['A', 'U', 'V'],
      init: { versionId: 1, name: 'PROD', status: 'I' },
    }
  },
  cdn: 'https://test-dev.tienda-{country}.tmx-internacional.com/cdn/',
  cdnProd: 'https://tiendaenlinea.claro.com.{country}/cdn/',
  pagination: [25, 35, 50],
  idPlatform: 2,
  idSkuExcluded: "2",
  sufixStorage: '_tlbo_',
  dateFormat: {
    locale: 'es-Es',
    format: 'dd/MM/yyyy hh:mm:ss'
  },
  secureSession: true
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
